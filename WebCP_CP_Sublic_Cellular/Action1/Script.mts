﻿

blnFind=True
err.number = 0

'Import data from Input data sheet  from folder
datatable.ImportSheet ProjectFolderPath&"\Input_Data_Sheet.xls","cpCellular","cpCellular"
n=datatable.GetSheet("cpCellular").GetRowCount

'For i = 1 To n Step 1
If n>0 Then
    datatable.SetCurrentRow(1)
   
    '====BASE STATION
    With Browser("Customer Home Page").Page("Application")
        If Strcomp(Datatable.Value("Edit","cpCellular"),"yes",1)<>0 Then
            Browser("name:=.*Application").Page("title:=.*Application.*").Link("name:=GSM Base Station.*","html id:=img_3").Clicks
            If Datatable.Value("Stationtype","cpCellular")="Cellular Mobile" Then  
                fn_LoadpageInWebCP   
				Wait 8                
               .Frame("Frame_2").WebRadioGroup("tbl_site__subStationType").Select "1"
            ElseIf Datatable.Value("Stationtype","cpCellular")="WLL" Then	
                 fn_LoadpageInWebCP
                 Wait 8
                .Frame("Frame_2").WebRadioGroup("tbl_site__subStationType").Select "#1"
            ElseIf Datatable.Value("Stationtype","cpCellular")="RLAN"  Then 
                 fn_LoadpageInWebCP
                 Wait 8
                .Frame("Frame_2").WebRadioGroup("tbl_site__subStationType").Select "#4"
            ElseIf Datatable.Value("Stationtype","cpCellular")="BWA" Then 
                 fn_LoadpageInWebCP
                 Wait 8
                .Frame("Frame_2").WebRadioGroup("tbl_site__subStationType").Select "#5"
            ElseIf Datatable.Value("Stationtype","cpCellular")="PSTN" Then 
                fn_LoadpageInWebCP
                Wait 8
                Browser("Customer Home Page").Page("Application_2").Frame("Frame_2").WebRadioGroup("tbl_site__subStationType").Select "#2"
            ElseIf Datatable.Value("Stationtype","cpCellular")="WLAN" Then 
                fn_LoadpageInWebCP
                Wait 8
                Browser("Customer Home Page").Page("Application_2").Frame("Frame_2").WebRadioGroup("tbl_site__subStationType").Select "#3"
            End If
        End If
        If Strcomp(Datatable.Value("Edit","cpCellular"),"yes",1)=0 Then
            fn_EditSubLicenses "station",  Datatable.Value("typeOfStation","cpCellular"),Datatable.Value("Stationtype","cpCellular")
            .Frame("Frame_2").WebList("tbl_site__siteCity").Selects "random"' "Aero Drome (Extension 127)"
            fn_LoadpageInWebCP
        Else
            If Browser("name:=Application").Page("title:=Application").Webedit("html id:=tbl_site__nomRadius","name:=tbl_site__nomRadius").Exist(8) Then
                Browser("name:=Application").Page("title:=Application").Webedit("html id:=tbl_site__nomRadius","name:=tbl_site__nomRadius").Set RandomNumber(1,10)
            End If
            With Browser("Customer Home Page").Page("Application").Frame("Frame")
            	  If .WebEdit("tbl_site__transArea").Exist(5) Then
            	  	.WebEdit("tbl_site__transArea").Set GenerateRandomString(5)
            	  End If
            	  If .WebList("tbl_site__siteInfo110").Exist(5) Then
            	  	.WebList("tbl_site__siteInfo110").Select 2
            	  End If
            End With
 @@ script infofile_;_ZIP::ssf75.xml_;_
            .Frame("Frame_2").WebEdit("tbl_site__siteStreet").Set Datatable.Value("Street","cpCellular")
            .Frame("Frame_2").WebList("tbl_site__siteCity").Select(1)' "Aero Drome (Extension 127)"
            .Frame("Frame_2").WebEdit("tbl_site__siteInfo16").Set Datatable.Value("Contactperson","cpCellular")
            .Frame("Frame_2").WebEdit("tbl_site__siteInfo17").Set Datatable.Value("Tel","cpCellular")
            .Frame("Frame_2").WebEdit("tbl_site__siteInfo15").Set Datatable.Value("Fax","cpCellular")
        End If
       
        'Save Station
        .Frame("Frame_2").WebElement("img_0").Clicks
        
        'Add equipment- Cellular Mobile- Receiver
        If Strcomp(Datatable.Value("Edit","cpCellular"),"yes",1)<>0 Then
        	 If Datatable.Value("Equipmenttype","cpCellular")="Add Receiver" Then			
            .Frame("Frame_2").WebElement("img_1").Clicks
            fn_LoadpageInWebCP
        ElseIf Datatable.Value("Equipmenttype","cpCellular")="Add Transceiver" Then		
            .Frame("Frame_2").WebElement("img_2").Clicks
            fn_LoadpageInWebCP
        ElseIf Datatable.Value("Equipmenttype","cpCellular")="Add Transmitter" Then
            .Frame("Frame_2").WebElement("img_3").Clicks
            fn_LoadpageInWebCP
        End If
        End If
       
        
        'Equipments adding
        If Strcomp(Datatable.Value("Edit","cpCellular"),"yes",1)=0 Then
            fn_LoadpageInWebCP
            fn_EditSubLicenses "",  Datatable.Value("typeOfStation","cpCellular"),Datatable.Value("Stationtype","cpCellular")
             With Browser("name:=Application").Page("title:=Application")
                If .WebList("html id:=tbl_equipment__ApprovalNum").Exist(40) Then
                    
                    If Datatable.Value("EquipmentApprovalNum","cpCellular")="" Then
                    	.WebList("html id:=tbl_equipment__ApprovalNum").Selects 1'"random"
                    Else
                       .WebList("html id:=tbl_equipment__ApprovalNum").Selects  1'Datatable.Value("EquipmentApprovalNum","cpCellular")
                    End If
                    For intC = 1 To 20 Step 1
                        If Instr(1,.WebList("html id:=tbl_equipment__equipMake","name:=tbl_equipment__equipMake").GetROProperty("selection"),"Select",1)>0 Then
                            Wait 3
                        Else
                            Exit For
                        End If
                    Next
                End If
            End With
            fn_LoadpageInWebCP    
            With Browser("name:=Application").Page("title:=Application")
                If .WebList("html id:=tbl_antenna__ApprovalNum").Exist(40) Then
                    If Datatable.Value("AntennaApprovalNum","cpCellular")="" Then
                        .WebList("html id:=tbl_antenna__ApprovalNum").Selects 1'"random"
                    Else
                        .WebList("html id:=tbl_antenna__ApprovalNum").Select  1'Datatable.Value("AntennaApprovalNum","cpCellular")
                    End If
                    For intC = 1 To 20 Step 1
                        If Instr(1,.WebList("name:=tbl_antenna__equipMake").GetROProperty("selection"),"Select",1)>0 Then
                           Wait 3
                        Else
                            Exit For
                          
                        End If
                    Next
                End If
            End With     
            fn_LoadpageInWebCP
        Else
            fn_LoadpageInWebCP
            With Browser("name:=Application").Page("title:=Application")
                If .WebList("html id:=tbl_equipment__ApprovalNum").Exist(40) Then
                    If Datatable.Value("EquipmentApprovalNum","cpCellular")="" Then
                        .WebList("html id:=tbl_equipment__ApprovalNum").Selects 1'"random"
                    Else 
                        .WebList("html id:=tbl_equipment__ApprovalNum").Selects 1'Datatable.Value("EquipmentApprovalNum ","cpCellular")
                    End If
                    For intC = 1 To 20 Step 1
                        If Instr(1,.WebList("html id:=tbl_equipment__equipMake","name:=tbl_equipment__equipMake").GetROProperty("selection"),"Select",1)>0 Then
                            Wait 3
                        Else
                            Exit For
                        End If
                    Next
                End If
            End With
            
            
            fn_LoadpageInWebCP
            .Frame("Frame_3").WebEdit("tbl_equipment__equipSerialNum").Set Datatable.Value("Equipmentserial","cpCellular")&fnRandomNumber(4)
            fn_LoadpageInWebCP
            'antenna tab
            With Browser("name:=Application").Page("title:=Application")
                If .WebList("html id:=tbl_antenna__ApprovalNum").Exist(3) Then
                    If Datatable.Value("AntennaApprovalNum","cpCellular")="" Then
                        If .WebList("html id:=tbl_antenna__ApprovalNum").Exist(40) Then
                        	.WebList("html id:=tbl_antenna__ApprovalNum").Selects 1'"random"
                        End If
                    Else
                        .WebList("html id:=tbl_antenna__ApprovalNum").Select 1'Datatable.Value("AntennaApprovalNum","cpCellular")
                    End If
                    
                    For intC = 1 To 20 Step 1
                        If Instr(1,.WebList("html id:=tbl_antenna__equipMake","name:=tbl_antenna__equipMake").GetROProperty("selection"),"Select",1)>0 Then
                            wait 3
                        Else
                            Exit For
                            
                        End If
                    Next
                End If
            End With     
            fn_LoadpageInWebCP
            'Site specific
            .Frame("Frame_3").WebEdit("tbl_antenna__physicHt").Set Datatable.Value("Height","cpCellular")
            .Frame("Frame_3").WebEdit("tbl_antenna__mainLobeAzi").Set Datatable.Value("mainlobeazi","cpCellular")
            .Frame("Frame_3").WebEdit("tbl_antenna__tiltAngle").Set Datatable.Value("tiltangle","cpCellular")
        End If
        With Browser("name:=Application").Page("title:=Application")
            If Lcase(Datatable.Value("FeederLoss","cpCellular"))="cable" And Strcomp(Datatable.Value("Edit","cpCellular"),"yes",1)<>0 Then
                If .WebRadioGroup("html id:=tbl_antenna__By.*","name:=tbl_antenna__ByCableOrLineAttenuation").Exist(8) Then
                    .WebRadioGroup("html id:=tbl_antenna__By.*","name:=tbl_antenna__ByCableOrLineAttenuation").Select 1
                    .WebEdit("name:=tbl_antenna__FeedLength").Set Datatable.Value("Feedlength","cpCellular")
                ElseIf .WebRadioGroup("html id:=tbl_antenna__By.*","name:=tbl_antenna__ByCableOrLineAttenuation").Exist(8) Then
                    .WebRadioGroup("html id:=tbl_antenna__By.*","name:=tbl_antenna__ByCableOrLineAttenuation").Select 2            
                    .WebEdit("name:=tbl_antenna__LineAtten").Set Datatable.Value("Lineatten","cpCellular")	
                End If 
            End If
        End With
      
        fn_LoadpageInWebCP
        If  .Frame("Frame_3").WebList("tbl_antenna__CableID").Exist Then
        	 .Frame("Frame_3").WebList("tbl_antenna__CableID").Select(1)' "ANDREW - FSJ-50B"
        End If
        '====Save Equipment
        .Frame("Frame_3").WebElement("img_0").Clicks
        .Sync
         fn_LoadpageInWebCP
    End With
       
End If


'Test results dsiplayed in Excel
If err.number = 0 Then
    blnFind = True
Else
    blnFind = False
End If

UpdateTestCaseStatusInExcel strTestCaseName, blnFind

'fn_SendStatusToExcelReport blnFind




