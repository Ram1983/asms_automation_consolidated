﻿blnVal=False
datatable.ImportSheet ProjectFolderPath &"\Desktop_Input_Data_Sheet\Input_Data_Sheet.xls","asmsRadioDealer","asmsRadioDealer"
n=datatable.GetSheet("asmsRadioDealer").GetRowCount
datatable.SetCurrentRow(1)	
With VbWindow("frmMDI").VbWindow("frmBrowser_2")
    
    If Strcomp(datatable.value("FunctionType","asmsRadioDealer"),"New",1)=0 Then		
'        fn_NavigateToScreen "Application Processing;Radio Dealer;New"
'       	Email=GetDataFromDB("stp_Automation_GetLatestCustomerForRadioDealer '"&Environment("Client") &"' ,'"&Environment("UserID")&"'" )
'        While Not VbWindow("frmMDI").VbWindow("frmBrowser_2").Page("Page").WebList("clientEmail").Exist
'        Wend
'        VbWindow("frmMDI").VbWindow("frmBrowser_2").Page("Page").WebList("clientEmail").Select datatable.value("Email","asmsRadioDealer")
'        VbWindow("frmMDI").VbWindow("frmBrowser").Page("Page").Link("Submit Search").Click
        If VbWindow("frmMDI").VbWindow("frmBrowser_3").Page("Page").Link("DLR").Exist(30) Then
            VbWindow("frmMDI").VbWindow("frmBrowser_3").Page("Page").Link("DLR").Click
            If VbWindow("frmMDI").VbWindow("frmBrowser_2").Page("Dealer_4").Link("Add Dealer License").Exist(40) Then
                VbWindow("frmMDI").VbWindow("frmBrowser_2").Page("Dealer_4").Link("Add Dealer License").Click
            End If
        Else
            .Page("Page").Link("DLR+").Click
        End If
        
        .Page("Dealer").WebList("clientId").Select datatable.value("ClientID","asmsRadioDealer")
    End If
        
    If Strcomp(datatable.value("FunctionType","asmsRadioDealer"),"Modify",1)=0  Then
        'Modify Screen navigation thru function calling
        fn_NavigateToScreen "Application Processing;Radio Dealer;Modify"
        'Select customer Number		
        wait(10)
        VbWindow("VbWindow").VbWindow("VbWindow").ActiveX("Microsoft Web Browser").Page("Dealer").WebList("select").Select datatable.value("CustomerNumber","asmsRadioDealer")
        wait 20
        VbWindow("frmMDI").VbWindow("frmBrowser_2").Page("Dealer_3").Image("edit").Click
        .Page("Dealer").WebList("clientId").Select(1)
    End If
    
    If Datatable("Wholesale","asmsRadioDealer")="YES" Then
        .Page("Dealer").WebCheckBox("Wholesaling").Set "ON"
    End If
    If Datatable("Supplying","asmsRadioDealer")="YES"  Then
        .Page("Dealer").WebCheckBox("supplying").Set "ON"
    End If
    If Datatable("Importing","asmsRadioDealer")="YES" Then	
        .Page("Dealer").WebCheckBox("importing").Set "ON"
    End If
    If  Datatable("Dealing","asmsRadioDealer")="YES" Then
        .Page("Dealer").WebCheckBox("dealing").Set "ON"
    End If
    
    If  Datatable("Reparing","asmsRadioDealer")="YES" Then	
        .Page("Dealer").WebCheckBox("repairing").Set "ON"
    End If
    
    .Page("Dealer").WebList("activity1").Select 1'"Basic Router"
    If Datatable("Workshops","asmsRadioDealer")="YES" Then
        .Page("Dealer").WebCheckBox("assembling").Set "ON"
    End If
    
    .Page("Dealer").WebEdit("owner1").Set Datatable("Ownername","asmsRadioDealer")
    If .Page("Dealer").WebList("ownerclientid1").GetROProperty("items count")>1 Then
        .Page("Dealer").WebList("ownerclientid1").Select(1)
    End If
    wait(10)
    'Update
    .Page("Dealer").WebElement("img_1").Click
    .Page("Dealer").Sync
    path=fileCreate
    Wait 10
    'Attachments
    .Page("Dealer").WebElement("Official Use / Attachments").Click
    wait 15
    .Page("Dealer").Frame("Frame").WebFile("BrowserHidden").Set path
    wait 8
    .Page("Dealer").Frame("Frame").Image("Pressing Upload, the document").Click
    wait(5)
    
    .Page("Dealer_2").WebElement("img_1").Click
    'Approve
    If datatable.value("FunctionType","asmsRadioDealer")="New" Then
        .Page("Dealer_2").WebElement("img_1_0").Click
    End If
    If datatable.value("Approve","asmsRadioDealer")="yes" Then
        .Page("Dealer_2").WebElement("img_21_0").Click
        Wait 6
        .Page("Dealer_2").Sync
        If Instr(1,Trim(VbWindow("frmMDI").VbWindow("frmBrowser_3").Page("Dealer").WebElement("Status").GetROProperty("innertext")),Datatable.Value("ApprovedStatus"),1) >0 Then
            blnVal=True
        End If
    End If
    If datatable.value("Reject","asmsRadioDealer")="yes" Then
        VbWindow("frmMDI").VbWindow("frmBrowser_3").Page("Dealer").WebElement("img_23_0").Click
        VbWindow("frmMDI").VbWindow("frmBrowser_3").Page("Dealer").Sync
        If Instr(1,Trim(VbWindow("frmMDI").VbWindow("frmBrowser_3").Page("Dealer").WebElement("Status").GetROProperty("innertext")),Datatable.Value("RejectStatus"),1)>0 Then
            blnVal=True
        End If
    End If
    
End With

UpdateTestCaseStatusInExcel Environment("TestName"),blnVal
