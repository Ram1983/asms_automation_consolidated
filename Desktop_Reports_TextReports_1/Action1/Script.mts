﻿blnFind=True
err.number = 0
datatable.ImportSheet ProjectFolderPath &"\Input_Data_Sheet\Input_Data_Sheet.xls","Reports_TextReports","Reports_TextReports"
n=datatable.GetSheet("Reports_TextReports").GetRowCount()

'For i = 1 To n Step 1 
If n>0 Then
	datatable.SetCurrentRow(1)	
	Environment.LoadFromFile(ProjectFolderPath &"\ASMS_Variables.xml")
	proj = Datatable.Value("Client","Reports_TextReports")
	For Iterator = 1 To datatable.GetSheet("Reports_TextReports").GetRowCount
	
		DataTable.LocalSheet.SetCurrentRow(Iterator)
		SelectedReport="No"
		
		If Datatable.Value("ReportName","Reports_TextReports")="" and Datatable.Value("RunReport","Reports_TextReports")="" Then
			Exit for
		End If
		
		With	VbWindow("frmMDI")
			If Datatable.Value("ReportName","Reports_TextReports")="ITU Frequency Allocation Report" and Datatable.Value("RunReport","Reports_TextReports")="Yes" Then
				With .VbWindow("frmRptFreqAlloc")

					'If InStr(Environment("Portuguese_Language"),proj)>0 Then
						'VbWindow("frmMDI").InsightObject("InsightObject_3").Click
						'VbWindow("frmMDI").InsightObject("InsightObject_4").Click @@ hightlight id_;_11_;_script infofile_;_ZIP::ssf361.xml_;_
						'VbWindow("frmMDI").InsightObject("InsightObject_7").Click	
						'Else
'							VbWindow("frmMDI").InsightObject("InsightObject").Click @@ hightlight id_;_12_;_script infofile_;_ZIP::ssf140.xml_;_
'							VbWindow("frmMDI").InsightObject("InsightObject_2").Click @@ hightlight id_;_16_;_script infofile_;_ZIP::ssf141.xml_;_
'							VbWindow("frmMDI").InsightObject("ITU-Freq").Click @@ hightlight id_;_20_;_script infofile_;_ZIP::ssf142.xml_;_

'                    fn_NavigateToScreen "Report;Text Report;ITU Frequency Allocation Report"
					'End If
					
					If .ActiveX("MaskEdBox").Exist Then
						wait(2)
						.ActiveX("MaskEdBox").Type Datatable.Value("FreqStart","Reports_TextReports")
						.ActiveX("MaskEdBox").Type  micTab
					End If

					If .ActiveX("MaskEdBox_2").Exist Then
						wait(1)
						.ActiveX("MaskEdBox_2").Type Datatable.Value("FreqEnd","Reports_TextReports")
						.ActiveX("MaskEdBox_2").Type  micTab
					End If
					wait(1)

					'			VbWindow("frmMDI").VbWindow("frmRptFreqAlloc").ActiveX("TCIComboBox.tcb").VbComboBox("cboServiceCategory").Select Datatable.Value("ServiceCategory","Reports_TextReports") '"Primary"
					'			VbWindow("frmMDI").VbWindow("frmRptFreqAlloc").ActiveX("TCIComboBox.tcb_2").VbComboBox("cboServiceClass").Select Datatable.Value("ServiceClass","Reports_TextReports") '"AERONÁUTICA MÓVEL"
					'.ActiveX("TCIComboBox.tcb_3").VbComboBox("cboRegion").Select Datatable.Value("Region","Reports_TextReports") '"Nacional" '"ITU Region 1"
					
					If Datatable.Value("Client","Reports_TextReports")="BOT" Then
						
						'VbWindow("frmMDI").VbWindow("frmRptFreqAlloc").ActiveX("TCIComboBox.tcb").VbComboBox("cboServiceCategory").Select "permitted" @@ hightlight id_;_920098_;_script infofile_;_ZIP::ssf398.xml_;_
						'VbWindow("frmMDI").VbWindow("frmRptFreqAlloc").ActiveX("TCIComboBox.tcb_2").VbComboBox("cboServiceClass").Select "AERONAUTICAL MOBILE" @@ hightlight id_;_3082712_;_script infofile_;_ZIP::ssf399.xml_;_
						While Not VbWindow("frmMDI").VbWindow("frmRptFreqAlloc").VbComboBox("cboRegion").Exist(30)
						Wend
						VbWindow("frmMDI").VbWindow("frmRptFreqAlloc").VbComboBox("cboRegion").Select Datatable.Value("Region","Reports_TextReports") '"National" @@ hightlight id_;_2623188_;_script infofile_;_ZIP::ssf400.xml_;_
						
					End If
									
					.VbButton("View").Click
					SelectedReport="Yes"
				End With
				ElseIf Datatable.Value("ReportName","Reports_TextReports")="Frequency Plan Report" and  Datatable.Value("RunReport","Reports_TextReports")="Yes" Then
					With	.VbWindow("frmRptFreqPlan")
						'If InStr(Environment("Portuguese_Language"),proj)>0 Then
							'VbWindow("frmMDI").InsightObject("InsightObject_3").Click
							'VbWindow("frmMDI").InsightObject("InsightObject_4").Click
							'VbWindow("frmMDI").InsightObject("InsightObject_32").Click
							'Else
								VbWindow("frmMDI").InsightObject("InsightObject").Click
								VbWindow("frmMDI").InsightObject("InsightObject_2").Click
								VbWindow("frmMDI").InsightObject("InsightObject_6").Click

						'End If
						.ActiveX("MaskEdBox").Click 73,12
						.ActiveX("MaskEdBox").Type Datatable.Value("FreqStart","Reports_TextReports")
						.ActiveX("MaskEdBox").Type  micTab
						.ActiveX("MaskEdBox_2").Type Datatable.Value("FreqEnd","Reports_TextReports")
						.ActiveX("TCIComboBox.tcb").VbComboBox("cboServiceClass").Select Datatable.Value("ServiceClass","Reports_TextReports") '"AERONÁUTICA MÓVEL" @@ hightlight id_;_657744_;_script infofile_;_ZIP::ssf409.xml_;_
						.VbButton("View").Click
						SelectedReport="Yes"

					End With
				ElseIf Datatable.Value("ReportName","Reports_TextReports")="Licensed Station Report" and  Datatable.Value("RunReport","Reports_TextReports")="Yes" Then
					With	.VbWindow("frmRptLicenseStation")
						'If InStr(Environment("Portuguese_Language"),proj)>0 Then
							'VbWindow("frmMDI").InsightObject("InsightObject_3").Click
							'VbWindow("frmMDI").InsightObject("InsightObject_4").Click
							'VbWindow("frmMDI").InsightObject("InsightObject_14").Click
							'Else
								VbWindow("frmMDI").InsightObject("InsightObject").Click
								VbWindow("frmMDI").InsightObject("InsightObject_2").Click
								VbWindow("frmMDI").InsightObject("InsightObject_9").Click
						'End If
						While Not VbWindow("frmMDI").VbWindow("frmRptLicenseStation").ActiveX("TCIComboBox.tcb").VbComboBox("cboLicense").Exist(10)	
						Wend
						.ActiveX("TCIComboBox.tcb").VbComboBox("cboLicense").Select Datatable.Value("LicenceNum","Reports_TextReports") '"0019"
						.VbButton("View").Click
						SelectedReport="Yes"
					End With
				ElseIf Datatable.Value("ReportName","Reports_TextReports")="Complaint Report" and  Datatable.Value("RunReport","Reports_TextReports")="Yes" Then
					With	.VbWindow("frmRptComplaint")
						'If InStr(Environment("Portuguese_Language"),proj)>0 Then
							'VbWindow("frmMDI").InsightObject("InsightObject_3").Click
							'VbWindow("frmMDI").InsightObject("InsightObject_4").Click
							'VbWindow("frmMDI").InsightObject("InsightObject_20").Click
							'Else
								VbWindow("frmMDI").InsightObject("InsightObject").Click
								VbWindow("frmMDI").InsightObject("InsightObject_2").Click
								VbWindow("frmMDI").InsightObject("InsightObject_11").Click
						'End If
						wait(5)
						.ActiveX("TCIComboBox.tcb").VbComboBox("cboLastName").Select Datatable.Value("LastName","Reports_TextReports") '"Benson"
						wait(1)
'						.ActiveX("TCIComboBox.tcb_2").VbComboBox("cboFirstName").Select Datatable.Value("FirstName","Reports_TextReports") '"Mark"
'						wait(1)
'						.ActiveX("TCIComboBox.tcb_3").VbComboBox("cboCompany").Select Datatable.Value("Company","Reports_TextReports") '"Ablue Comms"
'						wait(1)
						'.VbEdit("txtCallSign").Set Datatable.Value("CallSign","Reports_TextReports")
						'VbWindow("frmMDI").VbWindow("frmRptComplaint").ActiveX("TCIComboBox.tcb_4").VbComboBox("cboStatus").Select "New"
						'wait(1)

'						.ActiveX("MaskEdBox").Click 82,5
'						.ActiveX("MaskEdBox").Type Datatable.Value("FreqStart","Reports_TextReports")
'						'.ActiveX("MaskEdBox").Type  micTab
'						.ActiveX("MaskEdBox_2").Type Datatable.Value("FreqEnd","Reports_TextReports")
'						wait(1)
'
'						.ActiveX("MaskEdBox_3").Click 5,2
'						'.ActiveX("MaskEdBox_3").Type  micBack
'						.ActiveX("MaskEdBox_3").Type Datatable.Value("StartDate","Reports_TextReports")'"11112011"
'						'.ActiveX("MaskEdBox_3").Type  micTab
'						wait(1)
'						.ActiveX("MaskEdBox_4").Type Datatable.Value("EndDate","Reports_TextReports") '"12122020"
						wait(1)

						'.VbButton("View").Click
						SelectedReport="Yes"
					End With
				ElseIf Datatable.Value("ReportName","Reports_TextReports")="Violation Report" and  Datatable.Value("RunReport","Reports_TextReports")="Yes" Then
					With	.VbWindow("frmRptViolation")
						'If InStr(Environment("Portuguese_Language"),proj)>0 Then
							'VbWindow("frmMDI").InsightObject("InsightObject_3").Click
							'VbWindow("frmMDI").InsightObject("InsightObject_4").Click
							'VbWindow("frmMDI").InsightObject("InsightObject_21").Click
							'Else
								VbWindow("frmMDI").InsightObject("InsightObject").Click
								VbWindow("frmMDI").InsightObject("InsightObject_2").Click
								If Datatable.Value("Client","Reports_TextReports")="MOZ" Then
									VbWindow("frmMDI").InsightObject("InsightObject_12").Click
								End If
										
                                If Datatable.Value("Client","Reports_TextReports")="BOT" Then                                	                          
                                	VbWindow("frmMDI").InsightObject("InsightObject_36").Click
                                	Wait(3)
                                End If								
 @@ hightlight id_;_589868_;_script infofile_;_ZIP::ssf429.xml_;_
						'End If
						wait(1)
						.ActiveX("TCIComboBox.tcb").VbComboBox("cboCompany").Select Datatable.Value("Company","Reports_TextReports") '"Ablue Comms"
						'name
						'entitle
						.ActiveX("MaskEdBox").Click 141,13
						.ActiveX("MaskEdBox").Type Datatable.Value("FreqStart","Reports_TextReports")
						.ActiveX("MaskEdBox").Type  micTab
						.ActiveX("MaskEdBox_2").Type Datatable.Value("FreqEnd","Reports_TextReports")
						.ActiveX("MaskEdBox_2").Type  micTab
						.ActiveX("MaskEdBox_3").Type Datatable.Value("StartDate","Reports_TextReports") '"11/11/2011"
						.ActiveX("MaskEdBox_3").Type  micTab
						.ActiveX("MaskEdBox_4").Type Datatable.Value("EndDate","Reports_TextReports") '"12/12/2020"
						.ActiveX("MaskEdBox_4").Type  micTab
'						.ActiveX("MaskEdBox_5").Type Datatable.Value("S-Lat1","Reports_TextReports") '"18"
'						.ActiveX("MaskEdBox_5").Type  micTab
'						.ActiveX("MaskEdBox_6").Type Datatable.Value("S-Lat2","Reports_TextReports") '
'						.ActiveX("MaskEdBox_6").Type  micTab
'						.ActiveX("MaskEdBox_7").Type Datatable.Value("S-Lat3","Reports_TextReports") '
'						.ActiveX("MaskEdBox_7").Type  micTab
'						.ActiveX("TCIComboBox.tcb_2").VbComboBox("cboFromLatHem").Select Datatable.Value("S-Lat4","Reports_TextReports") 'N
'						.ActiveX("TCIComboBox.tcb_2").VbComboBox("cboFromLatHem").Type  micTab
'						.ActiveX("MaskEdBox_8").Type Datatable.Value("E-Lat1","Reports_TextReports") '"18"
'						.ActiveX("MaskEdBox_8").Type  micTab
'						.ActiveX("MaskEdBox_9").Type Datatable.Value("E-Lat2","Reports_TextReports")
'						.ActiveX("MaskEdBox_9").Type  micTab
'						.ActiveX("MaskEdBox_10").Type Datatable.Value("E-Lat3","Reports_TextReports")
'						.ActiveX("MaskEdBox_10").Type  micTab
'						.ActiveX("TCIComboBox.tcb_3").VbComboBox("cboToLatHem").Select Datatable.Value("E-Lat4","Reports_TextReports") '"S"
'						.ActiveX("TCIComboBox.tcb_3").VbComboBox("cboToLatHem").Type  micTab
'						.ActiveX("MaskEdBox_11").Type Datatable.Value("S-Long1","Reports_TextReports") '"37"
'						.ActiveX("MaskEdBox_11").Type  micTab
'						.ActiveX("MaskEdBox_12").Type Datatable.Value("S-Long2","Reports_TextReports") '
'						.ActiveX("MaskEdBox_12").Type  micTab
'						.ActiveX("MaskEdBox_13").Type Datatable.Value("S-Long3","Reports_TextReports") '
'						.ActiveX("MaskEdBox_13").Type  micTab
'						.ActiveX("TCIComboBox.tcb_4").VbComboBox("cboFromLongHem").Select Datatable.Value("S-Long4","Reports_TextReports") '"E"
'						.ActiveX("TCIComboBox.tcb_4").VbComboBox("cboFromLongHem").Type  micTab
'						.ActiveX("MaskEdBox_14").Type Datatable.Value("E-Long1","Reports_TextReports") '"38"
'						.ActiveX("MaskEdBox_14").Type  micTab
'						.ActiveX("MaskEdBox_15").Type Datatable.Value("E-Long2","Reports_TextReports") '
'						.ActiveX("MaskEdBox_15").Type  micTab
'						.ActiveX("MaskEdBox_16").Type Datatable.Value("E-Long3","Reports_TextReports") '
'						.ActiveX("MaskEdBox_16").Type  micTab
'						.ActiveX("TCIComboBox.tcb_5").VbComboBox("cboToLongHem").Select Datatable.Value("E-Long4","Reports_TextReports") '"W"

						.VbButton("View").Click
						SelectedReport="Yes"
					End With
				ElseIf Datatable.Value("ReportName","Reports_TextReports")="Inspection Report" and  Datatable.Value("RunReport","Reports_TextReports")="Yes" Then
					With	.VbWindow("frmRptInspections")
						'If InStr(Environment("Portuguese_Language"),proj)>0 Then
							'VbWindow("frmMDI").InsightObject("InsightObject_3").Click
							'VbWindow("frmMDI").InsightObject("InsightObject_4").Click
							'VbWindow("frmMDI").InsightObject("InsightObject_22").Click
							'Else
'								VbWindow("frmMDI").InsightObject("InsightObject").Click
'								VbWindow("frmMDI").InsightObject("InsightObject_2").Click
'								VbWindow("frmMDI").InsightObject("InsightObject_13").Click

						'End If
						.ActiveX("TCIComboBox.tcb").VbComboBox("cboInspctionNum").Select Datatable.Value("LicenceNum","Reports_TextReports") '"53"
						'			VbWindow("frmMDI").VbWindow("frmRptInspections").ActiveX("TCIComboBox.tcb_2").VbComboBox("cboType").Select "Installation"
						'			VbWindow("frmMDI").VbWindow("frmRptInspections").ActiveX("TCIComboBox.tcb_3").VbComboBox("cboStatus").Select "New"

						.ActiveX("MaskEdBox").Click 2,8
						.ActiveX("MaskEdBox").Type Datatable.Value("StartDate","Reports_TextReports") '"11112011"
						.ActiveX("MaskEdBox").Type  micTab
						.ActiveX("MaskEdBox_2").Type Datatable.Value("EndDate","Reports_TextReports") '"12122020"

						.VbButton("View").Click
						SelectedReport="Yes"
					End With
				ElseIf Datatable.Value("ReportName","Reports_TextReports")="Equipment Report" and  Datatable.Value("RunReport","Reports_TextReports")="Yes" Then
					With	.VbWindow("frmRptEquipment")
						'If InStr(Environment("Portuguese_Language"),proj)>0 Then
							'VbWindow("frmMDI").InsightObject("InsightObject_3").Click
							'VbWindow("frmMDI").InsightObject("InsightObject_4").Click
							'VbWindow("frmMDI").InsightObject("InsightObject_24").Click
							'Else
								VbWindow("frmMDI").InsightObject("InsightObject").Click
								VbWindow("frmMDI").InsightObject("InsightObject_2").Click
								VbWindow("frmMDI").InsightObject("InsightObject_15").Click
						'End If
						.ActiveX("TCIComboBox.tcb").VbComboBox("cboEquipType").Select Datatable.Value("ReportType","Reports_TextReports") '"Transceptor" '"Other" @@ hightlight id_;_132848_;_script infofile_;_ZIP::ssf406.xml_;_
						'VbWindow("frmMDI").VbWindow("frmRptEquipment").ActiveX("TCIComboBox.tcb_2").VbComboBox("cboMake").Select "MX123 JXX 112"
						'VbWindow("frmMDI").VbWindow("frmRptEquipment").ActiveX("TCIComboBox.tcb_3").VbComboBox("cboModel").Select "SW TCI SMS"

						'VbWindow("frmMDI").VbWindow("frmRptEquipment").ActiveX("MaskEdBox").Click 7,9
						'VbWindow("frmMDI").VbWindow("frmRptEquipment").ActiveX("MaskEdBox").Type  micBack
						.ActiveX("MaskEdBox").Type Datatable.Value("StartDate","Reports_TextReports") '"11112015"
						.ActiveX("MaskEdBox").Type  micTab
						.ActiveX("MaskEdBox_2").Type Datatable.Value("EndDate","Reports_TextReports") '"12122020"

						.VbButton("View").Click
						SelectedReport="Yes"
					End With
				ElseIf Datatable.Value("ReportName","Reports_TextReports")="Site Reports" and  Datatable.Value("RunReport","Reports_TextReports")="Yes" Then
					With	.VbWindow("frmRptSite")
						'If InStr(Environment("Portuguese_Language"),proj)>0 Then
							'VbWindow("frmMDI").InsightObject("InsightObject_3").Click
							'VbWindow("frmMDI").InsightObject("InsightObject_4").Click
							'VbWindow("frmMDI").InsightObject("InsightObject_25").Click
							'Else
								VbWindow("frmMDI").InsightObject("InsightObject").Click
								VbWindow("frmMDI").InsightObject("InsightObject_2").Click
								VbWindow("frmMDI").InsightObject("InsightObject_16").Click
						'End If
                         Wait(3)
                         While Not VbWindow("frmMDI").VbWindow("frmRptSite").ActiveX("TCIComboBox.tcb").VbComboBox("cboSite").Exist(20)	
                         Wend

						.ActiveX("TCIComboBox.tcb").VbComboBox("cboSite").Select Datatable.Value("SiteType","Reports_TextReports") '"Aviões" @@ hightlight id_;_67596_;_script infofile_;_ZIP::ssf385.xml_;_
'						'VbWindow("frmMDI").VbWindow("frmRptSite").ActiveX("TCIComboBox.tcb").VbComboBox("cboSite").Select "Aircraft"
'						'VbWindow("frmMDI").VbWindow("frmRptSite").ActiveX("TCIComboBox.tcb_2").VbComboBox("cboStationClass").Select "3RP Trunking"
'						'VbWindow("frmMDI").VbWindow("frmRptSite").ActiveX("TCIComboBox.tcb_2").VbComboBox("cboStationClass").Select "3RP Agregação"
'						.ActiveX("MaskEdBox").Click 14,10
'						.ActiveX("MaskEdBox").Type Datatable.Value("S-Lat1","Reports_TextReports") '"17"
'						.ActiveX("MaskEdBox").Type  micTab
'						.ActiveX("MaskEdBox_2").Type Datatable.Value("S-Lat2","Reports_TextReports") '
'						.ActiveX("MaskEdBox_2").Type  micTab
'						.ActiveX("MaskEdBox_3").Type Datatable.Value("S-Lat3","Reports_TextReports") '
'						.ActiveX("MaskEdBox_3").Type  micTab
'						.ActiveX("TCIComboBox.tcb_3").VbComboBox("cboFromLatHem").Select Datatable.Value("S-Lat4","Reports_TextReports") '"N"
'						.ActiveX("TCIComboBox.tcb_3").VbComboBox("cboFromLatHem").Type  micTab
'						.ActiveX("MaskEdBox_4").Type Datatable.Value("E-Lat1","Reports_TextReports") '"17"
'						.ActiveX("MaskEdBox_4").Type  micTab
'						.ActiveX("MaskEdBox_5").Type Datatable.Value("E-Lat2","Reports_TextReports") '
'						.ActiveX("MaskEdBox_5").Type  micTab
'						.ActiveX("MaskEdBox_6").Type Datatable.Value("E-Lat3","Reports_TextReports") '
'						.ActiveX("MaskEdBox_6").Type  micTab
'						.ActiveX("TCIComboBox.tcb_4").VbComboBox("cboToLatHem").Select Datatable.Value("E-Lat4","Reports_TextReports") '"S"
'						.ActiveX("MaskEdBox_7").Click 12,10
'						.ActiveX("MaskEdBox_7").Type Datatable.Value("S-Long1","Reports_TextReports") '"35"
'						.ActiveX("MaskEdBox_7").Type  micTab
'						.ActiveX("MaskEdBox_8").Type Datatable.Value("S-Long2","Reports_TextReports") '
'						.ActiveX("MaskEdBox_8").Type  micTab
'						.ActiveX("MaskEdBox_9").Type Datatable.Value("S-Long3","Reports_TextReports") '
'						.ActiveX("MaskEdBox_9").Type  micTab
'						.ActiveX("TCIComboBox.tcb_5").VbComboBox("cboFromLongHem").Select Datatable.Value("S-Long4","Reports_TextReports") '"E"
'						.ActiveX("TCIComboBox.tcb_5").VbComboBox("cboFromLongHem").Type  micTab
'						.ActiveX("MaskEdBox_10").Type Datatable.Value("E-Long1","Reports_TextReports") '"35"
'						.ActiveX("MaskEdBox_10").Type  micTab
'						.ActiveX("MaskEdBox_11").Type Datatable.Value("E-Long2","Reports_TextReports") '
'						.ActiveX("MaskEdBox_11").Type  micTab
'						.ActiveX("MaskEdBox_12").Type Datatable.Value("E-Long3","Reports_TextReports") '
'						.ActiveX("MaskEdBox_12").Type  micTab
'						.ActiveX("TCIComboBox.tcb_6").VbComboBox("cboToLongHem").Select Datatable.Value("E-Long4","Reports_TextReports") '"W"
'						.ActiveX("TCIComboBox.tcb_6").VbComboBox("cboToLongHem").Type  micTab
'						'VbWindow("frmMDI").VbWindow("frmRptSite").ActiveX("TCIComboBox.tcb_7").VbComboBox("cboApplicationNo").Select "100 / RCL"
						.VbButton("View").Click
						SelectedReport="Yes"
					End With
				ElseIf Datatable.Value("ReportName","Reports_TextReports")="Client with Type, Make and Model" and  Datatable.Value("RunReport","Reports_TextReports")="Yes" Then
					With	.VbWindow("frmClientsWithMakeAndModel")
						'If InStr(Environment("Portuguese_Language"),proj)>0 Then
							'VbWindow("frmMDI").InsightObject("InsightObject_3").Click
							'VbWindow("frmMDI").InsightObject("InsightObject_4").Click
							'VbWindow("frmMDI").InsightObject("InsightObject_27").Click
							'Else
								VbWindow("frmMDI").InsightObject("InsightObject").Click
								VbWindow("frmMDI").InsightObject("InsightObject_2").Click
								VbWindow("frmMDI").InsightObject("InsightObject_17").Click
						'End If
						Wait(3)
						While Not VbWindow("frmMDI").VbWindow("frmClientsWithMakeAndModel").ActiveX("TCIComboBox.tcb").VbComboBox("cboType").Exist(20)
						Wend
						.ActiveX("TCIComboBox.tcb").VbComboBox("cboType").Select Datatable.Value("ReportType","Reports_TextReports") '"Transceptor" "Receptor"
						.ActiveX("TCIComboBox.tcb_2").VbComboBox("cboMake").Select Datatable.Value("Make","Reports_TextReports") '"Advantech"
						.ActiveX("TCIComboBox.tcb_3").VbComboBox("cboModel").Select Datatable.Value("Model","Reports_TextReports") '"AMT 65 Modem"

						'			VbWindow("frmMDI").VbWindow("frmClientsWithMakeAndModel").ActiveX("TCIComboBox.tcb_4").VbComboBox("cboUserNumber").Select "A TEST FOR 7946    (# CD32)"
						'			VbWindow("frmMDI").VbWindow("frmClientsWithMakeAndModel").ActiveX("TCIComboBox.tcb_5").VbComboBox("cboLicense").Select "Cellular /WLL/ RLAN"
						'			VbWindow("frmMDI").VbWindow("frmClientsWithMakeAndModel").ActiveX("TCIComboBox.tcb_6").VbComboBox("cboServiceClass").Select "AERONÁUTICA MÓVEL"
						'			VbWindow("frmMDI").VbWindow("frmClientsWithMakeAndModel").ActiveX("TCIComboBox.tcb_7").VbComboBox("cboServiceType").Select "móveis celulares"

						.VbButton("Search").Click
                        
                        Wait(3)
						If .AcxTable("Encontrado 1 cliente (s)").RowCount>0 Then
							.VbButton("View as Crystal report").Click
							SelectedReport="Yes"
						End If

					End With
				ElseIf Datatable.Value("ReportName","Reports_TextReports")="Audit Log" and  Datatable.Value("RunReport","Reports_TextReports")="Yes" Then
					With	.VbWindow("frmAuditLog")
						'If InStr(Environment("Portuguese_Language"),proj)>0 Then
							'VbWindow("frmMDI").InsightObject("InsightObject_3").Click
							'VbWindow("frmMDI").InsightObject("InsightObject_4").Click
							'VbWindow("frmMDI").InsightObject("InsightObject_29").Click
							'Else
								VbWindow("frmMDI").InsightObject("InsightObject").Click
								VbWindow("frmMDI").InsightObject("InsightObject_2").Click
								VbWindow("frmMDI").InsightObject("InsightObject_18").Click
						'End If
                         
						.ActiveX("TCIComboBox.tcb").VbComboBox("cboUserName").Select Datatable.Value("UserID","Reports_TextReports") '"ACCPAC"
						'VbWindow("frmMDI").VbWindow("frmAuditLog").ActiveX("TCIComboBox.tcb_2").VbComboBox("cboTaskType").Select "Application"
						'VbWindow("frmMDI").VbWindow("frmAuditLog").ActiveX("TCIComboBox.tcb_3").VbComboBox("cboAction").Select "Accepted"

						Wait(2)
						.ActiveX("MaskEdBox").Drag 63,8 @@ hightlight id_;_788042_;_script infofile_;_ZIP::ssf388.xml_;_
						'.VbFrame("PanSelection_2").Drop 269,182 @@ hightlight id_;_460666_;_script infofile_;_ZIP::ssf389.xml_;_
						
						If Datatable.Value("Client","Reports_TextReports")="BOT" Then
						   .VbFrame("PanSelection").Drop 242,177
						End If						
						
						.ActiveX("MaskEdBox").Type Datatable.Value("StartDate","Reports_TextReports") '"01012002" @@ hightlight id_;_788042_;_script infofile_;_ZIP::ssf390.xml_;_
						Wait(2)
						'VbWindow("frmMDI").VbWindow("frmAuditLog").ActiveX("MaskEdBox").Type  micTab
						.ActiveX("MaskEdBox_2").Drag 65,4 @@ hightlight id_;_396154_;_script infofile_;_ZIP::ssf392.xml_;_
						'.VbFrame("PanSelection_2").Drop 422,174 @@ hightlight id_;_460666_;_script infofile_;_ZIP::ssf393.xml_;_
						If Datatable.Value("Client","Reports_TextReports")="BOT" Then
						   .VbFrame("PanSelection").Drop 424,178
						End If
						.ActiveX("MaskEdBox_2").Type Datatable.Value("EndDate","Reports_TextReports") '"12122020" @@ hightlight id_;_396154_;_script infofile_;_ZIP::ssf394.xml_;_
						'VbWindow("frmMDI").VbWindow("frmAuditLog").ActiveX("MaskEdBox_2").Type  micTab

						.VbButton("View").Click

						SelectedReport="Yes"
					End With
				ElseIf Datatable.Value("ReportName","Reports_TextReports")="Security Breach Log Report" and  Datatable.Value("RunReport","Reports_TextReports")="Yes" Then
					With	.VbWindow("frmRptSecurityBreaches")
						'If InStr(Environment("Portuguese_Language"),proj)>0 Then
							'VbWindow("frmMDI").InsightObject("InsightObject_3").Click
							'VbWindow("frmMDI").InsightObject("InsightObject_4").Click
							'VbWindow("frmMDI").InsightObject("InsightObject_31").Click
							'Else
								VbWindow("frmMDI").InsightObject("InsightObject").Click
								VbWindow("frmMDI").InsightObject("InsightObject_2").Click
								 If Datatable.Value("Client","Reports_TextReports")="MOZ" Then     
								     VbWindow("frmMDI").InsightObject("InsightObject_19").Click @@ hightlight id_;_10_;_script infofile_;_ZIP::ssf421.xml_;_
                                 End If
                               If Datatable.Value("Client","Reports_TextReports")="BOT" Then     
                                    Wait(3)                                
                                    VbWindow("frmMDI").InsightObject("InsightObject_34").Click								
								 End If
								
						'End If
						Wait(2)
						.ActiveX("MaskEdBox").Click 4,8
						
						If Datatable.Value("Client","Reports_TextReports")="BOT" Then
						VbWindow("frmMDI").VbWindow("frmRptSecurityBreaches").ActiveX("MaskEdBox").Drag 66,7 @@ hightlight id_;_986858_;_script infofile_;_ZIP::ssf423.xml_;_
						VbWindow("frmMDI").VbWindow("frmRptSecurityBreaches").VbFrame("PanSelection").Drop 246,45 @@ hightlight id_;_658468_;_script infofile_;_ZIP::ssf424.xml_;_
                        End If @@ hightlight id_;_986858_;_script infofile_;_ZIP::ssf425.xml_;_
                        
						.ActiveX("MaskEdBox").Type Datatable.Value("StartDate","Reports_TextReports") '"11112011"
						.ActiveX("MaskEdBox").Type  micTab
						.ActiveX("MaskEdBox_2").Type Datatable.Value("EndDate","Reports_TextReports") '"12122020"
						.ActiveX("MaskEdBox_2").Type  micTab
						'VbWindow("frmMDI").VbWindow("frmRptSecurityBreaches").VbEdit("txtIPAddress").Set "192.168.1.20"
						.VbButton("View").Click
						SelectedReport="Yes"
					End With
				ElseIf Datatable.Value("ReportName","Reports_TextReports")="Revenue Report" and  Datatable.Value("RunReport","Reports_TextReports")="Yes" Then

					'SelectedReport="Yes"
				ElseIf Datatable.Value("ReportName","Reports_TextReports")="LAAS" and  Datatable.Value("RunReport","Reports_TextReports")="Yes" Then

					'SelectedReport="Yes"
			End If
		End With

		If SelectedReport="Yes" Then
			If VbWindow("frmReportViewer").Dialog("#32770").WinButton("OK").Exist(10) Then
				VbWindow("frmReportViewer").Dialog("#32770").WinButton("OK").Click
			Else
				
				Set fso = CreateObject("Scripting.FileSystemObject")
						
				file_location = ProjectFolderPath &"\Reports\"& GetCurrentDate
						
				If not fso.FolderExists(file_location) Then
					fso.CreateFolder file_location  
				End If
				wait(5)
						
				filename=file_location & "\" & Datatable.Value("ReportName","Reports_TextReports")
						
				While fso.FileExists(filename&".pdf")
					filename=file_location & "\" & Datatable.Value("ReportName","Reports_TextReports")&"_"&fnRandomNumber(2)
				Wend
						
				If Datatable.Value("ReportName","Reports_TextReports")="Equipment Report" Then
						With	Window("Internet Explorer")
							wait(3)
							.WinToolbar("Command Bar").Press "P&rint"
							wait(5)
							With	VbWindow("frmPrintConfig")
								.ActiveX("TimoSoft TabStrip Control_2").ActiveX("TimoSoft Frame Control").ActiveX("TimoSoft TextBox Control").Object.Text="" @@ hightlight id_;_2755552_;_script infofile_;_ZIP::ssf132.xml_;_
								.ActiveX("TimoSoft TabStrip Control_2").ActiveX("TimoSoft Frame Control").ActiveX("TimoSoft TextBox Control").Type filename&".pdf" @@ hightlight id_;_2755552_;_script infofile_;_ZIP::ssf133.xml_;_
								.ActiveX("TimoSoft TabStrip Control_2").ActiveX("TimoSoft Frame Control").ActiveX("TimoSoft CheckBox Control").Click 133,13 @@ hightlight id_;_726196_;_script infofile_;_ZIP::ssf134.xml_;_
								.ActiveX("TimoSoft CommandButton_2").Click 55,19 @@ hightlight id_;_721330_;_script infofile_;_ZIP::ssf135.xml_;_
							End With @@ hightlight id_;_132888_;_script infofile_;_ZIP::ssf408.xml_;_
							
							wait(5)
							.Close

						End With
						Else
							With VbWindow("frmReportViewer")
								While Not .Exist
								Wend
'								While Not .Activate
'								Wend
								wait(12)
								
								With .Dialog("#32770")
									If .WinButton("OK").Exist(10) Then
										.WinButton("OK").Click
									End If
								End With
                                Wait(5) @@ hightlight id_;_3410060_;_script infofile_;_ZIP::ssf401.xml_;_
								.VbButton("Imprimir").Click @@ hightlight id_;_1581324_;_script infofile_;_ZIP::ssf383.xml_;_
		
								wait(8)
								
								With .Dialog("#32770")
									If .WinButton("OK").Exist(10) Then
										.WinButton("OK").Click
										wait(3)
										VbWindow("frmReportViewer").VbButton("Imprimir").Click
									End If
								End With

								.Dialog("Print").WinButton("OK").Click
							End With
							wait(5)
							'If InStr(Environment("Portuguese_Language"),proj)>0 Then
								wait(5)
								If Datatable.Value("ReportName","Reports_TextReports")="Audit Log" and  Datatable.Value("RunReport","Reports_TextReports")="Yes" Then
									With	Dialog("Printing Records").Dialog("Save Print Output As")
										.WinEdit("File name:").Set filename&".pdf"
										.WinEdit("File name:").Type  micTab @@ hightlight id_;_135778_;_script infofile_;_ZIP::ssf53.xml_;_
										.WinButton("Save").Click
									End With
									Else
									If Datatable.Value("ReportName","Reports_TextReports")="Site Reports" and  Datatable.Value("RunReport","Reports_TextReports")="Yes" and Datatable.Value("Client","Reports_TextReports")="BOT" Then
										Wait(15)
									End If	
									
										With	VbWindow("frmPrintConfig")
											.ActiveX("TimoSoft TabStrip Control").ActiveX("TimoSoft Frame Control").ActiveX("TimoSoft TextBox Control").Object.Text=" "
											.ActiveX("TimoSoft TabStrip Control").ActiveX("TimoSoft Frame Control").ActiveX("TimoSoft TextBox Control").Type filename&".pdf" @@ hightlight id_;_1907306_;_script infofile_;_ZIP::ssf122.xml_;_
											.ActiveX("TimoSoft TabStrip Control").ActiveX("TimoSoft Frame Control").ActiveX("TimoSoft TextBox Control").Type  micTab @@ hightlight id_;_1907306_;_script infofile_;_ZIP::ssf123.xml_;_
											.ActiveX("TimoSoft TabStrip Control").ActiveX("TimoSoft Frame Control").ActiveX("TimoSoft CheckBox Control").Click 15,6 @@ hightlight id_;_921632_;_script infofile_;_ZIP::ssf124.xml_;_
											.ActiveX("TimoSoft CommandButton").Click 52,9 @@ hightlight id_;_2492840_;_script infofile_;_ZIP::ssf125.xml_;_
										End With
								End If 
								
								'Else
								If Datatable.Value("ReportName","Reports_TextReports")="ITU Frequency Allocation Report" and Datatable.Value("RunReport","Reports_TextReports")="Yes" Then
								Wait(5)									
								End If
								
									'With	Dialog("Printing Records").Dialog("Save Print Output As")
										'.WinEdit("File name:").Set filename&".pdf" @@ hightlight id_;_135778_;_script infofile_;_ZIP::ssf52.xml_;_
										'.WinEdit("File name:").Type  micTab @@ hightlight id_;_135778_;_script infofile_;_ZIP::ssf53.xml_;_
										'Wait(2)
										'.WinButton("Save").Click @@ hightlight id_;_201448_;_script infofile_;_ZIP::ssf54.xml_;_
									'End With
							'End If 

							wait(7)

							VbWindow("frmReportViewer").VbButton("Close").Click
				End If 
			End If 
		End If 
	
	With	VbWindow("frmMDI")
		If Datatable.Value("ReportName","Reports_TextReports")="ITU Frequency Allocation Report" and Datatable.Value("RunReport","Reports_TextReports")="Yes" Then
			.VbWindow("frmRptFreqAlloc").VbButton("Close").Click
			ElseIf Datatable.Value("ReportName","Reports_TextReports")="Frequency Plan Report" and  Datatable.Value("RunReport","Reports_TextReports")="Yes" Then
				.VbWindow("frmRptFreqPlan").VbButton("Close").Click
			ElseIf Datatable.Value("ReportName","Reports_TextReports")="Licensed Station Report" and  Datatable.Value("RunReport","Reports_TextReports")="Yes" Then
				.VbWindow("frmRptLicenseStation").VbButton("Close").Click
			ElseIf Datatable.Value("ReportName","Reports_TextReports")="Complaint Report" and  Datatable.Value("RunReport","Reports_TextReports")="Yes" Then
				.VbWindow("frmRptComplaint").VbButton("Close").Click
			ElseIf Datatable.Value("ReportName","Reports_TextReports")="Violation Report" and  Datatable.Value("RunReport","Reports_TextReports")="Yes" Then
				.VbWindow("frmRptViolation").VbButton("Close").Click
			ElseIf Datatable.Value("ReportName","Reports_TextReports")="Inspection Report" and  Datatable.Value("RunReport","Reports_TextReports")="Yes" Then
				.VbWindow("frmRptInspections").VbButton("Close").Click
			ElseIf Datatable.Value("ReportName","Reports_TextReports")="Equipment Report" and  Datatable.Value("RunReport","Reports_TextReports")="Yes" Then
				.VbWindow("frmRptEquipment").VbButton("Close").Click
			ElseIf Datatable.Value("ReportName","Reports_TextReports")="Site Reports" and  Datatable.Value("RunReport","Reports_TextReports")="Yes" Then
				.VbWindow("frmRptSite").VbButton("Close").Click
			ElseIf Datatable.Value("ReportName","Reports_TextReports")="Client with Type, Make and Model" and  Datatable.Value("RunReport","Reports_TextReports")="Yes" Then
				.VbWindow("frmClientsWithMakeAndModel").VbButton("Close").Click
			ElseIf Datatable.Value("ReportName","Reports_TextReports")="Audit Log" and  Datatable.Value("RunReport","Reports_TextReports")="Yes" Then
				.VbWindow("frmAuditLog").VbButton("Close").Click
			ElseIf Datatable.Value("ReportName","Reports_TextReports")="Security Breach Log Report" and  Datatable.Value("RunReport","Reports_TextReports")="Yes" Then
				.VbWindow("frmRptSecurityBreaches").VbButton("Close").Click
		End If
	End With
    Next
    
	strTestCaseName = Environment.Value("TestName")
	If err.number = 0 Then
		blnFind = True
	Else
		blnFind = False
	End If
		
	UpdateTestCaseStatusInExcel strTestCaseName, blnFind',""

End If



	
	
'	If InStr(Environment("Portuguese_Language"),proj)>0 Then
'		VbWindow("frmMDI").InsightObject("InsightObject_3").Click
'		VbWindow("frmMDI").InsightObject("InsightObject_4").Click
'		VbWindow("frmMDI").InsightObject("InsightObject_32").Click
'	Else
'		VbWindow("frmMDI").InsightObject("InsightObject").Click
'		VbWindow("frmMDI").InsightObject("InsightObject_2").Click
'		VbWindow("frmMDI").InsightObject("InsightObject_6").Click
'
'	End If 
'	
'	If InStr(Environment("Portuguese_Language"),proj)>0 Then
'		VbWindow("frmMDI").InsightObject("InsightObject_3").Click
'		VbWindow("frmMDI").InsightObject("InsightObject_4").Click
'		VbWindow("frmMDI").InsightObject("InsightObject_14").Click
'	Else
'		VbWindow("frmMDI").InsightObject("InsightObject").Click
'		VbWindow("frmMDI").InsightObject("InsightObject_2").Click
'		VbWindow("frmMDI").InsightObject("InsightObject_9").Click
'	End If 
'	If InStr(Environment("Portuguese_Language"),proj)>0 Then
'		VbWindow("frmMDI").InsightObject("InsightObject_3").Click
'		VbWindow("frmMDI").InsightObject("InsightObject_4").Click
'		VbWindow("frmMDI").InsightObject("InsightObject_20").Click
'	Else
'		VbWindow("frmMDI").InsightObject("InsightObject").Click
'		VbWindow("frmMDI").InsightObject("InsightObject_2").Click
'		VbWindow("frmMDI").InsightObject("InsightObject_11").Click
'	End If
'	
'	If InStr(Environment("Portuguese_Language"),proj)>0 Then
'		VbWindow("frmMDI").InsightObject("InsightObject_3").Click
'		VbWindow("frmMDI").InsightObject("InsightObject_4").Click
'		VbWindow("frmMDI").InsightObject("InsightObject_21").Click
'	Else
'		VbWindow("frmMDI").InsightObject("InsightObject").Click
'		VbWindow("frmMDI").InsightObject("InsightObject_2").Click
'		VbWindow("frmMDI").InsightObject("InsightObject_12").Click
'	End If
'	If InStr(Environment("Portuguese_Language"),proj)>0 Then
'		VbWindow("frmMDI").InsightObject("InsightObject_3").Click
'		VbWindow("frmMDI").InsightObject("InsightObject_4").Click
'		VbWindow("frmMDI").InsightObject("InsightObject_22").Click
'	Else
'		VbWindow("frmMDI").InsightObject("InsightObject").Click
'		VbWindow("frmMDI").InsightObject("InsightObject_2").Click
'		VbWindow("frmMDI").InsightObject("InsightObject_13").Click
'	End If
'	If InStr(Environment("Portuguese_Language"),proj)>0 Then
'		VbWindow("frmMDI").InsightObject("InsightObject_3").Click
'		VbWindow("frmMDI").InsightObject("InsightObject_4").Click
'		VbWindow("frmMDI").InsightObject("InsightObject_24").Click
'	Else
'		VbWindow("frmMDI").InsightObject("InsightObject").Click
'		VbWindow("frmMDI").InsightObject("InsightObject_2").Click
'		VbWindow("frmMDI").InsightObject("InsightObject_15").Click
'	End If
'	If InStr(Environment("Portuguese_Language"),proj)>0 Then
'		VbWindow("frmMDI").InsightObject("InsightObject_3").Click
'		VbWindow("frmMDI").InsightObject("InsightObject_4").Click
'		VbWindow("frmMDI").InsightObject("InsightObject_25").Click
'	Else
'		VbWindow("frmMDI").InsightObject("InsightObject").Click
'		VbWindow("frmMDI").InsightObject("InsightObject_2").Click
'		VbWindow("frmMDI").InsightObject("InsightObject_16").Click
'	End If
'	If InStr(Environment("Portuguese_Language"),proj)>0 Then
'		VbWindow("frmMDI").InsightObject("InsightObject_3").Click
'		VbWindow("frmMDI").InsightObject("InsightObject_4").Click
'		VbWindow("frmMDI").InsightObject("InsightObject_27").Click
'	Else
'		VbWindow("frmMDI").InsightObject("InsightObject").Click
'		VbWindow("frmMDI").InsightObject("InsightObject_2").Click
'		VbWindow("frmMDI").InsightObject("InsightObject_17").Click
'	End If
'	If InStr(Environment("Portuguese_Language"),proj)>0 Then
'		VbWindow("frmMDI").InsightObject("InsightObject_3").Click
'		VbWindow("frmMDI").InsightObject("InsightObject_4").Click
'		VbWindow("frmMDI").InsightObject("InsightObject_29").Click
'	Else
'		VbWindow("frmMDI").InsightObject("InsightObject").Click
'		VbWindow("frmMDI").InsightObject("InsightObject_2").Click
'		VbWindow("frmMDI").InsightObject("InsightObject_18").Click
'	End If
'	If InStr(Environment("Portuguese_Language"),proj)>0 Then
'		VbWindow("frmMDI").InsightObject("InsightObject_3").Click
'		VbWindow("frmMDI").InsightObject("InsightObject_4").Click
'		VbWindow("frmMDI").InsightObject("InsightObject_31").Click
'	Else
'		VbWindow("frmMDI").InsightObject("InsightObject").Click
'		VbWindow("frmMDI").InsightObject("InsightObject_2").Click
'		VbWindow("frmMDI").InsightObject("InsightObject_19").Click
'	End If

	
	
'	'1
'	
'VbWindow("frmMDI").VbWindow("frmRptFreqAlloc").ActiveX("MaskEdBox").Type "0"
'VbWindow("frmMDI").VbWindow("frmRptFreqAlloc").ActiveX("MaskEdBox").Type  micTab
'VbWindow("frmMDI").VbWindow("frmRptFreqAlloc").ActiveX("MaskEdBox_2").Type "100"
'VbWindow("frmMDI").VbWindow("frmRptFreqAlloc").ActiveX("MaskEdBox_2").Type  micTab
'VbWindow("frmMDI").VbWindow("frmRptFreqAlloc").ActiveX("TCIComboBox.tcb").VbComboBox("cboServiceCategory").Select "Primary"
'VbWindow("frmMDI").VbWindow("frmRptFreqAlloc").ActiveX("TCIComboBox.tcb_2").VbComboBox("cboServiceClass").Select "AERONÁUTICA MÓVEL"
'VbWindow("frmMDI").VbWindow("frmRptFreqAlloc").ActiveX("TCIComboBox.tcb_3").VbComboBox("cboRegion").Select "ITU Region 1"
'VbWindow("frmMDI").VbWindow("frmRptFreqAlloc").VbButton("View").Click
'VbWindow("frmMDI").VbWindow("frmRptFreqAlloc").VbButton("Close").Click
'
''2
'
'VbWindow("frmMDI").VbWindow("frmRptFreqPlan").ActiveX("MaskEdBox").Click 73,12
'VbWindow("frmMDI").VbWindow("frmRptFreqPlan").ActiveX("MaskEdBox").Type "0"
'VbWindow("frmMDI").VbWindow("frmRptFreqPlan").ActiveX("MaskEdBox").Type  micTab
'VbWindow("frmMDI").VbWindow("frmRptFreqPlan").ActiveX("MaskEdBox_2").Type "100"
'VbWindow("frmMDI").VbWindow("frmRptFreqPlan").ActiveX("TCIComboBox.tcb").VbComboBox("cboServiceClass").Select "AERONÁUTICA MÓVEL"
'VbWindow("frmMDI").VbWindow("frmRptFreqPlan").VbButton("View").Click
'VbWindow("frmReportViewer").VbButton("Close_2").Click
'VbWindow("frmMDI").VbWindow("frmRptFreqPlan").VbButton("Close").Click
''3
'
'VbWindow("frmMDI").VbWindow("frmRptLicenseStation").ActiveX("TCIComboBox.tcb").VbComboBox("cboLicense").Select "0019"
'VbWindow("frmMDI").VbWindow("frmRptLicenseStation").VbButton("View").Click
'VbWindow("frmReportViewer").VbButton("Close_2").Click
'VbWindow("frmMDI").VbWindow("frmRptLicenseStation").VbButton("Close").Click
''4
'
'VbWindow("frmMDI").VbWindow("frmRptComplaint").ActiveX("TCIComboBox.tcb").VbComboBox("cboLastName").Select "Benson"
'VbWindow("frmMDI").VbWindow("frmRptComplaint").ActiveX("TCIComboBox.tcb_2").VbComboBox("cboFirstName").Select "Mark"
'VbWindow("frmMDI").VbWindow("frmRptComplaint").ActiveX("TCIComboBox.tcb_3").VbComboBox("cboCompany").Select "Ablue Comms"
'VbWindow("frmMDI").VbWindow("frmRptComplaint").VbEdit("txtCallSign").Set "abc123"
'VbWindow("frmMDI").VbWindow("frmRptComplaint").ActiveX("TCIComboBox.tcb_4").VbComboBox("cboStatus").Select "New"
'VbWindow("frmMDI").VbWindow("frmRptComplaint").ActiveX("MaskEdBox").Click 82,5
'VbWindow("frmMDI").VbWindow("frmRptComplaint").ActiveX("MaskEdBox").Type "0"
'VbWindow("frmMDI").VbWindow("frmRptComplaint").ActiveX("MaskEdBox").Type  micTab
'VbWindow("frmMDI").VbWindow("frmRptComplaint").ActiveX("MaskEdBox_2").Type "100"
'VbWindow("frmMDI").VbWindow("frmRptComplaint").ActiveX("MaskEdBox_3").Click 5,2
'VbWindow("frmMDI").VbWindow("frmRptComplaint").ActiveX("MaskEdBox_3").Type  micBack
'VbWindow("frmMDI").VbWindow("frmRptComplaint").ActiveX("MaskEdBox_3").Type "11112011"
'VbWindow("frmMDI").VbWindow("frmRptComplaint").ActiveX("MaskEdBox_3").Type  micTab
'VbWindow("frmMDI").VbWindow("frmRptComplaint").ActiveX("MaskEdBox_4").Type "12122020"
'VbWindow("frmMDI").VbWindow("frmRptComplaint").VbButton("View").Click
'VbWindow("frmReportViewer").VbButton("Close_2").Click
'VbWindow("frmMDI").VbWindow("frmRptComplaint").VbButton("Close").Click
'
''5
'
'VbWindow("frmMDI").VbWindow("frmRptViolation").ActiveX("TCIComboBox.tcb").VbComboBox("cboCompany").Select "Ablue Comms"
'VbWindow("frmMDI").VbWindow("frmRptViolation").ActiveX("MaskEdBox").Click 141,13
'VbWindow("frmMDI").VbWindow("frmRptViolation").ActiveX("MaskEdBox").Type "1"
'VbWindow("frmMDI").VbWindow("frmRptViolation").ActiveX("MaskEdBox").Type  micTab
'VbWindow("frmMDI").VbWindow("frmRptViolation").ActiveX("MaskEdBox_2").Type "100"
'VbWindow("frmMDI").VbWindow("frmRptViolation").ActiveX("MaskEdBox_2").Type  micTab
'VbWindow("frmMDI").VbWindow("frmRptViolation").ActiveX("MaskEdBox_3").Type "11/11/2011"
'VbWindow("frmMDI").VbWindow("frmRptViolation").ActiveX("MaskEdBox_3").Type  micTab
'VbWindow("frmMDI").VbWindow("frmRptViolation").ActiveX("MaskEdBox_4").Type "12/12/2020"
'VbWindow("frmMDI").VbWindow("frmRptViolation").ActiveX("MaskEdBox_4").Type  micTab
'VbWindow("frmMDI").VbWindow("frmRptViolation").ActiveX("MaskEdBox_5").Type "18"
'VbWindow("frmMDI").VbWindow("frmRptViolation").ActiveX("MaskEdBox_5").Type  micTab
'VbWindow("frmMDI").VbWindow("frmRptViolation").ActiveX("MaskEdBox_6").Type "0"
'VbWindow("frmMDI").VbWindow("frmRptViolation").ActiveX("MaskEdBox_6").Type  micTab
'VbWindow("frmMDI").VbWindow("frmRptViolation").ActiveX("MaskEdBox_7").Type "0"
'VbWindow("frmMDI").VbWindow("frmRptViolation").ActiveX("MaskEdBox_7").Type  micTab
'VbWindow("frmMDI").VbWindow("frmRptViolation").ActiveX("TCIComboBox.tcb_2").VbComboBox("cboFromLatHem").Select "N"
'VbWindow("frmMDI").VbWindow("frmRptViolation").ActiveX("TCIComboBox.tcb_2").VbComboBox("cboFromLatHem").Type  micTab
'VbWindow("frmMDI").VbWindow("frmRptViolation").ActiveX("MaskEdBox_8").Type "18"
'VbWindow("frmMDI").VbWindow("frmRptViolation").ActiveX("MaskEdBox_8").Type  micTab
'VbWindow("frmMDI").VbWindow("frmRptViolation").ActiveX("MaskEdBox_9").Type "0"
'VbWindow("frmMDI").VbWindow("frmRptViolation").ActiveX("MaskEdBox_9").Type  micTab
'VbWindow("frmMDI").VbWindow("frmRptViolation").ActiveX("MaskEdBox_10").Type "0"
'VbWindow("frmMDI").VbWindow("frmRptViolation").ActiveX("MaskEdBox_10").Type  micTab
'VbWindow("frmMDI").VbWindow("frmRptViolation").ActiveX("TCIComboBox.tcb_3").VbComboBox("cboToLatHem").Select "N"
'VbWindow("frmMDI").VbWindow("frmRptViolation").ActiveX("TCIComboBox.tcb_3").VbComboBox("cboToLatHem").Select "S"
'VbWindow("frmMDI").VbWindow("frmRptViolation").ActiveX("TCIComboBox.tcb_3").VbComboBox("cboToLatHem").Type  micTab
'VbWindow("frmMDI").VbWindow("frmRptViolation").ActiveX("MaskEdBox_11").Type "37"
'VbWindow("frmMDI").VbWindow("frmRptViolation").ActiveX("MaskEdBox_11").Type  micTab
'VbWindow("frmMDI").VbWindow("frmRptViolation").ActiveX("MaskEdBox_12").Type "0"
'VbWindow("frmMDI").VbWindow("frmRptViolation").ActiveX("MaskEdBox_12").Type  micTab
'VbWindow("frmMDI").VbWindow("frmRptViolation").ActiveX("MaskEdBox_13").Type "0"
'VbWindow("frmMDI").VbWindow("frmRptViolation").ActiveX("MaskEdBox_13").Type  micTab
'VbWindow("frmMDI").VbWindow("frmRptViolation").ActiveX("TCIComboBox.tcb_4").VbComboBox("cboFromLongHem").Select "E"
'VbWindow("frmMDI").VbWindow("frmRptViolation").ActiveX("TCIComboBox.tcb_4").VbComboBox("cboFromLongHem").Type  micTab
'VbWindow("frmMDI").VbWindow("frmRptViolation").ActiveX("MaskEdBox_14").Type "38"
'VbWindow("frmMDI").VbWindow("frmRptViolation").ActiveX("MaskEdBox_14").Type  micTab
'VbWindow("frmMDI").VbWindow("frmRptViolation").ActiveX("MaskEdBox_15").Type "0"
'VbWindow("frmMDI").VbWindow("frmRptViolation").ActiveX("MaskEdBox_15").Type  micTab
'VbWindow("frmMDI").VbWindow("frmRptViolation").ActiveX("MaskEdBox_16").Type "0"
'VbWindow("frmMDI").VbWindow("frmRptViolation").ActiveX("MaskEdBox_16").Type  micTab
'VbWindow("frmMDI").VbWindow("frmRptViolation").ActiveX("TCIComboBox.tcb_5").VbComboBox("cboToLongHem").Select "E"
'VbWindow("frmMDI").VbWindow("frmRptViolation").ActiveX("TCIComboBox.tcb_5").VbComboBox("cboToLongHem").Select "W"
'VbWindow("frmMDI").VbWindow("frmRptViolation").VbButton("View").Click
'VbWindow("frmReportViewer").VbButton("Close_2").Click
'VbWindow("frmMDI").VbWindow("frmRptViolation").VbButton("Close").Click
'
''6
'
'VbWindow("frmMDI").VbWindow("frmRptInspections").ActiveX("TCIComboBox.tcb").VbComboBox("cboInspctionNum").Select "53"
'VbWindow("frmMDI").VbWindow("frmRptInspections").ActiveX("TCIComboBox.tcb_2").VbComboBox("cboType").Select "Installation"
'VbWindow("frmMDI").VbWindow("frmRptInspections").ActiveX("TCIComboBox.tcb_3").VbComboBox("cboStatus").Select "New"
'VbWindow("frmMDI").VbWindow("frmRptInspections").ActiveX("MaskEdBox").Click 2,8
'VbWindow("frmMDI").VbWindow("frmRptInspections").ActiveX("MaskEdBox").Type "11112011"
'VbWindow("frmMDI").VbWindow("frmRptInspections").ActiveX("MaskEdBox").Type  micTab
'VbWindow("frmMDI").VbWindow("frmRptInspections").ActiveX("MaskEdBox_2").Type "12122020"
'VbWindow("frmMDI").VbWindow("frmRptInspections").VbButton("View").Click
'VbWindow("frmReportViewer").VbButton("Close_2").Click
'VbWindow("frmMDI").VbWindow("frmRptInspections").VbButton("Close").Click
''7
'
'VbWindow("frmMDI").VbWindow("frmRptEquipment").ActiveX("TCIComboBox.tcb").VbComboBox("cboEquipType").Select "Other"
'VbWindow("frmMDI").VbWindow("frmRptEquipment").ActiveX("TCIComboBox.tcb_2").VbComboBox("cboMake").Select "MX123 JXX 112"
'VbWindow("frmMDI").VbWindow("frmRptEquipment").ActiveX("TCIComboBox.tcb_3").VbComboBox("cboModel").Select "SW TCI SMS"
'VbWindow("frmMDI").VbWindow("frmRptEquipment").ActiveX("MaskEdBox").Click 7,9
'VbWindow("frmMDI").VbWindow("frmRptEquipment").ActiveX("MaskEdBox").Type "1"
'VbWindow("frmMDI").VbWindow("frmRptEquipment").ActiveX("MaskEdBox").Type  micBack
'VbWindow("frmMDI").VbWindow("frmRptEquipment").ActiveX("MaskEdBox").Type  micBack
'VbWindow("frmMDI").VbWindow("frmRptEquipment").ActiveX("MaskEdBox").Type  micBack
'VbWindow("frmMDI").VbWindow("frmRptEquipment").ActiveX("MaskEdBox").Type  micBack
'VbWindow("frmMDI").VbWindow("frmRptEquipment").ActiveX("MaskEdBox").Type "11112011"
'VbWindow("frmMDI").VbWindow("frmRptEquipment").ActiveX("MaskEdBox").Type  micTab
'VbWindow("frmMDI").VbWindow("frmRptEquipment").ActiveX("MaskEdBox_2").Type "12122020"
'VbWindow("frmMDI").VbWindow("frmRptEquipment").VbButton("View").Click
'Window("Internet Explorer").Dialog("Internet Explorer 11").WinButton("Ask me later").Click
'Window("Internet Explorer").Close
'VbWindow("frmMDI").VbWindow("frmRptEquipment").VbButton("Close").Click
''8
'
'VbWindow("frmMDI").VbWindow("frmRptSite").ActiveX("TCIComboBox.tcb").VbComboBox("cboSite").Select "Aircraft"
'VbWindow("frmMDI").VbWindow("frmRptSite").ActiveX("TCIComboBox.tcb_2").VbComboBox("cboStationClass").Select "3RP Trunking"
'VbWindow("frmMDI").VbWindow("frmRptSite").ActiveX("MaskEdBox").Click 14,10
'VbWindow("frmMDI").VbWindow("frmRptSite").ActiveX("MaskEdBox").Type "17"
'VbWindow("frmMDI").VbWindow("frmRptSite").ActiveX("MaskEdBox").Type  micTab
'VbWindow("frmMDI").VbWindow("frmRptSite").ActiveX("MaskEdBox_2").Type "0"
'VbWindow("frmMDI").VbWindow("frmRptSite").ActiveX("MaskEdBox_2").Type  micTab
'VbWindow("frmMDI").VbWindow("frmRptSite").ActiveX("MaskEdBox_3").Type "0"
'VbWindow("frmMDI").VbWindow("frmRptSite").ActiveX("MaskEdBox_3").Type  micTab
'VbWindow("frmMDI").VbWindow("frmRptSite").ActiveX("TCIComboBox.tcb_3").VbComboBox("cboFromLatHem").Select "N"
'VbWindow("frmMDI").VbWindow("frmRptSite").ActiveX("TCIComboBox.tcb_3").VbComboBox("cboFromLatHem").Type  micTab
'VbWindow("frmMDI").VbWindow("frmRptSite").ActiveX("MaskEdBox_4").Type "17"
'VbWindow("frmMDI").VbWindow("frmRptSite").ActiveX("MaskEdBox_4").Type  micTab
'VbWindow("frmMDI").VbWindow("frmRptSite").ActiveX("MaskEdBox_5").Type "0"
'VbWindow("frmMDI").VbWindow("frmRptSite").ActiveX("MaskEdBox_5").Type  micTab
'VbWindow("frmMDI").VbWindow("frmRptSite").ActiveX("MaskEdBox_6").Type "0"
'VbWindow("frmMDI").VbWindow("frmRptSite").ActiveX("MaskEdBox_6").Type  micTab
'VbWindow("frmMDI").VbWindow("frmRptSite").ActiveX("TCIComboBox.tcb_4").VbComboBox("cboToLatHem").Select "N"
'VbWindow("frmMDI").VbWindow("frmRptSite").ActiveX("TCIComboBox.tcb_4").VbComboBox("cboToLatHem").Select "S"
'VbWindow("frmMDI").VbWindow("frmRptSite").ActiveX("MaskEdBox_7").Click 12,10
'VbWindow("frmMDI").VbWindow("frmRptSite").ActiveX("MaskEdBox_7").Type "35"
'VbWindow("frmMDI").VbWindow("frmRptSite").ActiveX("MaskEdBox_7").Type  micTab
'VbWindow("frmMDI").VbWindow("frmRptSite").ActiveX("MaskEdBox_8").Type "0"
'VbWindow("frmMDI").VbWindow("frmRptSite").ActiveX("MaskEdBox_8").Type  micTab
'VbWindow("frmMDI").VbWindow("frmRptSite").ActiveX("MaskEdBox_9").Type "0"
'VbWindow("frmMDI").VbWindow("frmRptSite").ActiveX("MaskEdBox_9").Type  micTab
'VbWindow("frmMDI").VbWindow("frmRptSite").ActiveX("TCIComboBox.tcb_5").VbComboBox("cboFromLongHem").Select "E"
'VbWindow("frmMDI").VbWindow("frmRptSite").ActiveX("TCIComboBox.tcb_5").VbComboBox("cboFromLongHem").Type  micTab
'VbWindow("frmMDI").VbWindow("frmRptSite").ActiveX("MaskEdBox_10").Type "35"
'VbWindow("frmMDI").VbWindow("frmRptSite").ActiveX("MaskEdBox_10").Type  micTab
'VbWindow("frmMDI").VbWindow("frmRptSite").ActiveX("MaskEdBox_11").Type "0"
'VbWindow("frmMDI").VbWindow("frmRptSite").ActiveX("MaskEdBox_11").Type  micTab
'VbWindow("frmMDI").VbWindow("frmRptSite").ActiveX("MaskEdBox_12").Type "0"
'VbWindow("frmMDI").VbWindow("frmRptSite").ActiveX("MaskEdBox_12").Type  micTab
'VbWindow("frmMDI").VbWindow("frmRptSite").ActiveX("TCIComboBox.tcb_6").VbComboBox("cboToLongHem").Select "W"
'VbWindow("frmMDI").VbWindow("frmRptSite").ActiveX("TCIComboBox.tcb_6").VbComboBox("cboToLongHem").Type  micTab
'VbWindow("frmMDI").VbWindow("frmRptSite").ActiveX("TCIComboBox.tcb_7").VbComboBox("cboApplicationNo").Select "100 / RCL"
'VbWindow("frmMDI").VbWindow("frmRptSite").VbButton("View").Click
'VbWindow("frmMDI").Dialog("#32770").WinButton("OK").Click
'VbWindow("frmMDI").VbWindow("frmRptSite").VbButton("Close").Click
'
''9
'
'VbWindow("frmMDI").VbWindow("frmClientsWithMakeAndModel").ActiveX("TCIComboBox.tcb").VbComboBox("cboType").Select "Receptor"
'VbWindow("frmMDI").VbWindow("frmClientsWithMakeAndModel").ActiveX("TCIComboBox.tcb").VbComboBox("cboType").Select "Transceptor"
'VbWindow("frmMDI").VbWindow("frmClientsWithMakeAndModel").ActiveX("TCIComboBox.tcb_2").VbComboBox("cboMake").Select "Advantech"
'VbWindow("frmMDI").VbWindow("frmClientsWithMakeAndModel").ActiveX("TCIComboBox.tcb_3").VbComboBox("cboModel").Select "AMT 65 Modem"
'VbWindow("frmMDI").VbWindow("frmClientsWithMakeAndModel").ActiveX("TCIComboBox.tcb_4").VbComboBox("cboUserNumber").Select "A TEST FOR 7946    (# CD32)"
'VbWindow("frmMDI").VbWindow("frmClientsWithMakeAndModel").ActiveX("TCIComboBox.tcb_5").VbComboBox("cboLicense").Select "Cellular /WLL/ RLAN"
'VbWindow("frmMDI").VbWindow("frmClientsWithMakeAndModel").ActiveX("TCIComboBox.tcb_6").VbComboBox("cboServiceClass").Select "AERONÁUTICA MÓVEL"
'VbWindow("frmMDI").VbWindow("frmClientsWithMakeAndModel").ActiveX("TCIComboBox.tcb_7").VbComboBox("cboServiceType").Select "móveis celulares"
'VbWindow("frmMDI").VbWindow("frmClientsWithMakeAndModel").VbButton("Search").Click
'VbWindow("frmMDI").VbWindow("frmClientsWithMakeAndModel").VbButton("View as Crystal report").Click
'VbWindow("frmMDI").Dialog("ASMS").WinButton("OK").Click
'VbWindow("frmMDI").Dialog("#32770").Click 69,92
'VbWindow("frmMDI").Dialog("#32770").WinButton("OK").Click
'VbWindow("frmMDI").VbWindow("frmClientsWithMakeAndModel").VbButton("Close").Click
'
''10
'
'
'VbWindow("frmMDI").VbWindow("frmAuditLog").ActiveX("TCIComboBox.tcb").VbComboBox("cboUserName").Select "ACCPAC"
'VbWindow("frmMDI").VbWindow("frmAuditLog").ActiveX("TCIComboBox.tcb_2").VbComboBox("cboTaskType").Select "Application"
'VbWindow("frmMDI").VbWindow("frmAuditLog").ActiveX("TCIComboBox.tcb_3").VbComboBox("cboAction").Select "Accepted"
'VbWindow("frmMDI").VbWindow("frmAuditLog").ActiveX("MaskEdBox").Drag 63,3
'VbWindow("frmMDI").VbWindow("frmAuditLog").VbFrame("PanSelection").Drop 240,177
'VbWindow("frmMDI").VbWindow("frmAuditLog").ActiveX("MaskEdBox").Type "10102010"
'VbWindow("frmMDI").VbWindow("frmAuditLog").ActiveX("MaskEdBox").Type  micTab
'VbWindow("frmMDI").VbWindow("frmAuditLog").ActiveX("MaskEdBox_2").Type  micShiftDwn  +  micRight  +  micRight
'VbWindow("frmMDI").VbWindow("frmAuditLog").ActiveX("MaskEdBox_2").Type  micRight  +  micRight  +  micRight
'VbWindow("frmMDI").VbWindow("frmAuditLog").ActiveX("MaskEdBox_2").Type  micRight  +  micRight  +  micRight
'VbWindow("frmMDI").VbWindow("frmAuditLog").ActiveX("MaskEdBox_2").Type  micRight  +  micRight  +  micRight
'VbWindow("frmMDI").VbWindow("frmAuditLog").ActiveX("MaskEdBox_2").Type  micRight  +  micRight  + "121"
'VbWindow("frmMDI").VbWindow("frmAuditLog").ActiveX("MaskEdBox_2").Type "22020"
'VbWindow("frmMDI").VbWindow("frmAuditLog").VbButton("View").Click
'VbWindow("frmMDI").Dialog("ASMS").WinButton("OK").Click
'VbWindow("frmMDI").Dialog("#32770").WinButton("OK").Click
'VbWindow("frmMDI").VbWindow("frmAuditLog").VbButton("Close").Click
'VbWindow("frmMDI").VbWindow("frmRptSecurityBreaches").ActiveX("MaskEdBox").Click 4,8
'VbWindow("frmMDI").VbWindow("frmRptSecurityBreaches").ActiveX("MaskEdBox").Type "11"
'VbWindow("frmMDI").VbWindow("frmRptSecurityBreaches").ActiveX("MaskEdBox").Type  micBack
'VbWindow("frmMDI").VbWindow("frmRptSecurityBreaches").ActiveX("MaskEdBox").Type  micBack
'VbWindow("frmMDI").VbWindow("frmRptSecurityBreaches").ActiveX("MaskEdBox").Type  micBack
'VbWindow("frmMDI").VbWindow("frmRptSecurityBreaches").ActiveX("MaskEdBox").Type  micBack
'VbWindow("frmMDI").VbWindow("frmRptSecurityBreaches").ActiveX("MaskEdBox").Type "11112011"
'VbWindow("frmMDI").VbWindow("frmRptSecurityBreaches").ActiveX("MaskEdBox").Type  micTab
'VbWindow("frmMDI").VbWindow("frmRptSecurityBreaches").ActiveX("MaskEdBox_2").Type "12122020"
'VbWindow("frmMDI").VbWindow("frmRptSecurityBreaches").ActiveX("MaskEdBox_2").Type  micTab
'VbWindow("frmMDI").VbWindow("frmRptSecurityBreaches").VbEdit("txtIPAddress").Set "192.168.1.20"
'VbWindow("frmMDI").VbWindow("frmRptSecurityBreaches").VbButton("View").Click
'VbWindow("frmMDI").Dialog("ASMS").WinButton("OK").Click
'VbWindow("frmMDI").Dialog("#32770").WinButton("OK").Click
'VbWindow("frmMDI").VbWindow("frmRptSecurityBreaches").VbButton("Close").Click



	'wait(7)
	
	'While Not VbWindow("frmMDI").VbWindow("frmRptMisc").Exist
	'Wend

'	SelectedReport="No"
'	
'	For Iterator = 1 To datatable.GetSheet("Reports_TextReports").GetRowCount
'	
'		DataTable.LocalSheet.SetCurrentRow(Iterator)
'		
'		If Datatable.Value("ReportName","Reports_TextReports")="" and Datatable.Value("RunReport","Reports_TextReports")="" Then
'			Exit for
'		End If
'		
'		If Datatable.Value("ReportName","Reports_TextReports")="With mobile communication ability" and Datatable.Value("RunReport","Reports_TextReports")="Yes" Then
'			VbWindow("frmMDI").VbWindow("frmRptMisc").VbRadioButton("With mobile communication").Set
'			SelectedReport="Yes"
'		ElseIf Datatable.Value("ReportName","Reports_TextReports")="Awaiting frequency assignment" and  Datatable.Value("RunReport","Reports_TextReports")="Yes" Then
'			VbWindow("frmMDI").VbWindow("frmRptMisc").VbRadioButton("Awaiting frequency assignment").Set
'			SelectedReport="Yes"
'		End If
'		
'		If SelectedReport="Yes" Then
'			VbWindow("frmMDI").VbWindow("frmRptMisc").VbButton("View").Click
'			
'			Set fso = CreateObject("Scripting.FileSystemObject")
'					
'			file_location = ProjectFolderPath &"\Reports\"& GetCurrentDate
'					
'			If not fso.FolderExists(file_location) Then
'				fso.CreateFolder file_location  
'			End If
'			wait(5)
'					
'			filename=file_location & "\" & Datatable.Value("ReportName","Reports_TextReports")
'					
'			While fso.FileExists(filename&".pdf")
'				filename=file_location & "\" & Datatable.Value("ReportName","Reports_TextReports")&"_"&fnRandomNumber(2)
'			Wend
'					
'			If Datatable.Value("ReportName","Reports_TextReports")="Awaiting frequency assignment" Then
'				wait(3)
'				Window("Internet Explorer").WinToolbar("Command Bar").Press "P&rint"
'				wait(5)
'				VbWindow("frmPrintConfig").ActiveX("TimoSoft TabStrip Control_2").ActiveX("TimoSoft Frame Control").ActiveX("TimoSoft TextBox Control").Object.Text=""
'				VbWindow("frmPrintConfig").ActiveX("TimoSoft TabStrip Control_2").ActiveX("TimoSoft Frame Control").ActiveX("TimoSoft TextBox Control").Type filename&".pdf"
'				VbWindow("frmPrintConfig").ActiveX("TimoSoft TabStrip Control_2").ActiveX("TimoSoft Frame Control").ActiveX("TimoSoft CheckBox Control").Click 133,13
'				VbWindow("frmPrintConfig").ActiveX("TimoSoft CommandButton_2").Click 55,19
'				wait(5)
'				Window("Internet Explorer").Close
'
'			Else
'				While Not VbWindow("frmReportViewer").Exist
'				Wend
'				wait(5)
'				VbWindow("frmReportViewer").InsightObject("InsightObject").Click
'				wait(5)
'				VbWindow("frmReportViewer").Dialog("Print").WinButton("OK").Click
'				wait(5)
'				If InStr(Environment("Portuguese_Language"),proj)>0 Then
'					wait(3)
'					VbWindow("frmPrintConfig").ActiveX("TimoSoft TabStrip Control").ActiveX("TimoSoft Frame Control").ActiveX("TimoSoft TextBox Control").Object.Text=" "
'					VbWindow("frmPrintConfig").ActiveX("TimoSoft TabStrip Control").ActiveX("TimoSoft Frame Control").ActiveX("TimoSoft TextBox Control").Type filename&".pdf"
'					VbWindow("frmPrintConfig").ActiveX("TimoSoft TabStrip Control").ActiveX("TimoSoft Frame Control").ActiveX("TimoSoft TextBox Control").Type  micTab
'					VbWindow("frmPrintConfig").ActiveX("TimoSoft TabStrip Control").ActiveX("TimoSoft Frame Control").ActiveX("TimoSoft CheckBox Control").Click 15,6
'					VbWindow("frmPrintConfig").ActiveX("TimoSoft CommandButton").Click 52,9
'				Else
'					Dialog("Printing Records").Dialog("Save Print Output As").WinEdit("File name:").Set filename&".pdf"
'					Dialog("Printing Records").Dialog("Save Print Output As").WinEdit("File name:").Type  micTab
'					Dialog("Printing Records").Dialog("Save Print Output As").WinButton("Save").Click
'				End If 
'				
'				wait(7)
'					
'				VbWindow("frmReportViewer").VbButton("Close").Click
'
'			End If 
'		End If 
'	Next
'	
'	VbWindow("frmMDI").VbWindow("frmRptMisc").VbButton("Close").Click
'
'	strTestCaseName = Environment.Value("TestName")
'	If err.number = 0 Then
'		blnFind = True
'	Else
'		blnFind = False
'	End If
'		
'	UpdateTestCaseStatusInExcel strTestCaseName, blnFind
'
'End If
