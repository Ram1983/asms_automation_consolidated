﻿wait(2)
blnFind=False
blnVal=False
err.number = 0

datatable.ImportSheet ProjectFolderPath &"\Desktop_Input_Data_Sheet\Input_Data_Sheet.xls","Licence_Terminate","Licence_Terminate"
n=datatable.GetSheet("Licence_Terminate").GetRowCount

'For i = 1 To n Step 1
With VbWindow("frmMDI")
  If n>0 Then
    datatable.SetCurrentRow(1)
    
    '=================Navigate to Licence Cancel
    .WinMenu("Menu").Select "Licence;Terminate"
    
    With .VbWindow("frmModLicense")
      While Not .Exist(15)
      Wend
      If .ActiveX("TCIComboBox.tcb").VbComboBox("cboApplicationNo").exist Then
        Set send=CreateObject("Wscript.Shell")
        .ActiveX("TCIComboBox.tcb").VbComboBox("cboApplicationNo").Type GenerateRandomString(6)
        If .VbButton("Next").GetROProperty("enabled")=False Then
          .ActiveX("TCIComboBox.tcb").VbComboBox("cboApplicationNo").Type GenerateRandomString(6)
          send.SendKeys "{DEL}"
          If .VbButton("Next").GetROProperty("enabled")=False Then
            blnVal=True
          End If
        End If
      End If
      '======Selecting Licence Number
      .ActiveX("TCIComboBox.tcb").VbComboBox("cboApplicationNo").Select 1'"SPAM2020-0001 / AMR"
      .ActiveX("TCIComboBox.tcb_2").VbComboBox("cboLicense").Select 1'"Amateur Licence(Temporary)"
      .ActiveX("TCIComboBox.tcb").VbComboBox("cboApplicationNo").Select 1'Datatable.value("LicenceNum","Licence_Terminate")'"0004 / RCL"
      wait(2)
      
      '====Next button 
      
      .VbButton("Next").Click
    End With
    
    'reason
    wait(3)
    With	.Dialog("#32770")
      .WinEdit("Please describe why you").Set Datatable.value("Reason","Licence_Terminate")'"reason terminate"
      wait(5)
      If .WinButton("OK").Exist Then
        .WinButton("OK").Click	
      End If
      
      'Confirmation
      wait(3)
      If .WinButton("Yes").Exist Then
        .WinButton("Yes").Click	
      End If
      wait(3)
      If .WinButton("OK").Exist Then
        .WinButton("OK").Click	
      End If
    End With
    strTestCaseName = Environment.Value("TestName")
    If err.number = 0 And blnVal Then
      blnFind = True
    Else
      blnFind = False
    End If
    UpdateTestCaseStatusInExcel strTestCaseName, blnFind
  End If
End With

