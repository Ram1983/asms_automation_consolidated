﻿blnVal=False
fn_NavigateToScreen "Accounting;Adjustments;Journal Entry;View Journal Entry"
With VbWindow("frmMDI")
	With .VbWindow("frmFindJournal")
		While Not .ActiveX("TCIComboBox.tcb").VbComboBox("cboJournalId").Exist
		Wend
		.ActiveX("TCIComboBox.tcb").VbComboBox("cboJournalId").Select 1 @@ hightlight id_;_397214_;_script infofile_;_ZIP::ssf1.xml_;_
		.VbButton("Next").Click @@ hightlight id_;_462736_;_script infofile_;_ZIP::ssf2.xml_;_
	End With
	With .VbWindow("frmARJournal_Entry")
		While Not .VbButton("Preview").Exist
		Wend
		.VbButton("Preview").Click @@ hightlight id_;_1444434_;_script infofile_;_ZIP::ssf3.xml_;_
		Wait 40
	End With
End With
With VbWindow("frmReportViewer")
	 @@ hightlight id_;_1181890_;_script infofile_;_ZIP::ssf6.xml_;_
	.VbButton("Print").Click @@ hightlight id_;_4065376_;_script infofile_;_ZIP::ssf7.xml_;_
	If .Dialog("Print").WinButton("OK").Exist(80) Then
		.Dialog("Print").WinButton("OK").Click
	End If @@ hightlight id_;_265590_;_script infofile_;_ZIP::ssf8.xml_;_
End With
With Dialog("Save Print Output As")
	.WinEdit("File name:").Set fn_report @@ hightlight id_;_1050678_;_script infofile_;_ZIP::ssf9.xml_;_
	.WinButton("Save").Click @@ hightlight id_;_2885934_;_script infofile_;_ZIP::ssf10.xml_;_
End With
With VbWindow("frmReportViewer")
	If .VbButton("Close").Exist Then
		.VbButton("Close").Click
	End If
End With
If Err.number=0 Then
	blnVal=True
End If

UpdateTestCaseStatusInExcel Environment("TestName"),blnVal
