﻿wait(2)
blnFind=True
err.number = 0

datatable.ImportSheet ProjectFolderPath &"\Input_Data_Sheet\Input_Data_Sheet.xls","Print_Licence_Notice","Print_Licence_Notice"
n=datatable.GetSheet("Print_Licence_Notice").GetRowCount

'For i = 1 To n Step 1
If n>0 Then
	datatable.SetCurrentRow(1) @@ hightlight id_;_1_;_script infofile_;_ZIP::ssf1.xml_;_
	
	'Print Licence Notice Screen navigation
    fn_NavigateToScreen "Licence;Print Licence;Notice"
	With VbWindow("frmMDI")
		With .VbWindow("frmFindLicense")
			While not .Exist(10)

			Wend
			'==Selecting custmer licence Number
			.ActiveX("TCIComboBox.tcb").VbComboBox("cboLicenseNo").Select 2'Datatable.Value("LicenceNumber","Print_Licence_Notice") '"00001-1 / SSL" @@ hightlight id_;_133638_;_script infofile_;_ZIP::ssf6.xml_;_
			.VbButton("Next").Click @@ hightlight id_;_133712_;_script infofile_;_ZIP::ssf7.xml_;_
		End With

		'===Selecting Option 

		With	.VbWindow("frmRptLicNotice")
			If Datatable.Value("NoticeType","Print_Licence_Notice")="Reinstatement" Then
				.VbRadioButton("Reinstatement").Set	
				ElseIf Datatable.Value("NoticeType","Print_Licence_Notice")="Termination" Then
					.VbRadioButton("Termination").Set
				ElseIf Datatable.Value("NoticeType","Print_Licence_Notice")="Modification" Then
					.VbRadioButton("Modification").Set
				ElseIf Datatable.Value("NoticeType","Print_Licence_Notice")="Service Notification"  Then
					.VbRadioButton("Service Notification").Set
				ElseIf Datatable.Value("NoticeType","Print_Licence_Notice")="Awaiting for equipment/sites" Then
					.VbRadioButton("Awaiting for equipment/sites").Set
				ElseIf Datatable.Value("NoticeType","Print_Licence_Notice")="Fee Increase" Then
					.VbRadioButton("Fee Increase").Set
				ElseIf Datatable.Value("NoticeType","Print_Licence_Notice")="Activity Notification" Then
					.VbRadioButton("Activity Notification").Set
			End If

     		'View the notice
			.VbButton("View").Click @@ hightlight id_;_133800_;_script infofile_;_ZIP::ssf15.xml_;_
			'Close
			.VbButton("Close").Click @@ hightlight id_;_133792_;_script infofile_;_ZIP::ssf16.xml_;_
		End With
	End With

	strTestCaseName = Environment.Value("TestName")
	If err.number = 0 Then
		blnFind = True
	  Else
		blnFind = False
	End If
	
	With Browser("'Generated Report'")
		.Page("'Generated Report'").Sync @@ hightlight id_;_Browser("'Generated Report'").Page("'Generated Report'")_;_script infofile_;_ZIP::ssf17.xml_;_
		If .Page("'Generated Report'").Exist Then
			.Page("'Generated Report'").Sync
			.CloseAllTabs
		End If
		 @@ hightlight id_;_265248_;_script infofile_;_ZIP::ssf18.xml_;_
	End With
	UpdateTestCaseStatusInExcel strTestCaseName, blnFind
End If

