﻿blnVal=False

'===Create Sublice for customer application click in admin portal
If Browser("Admin Home Page").Page("Admin Home Page").Link("Search Customer or Application").Exist Then
   Browser("Admin Home Page").Page("Admin Home Page").Link("Search Customer or Application").Click
End If
 @@ script infofile_;_ZIP::ssf1.xml_;_
''---Email search 
'Customer number search @@ hightlight id_;_263202_;_script infofile_;_ZIP::ssf4.xml_;_
CustomerNumber=GetDataFromDB("stp_Automation_GetLatestCustomerForSpectrumNewApplicationWebCP '"&Environment("Client")&"',''")
If Instr(1,CustomerNumber,"(#",1)>0 Then
	val=Split(CustomerNumber,"(#")
	CustomerNumber= Replace(Trim(val(Ubound(val))),")","")
End If
Browser("Admin Home Page").Page("Page").WebList("ccnum").Select CustomerNumber @@ script infofile_;_ZIP::ssf5.xml_;_
Browser("Admin Home Page").Page("Page").Link("Submit Search").Click @@ script infofile_;_ZIP::ssf3.xml_;_
While Not Browser("Admin Home Page").Page("Page").Link("+").Exist
Wend

'----Add new application
Browser("Admin Home Page").Page("Page").Link("+").Click @@ script infofile_;_ZIP::ssf6.xml_;_
While Not Browser("Admin Home Page").Page("Manage Application").WebElement("Current Application Settings").Exist
Wend

If Err.number=0 Then
	blnVal=true
End If

UpdateTestCaseStatusInExcel Environment("TestName"),blnVal
