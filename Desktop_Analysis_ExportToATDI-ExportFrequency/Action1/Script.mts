﻿
blnFind=True
err.number = 0
datatable.ImportSheet ProjectFolderPath &"\Input_Data_Sheet\Input_Data_Sheet.xls","ExportFrequency","ExportFrequency"
n=datatable.GetSheet("ExportFrequency").GetRowCount()

'For i = 1 To n Step 1
If n>0 Then
  datatable.SetCurrentRow(1)	
  
  
  'Screen navigation
  VbWindow("frmMDI").WinMenu("Menu").Select "Analysis;Export to ATDI;Export Frequency"
  
  With VbWindow("frmMDI")
    With .VbWindow("frmNatFreqFilter")
      .ActiveX("MaskEdBox").Type datatable.value("LowFrequency","ExportFrequency")' "000"
      wait(5)
      'VbWindow("frmMDI").VbWindow("frmNatFreqFilter").ActiveX("MaskEdBox_2").DblClick 29,12
      .ActiveX("MaskEdBox_2").Type datatable.value("HighFrequency","ExportFrequency")'"1000"
      wait(5)
      .ActiveX("TCIComboBox.tcb").VbComboBox("cboSel").Select(1)' "AERONAUTICAL MOBILE"
    End With
    wait(5)
    'submit
    VbWindow("frmMDI").VbWindow("frmNatFreqFilter").VbButton("OK").Click
    wait(3)
    'selecting frequency 
    With .VbWindow("frmNatFrequency")
      .VbButton("Select All").Click
      wait(3)
      'Export
      .VbButton("Export").Click
    End With
    wait(3)
    'Confirmation
    With .Dialog("#32770")
      .WinButton("Yes").Click
      'confirmation
      .WinButton("OK").Click
    End With
    wait(5)
    
    'Close the application screen
    .VbWindow("frmNatFrequency").VbButton("Close").Click
  End With
  
  
  strTestCaseName = Environment.Value("TestName")
  If err.number = 0 Then
    blnFind = True
  Else
    blnFind = False
  End If
  
  UpdateTestCaseStatusInExcel strTestCaseName, blnFind
End If

