﻿blnFind=True
err.number = 0
datatable.ImportSheet ProjectFolderPath &"\Desktop_Input_Data_Sheet\Input_Data_Sheet.xls","Reports_NoInputs","Reports_NoInputs"
n=datatable.GetSheet("Reports_NoInputs").GetRowCount()

'For i = 1 To n Step 1
If n>0 Then
datatable.SetCurrentRow(1)	

fn_NavigateToScreen "Report;Custom Reports"

	With VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling")
		wait(7)
		While Not .Exist
		Wend
	End With

For Iterator = 1 To datatable.GetSheet("Reports_NoInputs").GetRowCount

DataTable.LocalSheet.SetCurrentRow(Iterator)
	
If Datatable.Value("ReportTab","Reports_NoInputs")="" and Datatable.Value("ReportName","Reports_NoInputs")="" and Datatable.Value("RunReport","Reports_NoInputs")="" Then
	Exit for
End If

If Datatable.Value("RunReport","Reports_NoInputs")="Yes" Then
wait(3)
			With	VbWindow("frmMDI")

				SelectedReport="No" @@ hightlight id_;_922138_;_script infofile_;_ZIP::ssf58.xml_;_

				With	.VbWindow("frmRptMonthlyBilling")
					If Datatable.Value("ReportTab","Reports_NoInputs")="Licence Analysis" and Datatable.Value("RunReport","Reports_NoInputs")="Yes" Then
						.Activate
						wait(3)
						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("SSTab").VirtualObject("LicenseAnalysis").Click 0,0
						wait(3)

						
'						If VbWindow("frmMDI").InsightObject("InsightObject_5").Exist(10) Then
'							VbWindow("frmMDI").InsightObject("InsightObject_5").Click
'						ElseIf VbWindow("frmMDI").InsightObject("InsightObject_15").Exist(10) Then
'							VbWindow("frmMDI").InsightObject("InsightObject_15").Click
'						ElseIf VbWindow("frmMDI").InsightObject("InsightObject_10").Exist(10) Then
'							VbWindow("frmMDI").InsightObject("InsightObject_10").Click
'						End If

						If Datatable.Value("ReportName","Reports_NoInputs")="Licenses that expire within 30 days" and Datatable.Value("RunReport","Reports_NoInputs")="Yes" Then
							.ActiveX("SSTab").VbRadioButton("Licences that expire within").Set
						ElseIf Datatable.Value("ReportName","Reports_NoInputs")="Expired licenses" and  Datatable.Value("RunReport","Reports_NoInputs")="Yes" Then
							.ActiveX("SSTab").VbRadioButton("Expired Licences").Set @@ hightlight id_;_460788_;_script infofile_;_ZIP::ssf44.xml_;_
						End If
						SelectedReport="Yes"
						
					ElseIf Datatable.Value("ReportTab","Reports_NoInputs")="Financial 1" and Datatable.Value("RunReport","Reports_NoInputs")="Yes" Then
							.Activate
							wait(3)
							VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("SSTab").VirtualObject("Financial1").Click 0,0
							wait(3)

'							If VbWindow("frmMDI").InsightObject("InsightObject_7").Exist(10) Then
'								VbWindow("frmMDI").InsightObject("InsightObject_7").Click	
'								ElseIf VbWindow("frmMDI").InsightObject("InsightObject_11").Exist(10) Then
'									VbWindow("frmMDI").InsightObject("InsightObject_11").Click @@ hightlight id_;_5_;_script infofile_;_ZIP::ssf95.xml_;_
'								ElseIf VbWindow("frmMDI").InsightObject("InsightObject_14").Exist(10) Then
'									VbWindow("frmMDI").InsightObject("InsightObject_14").Click
'							End If

							If Datatable.Value("ReportName","Reports_NoInputs")="Aged Receivable" and  Datatable.Value("RunReport","Reports_NoInputs")="Yes" Then
								.ActiveX("SSTab").VbRadioButton("Aged Receivable").Set
							'ElseIf Datatable.Value("ReportName","Reports_NoInputs")="" and  Datatable.Value("RunReport","Reports_NoInputs")="Yes" Then

							End If
							SelectedReport="Yes"

						ElseIf Datatable.Value("ReportTab","Reports_NoInputs")="Financial 2" and Datatable.Value("RunReport","Reports_NoInputs")="Yes" Then
							.Activate
							wait(3)
							VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("SSTab").VirtualObject("Financial2").Click 0,0
							wait(3)
'							If VbWindow("frmMDI").InsightObject("InsightObject_8").Exist(10) Then
'								VbWindow("frmMDI").InsightObject("InsightObject_8").Click @@ hightlight id_;_798458_;_script infofile_;_ZIP::ssf96.xml_;_
'								ElseIf VbWindow("frmMDI").InsightObject("InsightObject_12").Exist(10) Then
'									VbWindow("frmMDI").InsightObject("InsightObject_12").Click @@ hightlight id_;_6_;_script infofile_;_ZIP::ssf97.xml_;_
'								ElseIf VbWindow("frmMDI").InsightObject("InsightObject_16").Exist(10) Then
'									VbWindow("frmMDI").InsightObject("InsightObject_16").Click
'							End If


							If Datatable.Value("ReportName","Reports_NoInputs")="Unpaid Invoices" and  Datatable.Value("RunReport","Reports_NoInputs")="Yes" Then
								.ActiveX("SSTab").VbRadioButton("Unpaid Invoices").Set
								'ElseIf Datatable.Value("ReportName","Reports_NoInputs")="" and  Datatable.Value("RunReport","Reports_NoInputs")="Yes" Then

							End If
							SelectedReport="Yes"

						ElseIf Datatable.Value("ReportTab","Reports_NoInputs")="Others" and Datatable.Value("RunReport","Reports_NoInputs")="Yes" Then
							.Activate
							wait(3)
							VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("SSTab").VirtualObject("Others").Click 0,0
							wait(3)
'							If VbWindow("frmMDI").InsightObject("InsightObject_9").Exist(10) Then
'								VbWindow("frmMDI").InsightObject("InsightObject_9").Click
'								ElseIf VbWindow("frmMDI").InsightObject("InsightObject_13").Exist(10) Then
'									VbWindow("frmMDI").InsightObject("InsightObject_13").Click
'								ElseIf VbWindow("frmMDI").InsightObject("InsightObject_17").Exist(10) Then
'									VbWindow("frmMDI").InsightObject("InsightObject_17").Click
'							End If


							If Datatable.Value("ReportName","Reports_NoInputs")="Outstanding Customer Balance" and  Datatable.Value("RunReport","Reports_NoInputs")="Yes" Then
								.ActiveX("SSTab").VbRadioButton("Outstanding Customer Balance").Set
								ElseIf Datatable.Value("ReportName","Reports_NoInputs")="Customer with outstanding credit" and  Datatable.Value("RunReport","Reports_NoInputs")="Yes" Then
									.ActiveX("SSTab").VbRadioButton("Customers with Outstanding").Set
							End If
							SelectedReport="Yes"

					End If
				End With
			End With

wait(2)
	
If SelectedReport="Yes" Then


	VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbButton("View").Click @@ hightlight id_;_69160_;_script infofile_;_ZIP::ssf7.xml_;_

	Set fso = CreateObject("Scripting.FileSystemObject")
	
	file_location = ProjectFolderPath &"\Desktop_Reports\"& GetCurrentDate
	
	If not fso.FolderExists(file_location) Then
		fso.CreateFolder file_location  
	End If

	wait(5)
	
				With VbWindow("frmReportViewer")
					While Not .Exist
					Wend
					wait(15)
					
					.VbButton("Print").Click @@ hightlight id_;_1_;_script infofile_;_ZIP::ssf59.xml_;_
					wait(10)
					While not .Dialog("Print").WinButton("OK").Exist
						If .Dialog("#32770").WinButton("OK").Exist(5) Then
							.Dialog("#32770").WinButton("OK").Click
							wait(10)
							.VbButton("Print").Click
							wait(6)
						End If
					Wend 
					
					.Dialog("Print").WinButton("OK").Click @@ hightlight id_;_1377628_;_script infofile_;_ZIP::ssf60.xml_;_
				End With
				
				filename=file_location & "\" & Datatable.Value("ReportName","Reports_NoInputs")

				While fso.FileExists(filename&".pdf")
					filename=file_location & "\" & Datatable.Value("ReportName","Reports_NoInputs")&"_"&fnRandomNumber(2)
				Wend

				If Dialog("Printing Records").Exist Then
					With	Dialog("Printing Records").Dialog("Save Print Output As")

						.WinEdit("File name:").Set filename&".pdf"
						.WinEdit("File name:").Type  micTab
						.WinButton("Save").Click
					End With
					ElseIf VbWindow("frmPrintConfig").Exist Then
						With VbWindow("frmPrintConfig")
							.ActiveX("TimoSoft TabStrip Control").ActiveX("TimoSoft Frame Control").ActiveX("TimoSoft TextBox Control").Object.Text=""
							.ActiveX("TimoSoft TabStrip Control").ActiveX("TimoSoft Frame Control").ActiveX("TimoSoft TextBox Control").Type filename &".pdf"
							.ActiveX("TimoSoft TabStrip Control").ActiveX("TimoSoft Frame Control").ActiveX("TimoSoft CheckBox Control").Click 16,7 @@ hightlight id_;_68614_;_script infofile_;_ZIP::ssf110.xml_;_
							Wait(2)
							.ActiveX("TimoSoft CommandButton").Click 39,6
						End With
				End If 

		wait(8)
		VbWindow("frmReportViewer").VbButton("Close").Click
	
	End If
End If

Next
wait(5)
VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbButton("Close").Click

strTestCaseName = Environment.Value("TestName")
	If err.number = 0 Then
		blnFind = True
	Else
		blnFind = False
	End If
	
	UpdateTestCaseStatusInExcel strTestCaseName, blnFind

End If 



