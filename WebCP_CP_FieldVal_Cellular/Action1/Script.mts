﻿
 
blnFind=True
err.number = 0

'Import data from Input data sheet  from folder
datatable.ImportSheet ProjectFolderPath&"\Input_Data_Sheet\Input_Data_Sheet.xls","cpCellular","Global"
n=datatable.GetSheet("Global").GetRowCount

'For i = 1 To n Step 1
If n>0 Then
    datatable.SetCurrentRow(1)
'	
	If Browser("name:=Application","title:=Application").Page("title:=Application").WebElement("innertext:=TYPE:.*"&Datatable.Value("SubLic","Global")&".*","html tag:=SPAN").Exist Then
		Browser("name:=Application","title:=Application").Page("title:=Application").WebElement("innertext:=TYPE:.*"&Datatable.Value("SubLic","Global")&".*","html tag:=SPAN").Click
	End If
	wait 5
	If Browser("name:=Application","title:=Application").Page("title:=Application").Link("html id:=img.*","name:="&Datatable.Value("StationName","Global")).Exist(20) Then
	      Browser("name:=Application","title:=Application").Page("title:=Application").Link("html id:=img.*","name:="&Datatable.Value("StationName","Global")).Click
	 End If
End If
Set obj=Browser("name:=Application","title:=Application").Page("title:=Application").WebEdit("html id:=.*stationName","name:=.*stationName")
If obj.Exist Then
   fn_WebCPSubLic_FieldLevelVal obj,"alphanumeric",500,""
   fn_WebCPSubLic_FieldLevelVal obj,"","","Station Location"
End If
Set objRadius=Browser("name:=Application","title:=Application").Page("title:=Application").WebEdit("html id:=.*Radius.*","name:=.*nomRadius.*")
If objRadius.Exist Then
   fn_WebCPSubLic_FieldLevelVal objRadius,"numeric",500,""
   fn_WebCPSubLic_FieldLevelVal objRadius,"","","Radius"
End If
If Browser("name:=Application","title:=Application").Page("title:=Application").WebElement("innertext:="&Datatable.Value("Equipmenttype","Global"),"html id:=img.*").exist Then
     Browser("name:=Application","title:=Application").Page("title:=Application").WebElement("innertext:="&Datatable.Value("Equipmenttype","Global"),"html id:=img.*").click
     Browser("name:=Application","title:=Application").Page("title:=Application").Sync
     Wait 10     
    Set objEquipSerialNum=Browser("name:=Application","title:=Application").Page("title:=Application").WebEdit("name:=.*equipSerialNum")
	If objEquipSerialNum.Exist Then
	   fn_WebCPSubLic_FieldLevelVal objEquipSerialNum,"alphanumeric",500,""
	   fn_WebCPSubLic_FieldLevelVal objEquipSerialNum,"","","Equipment Serial Number"
	End If
End If


    Set objAntennaPower=Browser("name:=Application","title:=Application").Page("title:=Application").WebEdit("html id:=.*antennaPower.*","name:=.*antennaPower.*")
	If objAntennaPower.Exist Then
	   fn_WebCPSubLic_FieldLevelVal objAntennaPower,"numeric",10000001,""
	   fn_WebCPSubLic_FieldLevelVal objAntennaPower,"","","Output Power \(W\)"
	End If


 Set objEquipSerialNum=Browser("name:=Application","title:=Application").Page("title:=Application").WebEdit("name:=.*equipSerialNum")
	If objEquipSerialNum.Exist Then
	   fn_WebCPSubLic_FieldLevelVal objEquipSerialNum,"alphanumeric",500,""
	   fn_WebCPSubLic_FieldLevelVal objEquipSerialNum,"","","Equipment Serial Number"
	End If


 Set objHeight=Browser("name:=Application","title:=Application").Page("title:=Application").WebEdit("html id:=tbl_antenna__physicHt","name:=tbl_antenna__physicHt")
	If objHeight.Exist Then
	   fn_WebCPSubLic_FieldLevelVal objHeight,"alphanumeric",500,""
	   fn_WebCPSubLic_FieldLevelVal objHeight,"numeric",5000000000000000,""
	   fn_WebCPSubLic_FieldLevelVal objHeight,"","","Height A. G. L. (m)"
	   valValueinTODO="Height A. G. L. (m)"
	   If Browser("name:=Application","title:=Application").Page("title:=Application").WebElement("outerhtml:=<b>"&valValueinTODO&"</b>","index:=1").Exist(8) OR  fn_WebCPSubLic_FieldLevelVal(objHeight,"","","Height A. G. L. (m)") Then
	          Browser("name:=Application","title:=Application").Page("title:=Application").WebElement("outerhtml:=<b>"&valValueinTODO&"</b>","index:=1").highlight
	   	End If
	End If
	
 Set objMainLobe=Browser("name:=Application","title:=Application").Page("title:=Application").WebEdit("name:=tbl_antenna__mainLobeAzi","index:=0")
	If objMainLobe.Exist Then
	   fn_WebCPSubLic_FieldLevelVal objMainLobe,"alphanumeric",500,""
	   fn_WebCPSubLic_FieldLevelVal objMainLobe,"numeric",500,""
	   fn_WebCPSubLic_FieldLevelVal objMainLobe,"","","Main Lobe Azimuth \(deg\)"
End If

 @@ script infofile_;_ZIP::ssf1.xml_;_

 Set objTiltAngle=Browser("name:=Application","title:=Application").Page("title:=Application").WebEdit("html id:=.*tiltAngle.*","name:=.*tiltAngle.*")
 If objTiltAngle.Exist Then
	   fn_WebCPSubLic_FieldLevelVal objTiltAngle,"alphanumeric",500,""
	   fn_WebCPSubLic_FieldLevelVal objTiltAngle,"numeric",91,""
	   fn_WebCPSubLic_FieldLevelVal objTiltAngle,"numeric",-89,""
	   fn_WebCPSubLic_FieldLevelVal objTiltAngle,"","","Tilt Angle \(deg\)"
End If

 Set objFeedLength=Browser("name:=Application","title:=Application").Page("title:=Application").WebEdit("html id:=.*FeedLength.*","name:=.*FeedLength.*")
 If Browser("name:=Application","title:=Application").Page("title:=Application").WebRadioGroup("html id:=tbl_antenna__ByCableOrLineAttenuation").Exist(5) Then
 	Browser("name:=Application","title:=Application").Page("title:=Application").WebRadioGroup("html id:=tbl_antenna__ByCableOrLineAttenuation").Select "#0"
 End If
 If objFeedLength.Exist Then
	   fn_WebCPSubLic_FieldLevelVal objFeedLength,"alphanumeric",500,""
	   fn_WebCPSubLic_FieldLevelVal objFeedLength,"numeric",1000000,""
End If

 Set objLineAttenuation=Browser("name:=Application","title:=Application").Page("title:=Application").WebEdit("html id:=.*FeedLength.*","name:=.*FeedLength.*")
 If Browser("name:=Application","title:=Application").Page("title:=Application").WebRadioGroup("html id:=tbl_antenna__ByCableOrLineAttenuation").Exist(5) Then
 	Browser("name:=Application","title:=Application").Page("title:=Application").WebRadioGroup("html id:=tbl_antenna__ByCableOrLineAttenuation").Select "#1"
 End If
 If objLineAttenuation.Exist Then
	   fn_WebCPSubLic_FieldLevelVal objLineAttenuation,"alphanumeric",500,""
	   fn_WebCPSubLic_FieldLevelVal objLineAttenuation,"numeric",1000000,""
End If


