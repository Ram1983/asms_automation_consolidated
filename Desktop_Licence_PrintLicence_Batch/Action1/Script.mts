﻿wait(2)
blnFind=True
err.number = 0
datatable.ImportSheet ProjectFolderPath &"\Desktop_Input_Data_Sheet\Input_Data_Sheet.xls","Licence_Batch","Licence_Batch"
n=datatable.GetSheet("Licence_Batch").GetRowCount
'For i = 1 To n Step 1
If n>0 Then
	datatable.SetCurrentRow(1)
	
	'====Screen Navigation
     fn_NavigateToScreen "Licence;Print Licence;Batch"
	wait(3)
	With VbWindow("frmMDI").VbWindow("frmPrintLicense")
		While not .ActiveX("TCIComboBox.tcb").exist(10)
		Wend
		.ActiveX("TCIComboBox.tcb").VbComboBox("cboClient").Select 2'Datatable.value("CompanyName","Licence_Batch")'"00006Bruce Wayne" @@ hightlight id_;_1117742_;_script infofile_;_ZIP::ssf4.xml_;_
		'Search
		.VbButton("Search").Click @@ hightlight id_;_330584_;_script infofile_;_ZIP::ssf5.xml_;_
		'Select all
		If .VbButton("Select All").Exist(5) Then @@ hightlight id_;_330740_;_script infofile_;_ZIP::ssf6.xml_;_
			.VbButton("Select All").Click
		End If
		.VbButton("Print").Click @@ hightlight id_;_330768_;_script infofile_;_ZIP::ssf11.xml_;_
		Wait 10
	End With

	With Dialog("Printing Records").Dialog("Save Print Output As")
		.WinEdit("File name:").Set fn_report @@ hightlight id_;_136174_;_script infofile_;_ZIP::ssf13.xml_;_
		.WinButton("Save").Click @@ hightlight id_;_136184_;_script infofile_;_ZIP::ssf14.xml_;_
	End With
	'Close
	wait(5)
	VbWindow("frmMDI").VbWindow("frmPrintLicense").VbButton("Close").Click @@ hightlight id_;_593438_;_script infofile_;_ZIP::ssf12.xml_;_
	
	strTestCaseName = Environment.Value("TestName")
	If err.number = 0 Then
		blnFind = True
	  Else
		blnFind = False
	End If
	
	UpdateTestCaseStatusInExcel strTestCaseName, blnFind
End If

