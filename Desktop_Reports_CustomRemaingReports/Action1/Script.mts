﻿
Public Function GetItemIndex(ByRef test_object, ByRef itemText)
       content = test_object.GetContent()
       lines = split(content, vbLf)
       linesCount = uBound(lines) + 1
       For index = 0 To linesCount
       		text = lines(index)
       		if text = itemText Then
       			GetItemIndex = index
       			Exit Function
       		end if
       Next
       Reporter.ReportEvent micFail, "GetItemIndex", "Cannot find the specified item"
       GetItemIndex = -1
       
End Function
RegisterUserFunc "VbComboBox", "GetItemIndex", "GetItemIndex", True

Public Function ASMSComboBox(ByRef objectName, ByRef objectValue)

Set objD=Description.Create
objD("micclass").value="VbComboBox"
objD("vbname").value=objectName
Set obj=VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ChildObjects(objD)
'msgbox obj.count
'obj(0).highlight
'print obj(0).GetSelection
'print objectValue
'msgbox len(obj(0).GetSelection)
'msgbox len(objectValue)

If not obj(0).GetSelection = objectValue Then
	obj(0).Select objectValue
End If


End Function

'ASMSComboBox "cboAppType","WEBCP"
'ASMSComboBox "cboAppStatus","*"
'ASMSComboBox "cboApplicantLinked","AABB TEl   (# MP9)"
'ASMSComboBox "cboServiceClassSingle","AERONÁUTICA MÓVEL"
'ASMSComboBox "cboApplicantLinked",Datatable.Value("ApplicantName","CustomReportsRemaining") '"AABB TEl   (# MP9)"
'cboAppType.Select "*"
'cboAppStatus
'cboApplicant
			
blnFind=True
err.number = 0
datatable.ImportSheet ProjectFolderPath &"\Desktop_Input_Data_Sheet\Input_Data_Sheet.xls","CustomReportsRemaining","CustomReportsRemaining"
n=datatable.GetSheet("CustomReportsRemaining").GetRowCount()

'For i = 1 To n Step 1
If n>0 Then
	datatable.SetCurrentRow(1)	
	Environment.LoadFromFile(ProjectFolderPath &"\ASMS_Variables.xml")
	proj = Datatable.Value("Client","CustomReportsRemaining")
	
	'If InStr(Environment("Portuguese_Language"),proj)>0 Then
		'VbWindow("frmMDI").InsightObject("InsightObject_18").Click
		'VbWindow("frmMDI").InsightObject("InsightObject_19").Click
		
	fn_NavigateToScreen "Report;Custom Reports"
	
	wait(7)
	
	While Not VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").Exist(50)
	Wend

	ReportTab=""
	ReportTabCount=0
	For Iterator = 1 To datatable.GetSheet("CustomReportsRemaining").GetRowCount
		DataTable.LocalSheet.SetCurrentRow(Iterator)
		AssignInputs="No"
		If Datatable.Value("ReportTab","CustomReportsRemaining")="" and Datatable.Value("ReportName","CustomReportsRemaining")="" and Datatable.Value("RunReport","CustomReportsRemaining")="" Then
			Exit for
		End If
	
		If Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then	
			
			If ReportTab<>Datatable.Value("ReportTab","CustomReportsRemaining") Then
				wait(3)
				ReportTab=Datatable.Value("ReportTab","CustomReportsRemaining")
				ReportTabCount=0
				AssignInputs="No"
			End If
			If Iterator > 1 and SelectedReport="Yes"	Then
				'VbWindow("frmMDI").WinObject("MDIClient").VScroll micSetPos, -7
			End If
			SelectedReport="No" @@ hightlight id_;_922138_;_script infofile_;_ZIP::ssf58.xml_;_
			Wait(1)
			With	VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling")
				If Datatable.Value("ReportTab","CustomReportsRemaining")="Application Analysis" and Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
					If ReportTabCount=0 Then
						ReportTabCount=1	
						.Activate
						Wait(5)
						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("SSTab").VirtualObject("ApplicationAnalysis").Click 0,0
						
						'If InStr(Environment("Portuguese_Language"),proj)>0 Then
						'VbWindow("frmMDI").InsightObject("InsightObject_21").Click
						
'						If VbWindow("frmMDI").InsightObject("InsightObject_4").Exist(10) Then
'							VbWindow("frmMDI").InsightObject("InsightObject_4").Click
'							ElseIf VbWindow("frmMDI").InsightObject("InsightObject_12").Exist(10) Then
'								VbWindow("frmMDI").InsightObject("InsightObject_12").Click	
'							ElseIf VbWindow("frmMDI").InsightObject("InsightObject_29").Exist(10) Then
'								VbWindow("frmMDI").InsightObject("InsightObject_29").Click
'						End If
					End If
					Wait(5)
					If Datatable.Value("ReportName","CustomReportsRemaining")="Status of application for a specified customer" and Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
						.ActiveX("SSTab").VbRadioButton("Status of an application").Set
						SelectedReport="Yes"
						ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="Number of applications of a particular type and status over a defined period of time" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
							.ActiveX("SSTab").VbRadioButton("Number of applications").Set
							SelectedReport="Yes"
						ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="No. Of type approval appl submitted day, week and month" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
							.ActiveX("SSTab").VbRadioButton("Number of Type approval").Set
							SelectedReport="Yes"
					End If

				ElseIf Datatable.Value("ReportTab","CustomReportsRemaining")="Licence Analysis" and Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
						If ReportTabCount=0 Then
							ReportTabCount=1	
							.Activate
							wait(3)
							VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("SSTab").VirtualObject("LicenseAnalysis").Click 0,0
							'If InStr(Environment("Portuguese_Language"),proj)>0 Then
							'VbWindow("frmMDI").InsightObject("InsightObject_22").Click
'							If VbWindow("frmMDI").InsightObject("InsightObject_5").Exist(10) Then
'								VbWindow("frmMDI").InsightObject("InsightObject_5").Click
'								ElseIf VbWindow("frmMDI").InsightObject("InsightObject_13").Exist(10) Then
'									VbWindow("frmMDI").InsightObject("InsightObject_13").Click	
'								ElseIf VbWindow("frmMDI").InsightObject("InsightObject_30").Exist(10) Then
'									VbWindow("frmMDI").InsightObject("InsightObject_30").Click
'							End If
						End If

						'If proj<>"MOZ" Then
						Wait(5)
						If Datatable.Value("ReportName","CustomReportsRemaining")="Applications ready to be licensed" and Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
							.ActiveX("SSTab").VbRadioButton("Applications ready to").Set
							SelectedReport="Yes"
						ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="Licenses status over a defined period of time" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
								.ActiveX("SSTab").VbRadioButton("Licenses status over a").Set @@ hightlight id_;_264228_;_script infofile_;_ZIP::ssf361.xml_;_
								SelectedReport="Yes"
						ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="Licenses status over a defined period of time with customers contact information" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
								.ActiveX("SSTab").VbRadioButton("Licenses status over a_2").Set
								SelectedReport="Yes"
						ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="Systems" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
								.ActiveX("SSTab").VbRadioButton("Systems").Set
								SelectedReport="Yes"
						End If

						'End If
				ElseIf Datatable.Value("ReportTab","CustomReportsRemaining")="Financial 1" and Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
						If ReportTabCount=0 Then
							ReportTabCount=1	
							.Activate
							wait(3)
							VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("SSTab").VirtualObject("Financial1").Click 0,0
							'If InStr(Environment("Portuguese_Language"),proj)>0 Then
							'VbWindow("frmMDI").InsightObject("InsightObject_24").Click
'							If VbWindow("frmMDI").InsightObject("InsightObject_6").Exist(10) Then
'								VbWindow("frmMDI").InsightObject("InsightObject_6").Click
'								ElseIf VbWindow("frmMDI").InsightObject("InsightObject_14").Exist(10) Then
'									VbWindow("frmMDI").InsightObject("InsightObject_14").Click
'								ElseIf VbWindow("frmMDI").InsightObject("InsightObject_31").Exist(10) Then
'									VbWindow("frmMDI").InsightObject("InsightObject_31").Click
'							End If
						End If
						Wait(5)
						If Datatable.Value("ReportName","CustomReportsRemaining")="List of Customers" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
							.ActiveX("SSTab").VbRadioButton("List of Customers").Set @@ hightlight id_;_198626_;_script infofile_;_ZIP::ssf364.xml_;_
							SelectedReport="Yes"
						ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="Account Statement" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
								.ActiveX("SSTab").VbRadioButton("Account Statement").Set @@ hightlight id_;_198648_;_script infofile_;_ZIP::ssf365.xml_;_
								SelectedReport="Yes"
						ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="Annual Billing" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
								.ActiveX("SSTab").VbRadioButton("Annual Billing").Set @@ hightlight id_;_199924_;_script infofile_;_ZIP::ssf366.xml_;_
								SelectedReport="Yes"
						ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="Fee collected for a particular type of service over a defined period of time" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
								.ActiveX("SSTab").VbRadioButton("Fees collected for a particula").Set @@ hightlight id_;_264070_;_script infofile_;_ZIP::ssf367.xml_;_
								SelectedReport="Yes"
						ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="License fee calculated and collected over a defined period of time" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
								.ActiveX("SSTab").VbRadioButton("Licence fee calculated").Set @@ hightlight id_;_1904068_;_script infofile_;_ZIP::ssf368.xml_;_
								SelectedReport="Yes"
						End If
					ElseIf Datatable.Value("ReportTab","CustomReportsRemaining")="Financial 2" and Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
						If ReportTabCount=0 Then
							ReportTabCount=1	
							.Activate
							wait(3)
							VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("SSTab").VirtualObject("Financial2").Click 0,0
							
							'If InStr(Environment("Portuguese_Language"),proj)>0 Then
							'VbWindow("frmMDI").InsightObject("InsightObject_25").Click
'							If VbWindow("frmMDI").InsightObject("InsightObject_8").Exist(10) Then
'								VbWindow("frmMDI").InsightObject("InsightObject_8").Click
'								ElseIf VbWindow("frmMDI").InsightObject("InsightObject_15").Exist(10) Then
'									VbWindow("frmMDI").InsightObject("InsightObject_15").Click
'								ElseIf VbWindow("frmMDI").InsightObject("InsightObject_32").Exist(10) Then
'									VbWindow("frmMDI").InsightObject("InsightObject_32").Click
'							End If
						End If	
						wait(5)
						If Datatable.Value("ReportName","CustomReportsRemaining")="Journal Report" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
							.ActiveX("SSTab").VbRadioButton("Journal Report").Set
							SelectedReport="Yes"
						ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="Payment Gateway Response" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
							Wait(3)
							.ActiveX("SSTab").VbRadioButton("Payment Gateway Response").Set @@ hightlight id_;_67614_;_script infofile_;_ZIP::ssf862.xml_;_
							SelectedReport="Yes"
						End If
					ElseIf Datatable.Value("ReportTab","CustomReportsRemaining")="Others" and Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
						If ReportTabCount=0 Then
							ReportTabCount=1
							.Activate
							wait(3)
							VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("SSTab").VirtualObject("Others").Click 0,0
							'If InStr(Environment("Portuguese_Language"),proj)>0 Then
							'VbWindow("frmMDI").InsightObject("InsightObject_26").Click
'							If VbWindow("frmMDI").InsightObject("InsightObject_9").Exist(10) Then
'								VbWindow("frmMDI").InsightObject("InsightObject_9").Click
'								ElseIf VbWindow("frmMDI").InsightObject("InsightObject_16").Exist(10) Then
'									VbWindow("frmMDI").InsightObject("InsightObject_16").Click
'								ElseIf VbWindow("frmMDI").InsightObject("InsightObject_33").Exist(10) Then
'									VbWindow("frmMDI").InsightObject("InsightObject_33").Click
'							End If

						End If
						wait(5)
						If Datatable.Value("ReportName","CustomReportsRemaining")="Customer Transactions" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
							.ActiveX("SSTab").VbRadioButton("Customer Transactions").Set @@ hightlight id_;_198628_;_script infofile_;_ZIP::ssf370.xml_;_
							SelectedReport="Yes"
						ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="Payments Received" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
								.ActiveX("SSTab").VbRadioButton("Payments Received").Set @@ hightlight id_;_198634_;_script infofile_;_ZIP::ssf371.xml_;_
								SelectedReport="Yes"
						ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="Forecast of fee collection over a defined period of time" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
								.ActiveX("SSTab").VbRadioButton("Forecast of fee collection").Set @@ hightlight id_;_198646_;_script infofile_;_ZIP::ssf372.xml_;_
								SelectedReport="Yes"
						ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="Customer License Types" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
								.ActiveX("SSTab").VbRadioButton("Customers License Types").Set
								SelectedReport="Yes"
						End If
					ElseIf Datatable.Value("ReportTab","CustomReportsRemaining")="Wireless Station Analysis" and Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
						If ReportTabCount=0 Then
							ReportTabCount=1
							.Activate
							wait(3)
							VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("SSTab").VirtualObject("WirelessStationAnalysis").Click 0,0

							'If InStr(Environment("Portuguese_Language"),proj)>0 Then
							'VbWindow("frmMDI").InsightObject("InsightObject_20").Click

'							If VbWindow("frmMDI").InsightObject("InsightObject_3").Exist(10) Then
'								VbWindow("frmMDI").InsightObject("InsightObject_3").Click
'								ElseIf VbWindow("frmMDI").InsightObject("InsightObject_28").Exist(10) Then
'									VbWindow("frmMDI").InsightObject("InsightObject_28").Click @@ hightlight id_;_2_;_script infofile_;_ZIP::ssf838.xml_;_
'								ElseIf VbWindow("frmMDI").InsightObject("InsightObject_34").Exist(10) Then
'									VbWindow("frmMDI").InsightObject("InsightObject_34").Click
'							End If
						End If
						Wait(3)
						If Datatable.Value("ReportName","CustomReportsRemaining")="Number of wireless stations operating in given frequency band" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
							.ActiveX("SSTab").VbRadioButton("Number of wireless stations").Set
							SelectedReport="Yes"
						ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="Number of channels, associated bandwidth, and service type supported, Available for assignment in a particular area" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
								.ActiveX("SSTab").VbRadioButton("Number of channels, associated").Set
								SelectedReport="Yes"
						ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="Number of wireless stations operating in particular area" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
								Wait(3)
								.ActiveX("SSTab").VbRadioButton("Number of wireless stations_2").Set
								SelectedReport="Yes"
								Wait(3)
						ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="Complete details of all networks, irrespective of Band and location, of a  particular user" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
								.ActiveX("SSTab").VbRadioButton("Complete details of all").Set
								SelectedReport="Yes"
						ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="The number and types of equipment that has been Certified for operation" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
								.ActiveX("SSTab").VbRadioButton("The number and types of").Set
								SelectedReport="Yes"
						ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="Users by band report" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
								.ActiveX("SSTab").VbRadioButton("Users by band Report").Set @@ hightlight id_;_593078_;_script infofile_;_ZIP::ssf409.xml_;_
								SelectedReport="Yes"
						ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="Frequencies available in a particular area" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
								Wait(3)
								.ActiveX("SSTab").VbRadioButton("Frequencies available").Set @@ hightlight id_;_985108_;_script infofile_;_ZIP::ssf410.xml_;_
								SelectedReport="Yes"
						ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="Frequency Information on Microwave Links" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
								.ActiveX("SSTab").VbRadioButton("Frequency Information").Set
								SelectedReport="Yes"	
						ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="New frequency assignments made versus frequency Band and service over a defined period of time" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
								Wait(5)
								.ActiveX("SSTab").VbRadioButton("New frequency assignments").Set @@ hightlight id_;_723180_;_script infofile_;_ZIP::ssf839.xml_;_
								SelectedReport="Yes"
						End If
					ElseIf Datatable.Value("ReportTab","CustomReportsRemaining")="Complaints" and Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
						If ReportTabCount=0 Then
							ReportTabCount=1
							.Activate
							wait(3)
							VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("SSTab").VirtualObject("Complaints").Click 0,0
														
							'If InStr(Environment("Portuguese_Language"),proj)>0 Then
							'VbWindow("frmMDI").InsightObject("InsightObject_23").Click
'							If VbWindow("frmMDI").InsightObject("InsightObject_10").Exist(10) Then
'								VbWindow("frmMDI").InsightObject("InsightObject_10").Click
'								ElseIf VbWindow("frmMDI").InsightObject("InsightObject_27").Exist(10) Then @@ hightlight id_;_332168_;_script infofile_;_ZIP::ssf820.xml_;_
'									VbWindow("frmMDI").InsightObject("InsightObject_27").Click @@ hightlight id_;_3_;_script infofile_;_ZIP::ssf821.xml_;_
'								ElseIf VbWindow("frmMDI").InsightObject("InsightObject_35").Exist(10) Then
'									VbWindow("frmMDI").InsightObject("InsightObject_35").Click
'							End If
						End If
						wait(3)
						If Datatable.Value("ReportName","CustomReportsRemaining")="Number of Interface complaints cleared and pending in a particular area over a defined period of time" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
							.ActiveX("SSTab").VbRadioButton("Number of interference").Set
							SelectedReport="Yes"
						ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="Trend of complaints" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
								.ActiveX("SSTab").VbRadioButton("Trend of complaints").Set
								SelectedReport="Yes"
						ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="Number of days to resolve complaints over defined period of time" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
								.ActiveX("SSTab").VbRadioButton("Number of days to resolve").Set @@ hightlight id_;_464500_;_script infofile_;_ZIP::ssf870.xml_;_
								SelectedReport="Yes"
						ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="Aging of currently open complaints" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
								.ActiveX("SSTab").VbRadioButton("Aging of currently open").Set
								SelectedReport="Yes"
						End If
				End If

				'Input parameters
				If SelectedReport="Yes" and AssignInputs="No" Then

					If Datatable.Value("ReportTab","CustomReportsRemaining")="Wireless Station Analysis" Then

						If Datatable.Value("ReportName","CustomReportsRemaining")="Number of wireless stations operating in given frequency band" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then

							.ActiveX("FrequencyStart").Object.Text=""
							.ActiveX("FrequencyStart").Type Datatable.Value("FreqStart","CustomReportsRemaining") @@ hightlight id_;_267768_;_script infofile_;_ZIP::ssf516.xml_;_
							.ActiveX("FrequencyStart").Type  micTab @@ hightlight id_;_267768_;_script infofile_;_ZIP::ssf517.xml_;_
							.ActiveX("FrequencyEnd").Object.Text=""
							.ActiveX("FrequencyEnd").Type Datatable.Value("FreqEnd","CustomReportsRemaining") @@ hightlight id_;_1051982_;_script infofile_;_ZIP::ssf520.xml_;_
							.ActiveX("FrequencyEnd").Type  micTab @@ hightlight id_;_1051982_;_script infofile_;_ZIP::ssf521.xml_;_

						ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="Number of channels, associated bandwidth, and service type supported, Available for assignment in a particular area" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then

							.ActiveX("TCIComboBox.tcb").VbComboBox("cboCity").Select Datatable.Value("City","CustomReportsRemaining") '"Alto Molocue" @@ hightlight id_;_593422_;_script infofile_;_ZIP::ssf522.xml_;_
							.VbEdit("txtDistance").Set Datatable.Value("Distance","CustomReportsRemaining")
							.ActiveX("FrequencyStart").Object.Text=""
							.ActiveX("FrequencyStart").Type Datatable.Value("FreqStart","CustomReportsRemaining") @@ hightlight id_;_267768_;_script infofile_;_ZIP::ssf516.xml_;_
							.ActiveX("FrequencyEnd").Object.Text=""
							.ActiveX("FrequencyEnd").Type Datatable.Value("FreqEnd","CustomReportsRemaining") @@ hightlight id_;_1051982_;_script infofile_;_ZIP::ssf520.xml_;_
							'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_2").VbComboBox("cboServiceClassSingle").Select Datatable.Value("ServiceClass","CustomReportsRemaining") '"AERONÁUTICA MÓVEL"
							ASMSComboBox "cboServiceClassSingle",Datatable.Value("ServiceClass","CustomReportsRemaining") '"AERONÁUTICA MÓVEL"

						ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="Number of wireless stations operating in particular area" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then

							.ActiveX("TCIComboBox.tcb_3").VbComboBox("cboCity").Select Datatable.Value("City","CustomReportsRemaining") '"Alto Molocue" @@ hightlight id_;_593422_;_script infofile_;_ZIP::ssf532.xml_;_
							.VbEdit("txtDistance").Set Datatable.Value("Distance","CustomReportsRemaining") @@ hightlight id_;_724490_;_script infofile_;_ZIP::ssf534.xml_;_
							.VbEdit("txtDistance").Type  micTab 

						ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="Complete details of all networks, irrespective of Band and location, of a  particular user" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
								'ASMSComboBox "cboApplicantLinked",Datatable.Value("ApplicantName","CustomReportsRemaining") '"AABB TEl   (# MP9)"
								'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb").VbComboBox("cboApplicantLinked").Select Datatable.Value("ApplicantName","CustomReportsRemaining") '"AABB TEl   (# MP9)"
								'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_4").VbComboBox("cboDistrict").Select "*"
								'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_5").VbComboBox("cboCityOnly").Select "*"
								'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_6").VbComboBox("cboRegion").Select "*"

						ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="New frequency assignments made versus frequency Band and service over a defined period of time" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then

							 With VbWindow("text:=Automated Spectrum Management System","nativeclass:=ThunderRT6MDIForm")
								.ActiveX("acx_name:=MaskEdBox","nativeclass:=MSMaskWndClass","index:=0").Click
								For  count= 1 To Len(.ActiveX("acx_name:=MaskEdBox","nativeclass:=MSMaskWndClass","index:=0").GetROProperty("text")) Step 1
									.ActiveX("acx_name:=MaskEdBox","nativeclass:=MSMaskWndClass","index:=0").Type  micBack
									.ActiveX("acx_name:=MaskEdBox","nativeclass:=MSMaskWndClass","index:=0").Type  micDel
								Next
								Wait 2
								.ActiveX("acx_name:=MaskEdBox","nativeclass:=MSMaskWndClass","index:=0").Object.text =Datatable.Value("ReportingDateStart","CustomReportsRemaining") '"80"
								
								.ActiveX("acx_name:=MaskEdBox","nativeclass:=MSMaskWndClass","index:=1").Click
								For  count= 1 To Len(.ActiveX("acx_name:=MaskEdBox","nativeclass:=MSMaskWndClass","index:=1").GetROProperty("text")) Step 1
									.ActiveX("acx_name:=MaskEdBox","nativeclass:=MSMaskWndClass","index:=1").Type  micBack
									.ActiveX("acx_name:=MaskEdBox","nativeclass:=MSMaskWndClass","index:=1").Type  micDel
								Next
								Wait 2
								.ActiveX("acx_name:=MaskEdBox","nativeclass:=MSMaskWndClass","index:=1").Object.text = Datatable.Value("ReportingDateEnd","CustomReportsRemaining") '"100"
								
								.ActiveX("acx_name:=MaskEdBox","nativeclass:=MSMaskWndClass","index:=2").Click
								For  count= 1 To Len(.ActiveX("acx_name:=MaskEdBox","nativeclass:=MSMaskWndClass","index:=2").GetROProperty("text")) Step 1
									.ActiveX("acx_name:=MaskEdBox","nativeclass:=MSMaskWndClass","index:=2").Type  micBack
									.ActiveX("acx_name:=MaskEdBox","nativeclass:=MSMaskWndClass","index:=2").Type  micDel
								Next
								Wait 2
								.ActiveX("acx_name:=MaskEdBox","nativeclass:=MSMaskWndClass","index:=2").Object.text =Datatable.Value("FreqStart","CustomReportsRemaining") '"02022020"
								
								.ActiveX("acx_name:=MaskEdBox","nativeclass:=MSMaskWndClass","index:=3").Click
								For  count= 1 To Len(.ActiveX("acx_name:=MaskEdBox","nativeclass:=MSMaskWndClass","index:=3").GetROProperty("text")) Step 1
									.ActiveX("acx_name:=MaskEdBox","nativeclass:=MSMaskWndClass","index:=3").Type  micBack
									.ActiveX("acx_name:=MaskEdBox","nativeclass:=MSMaskWndClass","index:=3").Type  micDel
								Next
								Wait 2
								.ActiveX("acx_name:=MaskEdBox","nativeclass:=MSMaskWndClass","index:=3").Object.text = Datatable.Value("FreqEnd","CustomReportsRemaining") '"12312021"
							End With
							
							
''							'ActiveX("MaskEdBox_14").Drag 116,5 @@ hightlight id_;_1117170_;_script infofile_;_ZIP::ssf845.xml_;_
'							.VbFrame("panOption(1)").Drop 141,25 @@ hightlight id_;_854344_;_script infofile_;_ZIP::ssf846.xml_;_
'							.ActiveX("MaskEdBox_14").Type Datatable.Value("FreqStart","CustomReportsRemaining") '"80" @@ hightlight id_;_1117170_;_script infofile_;_ZIP::ssf847.xml_;_
'							.ActiveX("MaskEdBox_23").Drag 65,6 @@ hightlight id_;_919862_;_script infofile_;_ZIP::ssf848.xml_;_
'							.VbFrame("panOption(1)").Drop 493,32 @@ hightlight id_;_854344_;_script infofile_;_ZIP::ssf849.xml_;_
'							.ActiveX("MaskEdBox_23").Type Datatable.Value("FreqEnd","CustomReportsRemaining") '"100" @@ hightlight id_;_919862_;_script infofile_;_ZIP::ssf850.xml_;_
'							.ActiveX("MaskEdBox_24").Drag 76,12 @@ hightlight id_;_1182482_;_script infofile_;_ZIP::ssf851.xml_;_
'							.VbFrame("panOption(4)").Drop 293,32 @@ hightlight id_;_1313652_;_script infofile_;_ZIP::ssf852.xml_;_
'							.ActiveX("MaskEdBox_24").Type Datatable.Value("ReportingDateStart","CustomReportsRemaining") '"02022020" @@ hightlight id_;_1182482_;_script infofile_;_ZIP::ssf853.xml_;_
'							.ActiveX("MaskEdBox_25").Drag 78,10 @@ hightlight id_;_2756512_;_script infofile_;_ZIP::ssf854.xml_;_
'							.VbFrame("panOption(4)").Drop 450,34 @@ hightlight id_;_1313652_;_script infofile_;_ZIP::ssf855.xml_;_
'							.ActiveX("MaskEdBox_25").Type Datatable.Value("ReportingDateEnd","CustomReportsRemaining") '"12312021"
'							'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_3").VbComboBox("cboServiceClass").Select Datatable.Value("ServiceClass","CustomReportsRemaining") '"AERONAUTICAL MOBILE"
							Wait(3)
						ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="The number and types of equipment that has been Certified for operation" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then

							'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_3").VbComboBox("cboEquipmentType").Select "*"

							ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="Users by band report" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then

							.ActiveX("FrequencyStart").Object.Text=""
							.ActiveX("FrequencyStart").Type Datatable.Value("FreqStart","CustomReportsRemaining") @@ hightlight id_;_267768_;_script infofile_;_ZIP::ssf516.xml_;_
							.ActiveX("FrequencyStart").Type  micTab @@ hightlight id_;_267768_;_script infofile_;_ZIP::ssf517.xml_;_
							.ActiveX("FrequencyEnd").Object.Text=""
							.ActiveX("FrequencyEnd").Type Datatable.Value("FreqEnd","CustomReportsRemaining") @@ hightlight id_;_1051982_;_script infofile_;_ZIP::ssf520.xml_;_
							.ActiveX("FrequencyEnd").Type  micTab @@ hightlight id_;_2164456_;_script infofile_;_ZIP::ssf764.xml_;_
							'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_21").VbComboBox("cboServiceClass").Select "*"
							'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_21").VbComboBox("cboServiceClass").Type  micTab

						ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="Frequencies available in a particular area" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then

							.ActiveX("TCIComboBox.tcb").VbComboBox("cboCity").Select Datatable.Value("City","CustomReportsRemaining") '"Alto Molocue" @@ hightlight id_;_593422_;_script infofile_;_ZIP::ssf550.xml_;_
							.VbEdit("txtDistance").Set Datatable.Value("Distance","CustomReportsRemaining") @@ hightlight id_;_724490_;_script infofile_;_ZIP::ssf552.xml_;_
							
							With VbWindow("text:=Automated Spectrum Management System","nativeclass:=ThunderRT6MDIForm")
								.ActiveX("acx_name:=MaskEdBox","nativeclass:=MSMaskWndClass","index:=7").Click
								For  count= 1 To Len(.ActiveX("acx_name:=MaskEdBox","nativeclass:=MSMaskWndClass","index:=7").GetROProperty("text")) Step 1
									.ActiveX("acx_name:=MaskEdBox","nativeclass:=MSMaskWndClass","index:=7").Type  micBack
									.ActiveX("acx_name:=MaskEdBox","nativeclass:=MSMaskWndClass","index:=7").Type  micDel
								Next
								Wait 2
								.ActiveX("acx_name:=MaskEdBox","nativeclass:=MSMaskWndClass","index:=7").Object.text =Datatable.Value("FreqStart","CustomReportsRemaining") '"80"
								
								.ActiveX("acx_name:=MaskEdBox","nativeclass:=MSMaskWndClass","index:=6").Click
								For  count= 1 To Len(.ActiveX("acx_name:=MaskEdBox","nativeclass:=MSMaskWndClass","index:=6").GetROProperty("text")) Step 1
									.ActiveX("acx_name:=MaskEdBox","nativeclass:=MSMaskWndClass","index:=6").Type  micBack
									.ActiveX("acx_name:=MaskEdBox","nativeclass:=MSMaskWndClass","index:=6").Type  micDel
								Next
								Wait 2
								.ActiveX("acx_name:=MaskEdBox","nativeclass:=MSMaskWndClass","index:=6").Object.text = Datatable.Value("FreqEnd","CustomReportsRemaining") '"100"
							End  With
'							
'							.ActiveX("MaskEdBox_3").Object.Text=""
'							.ActiveX("MaskEdBox_3").Type Datatable.Value("FreqStart","CustomReportsRemaining") @@ hightlight id_;_267768_;_script infofile_;_ZIP::ssf555.xml_;_
'							.ActiveX("MaskEdBox_4").Object.Text=""
'							.ActiveX("MaskEdBox_4").Type Datatable.Value("FreqEnd","CustomReportsRemaining") @@ hightlight id_;_1051982_;_script infofile_;_ZIP::ssf558.xml_;_
'							
							.ActiveX("TCIComboBox.tcb_2").VbComboBox("cboServiceClassSingle").Select Datatable.Value("ServiceClass","CustomReportsRemaining") '"AERONÁUTICA MÓVEL" @@ hightlight id_;_394516_;_script infofile_;_ZIP::ssf559.xml_;_
							
							' index 0 to 5 - lat and long
							
							'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_5").Drag 23,5
							'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbFrame("panOption(22)").Drop 269,25
							'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_5").Object.Text=""
							'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_5").Type Datatable.Value("Lat1","CustomReportsRemaining")
							'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_6").Drag 26,3
							'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbFrame("panOption(22)").Drop 309,17
							'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_6").Object.Text=""
							'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_6").Type Datatable.Value("Lat2","CustomReportsRemaining")
							'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_7").Drag 13,9
							'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_6").Drop 49,11
							'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_7").Object.Text=""
							'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_7").Type Datatable.Value("Lat3","CustomReportsRemaining")
							'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_4").VbComboBox("cboLatHem").Select Datatable.Value("Lat4","CustomReportsRemaining")
							'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_8").Drag 19,14
							'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbFrame("panOption(22)").Drop 288,71
							'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_8").Object.Text=""
							'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_8").Type Datatable.Value("Long1","CustomReportsRemaining")
							'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_9").Drag 20,7
							'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbFrame("panOption(22)").Drop 362,64
							'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_9").Object.Text=""
							'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_9").Type Datatable.Value("Long2","CustomReportsRemaining")
							'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_10").Drag 23,9
							'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_9").Drop 6,5
							'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_10").Object.Text=""
							'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_10").Type Datatable.Value("Long3","CustomReportsRemaining")
							'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_5").VbComboBox("cboLongHem").Select Datatable.Value("Long4","CustomReportsRemaining")

							.VbEdit("txtDist").Set Datatable.Value("Distance","CustomReportsRemaining") @@ hightlight id_;_139410_;_script infofile_;_ZIP::ssf812.xml_;_

						ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="Frequency Information on Microwave Links" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
							'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("FrequencyStart").Drag 62,13
							'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbFrame("panOption(1)").Drop 105,39
							.ActiveX("FrequencyStart").Object.Text=""
							.ActiveX("FrequencyStart").Type Datatable.Value("FreqStart","CustomReportsRemaining") @@ hightlight id_;_267768_;_script infofile_;_ZIP::ssf516.xml_;_
							.ActiveX("FrequencyStart").Type  micTab @@ hightlight id_;_267768_;_script infofile_;_ZIP::ssf517.xml_;_
							'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("FrequencyEnd").Drag 30,10
							'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbFrame("panOption(1)").Drop 439,26
							.ActiveX("FrequencyEnd").Object.Text=""
							.ActiveX("FrequencyEnd").Type Datatable.Value("FreqEnd","CustomReportsRemaining") @@ hightlight id_;_1051982_;_script infofile_;_ZIP::ssf520.xml_;_
							.ActiveX("FrequencyEnd").Type  micTab 

							'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_7").VbComboBox("cboRegion").Select "*"
							'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb").VbComboBox("cboCity").Select Datatable.Value("City","CustomReportsRemaining") '"Alto Molocue"

							''						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_3").Drag 136,12
							''						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbFrame("panOption(1)").Drop 133,22
							'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_3").Object.Text=""
							'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_3").Type Datatable.Value("FreqStart","CustomReportsRemaining")
							''						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_4").Drag 53,10
							''						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbFrame("panOption(1)").Drop 434,30
							'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_4").Object.Text=""
							'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_4").Type Datatable.Value("FreqStart","CustomReportsRemaining")
							'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_2").VbComboBox("cboServiceClassSingle").Select Datatable.Value("ServiceClass","CustomReportsRemaining") '"AERONÁUTICA MÓVEL"

							''						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_5").Drag 23,5
							''						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbFrame("panOption(22)").Drop 269,25
							'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_5").Object.Text=""
							'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_5").Type Datatable.Value("Lat1","CustomReportsRemaining")
							''						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_6").Drag 26,3
							''						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbFrame("panOption(22)").Drop 309,17
							'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_6").Object.Text=""
							'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_6").Type Datatable.Value("Lat2","CustomReportsRemaining")
							''						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_7").Drag 13,9
							''						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_6").Drop 49,11
							'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_7").Object.Text=""
							'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_7").Type Datatable.Value("Lat3","CustomReportsRemaining")
							'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_4").VbComboBox("cboLatHem").Select Datatable.Value("Lat4","CustomReportsRemaining") '"S"
							'						
							''						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_8").Drag 19,14
							''						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbFrame("panOption(22)").Drop 288,71
							'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_8").Object.Text=""
							'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_8").Type Datatable.Value("Long1","CustomReportsRemaining")
							''						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_9").Drag 20,7
							''						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbFrame("panOption(22)").Drop 362,64
							'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_9").Object.Text=""
							'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_9").Type Datatable.Value("Long2","CustomReportsRemaining")
							''						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_10").Drag 23,9
							''						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_9").Drop 6,5
							'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_10").Object.Text=""
							'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_10").Type Datatable.Value("Long3","CustomReportsRemaining")
							'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_5").VbComboBox("cboLongHem").Select Datatable.Value("Long4","CustomReportsRemaining") '"E"
							'
							'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbEdit("txtDistance").SetSelection 0,2
							'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbEdit("txtDistance").Set Datatable.Value("Distance","CustomReportsRemaining")						
							.VbEdit("txtDist").Set Datatable.Value("Distance","CustomReportsRemaining")

							'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_4").VbComboBox("cboAppStatus").Select "*"

						End If
					ElseIf Datatable.Value("ReportTab","CustomReportsRemaining")="Complaints" and Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
						If Datatable.Value("ReportName","CustomReportsRemaining")="Number of Interface complaints cleared and pending in a particular area over a defined period of time" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then

								.ActiveX("ReportDateStart").Click 79,6
								.ActiveX("ReportDateStart").Drag 79,6 @@ hightlight id_;_264310_;_script infofile_;_ZIP::ssf784.xml_;_
								.VbFrame("panOption(4)").Drop 277,42 @@ hightlight id_;_264312_;_script infofile_;_ZIP::ssf785.xml_;_
								.ActiveX("ReportDateStart").Type Datatable.Value("ReportingDateStart","CustomReportsRemaining") @@ hightlight id_;_264310_;_script infofile_;_ZIP::ssf793.xml_;_
								.ActiveX("ReportDateStart").Type  micTab @@ hightlight id_;_264310_;_script infofile_;_ZIP::ssf794.xml_;_

								.ActiveX("ReportDateEnd").Click 80,7 @@ hightlight id_;_264348_;_script infofile_;_ZIP::ssf795.xml_;_
								.ActiveX("ReportDateEnd").Drag 80,7 @@ hightlight id_;_264348_;_script infofile_;_ZIP::ssf796.xml_;_
								.VbFrame("panOption(4)").Drop 453,32 @@ hightlight id_;_264312_;_script infofile_;_ZIP::ssf797.xml_;_
								.ActiveX("ReportDateEnd").Type Datatable.Value("ReportingDateEnd","CustomReportsRemaining") @@ hightlight id_;_264348_;_script infofile_;_ZIP::ssf798.xml_;_
								.ActiveX("ReportDateEnd").Type  micTab 

								'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbRadioButton("Região").Set
								'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_8").VbComboBox("cboArea(1)").Select "*"

								'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbRadioButton("Distrito").Set
								'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_9").VbComboBox("cboArea(0)").Select "*"
							ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="Trend of complaints" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
								
								With VbWindow("text:=Automated Spectrum Management System","nativeclass:=ThunderRT6MDIForm")
									.ActiveX("acx_name:=MaskEdBox","nativeclass:=MSMaskWndClass","index:=0").Click
									For  count= 1 To Len(.ActiveX("acx_name:=MaskEdBox","nativeclass:=MSMaskWndClass","index:=0").GetROProperty("text")) Step 1
										.ActiveX("acx_name:=MaskEdBox","nativeclass:=MSMaskWndClass","index:=0").Type  micBack
										.ActiveX("acx_name:=MaskEdBox","nativeclass:=MSMaskWndClass","index:=0").Type  micDel
									Next
									Wait 2
									.ActiveX("acx_name:=MaskEdBox","nativeclass:=MSMaskWndClass","index:=0").Object.text =Datatable.Value("ReportYear","CustomReportsRemaining")
																
								End With
								
'								.ActiveX("MaskEdBox_11").Drag 51,9 @@ hightlight id_;_787468_;_script infofile_;_ZIP::ssf595.xml_;_
'								.VbFrame("panOption(9)").Drop 273,26 @@ hightlight id_;_724500_;_script infofile_;_ZIP::ssf596.xml_;_
'								'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_11").Object.Text=""
'								.ActiveX("MaskEdBox_11").Type Datatable.Value("ReportYear","CustomReportsRemaining") '"2020" @@ hightlight id_;_787468_;_script infofile_;_ZIP::ssf597.xml_;_
'								.ActiveX("MaskEdBox_11").Type  micTab @@ hightlight id_;_787468_;_script infofile_;_ZIP::ssf598.xml_;_

							ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="Number of days to resolve complaints over defined period of time" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then

 								With VbWindow("text:=Automated Spectrum Management System","nativeclass:=ThunderRT6MDIForm")
									.ActiveX("acx_name:=MaskEdBox","nativeclass:=MSMaskWndClass","index:=0").Click
									For  count= 1 To Len(.ActiveX("acx_name:=MaskEdBox","nativeclass:=MSMaskWndClass","index:=0").GetROProperty("text")) Step 1
										.ActiveX("acx_name:=MaskEdBox","nativeclass:=MSMaskWndClass","index:=0").Type  micBack
										.ActiveX("acx_name:=MaskEdBox","nativeclass:=MSMaskWndClass","index:=0").Type  micDel
									Next
									Wait 2
									.ActiveX("acx_name:=MaskEdBox","nativeclass:=MSMaskWndClass","index:=0").Object.text =Datatable.Value("ReportingDateStart","CustomReportsRemaining") '"80"
									
									.ActiveX("acx_name:=MaskEdBox","nativeclass:=MSMaskWndClass","index:=1").Click
									For  count= 1 To Len(.ActiveX("acx_name:=MaskEdBox","nativeclass:=MSMaskWndClass","index:=1").GetROProperty("text")) Step 1
										.ActiveX("acx_name:=MaskEdBox","nativeclass:=MSMaskWndClass","index:=1").Type  micBack
										.ActiveX("acx_name:=MaskEdBox","nativeclass:=MSMaskWndClass","index:=1").Type  micDel
									Next
									Wait 2
									.ActiveX("acx_name:=MaskEdBox","nativeclass:=MSMaskWndClass","index:=1").Object.text = Datatable.Value("ReportingDateEnd","CustomReportsRemaining") '"100"
								
								End With
							
'								.ActiveX("MaskEdBox_14").Drag 80,8 @@ hightlight id_;_464814_;_script infofile_;_ZIP::ssf878.xml_;_
'								.VbFrame("panOption(4)").Drop 278,38 @@ hightlight id_;_530022_;_script infofile_;_ZIP::ssf879.xml_;_
'								.ActiveX("MaskEdBox_14").Type Datatable.Value("ReportingDateStart","CustomReportsRemaining") '"01012020" @@ hightlight id_;_464814_;_script infofile_;_ZIP::ssf880.xml_;_
'								.ActiveX("MaskEdBox_24").Drag 78,11 @@ hightlight id_;_595550_;_script infofile_;_ZIP::ssf881.xml_;_
'								.ActiveX("MaskEdBox_24").Drop 6,20 @@ hightlight id_;_595550_;_script infofile_;_ZIP::ssf882.xml_;_
'								.ActiveX("MaskEdBox_24").Type Datatable.Value("ReportingDateEnd","CustomReportsRemaining") '"12312020"

							End If

						ElseIf Datatable.Value("ReportTab","CustomReportsRemaining")="Application Analysis" and Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then

							If Datatable.Value("ReportName","CustomReportsRemaining")="Status of application for a specified customer" and Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
								'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_10").VbComboBox("cboApplicant").Select "*"
								'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_9").VbComboBox("cboAppStatus").Select "*"
								'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_9").VbComboBox("cboAppStatus").Type  micTab
							ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="Number of applications of a particular type and status over a defined period of time" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then

								.ActiveX("ReportDateStart").Click 79,6
								.ActiveX("ReportDateStart").Drag 79,6 @@ hightlight id_;_264310_;_script infofile_;_ZIP::ssf784.xml_;_
								.VbFrame("panOption(4)").Drop 277,42 @@ hightlight id_;_264312_;_script infofile_;_ZIP::ssf785.xml_;_
								'.ActiveX("ReportDateStart").Object.Text=""
								.ActiveX("ReportDateStart").Type Datatable.Value("ReportingDateStart","CustomReportsRemaining") @@ hightlight id_;_264310_;_script infofile_;_ZIP::ssf793.xml_;_
								.ActiveX("ReportDateStart").Type  micTab @@ hightlight id_;_264310_;_script infofile_;_ZIP::ssf794.xml_;_

								.ActiveX("ReportDateEnd").Click 80,7 @@ hightlight id_;_264348_;_script infofile_;_ZIP::ssf795.xml_;_
								.ActiveX("ReportDateEnd").Drag 80,7 @@ hightlight id_;_264348_;_script infofile_;_ZIP::ssf796.xml_;_
								.VbFrame("panOption(4)").Drop 453,32 @@ hightlight id_;_264312_;_script infofile_;_ZIP::ssf797.xml_;_
								.ActiveX("ReportDateEnd").Type Datatable.Value("ReportingDateEnd","CustomReportsRemaining") @@ hightlight id_;_264348_;_script infofile_;_ZIP::ssf798.xml_;_
								.ActiveX("ReportDateEnd").Type  micTab 

								'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_9").VbComboBox("cboAppType").Select "*"
								'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_11").VbComboBox("cboAppStatus").Select "*"
								'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_11").VbComboBox("cboAppStatus").Type  micTab
							ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="No. Of type approval appl submitted day, week and month" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then

								.ActiveX("ReportDateStart").Click 79,6
								.ActiveX("ReportDateStart").Drag 79,6 @@ hightlight id_;_264310_;_script infofile_;_ZIP::ssf784.xml_;_
								.VbFrame("panOption(4)").Drop 277,42 @@ hightlight id_;_264312_;_script infofile_;_ZIP::ssf785.xml_;_
								.ActiveX("ReportDateStart").Type Datatable.Value("ReportingDateStart","CustomReportsRemaining") @@ hightlight id_;_264310_;_script infofile_;_ZIP::ssf793.xml_;_
								.ActiveX("ReportDateStart").Type  micTab @@ hightlight id_;_264310_;_script infofile_;_ZIP::ssf794.xml_;_

								.ActiveX("ReportDateEnd").Click 80,7 @@ hightlight id_;_264348_;_script infofile_;_ZIP::ssf795.xml_;_
								.ActiveX("ReportDateEnd").Drag 80,7 @@ hightlight id_;_264348_;_script infofile_;_ZIP::ssf796.xml_;_
								.VbFrame("panOption(4)").Drop 453,32 @@ hightlight id_;_264312_;_script infofile_;_ZIP::ssf797.xml_;_
								.ActiveX("ReportDateEnd").Type Datatable.Value("ReportingDateEnd","CustomReportsRemaining") @@ hightlight id_;_264348_;_script infofile_;_ZIP::ssf798.xml_;_
								.ActiveX("ReportDateEnd").Type  micTab 

								'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_9").VbComboBox("cboAppStatus").Select "*"
								'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_9").VbComboBox("cboAppStatus").Type  micTab
							End If
						ElseIf Datatable.Value("ReportTab","CustomReportsRemaining")="Licence Analysis" and Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
							If Datatable.Value("ReportName","CustomReportsRemaining")="Applications ready to be licensed" and Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then


								'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_10").VbComboBox("cboAppType").Select "*"
								'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_10").VbComboBox("cboAppType").Type  micTab
							ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="Licenses status over a defined period of time" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then

								'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_10").WinEdit("Edit").SetSelection 0,1
								'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_10").VbComboBox("cboApplicant").Select "*"

								.ActiveX("ReportDateStart").Click 79,6
								.ActiveX("ReportDateStart").Drag 79,6 @@ hightlight id_;_264310_;_script infofile_;_ZIP::ssf784.xml_;_
								.VbFrame("panOption(4)").Drop 277,42 @@ hightlight id_;_264312_;_script infofile_;_ZIP::ssf785.xml_;_
								.ActiveX("ReportDateStart").Type Datatable.Value("ReportingDateStart","CustomReportsRemaining") @@ hightlight id_;_264310_;_script infofile_;_ZIP::ssf793.xml_;_
								.ActiveX("ReportDateStart").Type  micTab @@ hightlight id_;_264310_;_script infofile_;_ZIP::ssf794.xml_;_

								.ActiveX("ReportDateEnd").Click 80,7 @@ hightlight id_;_264348_;_script infofile_;_ZIP::ssf795.xml_;_
								.ActiveX("ReportDateEnd").Drag 80,7 @@ hightlight id_;_264348_;_script infofile_;_ZIP::ssf796.xml_;_
								.VbFrame("panOption(4)").Drop 453,32 @@ hightlight id_;_264312_;_script infofile_;_ZIP::ssf797.xml_;_
								.ActiveX("ReportDateEnd").Type Datatable.Value("ReportingDateEnd","CustomReportsRemaining") @@ hightlight id_;_264348_;_script infofile_;_ZIP::ssf798.xml_;_
								.ActiveX("ReportDateEnd").Type  micTab 

								'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_11").VbComboBox("cboAppStatus").Select "*"
								'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_12").VbComboBox("cboAppType").Select "*"
								'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_12").VbComboBox("cboAppType").Type  micTab
							ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="Licenses status over a defined period of time with customers contact information" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then

								'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_10").WinEdit("Edit").SetSelection 0,1
								'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_10").VbComboBox("cboApplicant").Select "*"

								.ActiveX("ReportDateStart").Click 79,6
								.ActiveX("ReportDateStart").Drag 79,6 @@ hightlight id_;_264310_;_script infofile_;_ZIP::ssf784.xml_;_
								.VbFrame("panOption(4)").Drop 277,42 @@ hightlight id_;_264312_;_script infofile_;_ZIP::ssf785.xml_;_
								.ActiveX("ReportDateStart").Type Datatable.Value("ReportingDateStart","CustomReportsRemaining") @@ hightlight id_;_264310_;_script infofile_;_ZIP::ssf793.xml_;_
								.ActiveX("ReportDateStart").Type  micTab @@ hightlight id_;_264310_;_script infofile_;_ZIP::ssf794.xml_;_

								.ActiveX("ReportDateEnd").Click 80,7 @@ hightlight id_;_264348_;_script infofile_;_ZIP::ssf795.xml_;_
								.ActiveX("ReportDateEnd").Drag 80,7 @@ hightlight id_;_264348_;_script infofile_;_ZIP::ssf796.xml_;_
								.VbFrame("panOption(4)").Drop 453,32 @@ hightlight id_;_264312_;_script infofile_;_ZIP::ssf797.xml_;_
								.ActiveX("ReportDateEnd").Type Datatable.Value("ReportingDateEnd","CustomReportsRemaining") @@ hightlight id_;_264348_;_script infofile_;_ZIP::ssf798.xml_;_
								.ActiveX("ReportDateEnd").Type  micTab 

								'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_11").VbComboBox("cboAppType").Select "*"
								'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_12").VbComboBox("cboAppStatus").Select "*"
								'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_12").VbComboBox("cboAppStatus").Type  micTab
							ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="Systems" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then

								'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_10").VbComboBox("cboApplicant").Select "*"
							End If
						ElseIf Datatable.Value("ReportTab","CustomReportsRemaining")="Financial 1" and Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
							If Datatable.Value("ReportName","CustomReportsRemaining")="List of Customers" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then

								'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_13").VbComboBox("cboApplicant").Select "*"
								'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_14").VbComboBox("cboLegalType").Select "*"
								'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbCheckBox("Sinalizado").Set "ON"
								'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_15").VbComboBox("cboCityOnly").Select "*"
								'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_16").VbComboBox("cboAppType").Select "*"
								'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_17").VbComboBox("cboServiceType").Select "*"
								If Datatable.Value("Client","CustomReportsRemaining")="MOZ" Then
									.VbCheckBox("Sistema (s)").Set "ON" @@ hightlight id_;_852336_;_script infofile_;_ZIP::ssf653.xml_;_
								End If
								.VbCheckBox("Factura").Set "ON" @@ hightlight id_;_263430_;_script infofile_;_ZIP::ssf654.xml_;_
							ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="Account Statement" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then

								'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_13").VbComboBox("cboApplicant").Select "*"
								'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbRadioButton("Não").Set
								.VbRadioButton("Sim").Set @@ hightlight id_;_1902346_;_script infofile_;_ZIP::ssf657.xml_;_
								.VbCheckBox("incluir todos os aliases").Set "ON" @@ hightlight id_;_4786596_;_script infofile_;_ZIP::ssf658.xml_;_

								.ActiveX("ReportDateStart").Click 79,6
								.ActiveX("ReportDateStart").Drag 79,6 @@ hightlight id_;_264310_;_script infofile_;_ZIP::ssf784.xml_;_
								.VbFrame("panOption(4)").Drop 277,42 @@ hightlight id_;_264312_;_script infofile_;_ZIP::ssf785.xml_;_
								.ActiveX("ReportDateStart").Type Datatable.Value("ReportingDateStart","CustomReportsRemaining") @@ hightlight id_;_264310_;_script infofile_;_ZIP::ssf793.xml_;_
								.ActiveX("ReportDateStart").Type  micTab @@ hightlight id_;_264310_;_script infofile_;_ZIP::ssf794.xml_;_

								.ActiveX("ReportDateEnd").Click 80,7 @@ hightlight id_;_264348_;_script infofile_;_ZIP::ssf795.xml_;_
								.ActiveX("ReportDateEnd").Drag 80,7 @@ hightlight id_;_264348_;_script infofile_;_ZIP::ssf796.xml_;_
								.VbFrame("panOption(4)").Drop 453,32 @@ hightlight id_;_264312_;_script infofile_;_ZIP::ssf797.xml_;_
								.ActiveX("ReportDateEnd").Type Datatable.Value("ReportingDateEnd","CustomReportsRemaining") @@ hightlight id_;_264348_;_script infofile_;_ZIP::ssf798.xml_;_
								.ActiveX("ReportDateEnd").Type  micTab 

								'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_16").VbComboBox("cboServiceType").Select "*"
							ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="Annual Billing" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then

								With VbWindow("text:=Automated Spectrum Management System","nativeclass:=ThunderRT6MDIForm")
									.ActiveX("acx_name:=MaskEdBox","nativeclass:=MSMaskWndClass","index:=0").Click
									For  count= 1 To Len(.ActiveX("acx_name:=MaskEdBox","nativeclass:=MSMaskWndClass","index:=0").GetROProperty("text")) Step 1
										.ActiveX("acx_name:=MaskEdBox","nativeclass:=MSMaskWndClass","index:=0").Type  micBack
										.ActiveX("acx_name:=MaskEdBox","nativeclass:=MSMaskWndClass","index:=0").Type  micDel
									Next
									Wait 2
									.ActiveX("acx_name:=MaskEdBox","nativeclass:=MSMaskWndClass","index:=0").Object.text =Datatable.Value("ReportYear","CustomReportsRemaining") '"80"
								End With
								
'								.ActiveX("MaskEdBox_17").Drag 43,8
'								.VbFrame("panOption(9)").Drop 287,29 @@ hightlight id_;_724500_;_script infofile_;_ZIP::ssf667.xml_;_
'								'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_17").Object.Text=""
'								.ActiveX("MaskEdBox_17").Type Datatable.Value("ReportYear","CustomReportsRemaining")'"2020" @@ hightlight id_;_787468_;_script infofile_;_ZIP::ssf668.xml_;_
								'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_14").VbComboBox("cboLegalType").Select "*"
								'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbCheckBox("Sinalizado").Set "OFF"
								.VbCheckBox("Sinalizado").Set "ON" @@ hightlight id_;_395344_;_script infofile_;_ZIP::ssf671.xml_;_
								'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbRadioButton("Não").Set
								.VbRadioButton("Sim").Set @@ hightlight id_;_1902346_;_script infofile_;_ZIP::ssf673.xml_;_
								'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbCheckBox("incluir todos os aliases").Set "OFF"
								.VbCheckBox("incluir todos os aliases").Set "ON" @@ hightlight id_;_4786596_;_script infofile_;_ZIP::ssf675.xml_;_
							ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="Fee collected for a particular type of service over a defined period of time" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then

								With VbWindow("text:=Automated Spectrum Management System","nativeclass:=ThunderRT6MDIForm")
									.ActiveX("acx_name:=MaskEdBox","nativeclass:=MSMaskWndClass","index:=0").Click
									For  count= 1 To Len(.ActiveX("acx_name:=MaskEdBox","nativeclass:=MSMaskWndClass","index:=0").GetROProperty("text")) Step 1
										.ActiveX("acx_name:=MaskEdBox","nativeclass:=MSMaskWndClass","index:=0").Type  micBack
										.ActiveX("acx_name:=MaskEdBox","nativeclass:=MSMaskWndClass","index:=0").Type  micDel
									Next
									Wait 2
									.ActiveX("acx_name:=MaskEdBox","nativeclass:=MSMaskWndClass","index:=0").Object.text =Datatable.Value("ReportYear","CustomReportsRemaining") '"80"
								End With
								
'								.ActiveX("MaskEdBox_17").Drag 52,14 @@ hightlight id_;_787468_;_script infofile_;_ZIP::ssf676.xml_;_
'								.VbFrame("panOption(9)").Drop 281,26 @@ hightlight id_;_724500_;_script infofile_;_ZIP::ssf677.xml_;_
'								'	VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_17").Object.Text=""
'								.ActiveX("MaskEdBox_17").Type Datatable.Value("ReportYear","CustomReportsRemaining")'"2020" @@ hightlight id_;_787468_;_script infofile_;_ZIP::ssf678.xml_;_
								'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_14").VbComboBox("cboLegalType").Select "*"
								'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbCheckBox("Sinalizado").Set "OFF"
								.VbCheckBox("Sinalizado").Set "ON" @@ hightlight id_;_395344_;_script infofile_;_ZIP::ssf681.xml_;_
								'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbRadioButton("Não").Set
								.VbRadioButton("Sim").Set @@ hightlight id_;_1902346_;_script infofile_;_ZIP::ssf683.xml_;_
								'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbCheckBox("incluir todos os aliases").Set "OFF"
								.VbCheckBox("incluir todos os aliases").Set "ON" @@ hightlight id_;_4786596_;_script infofile_;_ZIP::ssf685.xml_;_
							ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="License fee calculated and collected over a defined period of time" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then

								.ActiveX("ReportDateStart").Click 79,6
								.ActiveX("ReportDateStart").Drag 79,6 @@ hightlight id_;_264310_;_script infofile_;_ZIP::ssf784.xml_;_
								.VbFrame("panOption(4)").Drop 277,42 @@ hightlight id_;_264312_;_script infofile_;_ZIP::ssf785.xml_;_
								.ActiveX("ReportDateStart").Type Datatable.Value("ReportingDateStart","CustomReportsRemaining") @@ hightlight id_;_264310_;_script infofile_;_ZIP::ssf793.xml_;_
								.ActiveX("ReportDateStart").Type  micTab @@ hightlight id_;_264310_;_script infofile_;_ZIP::ssf794.xml_;_

								.ActiveX("ReportDateEnd").Click 80,7 @@ hightlight id_;_264348_;_script infofile_;_ZIP::ssf795.xml_;_
								.ActiveX("ReportDateEnd").Drag 80,7 @@ hightlight id_;_264348_;_script infofile_;_ZIP::ssf796.xml_;_
								.VbFrame("panOption(4)").Drop 453,32 @@ hightlight id_;_264312_;_script infofile_;_ZIP::ssf797.xml_;_
								.ActiveX("ReportDateEnd").Type Datatable.Value("ReportingDateEnd","CustomReportsRemaining") @@ hightlight id_;_264348_;_script infofile_;_ZIP::ssf798.xml_;_
								.ActiveX("ReportDateEnd").Type  micTab 

								'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbRadioButton("Não").Set
								.VbRadioButton("Sim").Set @@ hightlight id_;_1902346_;_script infofile_;_ZIP::ssf694.xml_;_
								'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbCheckBox("incluir todos os aliases").Set "OFF"
								.VbCheckBox("incluir todos os aliases").Set "ON" @@ hightlight id_;_4786596_;_script infofile_;_ZIP::ssf696.xml_;_
							End If
						ElseIf Datatable.Value("ReportTab","CustomReportsRemaining")="Financial 2" and Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
							If Datatable.Value("ReportName","CustomReportsRemaining")="Journal Report" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then

								.ActiveX("ReportDateStart").Click 79,6
								.ActiveX("ReportDateStart").Drag 79,6 @@ hightlight id_;_264310_;_script infofile_;_ZIP::ssf784.xml_;_
								.VbFrame("panOption(4)").Drop 277,42 @@ hightlight id_;_264312_;_script infofile_;_ZIP::ssf785.xml_;_
								.ActiveX("ReportDateStart").Type Datatable.Value("ReportingDateStart","CustomReportsRemaining") @@ hightlight id_;_264310_;_script infofile_;_ZIP::ssf793.xml_;_
								.ActiveX("ReportDateStart").Type  micTab @@ hightlight id_;_264310_;_script infofile_;_ZIP::ssf794.xml_;_

								.ActiveX("ReportDateEnd").Click 80,7 @@ hightlight id_;_264348_;_script infofile_;_ZIP::ssf795.xml_;_
								.ActiveX("ReportDateEnd").Drag 80,7 @@ hightlight id_;_264348_;_script infofile_;_ZIP::ssf796.xml_;_
								.VbFrame("panOption(4)").Drop 453,32 @@ hightlight id_;_264312_;_script infofile_;_ZIP::ssf797.xml_;_
								.ActiveX("ReportDateEnd").Type Datatable.Value("ReportingDateEnd","CustomReportsRemaining") @@ hightlight id_;_264348_;_script infofile_;_ZIP::ssf798.xml_;_
								.ActiveX("ReportDateEnd").Type  micTab 

							ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="Payment Gateway Response" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then

								 With VbWindow("text:=Automated Spectrum Management System","nativeclass:=ThunderRT6MDIForm")
									.ActiveX("acx_name:=MaskEdBox","nativeclass:=MSMaskWndClass","index:=0").Click
									For  count= 1 To Len(.ActiveX("acx_name:=MaskEdBox","nativeclass:=MSMaskWndClass","index:=0").GetROProperty("text")) Step 1
										.ActiveX("acx_name:=MaskEdBox","nativeclass:=MSMaskWndClass","index:=0").Type  micBack
										.ActiveX("acx_name:=MaskEdBox","nativeclass:=MSMaskWndClass","index:=0").Type  micDel
									Next
									Wait 2
									.ActiveX("acx_name:=MaskEdBox","nativeclass:=MSMaskWndClass","index:=0").Object.text =Datatable.Value("ReportingDateStart","CustomReportsRemaining") '"80"
								
									.ActiveX("acx_name:=MaskEdBox","nativeclass:=MSMaskWndClass","index:=1").Click
									For  count= 1 To Len(.ActiveX("acx_name:=MaskEdBox","nativeclass:=MSMaskWndClass","index:=1").GetROProperty("text")) Step 1
										.ActiveX("acx_name:=MaskEdBox","nativeclass:=MSMaskWndClass","index:=1").Type  micBack
										.ActiveX("acx_name:=MaskEdBox","nativeclass:=MSMaskWndClass","index:=1").Type  micDel
									Next
									Wait 2
									.ActiveX("acx_name:=MaskEdBox","nativeclass:=MSMaskWndClass","index:=1").Object.text = Datatable.Value("ReportingDateEnd","CustomReportsRemaining") '"100"
									
								End With
							
'								.ActiveX("MaskEdBox_14").Drag 79,10 @@ hightlight id_;_67564_;_script infofile_;_ZIP::ssf863.xml_;_
'								.VbFrame("panOption(4)").Drop 282,44 @@ hightlight id_;_67562_;_script infofile_;_ZIP::ssf864.xml_;_
'								.ActiveX("MaskEdBox_14").Type Datatable.Value("ReportingDateStart","CustomReportsRemaining") '"02022020" @@ hightlight id_;_67564_;_script infofile_;_ZIP::ssf865.xml_;_
'								.ActiveX("MaskEdBox_24").Drag 75,8 @@ hightlight id_;_67566_;_script infofile_;_ZIP::ssf866.xml_;_
'								.VbFrame("panOption(4)").Drop 450,57 @@ hightlight id_;_67562_;_script infofile_;_ZIP::ssf867.xml_;_
'								.ActiveX("MaskEdBox_24").Type Datatable.Value("ReportingDateEnd","CustomReportsRemaining") '"12312020" @@ hightlight id_;_67566_;_script infofile_;_ZIP::ssf868.xml_;_
								.ActiveX("TCIComboBox.tcb_9").VbComboBox("cboApplicant").Select Datatable.Value("ApplicantName","CustomReportsRemaining") '"Ray320   (# 5929)" @@ hightlight id_;_67824_;_script infofile_;_ZIP::ssf869.xml_;_

							End If
						ElseIf Datatable.Value("ReportTab","CustomReportsRemaining")="Others" and Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
							If Datatable.Value("ReportName","CustomReportsRemaining")="Customer Transactions" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then

								'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_10").VbComboBox("cboApplicant").Select "*"

								.ActiveX("ReportDateStart").Click 79,6
								.ActiveX("ReportDateStart").Drag 79,6 @@ hightlight id_;_264310_;_script infofile_;_ZIP::ssf784.xml_;_
								.VbFrame("panOption(4)").Drop 277,42 @@ hightlight id_;_264312_;_script infofile_;_ZIP::ssf785.xml_;_
								.ActiveX("ReportDateStart").Type Datatable.Value("ReportingDateStart","CustomReportsRemaining") @@ hightlight id_;_264310_;_script infofile_;_ZIP::ssf793.xml_;_
								.ActiveX("ReportDateStart").Type  micTab @@ hightlight id_;_264310_;_script infofile_;_ZIP::ssf794.xml_;_

								.ActiveX("ReportDateEnd").Click 80,7 @@ hightlight id_;_264348_;_script infofile_;_ZIP::ssf795.xml_;_
								.ActiveX("ReportDateEnd").Drag 80,7 @@ hightlight id_;_264348_;_script infofile_;_ZIP::ssf796.xml_;_
								.VbFrame("panOption(4)").Drop 453,32 @@ hightlight id_;_264312_;_script infofile_;_ZIP::ssf797.xml_;_
								.ActiveX("ReportDateEnd").Type Datatable.Value("ReportingDateEnd","CustomReportsRemaining") @@ hightlight id_;_264348_;_script infofile_;_ZIP::ssf798.xml_;_
								.ActiveX("ReportDateEnd").Type  micTab 

								.VbCheckBox("Factura_2").Set "ON" @@ hightlight id_;_1052048_;_script infofile_;_ZIP::ssf713.xml_;_
								.VbCheckBox("Nota De Débito").Set "OFF" @@ hightlight id_;_920960_;_script infofile_;_ZIP::ssf714.xml_;_
								.VbCheckBox("Nota de Crédito").Set "OFF" @@ hightlight id_;_855068_;_script infofile_;_ZIP::ssf715.xml_;_
								.VbCheckBox("Pagamento").Set "OFF" @@ hightlight id_;_1902114_;_script infofile_;_ZIP::ssf716.xml_;_
								.VbCheckBox("Pagamentos Antecipados").Set "OFF" @@ hightlight id_;_920350_;_script infofile_;_ZIP::ssf717.xml_;_

								'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbCheckBox("Pagamentos Antecipados").Set "ON"
								'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbCheckBox("Pagamento").Set "ON"
								'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbCheckBox("Nota de Crédito").Set "ON"
								'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbCheckBox("Nota De Débito").Set "ON"
								'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbCheckBox("Factura_2").Set "ON"

								.VbCheckBox("ThunderRT6CheckBox").Set "OFF" @@ hightlight id_;_525496_;_script infofile_;_ZIP::ssf723.xml_;_
								'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbCheckBox("ThunderRT6CheckBox").Set "ON"
							ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="Payments Received" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then

								.ActiveX("ReportDateStart").Click 79,6
								.ActiveX("ReportDateStart").Drag 79,6 @@ hightlight id_;_264310_;_script infofile_;_ZIP::ssf784.xml_;_
								.VbFrame("panOption(4)").Drop 277,42 @@ hightlight id_;_264312_;_script infofile_;_ZIP::ssf785.xml_;_
								.ActiveX("ReportDateStart").Type Datatable.Value("ReportingDateStart","CustomReportsRemaining") @@ hightlight id_;_264310_;_script infofile_;_ZIP::ssf793.xml_;_
								.ActiveX("ReportDateStart").Type  micTab @@ hightlight id_;_264310_;_script infofile_;_ZIP::ssf794.xml_;_

								.ActiveX("ReportDateEnd").Click 80,7 @@ hightlight id_;_264348_;_script infofile_;_ZIP::ssf795.xml_;_
								.ActiveX("ReportDateEnd").Drag 80,7 @@ hightlight id_;_264348_;_script infofile_;_ZIP::ssf796.xml_;_
								.VbFrame("panOption(4)").Drop 453,32 @@ hightlight id_;_264312_;_script infofile_;_ZIP::ssf797.xml_;_
								.ActiveX("ReportDateEnd").Type Datatable.Value("ReportingDateEnd","CustomReportsRemaining") @@ hightlight id_;_264348_;_script infofile_;_ZIP::ssf798.xml_;_
								.ActiveX("ReportDateEnd").Type  micTab 

								'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_9").VbComboBox("cbopmttype").Select "*"
								'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_9").VbComboBox("cbopmttype").Type  micTab
							ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="Forecast of fee collection over a defined period of time" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then

									'					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_10").VbComboBox("cboMonth(0)").Select Datatable.Value("StartDate-Month","CustomReportsRemaining")'"Janeiro"
									''					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_19").Drag 50,3
									''					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_10").VbComboBox("cboMonth(0)").Drop 168,5
									'					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_19").Object.Text=""
									'					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_19").Type Datatable.Value("StartDate-Year","CustomReportsRemaining")'"2020"
									'					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_18").VbComboBox("cboMonth(1)").Select Datatable.Value("EndDate-Month","CustomReportsRemaining")'"Dezembro"
									''					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_20").Drag 49,5
									''					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_18").VbComboBox("cboMonth(1)").Drop 157,7
									'					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_20").Object.Text=""
									'					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_20").Type Datatable.Value("EndDate-Year","CustomReportsRemaining")'"2021"
									'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_19").VbComboBox("cboLegalType").Select "*"
									.VbCheckBox("Sinalizado").Set "OFF" @@ hightlight id_;_395344_;_script infofile_;_ZIP::ssf742.xml_;_
									'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbCheckBox("Sinalizado").Set "ON"
									'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_20").VbComboBox("cboAppType").Select "*"
							ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="Customer License Types" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then

									.ActiveX("ReportDateStart").Click 79,6
									.ActiveX("ReportDateStart").Drag 79,6 @@ hightlight id_;_264310_;_script infofile_;_ZIP::ssf784.xml_;_
									.VbFrame("panOption(4)").Drop 277,42 @@ hightlight id_;_264312_;_script infofile_;_ZIP::ssf785.xml_;_
									.ActiveX("ReportDateStart").Type Datatable.Value("ReportingDateStart","CustomReportsRemaining") @@ hightlight id_;_264310_;_script infofile_;_ZIP::ssf793.xml_;_
									.ActiveX("ReportDateStart").Type  micTab @@ hightlight id_;_264310_;_script infofile_;_ZIP::ssf794.xml_;_

									.ActiveX("ReportDateEnd").Click 80,7 @@ hightlight id_;_264348_;_script infofile_;_ZIP::ssf795.xml_;_
									.ActiveX("ReportDateEnd").Drag 80,7 @@ hightlight id_;_264348_;_script infofile_;_ZIP::ssf796.xml_;_
									.VbFrame("panOption(4)").Drop 453,32 @@ hightlight id_;_264312_;_script infofile_;_ZIP::ssf797.xml_;_
									.ActiveX("ReportDateEnd").Type Datatable.Value("ReportingDateEnd","CustomReportsRemaining") @@ hightlight id_;_264348_;_script infofile_;_ZIP::ssf798.xml_;_
									.ActiveX("ReportDateEnd").Type  micTab 

									'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_9").VbComboBox("cboApplicant").Select "*"
									'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_11").VbComboBox("cboAppType").Select "*"
									'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_12").VbComboBox("cboAppStatus").Select "*"
							End If

					End If 

					AssignInputs="Yes"
				End if
			End With

			wait(3)
				
			If SelectedReport="Yes" Then
			
			'VbWindow("frmMDI").WinObject("MDIClient").VScroll micSetPos, 10
			
			VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbButton("View").Click @@ hightlight id_;_69160_;_script infofile_;_ZIP::ssf7.xml_;_
			
			If VbWindow("frmReportViewer").Dialog("Crystal Report Viewer").WinButton("OK").Exist(10) Then
					With	VbWindow("frmReportViewer")
						.Dialog("Crystal Report Viewer").WinButton("OK").Click @@ hightlight id_;_1772388_;_script infofile_;_ZIP::ssf353.xml_;_
						wait(2)
						.VbButton("Close").Click
					End With
					Else
						Set fso = CreateObject("Scripting.FileSystemObject")

						file_location = ProjectFolderPath &"\Desktop_Reports\"& GetCurrentDate

						If not fso.FolderExists(file_location) Then
							fso.CreateFolder file_location  
						End If
						wait(5)

						If VbWindow("frmMDI").Dialog("#32770").WinButton("OK").Exist(5) Then
							VbWindow("frmMDI").Dialog("#32770").WinButton("OK").Click
							Else

								If Datatable.Value("ReportName","CustomReportsRemaining")="Forecast of fee collection over a defined period of time" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then

									filename=file_location & "\" & Datatable.Value("ReportName","CustomReportsRemaining")

									While fso.FileExists(filename&".pdf")
										filename=file_location & "\" & Datatable.Value("ReportName","CustomReportsRemaining")&"_"&fnRandomNumber(2)
									Wend

									'===2016 Excel

									With UIAWindow("rptFeeForecast3  [Compatibilit")
										While Not .UIAButton("File Tab").Exist(30)
										Wend
										.UIAButton("File Tab").Click
										Wait(2)
										.UIAList("File").UIAObject("Print").Select
									End With
									Wait(5)

									With Window("Excel")
										While Not .WinObject("WinObject").WinButton("Print").Exist(10)
										Wend
										.WinObject("WinObject").WinButton("Print").Click
									End With
									Wait(6)
									With	VbWindow("frmPrintConfig")
										.ActiveX("TimoSoft TabStrip Control").ActiveX("TimoSoft Frame Control").ActiveX("TimoSoft TextBox Control").Object.Text=""
										.ActiveX("TimoSoft TabStrip Control").ActiveX("TimoSoft Frame Control").ActiveX("TimoSoft TextBox Control").Type filename &".pdf" @@ hightlight id_;_134420_;_script infofile_;_ZIP::ssf349.xml_;_
										.ActiveX("TimoSoft TabStrip Control").ActiveX("TimoSoft Frame Control").ActiveX("TimoSoft CheckBox Control").Click 16,7 @@ hightlight id_;_134412_;_script infofile_;_ZIP::ssf350.xml_;_
										.ActiveX("TimoSoft CommandButton").Click 39,6
									End With
									' Window("Excel").Window("Printing").Dialog("Save Print Output As").WinEdit("File name:").Set filename &".pdf"'"qwerty"
									' Wait(3) 
									' Window("Excel").Window("Printing").Dialog("Save Print Output As").WinButton("Save").Click
									Wait(5)
									With	Window("Window")
										.Close
										Wait(3)
										.Window("Microsoft Excel").WinObject("Microsoft Excel").WinButton("Don't Save").Click @@ hightlight id_;_2026992048_;_script infofile_;_ZIP::ssf835.xml_;_
									End With


									'===2007 Excel
									'Window("rptFeeForecast1  [Compatibilit").WinObject("Ribbon").WinButton("Office Button").Click
									'Window("rptFeeForecast1  [Compatibilit").Window("Window").WinObject("WinObject").WinButton("Print").Click
									'Window("rptFeeForecast1  [Compatibilit").Window("Print").Click 441,370
									'VbWindow("frmPrintConfig").ActiveX("TimoSoft TabStrip Control_2").ActiveX("TimoSoft Frame Control").ActiveX("TimoSoft TextBox Control").Click 347,6
									'VbWindow("frmPrintConfig").ActiveX("TimoSoft TabStrip Control_2").ActiveX("TimoSoft Frame Control").ActiveX("TimoSoft TextBox Control").Type filename &".pdf"
									'VbWindow("frmPrintConfig").ActiveX("TimoSoft TabStrip Control_2").ActiveX("TimoSoft Frame Control").ActiveX("TimoSoft CheckBox Control").Click 13,10
									'VbWindow("frmPrintConfig").ActiveX("TimoSoft CommandButton_2").Click 53,10
									'Window("rptFeeForecast1  [Compatibilit").Close
									'==
									Else

										With VbWindow("frmReportViewer")
											While Not .Exist
											Wend
											wait(2)

											If Datatable.Value("ReportName","CustomReportsRemaining")="Annual Billing" Then
												Wait(6)
												ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="Account Statement" Then
													Wait(25)
												ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="List of Customers" Then
													Wait(7)
											End If

											If Datatable.Value("ReportName","CustomReportsRemaining")="List of Customers" Then
												Wait(100) '670
											ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="License fee calculated and collected over a defined period of time" Then
													Wait(50) '290
											End If

											If Datatable.Value("ReportName","CustomReportsRemaining")="The number and types of equipment that has been Certified for operation" Then
												Wait(80) '446
											End If

											If Datatable.Value("ReportName","CustomReportsRemaining")="Number of wireless stations operating in particular area" Then
												Wait(5)
											End If

											If Datatable.Value("ReportName","CustomReportsRemaining")="Licenses status over a defined period of time with customers contact information" Then
												Wait(10)
											End If

											If Datatable.Value("ReportName","CustomReportsRemaining")="No. Of type approval appl submitted day, week and month" Then
												Wait(10)
												ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="Number of applications of a particular type and status over a defined period of time" Then
													Wait(7)
												ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="Status of application for a specified customer" Then
													Wait(12)
											End If
											If Datatable.Value("ReportName","CustomReportsRemaining")="Number of Interface complaints cleared and pending in a particular area over a defined period of time" Then
												Wait(6)
											End If
											Wait(10)

											.VbButton("Print").Click
											wait(5)
											
											While not .Dialog("Print").WinButton("OK").Exist
												If .Dialog("#32770").WinButton("OK").Exist(15) Then
												.Dialog("#32770").WinButton("OK").Click
												Wait(15)
												.VbButton("Print").Click
												Wait(10)
												End  If
											Wend
'											If .Dialog("#32770").WinButton("OK").Exist(15) Then
'												.Dialog("#32770").WinButton("OK").Click
'												Wait(15)
'												.VbButton("Print").Click
'												Wait(5)
'												If .Dialog("#32770").WinButton("OK").Exist(15) Then
'													.Dialog("#32770").WinButton("OK").Click
'													Wait(15)
'													.VbButton("Print").Click
'													Wait(5)
'													If .Dialog("#32770").WinButton("OK").Exist(15) Then
'														.Dialog("#32770").WinButton("OK").Click
'														Wait(15)
'														.VbButton("Print").Click
'														Wait(5)
'													End If
'												End If
'											End If
											.Dialog("Print").WinButton("OK").Click
										End With
										wait(2)	
										filename=file_location & "\" & Datatable.Value("ReportName","CustomReportsRemaining")

										While fso.FileExists(filename&".pdf")
											filename=file_location & "\" & Datatable.Value("ReportName","CustomReportsRemaining")&"_"&fnRandomNumber(2)
										Wend
										wait(5)
										'If InStr(Environment("Portuguese_Language"),proj)>0 Then

										'If Datatable.Value("ReportName","CustomReportsRemaining")="Type approval log report" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
										If Dialog("Printing Records").Exist(10) Then
											With	Dialog("Printing Records").Dialog("Save Print Output As")

												.WinEdit("File name:").Set filename &".pdf"
												.WinEdit("File name:").Type  micTab @@ hightlight id_;_135778_;_script infofile_;_ZIP::ssf53.xml_;_
												.WinButton("Save").Click
											End With
										Else
												With	VbWindow("frmPrintConfig")
													If Datatable.Value("ReportName","CustomReportsRemaining")="Account Statement" Then
														Wait(15)
													End If
													Wait(3)
													.ActiveX("TimoSoft TabStrip Control").ActiveX("TimoSoft Frame Control").ActiveX("TimoSoft TextBox Control").Object.Text="" @@ hightlight id_;_134420_;_script infofile_;_ZIP::ssf340.xml_;_
													.ActiveX("TimoSoft TabStrip Control").ActiveX("TimoSoft Frame Control").ActiveX("TimoSoft TextBox Control").Type filename &".pdf" @@ hightlight id_;_134420_;_script infofile_;_ZIP::ssf349.xml_;_
													.ActiveX("TimoSoft TabStrip Control").ActiveX("TimoSoft Frame Control").ActiveX("TimoSoft CheckBox Control").Click 16,7 @@ hightlight id_;_134412_;_script infofile_;_ZIP::ssf350.xml_;_
													.ActiveX("TimoSoft CommandButton").Click 39,6 @@ hightlight id_;_134118_;_script infofile_;_ZIP::ssf351.xml_;_
												End With
										End If 
										'Else
										'Dialog("Printing Records").Dialog("Save Print Output As").WinEdit("File name:").Set filename &".pdf"
										'Dialog("Printing Records").Dialog("Save Print Output As").WinEdit("File name:").Type  micTab
										'Dialog("Printing Records").Dialog("Save Print Output As").WinButton("Save").Click
										'End If 

										wait(5)
										VbWindow("frmReportViewer").VbButton("Close").Click
								End If
						End If
				End If 
			End If
		End If
	Next
	
	If AssignInputs="No" Then
		'VbWindow("frmMDI").WinObject("MDIClient").VScroll micSetPos, 10
	End If

	VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbButton("Close").Click
	
	strTestCaseName = Environment.Value("TestName")
		If err.number = 0 Then
			blnFind = True
		Else
			blnFind = False
		End If
		
		UpdateTestCaseStatusInExcel strTestCaseName, blnFind
End If
