﻿wait(2)
blnFind=True
err.number = 0

datatable.ImportSheet ProjectFolderPath &"\Desktop_Input_Data_Sheet\Input_Data_Sheet.xls","NationalFreqAllocation","NationalFreqAllocation"
n=datatable.GetSheet("NationalFreqAllocation").GetRowCount

'For i = 1 To n Step 1
If n>0 Then
  datatable.SetCurrentRow(1)
  fn_NavigateToScreen "Administrative;System Table;National Frequency Allocations"
  With VbWindow("frmMDI")
    If Datatable.value("Client","NationalFreqAllocation")="JMC" Then
      With .VbWindow("frmNatFreqFilter")
        While Not .Exist
        Wend
        wait 10
        .ActiveX("MaskEdBox").Click 94,17
        Wait 2
        .ActiveX("MaskEdBox").Type Datatable.Value("LowFreq","NationalFreqAllocation")
        .ActiveX("MaskEdBox").Type  micTab
        .ActiveX("MaskEdBox_2").Type Datatable.Value("HighFreq","NationalFreqAllocation")
        .ActiveX("MaskEdBox_2").Type  micTab
        .ActiveX("TCIComboBox.tcb").VbComboBox("cboSel").Select Datatable.Value("ServiceClass","NationalFreqAllocation")
        wait(1)
        .VbButton("OK").Click
      End With
    Else
      With .VbWindow("frmNatFreqFilter_2")
        While Not .Exist
        Wend
        .ActiveX("MaskEdBox").Click 94,17
        .ActiveX("MaskEdBox").Type Datatable.Value("LowFreq","NationalFreqAllocation")
        .ActiveX("MaskEdBox").Type  micTab
        .ActiveX("MaskEdBox_2").Type Datatable.Value("HighFreq","NationalFreqAllocation")
        .ActiveX("MaskEdBox_2").Type  micTab
        .ActiveX("TCIComboBox.tcb").VbComboBox("cboSel").Select Datatable.Value("ServiceClass","NationalFreqAllocation")
      End With
    End If
    While Not .VbWindow("frmSystemTables").Exist
    Wend
  End With
  
  Wait(2)
  If Datatable.Value("Operation","NationalFreqAllocation")<>"Delete" Then
    With	VbWindow("frmMDI").VbWindow("frmSystemTables")
      Wait 10
      .AcxTable("National Frequency Allocations").SetCellData 1,2,Datatable.Value("LowFreq","NationalFreqAllocation")
      .AcxTable("National Frequency Allocations").Type  micTab
      
      .AcxTable("National Frequency Allocations").SetCellData 1,3,Datatable.Value("HighFreq","NationalFreqAllocation")
      .AcxTable("National Frequency Allocations").Type  micTab
      
      .AcxTable("National Frequency Allocations").SetCellData 1,4,Datatable.Value("ServiceClass","NationalFreqAllocation")
      .AcxTable("National Frequency Allocations").Type  micTab
      
      .AcxTable("National Frequency Allocations").SetCellData 1,5,Datatable.Value("Category","NationalFreqAllocation")
      .AcxTable("National Frequency Allocations").Type  micTab
      
      .AcxTable("National Frequency Allocations").SetCellData 1,6,Datatable.Value("StartChannel","NationalFreqAllocation")
      .AcxTable("National Frequency Allocations").Type  micTab
      
      .AcxTable("National Frequency Allocations").SetCellData 1,7,Datatable.Value("ChannelWidth","NationalFreqAllocation")
      .AcxTable("National Frequency Allocations").Type  micTab
      
      .AcxTable("National Frequency Allocations").SetCellData 1,8,Datatable.Value("LowFreq","NationalFreqAllocation") 'low freq
      .AcxTable("National Frequency Allocations").Type  micTab
      
      .AcxTable("National Frequency Allocations").SetCellData 1,9,Datatable.Value("TotalChannels","NationalFreqAllocation")
      .AcxTable("National Frequency Allocations").Type  micTab
      
      .AcxTable("National Frequency Allocations").SetCellData 1,10,Datatable.Value("RecFreqOffSet","NationalFreqAllocation")
      
      wait(1)
      
      .VbButton("Save").Click
    End With
  Else
    
    VbWindow("frmMDI_2").VbWindow("frmSystemTables").AcxTable("National Frequency Allocations").SelectRow 1
    wait(1)
    
    VbWindow("frmMDI").VbWindow("frmSystemTables").VbButton("Delete").Click
    
  End If
  
  wait 10
  
  With VbWindow("frmMDI")
    If .Dialog("#32770").WinButton("Yes").Exist Then
      .Dialog("#32770").WinButton("Yes").Click
    Else
      .VbWindow("frmSystemTables").VbButton("Save").Click
      With .Dialog("#32770")
        If .WinButton("Yes").Exist Then
          .WinButton("Yes").Click
        End If
      End With
    End If
    
    wait(2)
    
    If .VbWindow("frmSystemTables").VbButton("Close").Exist Then
      .VbWindow("frmSystemTables").VbButton("Close").Click
    End If
    If VbWindow("frmMDI_2").VbWindow("frmSystemTables").VbButton("Close").Exist Then
      VbWindow("frmMDI_2").VbWindow("frmSystemTables").VbButton("Close").Click
    End If
  End With
  
  strTestCaseName = Environment.Value("TestName")
  If err.number = 0 Then
    blnFind = True
  Else
    blnFind = False
  End If
  
  UpdateTestCaseStatusInExcel strTestCaseName, blnFind
End If

