﻿blnFind=False

err.number = 0

datatable.ImportSheet ProjectFolderPath &"\Desktop_Input_Data_Sheet\Input_Data_Sheet.xls","LicModify","LicModify"
n=datatable.GetSheet("LicModify").GetRowCount

'For i = 1 To n Step 1 
With VbWindow("frmMDI")
	If n>0 Then
		datatable.SetCurrentRow(1) 	
		fn_NavigateToScreen "Licence;Modify"
		With .VbWindow("frmModLicense")
			While not .Exist
			Wend
			.ActiveX("TCIComboBox.tcb").WinEdit("Edit").Set Datatable.Value("AppNo","LicModify")
			.ActiveX("TCIComboBox.tcb").VbComboBox("cboApplicationNo").Select Datatable.Value("AppNo","LicModify") @@ hightlight id_;_198494_;_script infofile_;_ZIP::ssf4.xml_;_
			.VbButton("Next").Click @@ hightlight id_;_329552_;_script infofile_;_ZIP::ssf12.xml_;_
		End With
		wait(3)
		With .Dialog("#32770")
			While Not .WinEdit("Please describe why you").Exist
			Wend
			.WinEdit("Please describe why you").Set GenerateRandomString(5)
			.WinEdit("Please describe why you").Type  micTab @@ hightlight id_;_199408_;_script infofile_;_ZIP::ssf16.xml_;_
			If .WinButton("OK").Exist Then
				.WinButton("OK").Click
			End If
		End With

		XMLDataFile = "C:\ASMS_Automation\ASMS_Variables.xml"
		Set xmlDoc = CreateObject("Microsoft.XMLDOM")
		xmlDoc.Async = False
		xmlDoc.Load(XMLDataFile)

		For Iterator = 1 To 10

			Set node = xmlDoc.SelectSingleNode("/Environment/Variable["&Iterator&"]/Value")
			node.Text = "False"
		Next
		' update the Sublicense Modify
		Set node = xmlDoc.SelectSingleNode("/Environment/Variable[11]/Value")
		node.Text = "True"

		' update the Sublicenses list to Modify
		Set node = xmlDoc.SelectSingleNode("/Environment/Variable[12]/Value")
		node.Text =  datatable.Value("AppNo","LicModify")

		' save changes   
		xmlDoc.Save(XMLDataFile)
		With .VbWindow("frmApplicationInfo")
			If .VbButton("Save").Exist Then
				.VbButton("Save").Click
			End If
		End With

		strTestCaseName = Environment.Value("TestName")
		If err.number = 0 Then
			blnFind=True
		End If
		UpdateTestCaseStatusInExcel strTestCaseName, blnFind
		wait(2)
	End If
End With
