﻿
blnFind=False
err.number = 0

'Import data from Input data sheet  from folder
datatable.ImportSheet ProjectFolderPath&"\Input_Data_Sheet.xls","WebCP_Ad_Maritime","Global"
n=datatable.GetSheet("Global").GetRowCount

'For i = 1 To n Step 1
If n>0 Then
		datatable.SetCurrentRow(1)	
		If Lcase(Datatable("Review","Global"))="yes" Then
		   wait 10
			If Browser("Application").Page("Application").Frame("Frame").Image("Edit_2").Exist Then
				Browser("Application").Page("Application").Frame("Frame").Image("Edit_2").Click
			End If
			Browser("Application").Page("Application").Sync
			If Browser("Application").Page("Application").Frame("Frame").Image("Edit").Exist Then
				Browser("Application").Page("Application").Frame("Frame").Image("Edit").Click
			End If
			Browser("Application").Page("Application").Sync
			wait 8
			If Not Browser("Application").Page("Application").Frame("Frame_2").WebEdit("tbl_site__stationName").Exist(8) Then
				Browser("Application").Page("Application").Frame("Frame_2").WebEdit("tbl_site__stationName").DoubleClick
			End If
           If Browser("Application").Page("Application").Frame("Frame_2").WebEdit("tbl_site__stationName").Exist Then
           	  Browser("Application").Page("Application").Frame("Frame_2").WebEdit("tbl_site__stationName").Set GenerateRandomString(6)
           End If @@ script infofile_;_ZIP::ssf28.xml_;_
           If Browser("Application").Page("Application").Frame("Frame_2").WebList("tbl_site__stationClass").Exist(8) Then
           	  Browser("Application").Page("Application").Frame("Frame_2").WebList("tbl_site__stationClass").Select Datatable("ServiceClass","Global")
           End If @@ script infofile_;_ZIP::ssf39.xml_;_
           If Browser("Application").Page("Application").Frame("Frame_2").WebList("tbl_site__stationPurpose").Exist(8) Then
           	  Browser("Application").Page("Application").Frame("Frame_2").WebList("tbl_site__stationPurpose").Select Datatable("StationClass","Global")
           End If @@ script infofile_;_ZIP::ssf41.xml_;_
      
           If Browser("Application").Page("Application").Frame("Frame_2").WebElement("img_0").Exist Then
           	 Browser("Application").Page("Application").Frame("Frame_2").WebElement("img_0").Click
           End If @@ script infofile_;_ZIP::ssf29.xml_;_
           Browser("Application").Page("Application").Sync
           If Browser("Application").Page("Application").WebElement("xpath:=(//span[contains(text(),'Ship')])[1]//following::span[2]").Exist Then
           	Browser("Application").Page("Application").WebElement("xpath:=//span[contains(text(),'Ship')]//following::span[contains(text(),'x')]").Click
           End If
           Browser("Application").Page("Application").Sync
		   If Browser("Application").Page("Application").Frame("Frame_3").WebEdit("tbl_equipment__equipSerialNum").Exist Then
				Browser("Application").Page("Application").Frame("Frame_3").WebEdit("tbl_equipment__equipSerialNum").Set RandomNumber(1,100)
			End If @@ script infofile_;_ZIP::ssf31.xml_;_
			
			If Browser("Application").Page("Application").Frame("Frame_3").WebElement("img_GCStbl_equipment__callSign").Exist(8) Then
				Browser("Application").Page("Application").Frame("Frame_3").WebElement("img_GCStbl_equipment__callSign").Click
			End If @@ script infofile_;_ZIP::ssf36.xml_;_
			If Browser("Application").Page("Application").Frame("Frame_3").WebList("tbl_equipment__relateEquipType").Exist(8) Then
				Browser("Application").Page("Application").Frame("Frame_3").WebList("tbl_equipment__relateEquipType").Select "Maritime Beacon"
			End If @@ script infofile_;_ZIP::ssf37.xml_;_
 @@ script infofile_;_ZIP::ssf38.xml_;_
			If Browser("Application").Page("Application").Frame("Frame_3").WebElement("img_0").Exist Then
				Browser("Application").Page("Application").Frame("Frame_3").WebElement("img_0").Click
			End If @@ script infofile_;_ZIP::ssf32.xml_;_
			Browser("Application").Page("Application").Sync
 @@ script infofile_;_ZIP::ssf30.xml_;_
Else
		
		
		While Not Browser("Application").Page("Application").Frame("Frame").Link("Ship").Exist
		Wend @@ script infofile_;_ZIP::ssf1.xml_;_
		Browser("Application").Page("Application").Frame("Frame").Link("Ship").Click
		Browser("Application").Page("Application").Sync
		While Not Browser("Application").Page("Application").Frame("Frame_2").WebEdit("tbl_site__stationName").Exist
		Wend
		Browser("Application").Page("Application").Sync
		Browser("Application").Page("Application").Frame("Frame_2").WebEdit("tbl_site__stationName").Set GenerateRandomString(5) @@ script infofile_;_ZIP::ssf2.xml_;_
		Browser("Application").Page("Application").Frame("Frame_2").WebEdit("tbl_site__siteInfo12").Set GenerateRandomString(5) @@ script infofile_;_ZIP::ssf3.xml_;_
		Wait 5
		Browser("Application").Page("Application").Frame("Frame_2").WebList("tbl_site__stationPurpose").Select DataTable("StationClass","Global")
		Browser("Application").Page("Application").Frame("Frame_2").WebList("tbl_site__stationClass").Select DataTable("ServiceClass","Global") @@ script infofile_;_ZIP::ssf5.xml_;_
		Browser("Application").Page("Application").Frame("Frame_2").WebElement("img_0").Click @@ script infofile_;_ZIP::ssf6.xml_;_
		Browser("Application").Page("Application").Sync

		If Lcase(Datatable.value("Equipment","Global"))="receiver" Then
			Browser("Application").Page("Application").Frame("Frame_2").WebElement("img_1").Click
	    ElseIf  Lcase(Datatable.value("Equipment","Global"))="transceiver" Then
			Browser("Application").Page("Application").Frame("Frame_2").WebElement("img_2").Click @@ script infofile_;_ZIP::ssf14.xml_;_
		 ElseIf  Lcase(Datatable.value("Equipment","Global"))="transmitter" Then
		    Browser("Application").Page("Application").Frame("Frame_2").WebElement("img_3").Click @@ script infofile_;_ZIP::ssf23.xml_;_
		End If
		
		Browser("Application").Page("Application").Sync
		Browser("Application").Page("Application").Frame("Frame_3").WebList("tbl_equipment__ApprovalNum").Select 1
		wait 5
		If Browser("Application").Page("Application").Frame("Frame_3").WebEdit("tbl_equipment__equipSerialNum").Exist(5) Then
			Browser("Application").Page("Application").Frame("Frame_3").WebEdit("tbl_equipment__equipSerialNum").Set GenerateRandomString(5)
		End If @@ script infofile_;_ZIP::ssf11.xml_;_
		Browser("Application").Page("Application").Frame("Frame_3").WebEdit("tbl_equipment__antennaPower").Set RandomNumber(1,999)
		If Browser("Application").Page("Application").Frame("Frame_3").WebList("tbl_equipment__relateEquipType").Exist(5) Then
		   Browser("Application").Page("Application").Frame("Frame_3").WebList("tbl_equipment__relateEquipType").Select 1
		End If
		Wait 15
		If Browser("Application").Page("Application").Frame("Frame_3").WebElement("img_GCStbl_equipment__callSign").Exist Then
		    Browser("Application").Page("Application").Frame("Frame_3").WebElement("img_GCStbl_equipment__callSign").Click
		End If
		Browser("Application").Page("Application").Frame("Frame_3").WebElement("img_0").Click @@ script infofile_;_ZIP::ssf20.xml_;_
		Browser("Application").Page("Application").Sync
		Wait 10
		
	End If			    
End If @@ script infofile_;_ZIP::ssf10.xml_;_
UpdateTestCaseStatusInExcel Environment("TestName"),blnFind
