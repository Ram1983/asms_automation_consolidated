﻿
blnFind=False
err.number = 0

'Import data from Input data sheet  from folder
datatable.ImportSheet ProjectFolderPath&"\Input_Data_Sheet.xls","Ad_NumPlan_Create","Global"

n=datatable.GetSheet("Global").GetRowCount
If n>0 Then
	datatable.SetCurrentRow(1)
	If Browser("Admin Home Page").Page("asmswebcpnmb:61032/management/").Link("Home").Exist Then
		Browser("Admin Home Page").Page("asmswebcpnmb:61032/management/").Link("Home").Click
	End If @@ script infofile_;_ZIP::ssf4.xml_;_
	If Browser("Admin Home Page").Page("Admin Home Page").Link("Numbering Plan").Exist Then
		Browser("Admin Home Page").Page("Admin Home Page").Link("Numbering Plan").Click
		If Browser("Admin Home Page").Page("Numbering Plan").WebList("select").Exist Then
			Browser("Admin Home Page").Page("Numbering Plan").WebList("select").Select DataTable.Value("NumberingPlanIDFromCP","Global")
			If Browser("Admin Home Page").Page("Numbering Plan").Image("edit").Exist Then
				Browser("Admin Home Page").Page("Numbering Plan").Image("edit").Click
			End If
			If DataTable.Value("License","Global")<>"" Then
				If Browser("Admin Home Page").Page("asmswebcpnmb:61032/management/").WebElement("img_10_0").Exist Then
					Browser("Admin Home Page").Page("asmswebcpnmb:61032/management/").WebElement("img_10_0").Click
					If Instr(1,Trim(Browser("Admin Home Page").Page("asmswebcpnmb:61032/management/").WebElement("status").GetROProperty("innertext")),DataTable.Value("LicenseStatus","Global"),1)>0 Then
						blnFind=True
						Status=DataTable.Value("LicenseStatus","Global")
					End If
				End If
			End If
			If DataTable.Value("Modify","Global")<>"" Then
				If Browser("Admin Home Page").Page("asmswebcpnmb:61032/management/").WebElement("img_11_0").Exist Then
					If Instr(1,Trim(Browser("Admin Home Page").Page("asmswebcpnmb:61032/management/").WebElement("status").GetROProperty("innertext")),DataTable.Value("ModifyStatus","Global"),1)>0 Then
						If Browser("Admin Home Page").Page("asmswebcpnmb:61032/management/").WebElement("img_21_0").Exist Then
							Browser("Admin Home Page").Page("asmswebcpnmb:61032/management/").WebElement("img_21_0").Click
							If Instr(1,Trim(Browser("Admin Home Page").Page("asmswebcpnmb:61032/management/").WebElement("status").GetROProperty("innertext")),DataTable.Value("ModifyApprovedStatus","Global"),1)>0 Then
								blnFind=True
								Status=DataTable.Value("ModifyApprovedStatus","Global")
							End If
						End If
					End If
				End If
			End If
			If DataTable.Value("Terminate","Global")<>"" Then
			    If Browser("Admin Home Page").Page("asmswebcpnmb:61032/management/").WebElement("img_30_0").Exist Then
					Browser("Admin Home Page").Page("asmswebcpnmb:61032/management/").WebElement("img_30_0").Click
					If Instr(1,Trim(Browser("Admin Home Page").Page("asmswebcpnmb:61032/management/").WebElement("status").GetROProperty("innertext")),DataTable.Value("TerminatedStatus","Global"),1)>0 Then
						blnFind=True
						Status=DataTable.Value("TerminatedStatus","Global")
					End If
				End If
			End If
		End If
	End If @@ script infofile_;_ZIP::ssf1.xml_;_
End If


fn_EmailLogin DataTable.Value("Email","Global"),DataTable.Value("Password","Global"),DataTable.Value("URL","Global")

Set obj=Browser("version:=internet explorer.*","openurl:=https://.*mail.google.com/mail.*").Page("url:=https://mail.google.com/mail.*")
Set odesc=Description.Create
odesc("micclass").value="Link"
odesc("innertext").value=".*New Numbering plan application has been.*"
Set Val=obj.ChildObjects(odesc)
For i = 0 To Val.count-1 Step 1
    Val(i).Click
    Wait 2
    If Val(i).exist(5) Then
    	Val(i).Click
    	Wait 10
    	If Val(i).exist Then
    		Val(i).Click
    	End If
    End If
    Exit For
Next

Set verify=Description.Create
'verify("micclass").value="Link"	
verify("innertext").value=".*"&Status&".*"
Set verifyEmail=Browser("version:=internet explorer.*","openurl:=https://.*mail.google.com/mail.*").Page("url:=https://mail.google.com/mail.*").ChildObjects(verify)
For count = 0 To verifyEmail.count-1 Step 1
    verifyEmail(count).highlight
Next
Wait 3
    
    
'Test results dsiplayed in Excel
If err.number = 0 Then
    blnFind = True
Else
    blnFind = False
End If
UpdateTestCaseStatusInExcel strTestCaseName, blnFind 
