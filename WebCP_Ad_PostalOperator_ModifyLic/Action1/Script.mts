﻿
blnFind=False
err.number = 0

'Import data from Input data sheet  from folder
datatable.ImportSheet ProjectFolderPath&"\Input_Data_Sheet.xls","Ad_PostalOp_MoodifyLic","Global"
n=datatable.GetSheet("Global").GetRowCount
If n>0 Then
	With Browser("Postal Operator").Page("Admin Home Page")
	        datatable.SetCurrentRow(1)
			.Link("Home").Click @@ script infofile_;_ZIP::ssf1.xml_;_
			While Not Browser("Postal Operator").Page("Admin Home Page").Image("Postal Operator").Exist
			Wend @@ script infofile_;_ZIP::ssf2.xml_;_
			.Image("Postal Operator").Click
			.WebList("select").Select Datatable.value("Status","Global") @@ script infofile_;_ZIP::ssf3.xml_;_
			Set obj=.WebTable("PostalOp").ChildItem(.WebTable("PostalOp").GetRowWithCellText(Datatable.value("PONumber","Global")) ,1,"WebElement",1)
			obj.click
			.Sync
			If Strcomp(Datatable.value("AppStatusType","Global"),"Terminate",1)=0 Then
				.WebElement("img_30_0").Click
				.Sync
				If Strcomp(Trim(.WebElement("Status").GetROProperty("innertext")),Datatable.value("AppStatusType","Global"),1)=0 Then
				  blnFind=True
				  Status=Datatable.value("AppStatusType","Global")
				End If

            ElseIf Strcomp(Datatable.value("AppStatusType","Global"),"Cancel",1)=0 Then
                 .WebElement("img_26_0").Click
                 .Sync
                  If Strcomp(Trim(.WebElement("Status").GetROProperty("innertext")),Datatable.value("AppStatusType","Global"),1)=0 Then
				     blnFind=True
				     Status=Datatable.value("AppStatusType","Global")
				End If
			ElseIf Strcomp(Datatable.value("License","Global"),"Yes",1)=0 Then
				Browser("Postal Operator").Page("Postal Operator").WebElement("License Application").Click @@ script infofile_;_ZIP::ssf6.xml_;_
				Browser("Postal Operator").Page("Postal Operator").Sync
				Status=Datatable.value("License","Global")
			Else
			.WebElement("img_11_0").Click
			.Sync
			.WebEdit("tbModify").Set GenerateRandomString(6)
            .WebElement("img_Modify").Click
            .Sync
            .WebElement("img_21_0").Click
            .WebElement("Yes").Click
			.Sync
       
           End If
          If Instr(1, .WebElement("xmlerror1").GetROProperty("innerhtml"),Datatable.value("ApproveStatus","Global"),1)>0 Then
       	     Status=Datatable.value("ApproveStatus","Global")
       	  End If
	End With
End If


fn_EmailLogin DataTable.Value("Email","Global"),DataTable.Value("Password","Global"),DataTable.Value("URL","Global")

Set obj=Browser("version:=internet explorer.*","openurl:=https://.*mail.google.com/mail.*").Page("url:=https://mail.google.com/mail.*")
Set odesc=Description.Create
odesc("micclass").value="Link"
odesc("innertext").value=".*New Postal Operator has been.*"
Set Val=obj.ChildObjects(odesc)
For i = 0 To Val.count-1 Step 1
    Val(i).Click
    Wait 2
    If Val(i).exist(5) Then
    	Val(i).Click
    	Wait 10
    	If Val(i).exist Then
    		Val(i).Click
    	End If
    End If
    Exit For
Next

Set verify=Description.Create
'verify("micclass").value="Link"	
verify("innertext").value=".*"&Status&".*"
Set verifyEmail=Browser("version:=internet explorer.*","openurl:=https://.*mail.google.com/mail.*").Page("url:=https://mail.google.com/mail.*").ChildObjects(verify)
For count = 0 To verifyEmail.count-1 Step 1
    verifyEmail(count).highlight
Next
Wait 3
    
    
'Test results dsiplayed in Excel
If err.number = 0 Then
    blnFind = True
Else
    blnFind = False
End If

UpdateTestCaseStatusInExcel Environment("TestName"),blnFind
