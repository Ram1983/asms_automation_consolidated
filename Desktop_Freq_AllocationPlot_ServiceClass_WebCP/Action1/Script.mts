﻿blnVal=False
With VbWindow("frmMDI")
	.WinMenu("Menu").Select "Frequency;Frequency Allocation Plot"
	With .VbWindow("frmFreqPlot")
		While Not .VbComboBox("cboRegion").Exist
		Wend
		.VbComboBox("cboRegion").Select 1
	End With
End With
With VbWindow("frmMDI").VbWindow("frmFreqPlot")
	.ActiveX("MaskEdBox").Type RandomNumber(100,100) @@ hightlight id_;_200510_;_script infofile_;_ZIP::ssf3.xml_;_
	.ActiveX("MaskEdBox_2").Type RandomNumber(10000,10000) @@ hightlight id_;_134980_;_script infofile_;_ZIP::ssf5.xml_;_
	.VbButton("View").Click @@ hightlight id_;_528090_;_script infofile_;_ZIP::ssf7.xml_;_
	VbWindow("frmMDI").VbWindow("frmFreqDiagram").VbButton("Print").Click @@ hightlight id_;_266042_;_script infofile_;_ZIP::ssf8.xml_;_
	Wait 3
	VbWindow("frmMDI").Dialog("Save Print Output As").WinEdit("File name:").Set fn_report @@ hightlight id_;_200610_;_script infofile_;_ZIP::ssf9.xml_;_
	VbWindow("frmMDI").Dialog("Save Print Output As").WinButton("Save").Click @@ hightlight id_;_135020_;_script infofile_;_ZIP::ssf10.xml_;_
	VbWindow("frmMDI").VbWindow("frmFreqDiagram").VbButton("Close").Click @@ hightlight id_;_266046_;_script infofile_;_ZIP::ssf11.xml_;_
End With
If Err.number=0 Then
	blnVal=True
End If
UpdateTestCaseStatusInExcel Environment("TestName"),blnVal

