﻿blnFind=True
err.number = 0

datatable.ImportSheet ProjectFolderPath &"\Desktop_Input_Data_Sheet\Input_Data_Sheet.xls","CityDelete","CityDelete"
n=datatable.GetSheet("CityDelete").GetRowCount

'For i = 1 To n Step 1 
With	VbWindow("frmMDI")
	If n>0 Then
		datatable.SetCurrentRow(1) 
		If Datatable.value("Client","CityDelete")="JMC" Then
			.WinMenu("Menu").Select "Administrative;System Table;Parish"
			wait(5)
			With .VbWindow("frmStateCode")
				While Not .ActiveX("TCIComboBox.tcb").VbComboBox("cboStateCode").Exist
				Wend
				.ActiveX("TCIComboBox.tcb").VbComboBox("cboStateCode").Select datatable.value("Parish","CityDelete")''"hyderabad6"
				wait(10)
				'===Delete city
				.VbButton("Delete City").Click
			End With
			wait(5)
			'====Confirmation
			.Dialog("#32770").WinButton("Yes").Click
			wait(5)
			'====Close
			.VbWindow("frmStateCode").VbButton("Close").Click @@ hightlight id_;_329470_;_script infofile_;_ZIP::ssf7.xml_;_
			Else
				'====Parish city delete screen navigation

				VbWindow("frmMDI").WinMenu("Menu").Select "Administrative;System Table;State"
				With .VbWindow("frmStateCode_2")
					While Not .ActiveX("TCIComboBox.tcb").VbComboBox("cboStateCode").Exist
					Wend
					.ActiveX("TCIComboBox.tcb").VbComboBox("cboStateCode").Select datatable.value("Parish","CityDelete")''"asphalt" @@ hightlight id_;_1520838_;_script infofile_;_ZIP::ssf14.xml_;_
					wait(6)
					'===Delete city
					Wait(5)
					.VbButton("Delete City").Click @@ hightlight id_;_801620_;_script infofile_;_ZIP::ssf10.xml_;_
				End With
				wait(5)
				'====Confirmation
				.Dialog("#32770").WinButton("Yes").Click
				wait(5)
				'====Close
				.VbWindow("frmStateCode_2").VbButton("Close").Click
		End If
		strTestCaseName = Environment.Value("TestName")
		If err.number = 0 Then
			blnFind = True
			Else
				blnFind = False
		End If

		UpdateTestCaseStatusInExcel strTestCaseName, blnFind
		'End If
		wait(2)
	End If
End With
