﻿

blnFind=False
err.number = 0

'Import data from Input data sheet  from folder
datatable.ImportSheet ProjectFolderPath&"\Input_Data_Sheet.xls","cpAeronautical","cpAeronautical"
n=datatable.GetSheet("cpAeronautical").GetRowCount

'For i = 1 To n Step 1
If n>0 Then
    
    datatable.SetCurrentRow(1)
    
    With Browser("Customer Home Page").Page("Application")
        
        '======Aeronautical Station
        If Datatable.Value("Stationtype","cpAeronautical")="Aeronautical Station" Then			
           
            If Strcomp(Datatable.Value("Edit","cpAeronautical"),"yes",1)<>0 Then
                 .Frame("Frame").Link("Aeronautical Station").Clicks
                 wait 5
                .Frame("Frame_2").WebList("tbl_site__siteCity").Selects(1)' "Aero Drome (Extension 127)"
            Else
                  fn_EditSubLicenses "station",  Datatable.Value("typeOfStation","cpAeronautical"),Datatable.Value("Stationtype","cpAeronautical")
                 .Frame("Frame_2").WebList("tbl_site__siteCity").Selects "random"
            End If
           
            '=====Save Station
            .Frame("Frame_2").WebElement("img_0").Clicks		
        ElseIf Datatable.Value("Stationtype","cpAeronautical")="Aircraft Station" Then  		
            If Strcomp(Datatable.Value("Edit","cpAeronautical"),"yes",1)<>0  Then
               .Frame("Frame").Link("Aircraft Station").Clicks
               wait 5
               .Frame("Frame_2").WebEdit("tbl_site__siteInfo13").Sets Datatable.Value("AircraftRegmark","cpAeronautical")
               .Frame("Frame_2").WebList("tbl_site__siteInfo1").Selects(2)' "Airbus A.300"
            Else
                wait 3
                fn_EditSubLicenses "station",Datatable.Value("Stationtype","cpAeronautical"),Datatable.Value("typeOfStation","cpAeronautical")
               .Frame("Frame_2").WebList("tbl_site__siteInfo1").Selects "random"
            End If
            
            .Frame("Frame_2").WebEdit("tbl_site__siteInfo12").Sets Datatable.Value("takeoffweight","cpAeronautical")
            'Save Aircraft station 
            .Frame("Frame_2").WebElement("img_0").Clicks
            .Sync
        End If
        
        '=======Receiver
        If Strcomp(Datatable.Value("Edit","cpAeronautical"),"yes",1)<>0 Then
	        If Datatable.Value("Equipmenttype","cpAeronautical")="Add Receiver" Then
	            .Frame("Frame_2").WebElement("img_1").Clicks
	            
	            '=====Addding -Aero Transceiver
	        ElseIf Datatable.Value("Equipmenttype","cpAeronautical")="Add Transceiver"  Then  		
	            .Frame("Frame_2").WebElement("img_2").Clicks
	            '=====Aero- Adding equipment for Transmitter		
	        ElseIf Datatable.Value("Equipmenttype","cpAeronautical")="Add Transmitter"  Then		
	            .Frame("Frame_2").WebElement("img_3").Clicks			
	            '====Adding equipment- other		
	        ElseIf Datatable.Value("Equipmenttype","cpAeronautical")="Add Other"  Then			
	            .Frame("Frame_2").WebElement("img_4").Clicks			
	        End If 
        	
        End If  
        'Equipment Adding all 
        If Strcomp(Datatable.Value("Edit","cpAeronautical"),"yes",1)<>0 Then
           If .Frame("Frame_3").WebList("tbl_equipment__equipType").Exist(5) Then
           	.Frame("Frame_3").WebList("tbl_equipment__equipType").Select(1)
           End If
        else
          fn_EditSubLicenses "",Datatable.Value("Stationtype","cpAeronautical"), Datatable.Value("typeOfStation","cpAeronautical")
          .Frame("Frame_3").WebList("tbl_equipment__equipType").Selects "random"
        End If
        
        With Browser("name:=Application").Page("title:=Application")
            If .WebList("html id:=tbl_equipment__ApprovalNum").Exist Then
            If Datatable.Value("EquipApprovalNum","cpAeronautical")="" Then
            	.WebList("html id:=tbl_equipment__ApprovalNum").Selects 1'"random"
            Else
               .WebList("html id:=tbl_equipment__ApprovalNum").Selects 1'Datatable.Value("EquipApprovalNum","cpAeronautical")
            End If
                
                For intC = 1 To 20 Step 1
                    If Instr(1,.WebList("html id:=tbl_equipment__equipMake","name:=tbl_equipment__equipMake").GetROProperty("selection"),"Select",1)>0 Then
                        Wait 3
                    Else
                        Exit For
                    End If
                Next
            ElseIf .WebEdit("html id:=tbl_equipment__equipMake","name:=tbl_equipment__equipMake").Exist(3) Then
                .WebEdit("html id:=tbl_equipment__equipMake","name:=tbl_equipment__equipMake").Sets Datatable.Value("Aeromake","cpAeronautical")&fnRandomNumber(4)
                .WebEdit("name:=tbl_equipment__modelNum","html id:=tbl_equipment__modelNum").Sets Datatable.Value("Aeromodel","cpAeronautical")&fnRandomNumber(4)
            End If
            If Browser("name:=Application").Page("title:=Application").WebList("html id:=tbl_equipment__modelNum","name:=tbl_equipment__modelNum").Exist(8) And Browser("name:=Application").Page("title:=Application").WebList("html id:=tbl_equipment__equipMake","name:=tbl_equipment__equipMake").Exist(8) Then
            	If Browser("name:=Application").Page("title:=Application").WebList("html id:=tbl_equipment__modelNum","name:=tbl_equipment__modelNum").GetROProperty("Selected Item Index")=0 Then
            		Browser("name:=Application").Page("title:=Application").WebList("html id:=tbl_equipment__modelNum","name:=tbl_equipment__modelNum").Select 1
				End If
				If  Browser("name:=Application").Page("title:=Application").WebList("html id:=tbl_equipment__equipMake","name:=tbl_equipment__equipMake").GetROProperty("Selected Item Index")=0 Then
					 Browser("name:=Application").Page("title:=Application").WebList("html id:=tbl_equipment__equipMake","name:=tbl_equipment__equipMake").Select 1
				End If
				            	
            End If
        End With
        
        'Condition field is availbe in below equipments only
        If Datatable.Value("Equipmenttype","cpAeronautical")="Add Transceiver" Or  Datatable.Value("Equipmenttype","cpAeronautical")="Add Transmitter"  Or Datatable.Value("Equipmenttype","cpAeronautical")="Add Other" Then
            If .Frame("Frame_3").WebEdit("tbl_equipment__antennaPower").Exist(5) Then
               If .Frame("Frame_3").WebEdit("tbl_equipment__antennaPower").GetROProperty("value")="" Then
               	  .Frame("Frame_3").WebEdit("tbl_equipment__antennaPower").Sets Datatable.Value("dbwattts","cpAeronautical")
               End If
            End If
            
            If Browser("Customer Home Page").Page("Application_2").Frame("Frame").WebEdit("tbl_equipment__equipMake").Exist(5) Then @@ script infofile_;_ZIP::ssf70.xml_;_
              Browser("Customer Home Page").Page("Application_2").Frame("Frame").WebEdit("tbl_equipment__equipMake").Set datatable.Value("Aeromake","cpAeronautical")
            End If
 @@ script infofile_;_ZIP::ssf71.xml_;_
            If Browser("Customer Home Page").Page("Application_2").Frame("Frame").WebEdit("tbl_equipment__modelNum").Exist(5) Then @@ script infofile_;_ZIP::ssf72.xml_;_
                Browser("Customer Home Page").Page("Application_2").Frame("Frame").WebEdit("tbl_equipment__modelNum").Set datatable.Value("Aeromodel","cpAeronautical")
            End If
            If Browser("name:=Application").Page("title:=Application").WebEdit("html id:=tbl_equipment__AssignedBand","name:=tbl_equipment__AssignedBand").Exist(3) Then
                Browser("name:=Application").Page("title:=Application").WebEdit("html id:=tbl_equipment__AssignedBand","name:=tbl_equipment__AssignedBand").Sets Datatable.Value("AeroAssignedband","cpAeronautical")
            End If
        End If
        With Browser("name:=Application").Page("title:=Application")
            If Browser("Application").Page("Application").Frame("Frame").WebEdit("tbl_equipment__lowerFreqRange").Exist(5) Then
                Browser("Application").Page("Application").Frame("Frame").WebEdit("tbl_equipment__lowerFreqRange").Sets datatable.Value("Aerolowerfreq","cpAeronautical")
            End If
            If Browser("Application").Page("Application").Frame("Frame").WebEdit("tbl_equipment__upperFreqRange").Exist(5) Then
                Browser("Application").Page("Application").Frame("Frame").WebEdit("tbl_equipment__upperFreqRange").Sets Datatable.Value("AeroUpperfreq","cpAeronautical")
            End If		
            
            If .WebEdit("html id:=tbl_equipment__equipSerialNum","name:=tbl_equipment__equipSerialNum").Exist(5) Then
                .WebEdit("html id:=tbl_equipment__equipSerialNum","name:=tbl_equipment__equipSerialNum").Sets "Equipment"&RandomNumber(1,1000)	
            End If
             If Strcomp(Datatable.Value("Edit","cpAeronautical"),"yes",1)<>0 And .WebList("all items:=Select Approval No.*","html id:=tbl_antenna__ApprovalNum").Exist(40)  Then
	             If Datatable.Value("AntenaApprovalNum","cpAeronautical")="" Then
	             	 .WebList("all items:=Select Approval No.*","html id:=tbl_antenna__ApprovalNum").Selects 1'"random"
	             Else
	                 .WebList("all items:=Select Approval No.*","html id:=tbl_antenna__ApprovalNum").Selects  1'Datatable.Value("AntenaApprovalNum","cpAeronautical")
	             End If
             ElseIf .WebList("all items:=Select Approval No.*","html id:=tbl_antenna__ApprovalNum").Exist(10) Then
                .WebList("all items:=Select Approval No.*","html id:=tbl_antenna__ApprovalNum").Selects 1'"random"
             End If
             If .WebList("all items:=Select Approval No.*","html id:=tbl_antenna__ApprovalNum").Exist(3) Then
	                For intC = 1 To 20 Step 1
	                    If Instr(1,.WebList("html id:=tbl_antenna__equipMake","name:=tbl_antenna__equipMake").GetROProperty("selection"),"Select",1)>0 Then
	                        Wait 5
	                    Else
	                        Exit For
	                    End If
	                Next
	        End If
           

            If Browser("name:=Application").Page("title:=Application").WebList("html id:=tbl_antenna__modelNum","name:=tbl_antenna__modelNum").Exist(8) And Browser("name:=Application").Page("title:=Application").WebList("html id:=tbl_antenna__equipMake","name:=tbl_antenna__equipMake").Exist(8) Then
            	If Browser("name:=Application").Page("title:=Application").WebList("html id:=tbl_antenna__modelNum","name:=tbl_antenna__modelNum").GetROProperty("Selected Item Index")=0 Then
            		Browser("name:=Application").Page("title:=Application").WebList("html id:=tbl_antenna__modelNum","name:=tbl_antenna__modelNum").Select 1
				End If
				If  Browser("name:=Application").Page("title:=Application").WebList("html id:=tbl_antenna__equipMake","name:=tbl_antenna__equipMake").GetROProperty("Selected Item Index")=0 Then
					 Browser("name:=Application").Page("title:=Application").WebList("html id:=tbl_antenna__equipMake","name:=tbl_antenna__equipMake").Select 1
				End If
				            	
            End If
           
        End With
        
        'Save Equpment
        If .Frame("Frame_3").WebElement("img_0").Exist Then
        	.Frame("Frame_3").WebElement("img_0").Clicks
        End If
        Browser("Customer Home Page").Sync 
    End With
    
End If


'Test results dsiplayed in Excel
If err.number = 0 Then
    blnFind = True
Else
    blnFind = False
End If

'fn_SendStatusToExcelReport blnFind
UpdateTestCaseStatusInExcel Environment("TestName"),blnFind



