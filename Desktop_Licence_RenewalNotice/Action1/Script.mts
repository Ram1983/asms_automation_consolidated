﻿ wait(2)
blnFind=True
err.number = 0
datatable.ImportSheet ProjectFolderPath &"\Desktop_Input_Data_Sheet\Input_Data_Sheet.xls","RenewalNotice","RenewalNotice"
n=datatable.GetSheet("RenewalNotice").GetRowCount

'For i = 1 To n Step 1
If n>0 Then
	datatable.SetCurrentRow(1)
	
	'======Renewal Notice screen Navigation
	 

	With VbWindow("frmMDI")
		.WinMenu("Menu").Select "Licence;Renew Licence;Renewal Notice"

		wait(10)
		With .VbWindow("frmLicRenewNotice")
			While Not .ActiveX("TCIComboBox.tcb").VbComboBox("cboMonth").Exist(50)
			Wend
			'Month Selecting
			.ActiveX("TCIComboBox.tcb").VbComboBox("cboMonth").Select Datatable.value("Month","RenewalNotice")' "April" @@ hightlight id_;_331224_;_script infofile_;_ZIP::ssf7.xml_;_
			.ActiveX("TCIComboBox.tcb").VbComboBox("cboMonth").Type  micTab @@ hightlight id_;_331224_;_script infofile_;_ZIP::ssf8.xml_;_
			.ActiveX("TCIComboBox.tcb_2").VbComboBox("cboYear").Select Datatable.value("Year","RenewalNotice")'"2020" @@ hightlight id_;_331296_;_script infofile_;_ZIP::ssf9.xml_;_
			.ActiveX("TCIComboBox.tcb_2").VbComboBox("cboYear").Type  micTab @@ hightlight id_;_331296_;_script infofile_;_ZIP::ssf10.xml_;_
			'VbWindow("frmMDI").VbWindow("frmLicRenewNotice").ActiveX("TCIComboBox.tcb_3").VbComboBox("cboLicType").Select(0)
			' Datatable.value("Year","LicenceType") '"Licence for Frequency"
			'Selecting check box

			.VbButton("Select All").Click

			'VbWindow("frmMDI").VbWindow("frmLicRenewNotice").AcxTable("List of Licences to be").SelectColumn 1
			'VbWindow("frmMDI").VbWindow("frmLicRenewNotice").AcxTable("List of Licences to be").SelectCell 1,1
			wait(10)
			'Calculating Fee
			'On error resume next

			if .VbButton("Calculate Fee").Exist then
				.VbButton("Calculate Fee").Click @@ hightlight id_;_1182836_;_script infofile_;_ZIP::ssf13.xml_;_
			end if

			'.....print notice

			Wait(10)
			If .VbButton("Print Notice").Exist Then
				.VbButton("Print Notice").Click
			End If
			wait(15)
			With .VbWindow("frmRptAppNotice")
				While not .exist
				Wend
				If .VbButton("View").Exist Then
					.VbButton("View").Click
				End If
			End With
		End With
	End With
	wait 20
'	 If  VbWindow("frmReportViewer").Exist Then
'		 VbWindow("frmReportViewer").Minimize
'	 End If
'	 If VbWindow("frmReportViewer_2").Exist Then
'	    VbWindow("frmReportViewer_2").Minimize
'	 End If

	With VbWindow("frmReportViewer_2")
		If .ActiveX("Crystal Report Viewer").WinObject("of 1+").Exist Then
			.ActiveX("Crystal Report Viewer").WinObject("of 1+").Highlight
		End If
		If .VbButton("Print").Exist Then
			.VbButton("Print").Click
		End If
		With .Dialog("Print")
			If .WinButton("OK").Exist Then
				.WinButton("OK").Click
			End If
		End With
	End With
	With Dialog("Save Print Output As")
		.WinEdit("File name:").Set GenerateRandomString(6) &RandomNumber(1,9999) @@ hightlight id_;_658088_;_script infofile_;_ZIP::ssf28.xml_;_
		If .WinButton("Save").Exist Then
			.WinButton("Save").Click
		End If
	End With
	With VbWindow("frmReportViewer_2")
		If .VbButton("Close").Exist Then
			.VbButton("Close").Click
		End If
	End With
	wait(3)
VbWindow("frmMDI").VbWindow("frmLicRenewNotice").VbButton("Close").Click
strTestCaseName = Environment.Value("TestName")
If err.number = 0 Then
	blnFind = True
Else
	blnFind = False
End If
UpdateTestCaseStatusInExcel strTestCaseName, blnFind

End If

