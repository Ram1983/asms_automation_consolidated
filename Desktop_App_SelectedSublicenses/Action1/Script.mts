﻿
'msgbox ProjectFolderPath

'While not VbWindow("frmMDI").VbWindow("frmApplicationInfo").Exist(20)
'Wend

If VbWindow("frmMDI").VbWindow("frmApplicationInfo").Exist(10) Then
		
	VbWindow("frmMDI").VbWindow("frmApplicationInfo").VbButton("Save").Click @@ hightlight id_;_528946_;_script infofile_;_ZIP::ssf12.xml_;_
	
	If VbWindow("frmMDI").Dialog("#32770").WinButton("Button").Exist Then
	    VbWindow("frmMDI").Dialog("#32770").WinButton("Button").Click @@ hightlight id_;_596682_;_script infofile_;_ZIP::ssf13.xml_;_
	End If

End If
'VbWindow("frmMDI").Dialog("#32770").WinButton("No").Click @@ hightlight id_;_334226_;_script infofile_;_ZIP::ssf16.xml_;_

While not VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Exist
Wend

Environment.LoadFromFile("D:\ASMS_Automation\ASMS_Variables.xml")
wait(5)
AppNum = VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").GetItem(1)
treeViewNode1="Application;"& AppNum &";Application Detail"
treeViewNode="Application;"& AppNum &";Application Detail;"

VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").ExpandAll(treeViewNode1)
wait(1)

CustomerNodeIndex = 0

'Fixed Link
'Aeronautical Sites
'Cellular Mobile / WLL / RLAN / BWA 
'TV / Radio Broadcast
'Land / Mobile
'Satellite
'Radio Transmitter Security
'Ship Site
'

'-----------------------------------------------


'msgbox treeViewNode
'wait(1)
'msgbox treeViewNode &"Fixed Link" 

'VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Activate treeViewNode &"Fixed Link"
'VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Activate "Application;Data Entry:4706;Application Detail;Fixed Link"
'wait 18
'VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Activate "Application;Pending:3800;Application Detail;Fixed Link" @@ hightlight id_;_2689308_;_script infofile_;_ZIP::ssf1.xml_;_
'VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Activate "Application;Pending:3800;Application Detail;Citizen Band Radio Licence" @@ hightlight id_;_2689308_;_script infofile_;_ZIP::ssf2.xml_;_
'VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Activate "Application;Pending:3800;Application Detail;Aeronautical Sites" @@ hightlight id_;_2689308_;_script infofile_;_ZIP::ssf3.xml_;_
''VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Select "Application;Pending:3800;Application Detail;Cellular Mobile / WLL / RLAN / BWA " @@ hightlight id_;_2689308_;_script infofile_;_ZIP::ssf4.xml_;_
'VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Activate "Application;Pending:3800;Application Detail;Cellular Mobile / WLL / RLAN / BWA " @@ hightlight id_;_2689308_;_script infofile_;_ZIP::ssf11.xml_;_
'VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Activate "Application;Pending:3800;Application Detail;TV / Radio Broadcast" @@ hightlight id_;_2689308_;_script infofile_;_ZIP::ssf5.xml_;_
'VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Activate "Application;Pending:3800;Application Detail;Land / Mobile" @@ hightlight id_;_2689308_;_script infofile_;_ZIP::ssf6.xml_;_
'VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Activate "Application;Pending:3800;Application Detail;Amateur Radio" @@ hightlight id_;_2689308_;_script infofile_;_ZIP::ssf7.xml_;_
'VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Activate "Application;Pending:3800;Application Detail;Satellite" @@ hightlight id_;_2689308_;_script infofile_;_ZIP::ssf8.xml_;_
'VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Activate "Application;Pending:3800;Application Detail;Radio Transmitter Security" @@ hightlight id_;_2689308_;_script infofile_;_ZIP::ssf9.xml_;_
'VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Activate "Application;Pending:3800;Application Detail;Ship Site" @@ hightlight id_;_2689308_;_script infofile_;_ZIP::ssf10.xml_;_


If Environment("Modify") Then

Const XMLDataFile = ProjectFolderPath &"\ASMS_Variables.xml"
Set xmlDoc = CreateObject("Microsoft.XMLDOM")
	xmlDoc.Async = False
	xmlDoc.Load(XMLDataFile)

For i =3 to VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").GetItemsCount

currentitem = VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").GetItem(i)
If (CustomerNodeIndex=i) OR (Not VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Exist) Then
	Exit for
End If

If (InStr(currentitem,"Customer")>0) Then
	CustomerNodeIndex=i+1
End  If

If InStr(currentitem,"Fixed Link") >0 Then
	Set node = xmlDoc.SelectSingleNode("/Environment/Variable[1]/Value")
	node.Text = "True"
	wait(1)	
	VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Type micDwn
ElseIf InStr(currentitem,"Citizen Band Radio Licence") >0 Then
	Set node = xmlDoc.SelectSingleNode("/Environment/Variable[2]/Value")
	node.Text = "True"
	wait(1)		
	VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Type micDwn
ElseIf InStr(currentitem,"Aeronautical Sites") >0 Then
	Set node = xmlDoc.SelectSingleNode("/Environment/Variable[3]/Value")
	node.Text = "True"
	wait(1)		
	VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Type micDwn
ElseIf InStr(currentitem,"Cellular Mobile / WLL / RLAN / BWA") >0 Then
	Set node = xmlDoc.SelectSingleNode("/Environment/Variable[4]/Value")
	node.Text = "True"
	wait(1)	
	VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Type micDwn
ElseIf InStr(currentitem,"TV / Radio Broadcast") >0 Then
	Set node = xmlDoc.SelectSingleNode("/Environment/Variable[5]/Value")
	node.Text = "True"
	wait(1)	
	VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Type micDwn
ElseIf InStr(currentitem,"Land / Mobile") >0 Then
	Set node = xmlDoc.SelectSingleNode("/Environment/Variable[6]/Value")
	node.Text = "True"
	wait(1)	
	VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Type micDwn
ElseIf InStr(currentitem,"Amateur Radio") >0 or InStr(currentitem,"Amateur Licence") >0 Then
	Set node = xmlDoc.SelectSingleNode("/Environment/Variable[7]/Value")
	node.Text = "True"
	wait(1)	
	VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Type micDwn
ElseIf InStr(currentitem,"Satellite") >0 Then
	Set node = xmlDoc.SelectSingleNode("/Environment/Variable[8]/Value")
	node.Text = "True"
	wait(1)	
	VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Type micDwn
ElseIf InStr(currentitem,"Radio Transmitter Security") >0 Then
	Set node = xmlDoc.SelectSingleNode("/Environment/Variable[9]/Value")
	node.Text = "True"
	wait(1)	
	VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Type micDwn
ElseIf InStr(currentitem,"Ship Site") >0 Then
	Set node = xmlDoc.SelectSingleNode("/Environment/Variable[10]/Value")
	node.Text = "True"
	wait(1)	
	VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Type micDwn
End If

Next

		' update the Sublicense Modify
	Set node = xmlDoc.SelectSingleNode("/Environment/Variable[11]/Value")
	   node.Text = "False"
	
	' save changes 
	xmlDoc.Save(XMLDataFile)
	
End If

MCL_Click=0
CBL_Click=0
ASL_Click=0
GSM_Click=0
TBL_Click=0
RCL_Click=0
AMR_Click=0
SAT_Click=0
Alarm_Click=0
SSL_Click=0

Environment.LoadFromFile(ProjectFolderPath &"\ASMS_Variables.xml")

For i =3 to VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").GetItemsCount

currentitem = VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").GetItem(i)
'print currentitem
'print i
If (CustomerNodeIndex=i) OR (Not VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Exist) Then
	Exit for
End If

If (InStr(currentitem,"Customer")>0) Then
	CustomerNodeIndex=i+1
End  If

'strleng = InStr(currentitem,"Citizen")  
' msgbox currentitem
If InStr(currentitem,"Fixed Link") >0 Then
	wait(5)	
	If Environment("MCL") Then
		MCL_Click=MCL_Click+1
		'Print "Automation process for MCL" & MCL_Click
		VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Type micDwn
		'VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Type micDwn
		'VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").GetItem i 'treeViewNode &"Fixed Link"
		'msgbox VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").GetItem(i) 
		wait(3)
		'VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Activate treeViewNode &"Fixed Link"
		If MCL_Click=1 Then
	         VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Activate i+1 
		Else
			i=i-1
	        ' VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Activate i+1+1 
		End  If
'	VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Type micDwn
		wait(15) '5
		
		If InStr(Environment("Modify_Sublicenses"),"MCL") >0 Then
            RunAction "FixedLink-RCL [Modify_MCL]", oneIteration
		Else
			RunAction "FixedLink-RCL [SubLic-FixedLink-RCL]", oneIteration
		End If
		
	End IF
ElseIf InStr(currentitem,"Citizen Band Radio Licence") >0 Then
	wait(5)
	If Environment("CBL") Then
		CBL_Click=CBL_Click+1
		'Print "Automation not yet done for Citizen Band Radio Licence" & CBL_Click
		VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Type micDwn
		'VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Select treeViewNode &"Citizen Band Radio Licence"
		wait(3)
		'VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Activate treeViewNode &"Citizen Band Radio Licence"
		'VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Activate i+1 
		'VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Type micDwn
		If CBL_Click=1 Then
	         VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Activate i+1 
		Else
			i=i-1
	        ' VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Activate i+1+1 
		End  If
		wait(3)
	End IF
ElseIf InStr(currentitem,"Aeronautical Sites") >0 Then
	wait(5)
	If Environment("ASL") Then
		ASL_Click=ASL_Click+1
		'Print "Automation process for ASL"&ASL_Click
		VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Type micDwn
		'VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Select treeViewNode &"Aeronautical Sites"
		wait(3)
		'VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Activate treeViewNode &"Aeronautical Sites"
		'VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Activate i+1 
		'VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Type micDwn
		If ASL_Click=1 Then
	         VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Activate i+1 
		Else
			i=i-1
	        ' VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Activate i+1+1 
		End  If
		wait(15) '5

		If InStr(Environment("Modify_Sublicenses"),"ASL") >0 Then
			RunAction "SubLic_ASL [SubLic-ModifyAeronautical-ASL]", oneIteration
		Else
			RunAction "SubLic_ASL [SubLic-Aeronautical-ASL]", oneIteration
		End If
		
	End IF
ElseIf InStr(currentitem,"Cellular Mobile / WLL / RLAN / BWA") >0 Then
	wait(5)	
	If Environment("GSM") Then
		GSM_Click=GSM_Click+1
		'Print "Automation process for GSM"&GSM_Click
		VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Type micDwn
		'VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Type micDwn		
		 If GSM_Click>1 Then
			VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Type micDwn
		End If
		'msgbox VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").GetItem(i+1)
		'VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Select treeViewNode &"Cellular Mobile / WLL / RLAN / BWA "
		wait(5)
		'VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Activate treeViewNode &"Cellular Mobile / WLL / RLAN / BWA "
		'VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Activate i+1 
		'VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Type micDwn
		If GSM_Click=1 Then
	         VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Activate i+1 
		Else
			i=i-1
	        ' VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Activate i+1+1 
		End  If
		wait(15) '5

		If InStr(Environment("Modify_Sublicenses"),"GSM") >0 Then
			RunAction "GSM [Modify_GSM]", oneIteration
		Else
			RunAction "SubLic_GSM [SubLic-CellularMobile_WLL_RLAN_BWA-GSM]", oneIteration
		End If
	End IF
ElseIf InStr(currentitem,"TV / Radio Broadcast") >0 Then
	wait(5)
	If Environment("TBL") Then
		TBL_Click=TBL_Click+1
		'Print "Automation process for TBL"&TBL_Click
		VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Type micDwn
		
		 If TBL_Click>1 Then
			VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Type micDwn
		End If
		'VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Select treeViewNode &"TV / Radio Broadcast"
		wait(5)
		'VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Activate treeViewNode &"TV / Radio Broadcast"
		'VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Activate i+1 
		'i=i+1
		
		'msgbox VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").GetItem(i+1)
		
		If TBL_Click=1 Then
	         VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Activate i+1 
		Else
			i=i-1
	        ' VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Activate i+1+1 
		End  If
		wait(15) '5
			'VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Type micDwn 
			
	    If InStr(Environment("Modify_Sublicenses"),"TBL") >0 Then
			RunAction "TBL [Modify_TBL]", oneIteration
	    Else
	    	RunAction "SubLic_TBL [SubLic-TelevisionAndSoundBroadcasting-TBL]", oneIteration
	    End If
	End IF
ElseIf InStr(currentitem,"Land / Mobile") >0 Then
	wait(5)	
	If Environment("RCL") Then
		RCL_Click=RCL_Click+1
		'Print "Automation process for RCL- land"&RCL_Click
		VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Type micDwn
		 If RCL_Click>1 Then
			VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Type micDwn
		End If
		'VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Select treeViewNode &"Land / Mobile"
		wait(5)
		'VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Activate treeViewNode &"Land / Mobile"
		'VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Activate i+1 
		'VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Type micDwn
		If RCL_Click=1 Then
	         VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Activate i+1 
		Else
			i=i-1
	        ' VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Activate i+1+1 
		End  If
		wait(15) '5
		
	   If InStr(Environment("Modify_Sublicenses"),"RCL") >0 Then
			RunAction "RCL [Modify_RCL]", oneIteration
	   Else
	    	RunAction "SubLic_RCL [SubLic-LandMobile-RCL]", oneIteration
	   End If
	   
	End IF
ElseIf InStr(currentitem,"Amateur Radio") >0 or InStr(currentitem,"Amateur Licence") >0 Then
	wait(5)
	If Environment("AMR") Then
		AMR_Click=AMR_Click+1
		'Print "Automation not yet done for Amateur Radio"&AMR_Click
		VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Type micDwn
		wait(5)
		
'		If InStr(currentitem,"Amateur Radio") >0 Then
'			'VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Select treeViewNode &"Amateur Radio"
'			wait(1)
'			'VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Activate treeViewNode &"Amateur Radio"
'		Else
'			'VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Select treeViewNode &"Amateur Licence"
'			'wait(1)
'			'VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Activate treeViewNode &"Amateur Licence"		
'		End IF
		'VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Activate i+1 
		'VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Type micDwn
		If AMR_Click=1 Then
	         VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Activate i+1 
		Else
			i=i-1
	        ' VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Activate i+1+1 
		End  If
		wait(5)
	End IF
ElseIf InStr(currentitem,"Satellite") >0 Then
	wait(5)	
	If Environment("SAT") Then
		SAT_Click=SAT_Click+1
		'Print "Automation process for SAT"&SAT_Click
		VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Type micDwn
		'VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Select treeViewNode &"Satellite"
		wait(5)
		'VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Activate treeViewNode &"Satellite"
		'VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Activate i+1
		'VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Type micDwn
		If SAT_Click=1 Then
	         VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Activate i+1 
		Else
			i=i-1
	        ' VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Activate i+1+1 
		End  If
		wait(15) '5
		
		If InStr(Environment("Modify_Sublicenses"),"SAT") >0 Then
		    RunAction "SAT [Modify_SAT]", oneIteration
		else
			RunAction "SubLic_SAT [SubLic_SateliteServices]", oneIteration
		End If
		
	End IF
ElseIf InStr(currentitem,"Radio Transmitter Security") >0 Then
	wait(5)	
	If Environment("Alarm") Then
		Alarm_Click=Alarm_Click+1
		'Print "Automation process for Alarm"&Alarm_Click
		VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Type micDwn
		'VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Select treeViewNode &"Radio Transmitter Security"
		wait(5)
		'VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Activate treeViewNode &"Radio Transmitter Security"
		'VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Activate i+1 
		'VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Type micDwn
		If Alarm_Click=1 Then
	         VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Activate i+1 
		Else
			i=i-1
	        ' VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Activate i+1+1 
		End  If
		wait(15) '5
		
		If InStr(Environment("Modify_Sublicenses"),"Alarm") >0 Then
		     RunAction "Alarm [Modify_Alaram]", oneIteration
		Else
			RunAction "SubLic_ALARM [SubLic-RadioTransmitterSecurityAlarm-ALARM]", oneIteration
		End If
	End IF
ElseIf InStr(currentitem,"Ship Site") >0 Then
	wait(5)	
	If Environment("SSL") Then
		SSL_Click=SSL_Click+1
		'Print "Automation process for SSL"&SSL_Click
		VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Type micDwn
		'VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Select treeViewNode &"Ship Site"
		wait(5)
		'VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Activate treeViewNode &"Ship Site"
    	'VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Activate i+1 
		'VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Type micDwn
		If SSL_Click=1 Then
	         VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Activate i+1 
		Else
			i=i-1
	        ' VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Activate i+1+1 
		End  If
		wait(15) '5
		
		If InStr(Environment("Modify_Sublicenses"),"SSL") >0 Then
             RunAction "SSL [Modify_SSL]", oneIteration
        else
			RunAction "SubLic-SSL [SubLic-ShipSite-SSL]", oneIteration
		End If
	End IF
End If

Next














''=================
''VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Select "Application;Pending:2489;Application Detail" @@ hightlight id_;_1050674_;_script infofile_;_ZIP::ssf1.xml_;_
''VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Select "Application;Pending:2489;Application Detail;Fixed Link" @@ hightlight id_;_1050674_;_script infofile_;_ZIP::ssf2.xml_;_
''VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Select "Application;Pending:2489;Application Detail;Citizen Band Radio Licence" @@ hightlight id_;_1050674_;_script infofile_;_ZIP::ssf3.xml_;_
''VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Select "Application;Pending:2489;Application Detail;Aeronautical Sites" @@ hightlight id_;_1050674_;_script infofile_;_ZIP::ssf4.xml_;_
''VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Select "Application;Pending:2489;Application Detail;Cellular Mobile / WLL / RLAN / BWA " @@ hightlight id_;_1050674_;_script infofile_;_ZIP::ssf5.xml_;_
''VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Select "Application;Pending:2489;Application Detail;TV / Radio Broadcast" @@ hightlight id_;_1050674_;_script infofile_;_ZIP::ssf6.xml_;_
''VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Select "Application;Pending:2489;Application Detail;Land / Mobile" @@ hightlight id_;_1050674_;_script infofile_;_ZIP::ssf7.xml_;_
''VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Select "Application;Pending:2489;Application Detail;Amateur Radio" @@ hightlight id_;_1050674_;_script infofile_;_ZIP::ssf8.xml_;_
''VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Select "Application;Pending:2489;Application Detail;Satellite" @@ hightlight id_;_1050674_;_script infofile_;_ZIP::ssf9.xml_;_
''VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Select "Application;Pending:2489;Application Detail;Radio Transmitter Security" @@ hightlight id_;_1050674_;_script infofile_;_ZIP::ssf10.xml_;_
''VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Select "Application;Pending:2489;Application Detail;Ship Site" @@ hightlight id_;_1050674_;_script infofile_;_ZIP::ssf11.xml_;_
''
'
'AppNum = VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").GetItem(1)
''AppNum= Replace(AppNum,"Data Entry","")
'
'treeViewNode="Application;"& AppNum &";Application Detail"
''treeViewNode="Application;"& AppNum
'VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").ExpandAll(treeViewNode)
'wait(1)
''VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Activate "Application;Data Entry:4706;Application Detail;Fixed Link"
''VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Activate "Application;Data Entry:4706;Application Detail;Citizen Band Radio Licence"
''VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Activate "Application;Data Entry:4706;Application Detail;Aeronautical Sites" @@ hightlight id_;_1050674_;_script infofile_;_ZIP::ssf4.xml_;_
''VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Activate "Application;Data Entry:4706;Application Detail;Cellular Mobile / WLL / RLAN / BWA " @@ hightlight id_;_1050674_;_script infofile_;_ZIP::ssf5.xml_;_
''VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Activate "Application;Data Entry:4706;Application Detail;TV / Radio Broadcast" @@ hightlight id_;_1050674_;_script infofile_;_ZIP::ssf6.xml_;_
''VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Activate "Application;Data Entry:4706;Application Detail;Land / Mobile" @@ hightlight id_;_1050674_;_script infofile_;_ZIP::ssf7.xml_;_
''VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Activate "Application;Data Entry:4706;Application Detail;Amateur Radio" @@ hightlight id_;_1050674_;_script infofile_;_ZIP::ssf8.xml_;_
''VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Activate "Application;Data Entry:4706;Application Detail;Satellite" @@ hightlight id_;_1050674_;_script infofile_;_ZIP::ssf9.xml_;_
''VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Activate "Application;Data Entry:4706;Application Detail;Radio Transmitter Security" @@ hightlight id_;_1050674_;_script infofile_;_ZIP::ssf10.xml_;_
''VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Activate "Application;Data Entry:4706;Application Detail;Ship Site"	
''VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").ExpandAll("Application;Data Entry:4706;Application Detail")
'

''Set regEx = CreateObject("RegExp") ' Create regular expression.
'Set myRegEx = New regExp
'myRegEx.Pattern = treeViewNode &";Citizen Band Radio Licence" ' Set pattern.
'myRegEx.IgnoreCase = False ' Set case sensitivity.
'
'For i =1 to VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").GetItemsCount
'
'currentitem = VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").GetItem(i)
''strleng = InStr(currentitem,"Citizen") 
'' msgbox currentitem
'If InStr(currentitem,"Fixed Link") >0 Then
''VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").ExpandAll(treeViewNode)	
'	'VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Activate "Application;Data Entry:4706;Application Detail;Fixed Link"
'wait(1)	
'	VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Activate i+1 '"Application;Data Entry:4706;Application Detail;Citizen Band Radio Licence"
'	VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Type micDwn
'	'VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Select "Application;Pending:2489;Application Detail;Fixed Link"
'	Exit For
'ElseIf InStr(currentitem,"Citizen") >0 Then
'
''VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").ExpandAll(treeViewNode)	
'	'VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Activate "Application;Data Entry:4706;Application Detail;Fixed Link"
'wait(1)	
'	VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Activate i+1 '"Application;Data Entry:4706;Application Detail;Citizen Band Radio Licence"
'	VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Type micDwn
'	'VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Select "Application;Pending:2489;Application Detail;Fixed Link"
'	Exit For
'End If

'
''VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Activate "Application;Pending:2489;Application Detail;Fixed Link" @@ hightlight id_;_1050674_;_script infofile_;_ZIP::ssf2.xml_;_
'
''VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Activate "Application;Pending:2489;Application Detail;Citizen Band Radio Licence" @@ hightlight id_;_1050674_;_script infofile_;_ZIP::ssf3.xml_;_
''VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Activate "Application;Pending:2489;Application Detail;Aeronautical Sites" @@ hightlight id_;_1050674_;_script infofile_;_ZIP::ssf4.xml_;_
''VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Activate "Application;Pending:2489;Application Detail;Cellular Mobile / WLL / RLAN / BWA " @@ hightlight id_;_1050674_;_script infofile_;_ZIP::ssf5.xml_;_
''VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Activate "Application;Pending:2489;Application Detail;TV / Radio Broadcast" @@ hightlight id_;_1050674_;_script infofile_;_ZIP::ssf6.xml_;_
''VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Activate "Application;Pending:2489;Application Detail;Land / Mobile" @@ hightlight id_;_1050674_;_script infofile_;_ZIP::ssf7.xml_;_
''VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Activate "Application;Pending:2489;Application Detail;Amateur Radio" @@ hightlight id_;_1050674_;_script infofile_;_ZIP::ssf8.xml_;_
''VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Activate "Application;Pending:2489;Application Detail;Satellite" @@ hightlight id_;_1050674_;_script infofile_;_ZIP::ssf9.xml_;_
''VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Activate "Application;Pending:2489;Application Detail;Radio Transmitter Security" @@ hightlight id_;_1050674_;_script infofile_;_ZIP::ssf10.xml_;_
''VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Activate "Application;Pending:2489;Application Detail;Ship Site"
''
'
''VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Activate "Application;Pending:2489;Application Detail;Land / Mobile"
'
