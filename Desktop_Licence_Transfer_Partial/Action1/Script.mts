﻿wait(2)
blnFind=True
err.number = 0

datatable.ImportSheet ProjectFolderPath &"\Desktop_Input_Data_Sheet\Input_Data_Sheet.xls","Lic_Trans_Partial","Lic_Trans_Partial"
n=datatable.GetSheet("Lic_Trans_Partial").GetRowCount

'For i = 1 To n Step 1
With VbWindow("frmMDI")
	If n>0 Then
		datatable.SetCurrentRow(1)

		'=======Navigate to licence transfer partial screen
		On Error Resume Next:Err.Clear
		
		.WinMenu("ContextMenu").Select "Licence;Transfer;Partial"
		If Err.Number<>0 Then
			fn_NavigateToScreen "Licence;Transfer;Partial"
			Err.CLear
		End If
'		wait 20

		'=========Licenced or without licenced transfer option Selection

		With .VbWindow("frmSiteTransfer")
			If Datatable.value("LicenceChoice","Lic_Trans_Partial")="Licensed" Then
				If .VbRadioButton("Licensed").Exist(10) Then
					.VbRadioButton("Licensed").Set
				End If

				'from combobox
				intItemCount = .ActiveX("TCIComboBox.tcb").VbComboBox("cboClientFrom").GetItemsCount()

				For intIteration = 0 To intItemCount - 1
					If .ActiveX("TCIComboBox.tcb").VbComboBox("cboClientFrom").GetItem(intIteration) = Datatable.Value("ClientFrom","Lic_Trans_Partial") Then
						.ActiveX("TCIComboBox.tcb").VbComboBox("cboClientFrom").Select(intIteration)
						Exit For
						Else
'							wait(10)
							.ActiveX("TCIComboBox.tcb").VbComboBox("cboClientFrom").Select(intIteration)
					End If			
				Next
				Wait(5)
				'To Combobox

				intItemCount = .ActiveX("TCIComboBox.tcb_2").VbComboBox("cboClientTo").GetItemsCount()

				For intIteration = 0 To intItemCount - 1

					If .ActiveX("TCIComboBox.tcb_2").VbComboBox("cboClientTo").GetItem(intIteration) = Datatable.Value("ClientTo","Lic_Trans_Partial") Then

						.ActiveX("TCIComboBox.tcb_2").VbComboBox("cboClientTo").Select(intIteration)

						Exit For

						Else

							.ActiveX("TCIComboBox.tcb_2").VbComboBox("cboClientTo").Select(intIteration)

					End If

				Next	
				ElseIf Datatable.value("LicenceChoice","Lic_Trans_Partial")="without License" Then
					.VbRadioButton("without License").Set
					wait(5)
					.ActiveX("TCIComboBox.tcb").VbComboBox("cboClientFrom").Select Datatable.Value("ClientFrom","Lic_Trans_Partial")'"00046-12 / RCL"
					wait(10)
					.ActiveX("TCIComboBox.tcb").WinEdit("Edit").Type  micTab @@ hightlight id_;_854488_;_script infofile_;_ZIP::ssf29.xml_;_
					.VbButton("Select All").Type  micTab @@ hightlight id_;_723930_;_script infofile_;_ZIP::ssf30.xml_;_
					'VbWindow("frmMDI").VbWindow("frmSiteTransfer").ActiveX("TCIComboBox.tcb_2").VbComboBox("cboUnLicensedClientTo").Select "'Bruce Wayne    (# 02690-1)"
					wait(3)
					.ActiveX("TCIComboBox.tcb_2").VbComboBox("cboUnLicensedClientTo").Select Datatable.Value("UnlicenceTo","Lic_Trans_Partial")'UnlicenceTo ' "00Bruce Reddy    (# 02643-1)" @@ hightlight id_;_1050042_;_script infofile_;_ZIP::ssf26.xml_;_
					.ActiveX("TCIComboBox.tcb_2").WinEdit("Edit").Type  micTab @@ hightlight id_;_265294_;_script infofile_;_ZIP::ssf32.xml_;_

					'VbWindow("frmMDI").InsightObject("InsightObject_4").Click

					.ActiveX("TCIComboBox.tcb_2").VbComboBox("cboUnLicensedClientTo").Select Datatable.Value("UnlicenceTo","Lic_Trans_Partial")'UnlicenceTo ' "00Bruce Reddy    (# 02643-1)" @@ hightlight id_;_1050042_;_script infofile_;_ZIP::ssf26.xml_;_

					'VbWindow("frmMDI").VbWindow("frmSiteTransfer").ActiveX("TCIComboBox.tcb_2").VbComboBox("cboClientTo").Select 4'Datatable.Value("ClientTo","Lic_Trans_Partial")'"00056-1 / AMR"

			End If




			'VbWindow("frmMDI").VbWindow("frmSiteTransfer").VbRadioButton("without License").Set
			'VbWindow("frmMDI").VbWindow("frmSiteTransfer").VbRadioButton("Licensed").Set

			'========Source From
			'VbWindow("frmMDI").VbWindow("frmSiteTransfer").ActiveX("TCIComboBox.tcb").VbComboBox("cboClientFrom").Select 3 'Datatable.Value("ClientFrom","Lic_Trans_Partial")'"00046-12 / RCL"

			'wait(10)
			'========Destination TO
			'VbWindow("frmMDI").VbWindow("frmSiteTransfer").ActiveX("TCIComboBox.tcb_2").VbComboBox("cboClientTo").Select 4'Datatable.Value("ClientTo","Lic_Trans_Partial")'"00056-1 / AMR"
			'wait(10)
			'VbWindow("frmMDI").VbWindow("frmSiteTransfer").AcxTable("List of Sites").SelectColumn 1
			'VbWindow("frmMDI").VbWindow("frmSiteTransfer").AcxTable("List of Sites").SelectCell 1,1

			'=======Select All check boxes

			.VbButton("Select All").Click @@ hightlight id_;_198352_;_script infofile_;_ZIP::ssf11.xml_;_
			wait(5)

			'=======Transfer

			.VbButton("Transfer").Click @@ hightlight id_;_132878_;_script infofile_;_ZIP::ssf12.xml_;_
		End With
		wait(5)

		'========Confirmation

		With	.Dialog("#32770")
			.WinButton("Yes").Click @@ hightlight id_;_263996_;_script infofile_;_ZIP::ssf13.xml_;_
			.WinButton("OK").Click @@ hightlight id_;_198452_;_script infofile_;_ZIP::ssf14.xml_;_
		End With
		wait(7)

		'========Close Application

		.VbWindow("frmSiteTransfer").VbButton("Close").Click @@ hightlight id_;_132880_;_script infofile_;_ZIP::ssf15.xml_;_

		strTestCaseName = Environment.Value("TestName")
		If err.number = 0 Then
			blnFind = True
			Else
				blnFind = False
		End If

		UpdateTestCaseStatusInExcel strTestCaseName, blnFind
	End If
End With






'		
'		
'		intItemCount = VbWindow("frmMDI").VbWindow("frmSiteTransfer").ActiveX("TCIComboBox.tcb").VbComboBox("cboClientFrom").GetItemsCount()
'		
'		For intIteration = 0 To intItemCount - 1
'		
'		If VbWindow("frmMDI").VbWindow("frmSiteTransfer").ActiveX("TCIComboBox.tcb").VbComboBox("cboClientFrom").GetItem(intIteration) = "00089-1 / AMR" Then
'			
'			VbWindow("frmMDI").VbWindow("frmSiteTransfer").ActiveX("TCIComboBox.tcb").VbComboBox("cboClientFrom").Select(intIteration)
'			
'			Exit For
'			
'		Else
'		
'			VbWindow("frmMDI").VbWindow("frmSiteTransfer").ActiveX("TCIComboBox.tcb").VbComboBox("cboClientFrom").Select(intIteration)
'			
'		End If
'			
'		Next
'		
'		
'		
'		intItemCount = VbWindow("frmMDI").VbWindow("frmSiteTransfer").ActiveX("TCIComboBox.tcb_2").VbComboBox("cboClientTo").GetItemsCount()
'		
'		For intIteration = 0 To intItemCount - 1
'		
'		If VbWindow("frmMDI").VbWindow("frmSiteTransfer").ActiveX("TCIComboBox.tcb_2").VbComboBox("cboClientTo").GetItem(intIteration) = "00042-1 /" Then
'			
'			VbWindow("frmMDI").VbWindow("frmSiteTransfer").ActiveX("TCIComboBox.tcb_2").VbComboBox("cboClientTo").Select(intIteration)
'			
'			Exit For
'			
'		Else
'		
'			VbWindow("frmMDI").VbWindow("frmSiteTransfer").ActiveX("TCIComboBox.tcb_2").VbComboBox("cboClientTo").Select(intIteration)
'			
'		End If
'			
'		Next
'		
'			'VbWindow("frmMDI").VbWindow("frmSiteTransfer").ActiveX("TCIComboBox.tcb_2").VbComboBox("cboClientTo").Select Datatable.Value("ClientTo","Lic_Trans_Partial")'"00056-1 / AMR"
'
'		
'		'VbWindow("frmMDI").VbWindow("frmSiteTransfer").ActiveX("TCIComboBox.tcb").VbComboBox("cboClientFrom").Select "00089-1 / AMR"
