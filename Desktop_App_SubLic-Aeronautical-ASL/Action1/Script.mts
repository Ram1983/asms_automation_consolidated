﻿blnFind=True
err.number = 0
datatable.ImportSheet ProjectFolderPath &"\Desktop_Input_Data_Sheet\Input_Data_Sheet.xls","ASL","ASL"
n=datatable.GetSheet("ASL").GetRowCount()

'For i = 1 To n Step 1
With	VbWindow("frmMDI").VbWindow("frmAircraft")
	If n>0 Then
		datatable.SetCurrentRow(1)	

		'========Aeronautical selection screen

		If (Datatable.Value("StationType","ASL") = "Aircraft station") Then
			.ActiveX("SSTab").VbRadioButton("Aircraft station").Set
			ElseIf (Datatable.Value("StationType","ASL") = "Aeronautical station") Then
				.ActiveX("SSTab").VbRadioButton("Aeronautical station").Set
		End If

		wait(2) @@ hightlight id_;_135404_;_script infofile_;_ZIP::ssf107.xml_;_
		.ActiveX("SSTab").VbEdit("txtStationName").Set Datatable.value("StationName","ASL")& "" & fnRandomNumber(3) @@ hightlight id_;_135452_;_script infofile_;_ZIP::ssf104.xml_;_

		'============Aeronautical selection first and else aircraft selection
		If (Datatable.Value("StationType","ASL") = "Aeronautical station") Then

			.ActiveX("SSTab").ActiveX("TCIComboBox.tcb").VbComboBox("cboSiteCity").Select(0)


			'VbWindow("vbname:=frmMDI").VbWindow("vbname:=frmAircraft,nativeclass:=ThunderRT6FormDC").ActiveX("acx_name:=SSTab,nativeclass:=SSTabCtlWndClass,progid:=TabDlg.SSTab.1").ActiveX("acx_name:=TCIComboBox.tcb,nativeclass:=ThunderRT6UserControlDC,progid:=TCIComboBox.tcb").VbComboBox("vbname:=cboSiteCity").Highlight


			'VbWindow("frmMDI").WinObject("MDIClient").VScroll micLineNext, 1
			'VbWindow("frmMDI").WinObject("MDIClient").VScroll micLineNext, 1
			'VbWindow("frmMDI").WinObject("MDIClient").VScroll micLineNext, 1
			'VbWindow("frmMDI").WinObject("MDIClient").VScroll micLineNext, 1
			'===============Saving Aeronautical site
			.ActiveX("SSTab").VbButton("Save").Click @@ hightlight id_;_395730_;_script infofile_;_ZIP::ssf192.xml_;_

			else				 If (Datatable.Value("StationType","ASL") = "Aircraft station") Then
					.ActiveX("SSTab_2").VbEdit("txtRegistration").Set datatable.value("Reg","ASL")& "" & fnRandomNumber(3) @@ hightlight id_;_135404_;_script infofile_;_ZIP::ssf106.xml_;_
					wait(2)
					.ActiveX("SSTab").VbEdit("txtStationName").Set datatable.value("StationName","ASL")& "" & fnRandomNumber(3) @@ hightlight id_;_789110_;_script infofile_;_ZIP::ssf170.xml_;_
					.ActiveX("SSTab").VbEdit("txtStationName").Type  micTab @@ hightlight id_;_789110_;_script infofile_;_ZIP::ssf171.xml_;_
					.ActiveX("SSTab").VbEdit("txtSiteRegNum").Set Datatable.Value("StRegNum","ASL")& "" & fnRandomNumber(3)
					wait(2)
					.ActiveX("SSTab").VbEdit("txtSiteRegNum").Type  micTab @@ hightlight id_;_68150_;_script infofile_;_ZIP::ssf172.xml_;_
					.ActiveX("SSTab").VbComboBox("cboCountry").Type  micTab @@ hightlight id_;_68186_;_script infofile_;_ZIP::ssf173.xml_;_
					.ActiveX("SSTab").VbComboBox("cboHost2").Type  micTab @@ hightlight id_;_68202_;_script infofile_;_ZIP::ssf174.xml_;_
					.ActiveX("SSTab").VbComboBox("cboAircraftType").Select(1) '"Airbus A.300" @@ hightlight id_;_68160_;_script infofile_;_ZIP::ssf175.xml_;_
					wait(2)


					'......Country selection advanced implemention
					'VbWindow("frmMDI").VbWindow("frmAircraft").ActiveX("SSTab").Click 999,126
					'VbWindow("frmMDI").VbWindow("frmAircraft").ActiveX("SSTab").ActiveX("TCIComboBox.tcb_2").VbComboBox("cboAircraftType").Select(0)

					.ActiveX("SSTab").VbEdit("txtWeightCat").Set Datatable.Value("AeroWtCat","ASL")
					wait(2)

					'VbWindow("frmMDI").VbWindow("frmAircraft").ActiveX("SSTab").WinObject("28-02-2020").Drag 73,6
					'VbWindow("frmMDI").VbWindow("frmAircraft").ActiveX("SSTab").VbFrame("Frame1").Drop 809,136



					'VbWindow("frmMDI").VbWindow("frmAircraft").ActiveX("SSTab").ActiveX("MaskEdBox").Drag 65,10
					'VbWindow("frmMDI").VbWindow("frmAircraft").ActiveX("SSTab").VbFrame("Frame1").Drop 828,131
					'======VbWindow("frmMDI").VbWindow("frmAircraft").ActiveX("SSTab").ActiveX("MaskEdBox").Type Datatable.Value("AeroDate","ASL")

				End IF


				.ActiveX("SSTab").VbEditor("txtRemark").SetCaretPos 0,0 @@ hightlight id_;_461416_;_script infofile_;_ZIP::ssf78.xml_;_
				.ActiveX("SSTab").VbEditor("txtRemark").Type Datatable.value("AeroNotes","ASL") @@ hightlight id_;_461416_;_script infofile_;_ZIP::ssf79.xml_;_


				'
				''..... Navigate to OfficiaL Use Tab....


				'VbWindow("frmMDI").VbWindow("frmAircraft").ActiveX("SSTab").Click 349,14


				'If (Datatable.Value("StationType","ASL") = "Aircraft station") Then
				'======================Automatically data values displayed station and service class 
				'VbWindow("frmMDI").VbWindow("frmAircraft").ActiveX("SSTab").ActiveX("TCIComboBox.tcb_6").VbComboBox("cboServiceClass").Select(0) '"AERONAUTICAL MOBILE-SATELLITE"
				'VbWindow("frmMDI").VbWindow("frmAircraft").ActiveX("SSTab").ActiveX("TCIComboBox.tcb_6").VbComboBox("cboServiceClass").Type  micTab
				'VbWindow("frmMDI").VbWindow("frmAircraft").ActiveX("SSTab").ActiveX("TCIComboBox.tcb_7").VbComboBox("cboStationClass").Select(0) '"Mobile Station"
				'VbWindow("frmMDI").VbWindow("frmAircraft").ActiveX("SSTab").ActiveX("TCIComboBox.tcb_7").VbComboBox("cboStationClass").Type  micTab
				'VbWindow("frmMDI").VbWindow("frmAircraft").ActiveX("SSTab").ActiveX("TCIComboBox.tcb").VbComboBox("cboServiceNature").Select(0)' "Fixed station used for press transmission"
				'VbWindow("frmMDI").VbWindow("frmAircraft").ActiveX("SSTab").ActiveX("TCIComboBox.tcb").VbComboBox("cboServiceNature").Type  micTab
				'
				''
				''VbWindow("frmMDI").VbWindow("frmAircraft").ActiveX("SSTab").ActiveX("TCIComboBox.tcb").VbComboBox("cboServiceClass").Select(0)
				''wait(5)
				''VbWindow("frmMDI").VbWindow("frmAircraft").ActiveX("SSTab").ActiveX("TCIComboBox.tcb_3").VbComboBox("cboStationClass").Select(5)
				''wait(2)
				''VbWindow("frmMDI").VbWindow("frmAircraft").ActiveX("SSTab").ActiveX("TCIComboBox.tcb_2").VbComboBox("cboServiceNature").Select(4)
				wait(2)
				'''''''VbWindow("frmMDI").VbWindow("frmAircraft").ActiveX("SSTab").VbEdit("txtStaff").Set "1237"

				''ElseIf (Datatable.Value("StationType","ASL") = "Aeronautical station") Then	
				''
				''VbWindow("frmMDI").VbWindow("frmAircraft").ActiveX("SSTab").ActiveX("TCIComboBox.tcb_2").VbComboBox("cboServiceClass").Select (4) '"AERONAUTICAL MOBILE ( R)"
				''VbWindow("frmMDI").VbWindow("frmAircraft").ActiveX("SSTab").ActiveX("TCIComboBox.tcb_4").VbComboBox("cboStationClass").Select (5) ' "Aeronautical station in the aeronautical mobile (OR) svc"
				''VbWindow("frmMDI").VbWindow("frmAircraft").ActiveX("SSTab").ActiveX("TCIComboBox.tcb_5").VbComboBox("cboServiceNature").Select (4) '"Radio direction-finding station"
				''VbWindow("frmMDI").VbWindow("frmAircraft").ActiveX("SSTab").VbEdit("txtStaff").Set "DWP1"
				''
				''End IF

				'=====back to tab
				'VbWindow("frmMDI").VbWindow("frmAircraft").ActiveX("SSTab").Click 126,13

				'......Back Navigate to Aeronautical details tab
				'VbWindow("frmMDI").VbWindow("frmAircraft").ActiveX("SSTab").Click 46,7
				'VbWindow("frmMDI").WinObject("MDIClient").VScroll micLineNext, 1
				'VbWindow("frmMDI").WinObject("MDIClient").VScroll micLineNext, 1
				'VbWindow("frmMDI").WinObject("MDIClient").VScroll micLineNext, 1

				'VbWindow("frmMDI").VbWindow("frmAircraft").ActiveX("SSTab").VbEditor("txtRemark").Type  micTab




				'=================Saaving Site
				.ActiveX("SSTab").VbButton("Save").Click @@ hightlight id_;_70464_;_script infofile_;_ZIP::ssf32.xml_;_


		End if

		'=======================Adding equipment

		If .ActiveX("SSTab").VbButton("Add Equipment").Exist Then
			Wait 8
			.ActiveX("SSTab").VbButton("Add Equipment").Click
		End If @@ hightlight id_;_70474_;_script infofile_;_ZIP::ssf35.xml_;_



		'msgbox (datatable.value("Equipment","ASL"))

		If datatable.value("Equipment","ASL")="Transceiver" Then

			If .ActiveX("SSTab_2").ActiveX("SSTab").VbRadioButton("Transceiver").exist Then
				.ActiveX("SSTab_2").ActiveX("SSTab").VbRadioButton("Transceiver").Set
			End If
			ElseIf datatable.value("Equipment","ASL")="Transmitter" Then

				.ActiveX("SSTab_2").ActiveX("SSTab").VbRadioButton("Transmitter").Set @@ hightlight id_;_199388_;_script infofile_;_ZIP::ssf227.xml_;_


			ElseIf datatable.value("Equipment","ASL")="Receiver" Then
				if .ActiveX("SSTab_2").ActiveX("SSTab").VbRadioButton("Receiver").Exist then
					.ActiveX("SSTab_2").ActiveX("SSTab").VbRadioButton("Receiver").Set
				end if
		end if

		'=============If transceiver
		If datatable.value("Equipment","ASL")="Transceiver" Then
			'====Equipment type
			.ActiveX("SSTab_2").ActiveX("SSTab").ActiveX("TCIComboBox.tcb").VbComboBox("cboEquipType").Select (0) '"Aircraft earth station" @@ hightlight id_;_70646_;_script infofile_;_ZIP::ssf38.xml_;_
			'========Approvalnumber
			.ActiveX("SSTab_2").ActiveX("SSTab").ActiveX("TCIComboBox.tcb_2").VbComboBox("txtApprovalNum").Select (0) '"103"
			'====Equipment serial number
			.ActiveX("SSTab_2").ActiveX("SSTab").VbEdit("txtEquipSerial").Set Datatable.value("EquipSerialNum","ASL") & "" & fnRandomNumber(5)'"7896"
			'VbWindow("frmMDI").VbWindow("frmAircraft").ActiveX("SSTab_2").ActiveX("SSTab").VbRadioButton("dBm").Set
			'VbWindow("frmMDI").VbWindow("frmAircraft").ActiveX("SSTab_2").ActiveX("SSTab").VbRadioButton("Watts").Set
			.ActiveX("SSTab_2").ActiveX("SSTab").ActiveX("MaskEdBox").Drag 58,10 @@ hightlight id_;_70632_;_script infofile_;_ZIP::ssf45.xml_;_
			.ActiveX("SSTab_2").ActiveX("SSTab").VbFrame("frmEquipment(0)").Drop 809,67
			'====Modulation
			'VbWindow("frmMDI").VbWindow("frmAircraft").ActiveX("SSTab_2").ActiveX("SSTab").ActiveX("TCIComboBox.tcb_6").VbComboBox("cboModulation").Select (0)
			'=====Selectivity field
			.ActiveX("SSTab_2").ActiveX("SSTab").ActiveX("MaskEdBox_2").Click 21,17 @@ hightlight id_;_70634_;_script infofile_;_ZIP::ssf48.xml_;_
			.ActiveX("SSTab_2").ActiveX("SSTab").ActiveX("MaskEdBox_2").Drag 23,12 @@ hightlight id_;_70634_;_script infofile_;_ZIP::ssf49.xml_;_
			.ActiveX("SSTab_2").ActiveX("SSTab").VbFrame("frmEquipment(0)").Drop 815,211 @@ hightlight id_;_70484_;_script infofile_;_ZIP::ssf50.xml_;_
			.ActiveX("SSTab_2").ActiveX("SSTab").VbFrame("frmEquipment(0)").Click 651,216 @@ hightlight id_;_70484_;_script infofile_;_ZIP::ssf51.xml_;_
			.ActiveX("SSTab_2").ActiveX("SSTab").ActiveX("MaskEdBox_2").Type  datatable.value("Selectivity","ASL") '"10"
			.ActiveX("SSTab_2").ActiveX("SSTab").ActiveX("TCIComboBox.tcb_7").VbComboBox("cboTransmissionType").Select (0) '"ALARM" @@ hightlight id_;_70606_;_script infofile_;_ZIP::ssf56.xml_;_
			.ActiveX("SSTab_2").ActiveX("SSTab").ActiveX("TCIComboBox.tcb_8").VbComboBox("cboOperationMethod").Select (0)   '"Dual Frequency Operation (Duplex)"
			'======offcial use 
			'====callsign
			.ActiveX("SSTab_2").ActiveX("SSTab").VbButton("VbButton").Click @@ hightlight id_;_70556_;_script infofile_;_ZIP::ssf58.xml_;_
			'=====Equipment code
			.ActiveX("SSTab_2").ActiveX("SSTab").ActiveX("TCIComboBox.tcb_9").VbComboBox("cboEquipFeeCode").Select (0) '"Aeronautical Mobile (Aircraft) station"
			'==========Navigate to Antenna Tab

			.ActiveX("SSTab_2").ActiveX("SSTab").Click 511,9 @@ hightlight id_;_723422_;_script infofile_;_ZIP::ssf218.xml_;_

			.ActiveX("SSTab_2").ActiveX("SSTab").ActiveX("MaskEdBox_3").Drag 53,6 @@ hightlight id_;_1444316_;_script infofile_;_ZIP::ssf219.xml_;_
			.ActiveX("SSTab_2").ActiveX("SSTab").VbFrame("Frame5").Drop 743,148 @@ hightlight id_;_1181512_;_script infofile_;_ZIP::ssf220.xml_;_
			.ActiveX("SSTab_2").ActiveX("SSTab").ActiveX("MaskEdBox_3").Type Datatable.value("HVratio","ASL") @@ hightlight id_;_1444316_;_script infofile_;_ZIP::ssf221.xml_;_
			.ActiveX("SSTab_2").ActiveX("SSTab").ActiveX("MaskEdBox_3").Type  micTab @@ hightlight id_;_1444316_;_script infofile_;_ZIP::ssf222.xml_;_
			.ActiveX("SSTab_2").ActiveX("SSTab").ActiveX("MaskEdBox_2").Drag 25,5 @@ hightlight id_;_920078_;_script infofile_;_ZIP::ssf223.xml_;_
			.ActiveX("SSTab_2").ActiveX("SSTab").VbFrame("Frame5").Drop 795,185 @@ hightlight id_;_1181512_;_script infofile_;_ZIP::ssf224.xml_;_
			.ActiveX("SSTab_2").ActiveX("SSTab").ActiveX("MaskEdBox_2").Type Datatable.value("PolaAngle","ASL") @@ hightlight id_;_920078_;_script infofile_;_ZIP::ssf225.xml_;_
			.ActiveX("SSTab_2").ActiveX("SSTab").ActiveX("MaskEdBox_2").Type  micTab @@ hightlight id_;_920078_;_script infofile_;_ZIP::ssf226.xml_;_


			'..Saving Equipment

			'VbWindow("frmMDI").VbWindow("frmAircraft").ActiveX("SSTab_2").VbButton("Save Equipment").Click


			'==========transmitter

			ElseIf datatable.value("Equipment","ASL")="Transmitter" Then
				'====Equipment type
				.ActiveX("SSTab_2").ActiveX("SSTab").ActiveX("TCIComboBox.tcb").VbComboBox("cboEquipType").Select (0) '"Aircraft earth station" @@ hightlight id_;_70646_;_script infofile_;_ZIP::ssf38.xml_;_
				'========Approvalnumber
				.ActiveX("SSTab_2").ActiveX("SSTab").ActiveX("TCIComboBox.tcb_2").VbComboBox("txtApprovalNum").Select (1) '"103"
				'====Equipment serial number
				.ActiveX("SSTab_2").ActiveX("SSTab").VbEdit("txtEquipSerial").Set Datatable.value("EquipSerialNum","ASL") & "" & fnRandomNumber(5)'"7896"
				'VbWindow("frmMDI").VbWindow("frmAircraft").ActiveX("SSTab_2").ActiveX("SSTab").VbRadioButton("dBm").Set
				'VbWindow("frmMDI").VbWindow("frmAircraft").ActiveX("SSTab_2").ActiveX("SSTab").VbRadioButton("Watts").Set
'				.ActiveX("SSTab_2").ActiveX("SSTab").ActiveX("MaskEdBox").Drag 58,10 @@ hightlight id_;_70632_;_script infofile_;_ZIP::ssf45.xml_;_
				.ActiveX("SSTab_2").ActiveX("SSTab").ActiveX("MaskEdBox").Object.Text=RandomNumber(1,10)
'				.ActiveX("SSTab_2").ActiveX("SSTab").VbFrame("frmEquipment(0)").Drop 809,67
				'====Modulation
				'VbWindow("frmMDI").VbWindow("frmAircraft").ActiveX("SSTab_2").ActiveX("SSTab").ActiveX("TCIComboBox.tcb_6").VbComboBox("cboModulation").Select (0)
				'=====Selectivity field

				.ActiveX("SSTab_2").ActiveX("SSTab").ActiveX("TCIComboBox.tcb_7").VbComboBox("cboTransmissionType").Select (0) '"ALARM" @@ hightlight id_;_70606_;_script infofile_;_ZIP::ssf56.xml_;_
				.ActiveX("SSTab_2").ActiveX("SSTab").ActiveX("TCIComboBox.tcb_8").VbComboBox("cboOperationMethod").Select (0)   '"Dual Frequency Operation (Duplex)"
				'======offcial use 
				'====callsign
				.ActiveX("SSTab_2").ActiveX("SSTab").VbButton("VbButton").Click @@ hightlight id_;_70556_;_script infofile_;_ZIP::ssf58.xml_;_
				'=====Equipment code
				.ActiveX("SSTab_2").ActiveX("SSTab").ActiveX("TCIComboBox.tcb_9").VbComboBox("cboEquipFeeCode").Select (0) '"Aeronautical Mobile (Aircraft) station"



				'==========Navigate to Antenna Tab

				.ActiveX("SSTab_2").ActiveX("SSTab").Click 511,9 @@ hightlight id_;_723422_;_script infofile_;_ZIP::ssf218.xml_;_


				'------if notrequired testing phase
				.ActiveX("SSTab_2").ActiveX("SSTab").ActiveX("MaskEdBox_5").Drag 39,11 @@ hightlight id_;_2231044_;_script infofile_;_ZIP::ssf210.xml_;_
				.ActiveX("SSTab_2").ActiveX("SSTab").VbFrame("Frame5").Drop 763,157 @@ hightlight id_;_1837588_;_script infofile_;_ZIP::ssf211.xml_;_
				.ActiveX("SSTab_2").ActiveX("SSTab").ActiveX("MaskEdBox_5").Type Datatable.value("HVratio","ASL") @@ hightlight id_;_2231044_;_script infofile_;_ZIP::ssf212.xml_;_
				.ActiveX("SSTab_2").ActiveX("SSTab").ActiveX("MaskEdBox_5").Type  micTab @@ hightlight id_;_2231044_;_script infofile_;_ZIP::ssf213.xml_;_
				.ActiveX("SSTab_2").ActiveX("SSTab").ActiveX("MaskEdBox_4").Drag 48,9 @@ hightlight id_;_984996_;_script infofile_;_ZIP::ssf214.xml_;_
				.ActiveX("SSTab_2").ActiveX("SSTab").VbFrame("Frame5").Drop 735,179 @@ hightlight id_;_1837588_;_script infofile_;_ZIP::ssf215.xml_;_
				.ActiveX("SSTab_2").ActiveX("SSTab").ActiveX("MaskEdBox_4").Type Datatable.value("PolaAngle","ASL") @@ hightlight id_;_984996_;_script infofile_;_ZIP::ssf216.xml_;_
				.ActiveX("SSTab_2").ActiveX("SSTab").ActiveX("MaskEdBox_4").Type  micTab @@ hightlight id_;_984996_;_script infofile_;_ZIP::ssf217.xml_;_
				'''
				'..Saving Equipment

				'VbWindow("frmMDI").VbWindow("frmAircraft").ActiveX("SSTab_2").VbButton("Save Equipment").Click


				'=======receiver option

			ElseIf datatable.value("Equipment","ASL")="Receiver" Then
				'====Equipment type
				.ActiveX("SSTab_2").ActiveX("SSTab").ActiveX("TCIComboBox.tcb").VbComboBox("cboEquipType").Select (0) '"Aircraft earth station" @@ hightlight id_;_70646_;_script infofile_;_ZIP::ssf38.xml_;_
				'========Approvalnumber
				.ActiveX("SSTab_2").ActiveX("SSTab").ActiveX("TCIComboBox.tcb_2").VbComboBox("txtApprovalNum").Select (0) '"103"
				'====Equipment serial number
				.ActiveX("SSTab_2").ActiveX("SSTab").VbEdit("txtEquipSerial").Set Datatable.value("EquipSerialNum","ASL") & "" & fnRandomNumber(5)'"7896"

				.ActiveX("SSTab_2").ActiveX("SSTab").ActiveX("MaskEdBox_4").Drag 43,14 @@ hightlight id_;_1116848_;_script infofile_;_ZIP::ssf202.xml_;_
				.ActiveX("SSTab_2").ActiveX("SSTab").VbFrame("frmEquipment(0)").Drop 850,221 @@ hightlight id_;_1050960_;_script infofile_;_ZIP::ssf203.xml_;_
				.ActiveX("SSTab_2").ActiveX("SSTab").ActiveX("MaskEdBox_4").Type datatable.value("Selectivity","ASL") @@ hightlight id_;_1116848_;_script infofile_;_ZIP::ssf204.xml_;_
				.ActiveX("SSTab_2").ActiveX("SSTab").ActiveX("MaskEdBox_4").Type  micTab @@ hightlight id_;_1116848_;_script infofile_;_ZIP::ssf205.xml_;_
				.ActiveX("SSTab_2").ActiveX("SSTab").ActiveX("MaskEdBox_4").Click 37,12 @@ hightlight id_;_1116848_;_script infofile_;_ZIP::ssf206.xml_;_
				.ActiveX("SSTab_2").ActiveX("SSTab").ActiveX("TCIComboBox.tcb_6").VbComboBox("cboTransmissionType").Select(0) '"ALARM" @@ hightlight id_;_985740_;_script infofile_;_ZIP::ssf207.xml_;_
				.ActiveX("SSTab_2").ActiveX("SSTab").ActiveX("TCIComboBox.tcb_7").VbComboBox("cboOperationMethod").Select(0) '"Dual Frequency Operation (Duplex)" @@ hightlight id_;_920478_;_script infofile_;_ZIP::ssf208.xml_;_
				.ActiveX("SSTab_2").ActiveX("SSTab").ActiveX("TCIComboBox.tcb_8").VbComboBox("cboEquipFeeCode").Select(0) 
				'==========Navigate to Antenna Tab

				.ActiveX("SSTab_2").ActiveX("SSTab").Click 511,9 @@ hightlight id_;_723422_;_script infofile_;_ZIP::ssf218.xml_;_


				.ActiveX("SSTab_2").ActiveX("SSTab").ActiveX("MaskEdBox_5").Drag 39,11 @@ hightlight id_;_2231044_;_script infofile_;_ZIP::ssf210.xml_;_
				.ActiveX("SSTab_2").ActiveX("SSTab").VbFrame("Frame5").Drop 763,157 @@ hightlight id_;_1837588_;_script infofile_;_ZIP::ssf211.xml_;_
				.ActiveX("SSTab_2").ActiveX("SSTab").ActiveX("MaskEdBox_5").Type Datatable.value("HVratio","ASL") @@ hightlight id_;_2231044_;_script infofile_;_ZIP::ssf212.xml_;_
				.ActiveX("SSTab_2").ActiveX("SSTab").ActiveX("MaskEdBox_5").Type  micTab @@ hightlight id_;_2231044_;_script infofile_;_ZIP::ssf213.xml_;_
				.ActiveX("SSTab_2").ActiveX("SSTab").ActiveX("MaskEdBox_4").Drag 48,9 @@ hightlight id_;_984996_;_script infofile_;_ZIP::ssf214.xml_;_
				.ActiveX("SSTab_2").ActiveX("SSTab").VbFrame("Frame5").Drop 735,179 @@ hightlight id_;_1837588_;_script infofile_;_ZIP::ssf215.xml_;_
				.ActiveX("SSTab_2").ActiveX("SSTab").ActiveX("MaskEdBox_4").Type Datatable.value("PolaAngle","ASL") @@ hightlight id_;_984996_;_script infofile_;_ZIP::ssf216.xml_;_
				.ActiveX("SSTab_2").ActiveX("SSTab").ActiveX("MaskEdBox_4").Type  micTab @@ hightlight id_;_984996_;_script infofile_;_ZIP::ssf217.xml_;_


		End If



		If (Datatable.Value("StationType","ASL") = "Aeronautical station") Then
			'	VbWindow("frmMDI").InsightObject("InsightObject_2").Click

			.ActiveX("SSTab_2").ActiveX("SSTab").Click 793,13 @@ hightlight id_;_69776_;_script infofile_;_ZIP::ssf298.xml_;_
			wait(2)

			'VbWindow("frmMDI").VbWindow("frmAircraft").ActiveX("SSTab_2").ActiveX("SSTab").ActiveX("TCIComboBox.tcb_7").VbComboBox("cboFeed").Select(0) '"Coaxial line"
			If not datatable.value("Equipment","ASL")="Receiver" Then


				'V'bWindow("frmMDI").VbWindow("frmAircraft").ActiveX("SSTab_2").ActiveX("SSTab").ActiveX("TCIComboBox.tcb_8").VbComboBox("cboFeed").Select(0) '"Coaxial line"
				If datatable.value("Equipment","ASL")="Transmitter" and datatable("SpecificRadio","ASL")="Cable" Then

					.ActiveX("SSTab_2").ActiveX("SSTab").VbRadioButton("Cable").Set

					.ActiveX("SSTab_2").ActiveX("SSTab").ActiveX("TCIComboBox.tcb_9").VbComboBox("cboCable").Select(0) '"ANDREW - FSJ1-50A" @@ hightlight id_;_133948_;_script infofile_;_ZIP::ssf242.xml_;_
					.ActiveX("SSTab_2").ActiveX("SSTab").ActiveX("MaskEdBox_5").Click 160,11 @@ hightlight id_;_133940_;_script infofile_;_ZIP::ssf243.xml_;_
					.ActiveX("SSTab_2").ActiveX("SSTab").ActiveX("MaskEdBox_5").Type Datatable.value("FeedLen","ASL") @@ hightlight id_;_133940_;_script infofile_;_ZIP::ssf244.xml_;_
					.ActiveX("SSTab_2").ActiveX("SSTab").ActiveX("MaskEdBox_5").Type  micTab @@ hightlight id_;_133940_;_script infofile_;_ZIP::ssf245.xml_;_
					.ActiveX("SSTab_2").ActiveX("SSTab").ActiveX("MaskEdBox_4").Drag 93,12 @@ hightlight id_;_133936_;_script infofile_;_ZIP::ssf246.xml_;_
					.ActiveX("SSTab_2").ActiveX("SSTab").VbFrame("FrameAttenuation").Drop 201,113 @@ hightlight id_;_133976_;_script infofile_;_ZIP::ssf247.xml_;_
					.ActiveX("SSTab_2").ActiveX("SSTab").ActiveX("MaskEdBox_4").Type Datatable.value("Lineatt","ASL") @@ hightlight id_;_133936_;_script infofile_;_ZIP::ssf248.xml_;_
					.ActiveX("SSTab_2").ActiveX("SSTab").ActiveX("MaskEdBox_4").Type  micTab @@ hightlight id_;_133936_;_script infofile_;_ZIP::ssf249.xml_;_
					.ActiveX("SSTab_2").ActiveX("SSTab").ActiveX("MaskEdBox_6").Drag 34,4 @@ hightlight id_;_133856_;_script infofile_;_ZIP::ssf250.xml_;_
'					.ActiveX("SSTab_2").ActiveX("SSTab").VbFrame("FrameSiteSpec").Drop 219,350 @@ hightlight id_;_133978_;_script infofile_;_ZIP::ssf251.xml_;_
					.ActiveX("SSTab_2").ActiveX("SSTab").ActiveX("MaskEdBox_6").Type Datatable.value("Maxeffectheight","ASL") @@ hightlight id_;_133856_;_script infofile_;_ZIP::ssf252.xml_;_
					.ActiveX("SSTab_2").ActiveX("SSTab").ActiveX("MaskEdBox_6").Type  micTab @@ hightlight id_;_133856_;_script infofile_;_ZIP::ssf253.xml_;_
					.ActiveX("SSTab_2").ActiveX("SSTab").VbRadioButton("Cable").Set @@ hightlight id_;_133944_;_script infofile_;_ZIP::ssf254.xml_;_

					ElseIf datatable.value("Equipment","ASL")="Transmitter" and  datatable("SpecificRadio","ASL")="Line Attenuation (dB)" Then
						.ActiveX("SSTab_2").ActiveX("SSTab").VbRadioButton("Line Attenuation (dB)").Set	
						.ActiveX("SSTab_2").ActiveX("SSTab").VbRadioButton("Line Attenuation (dB)").Set @@ hightlight id_;_133942_;_script infofile_;_ZIP::ssf255.xml_;_
						.ActiveX("SSTab_2").ActiveX("SSTab").ActiveX("TCIComboBox.tcb_9").VbComboBox("cboCable").Select(0) '"ANDREW - FSJ1-50A" @@ hightlight id_;_133948_;_script infofile_;_ZIP::ssf256.xml_;_
						.ActiveX("SSTab_2").ActiveX("SSTab").ActiveX("MaskEdBox_5").Click 45,13 @@ hightlight id_;_133940_;_script infofile_;_ZIP::ssf257.xml_;_
						.ActiveX("SSTab_2").ActiveX("SSTab").ActiveX("MaskEdBox_5").Drag 47,11 @@ hightlight id_;_133940_;_script infofile_;_ZIP::ssf258.xml_;_
						.ActiveX("SSTab_2").ActiveX("SSTab").VbFrame("FrameAttenuation").Drop 195,90 @@ hightlight id_;_133976_;_script infofile_;_ZIP::ssf259.xml_;_
						.ActiveX("SSTab_2").ActiveX("SSTab").ActiveX("MaskEdBox_5").Type Datatable.value("FeedLen","ASL") @@ hightlight id_;_133940_;_script infofile_;_ZIP::ssf260.xml_;_
						.ActiveX("SSTab_2").ActiveX("SSTab").ActiveX("MaskEdBox_4").Drag 29,13 @@ hightlight id_;_133936_;_script infofile_;_ZIP::ssf261.xml_;_
						.ActiveX("SSTab_2").ActiveX("SSTab").VbFrame("FrameAttenuation").Drop 228,122 @@ hightlight id_;_133976_;_script infofile_;_ZIP::ssf262.xml_;_
						.ActiveX("SSTab_2").ActiveX("SSTab").ActiveX("MaskEdBox_4").Type Datatable.value("Lineatt","ASL") @@ hightlight id_;_133936_;_script infofile_;_ZIP::ssf263.xml_;_
						.ActiveX("SSTab_2").ActiveX("SSTab").ActiveX("MaskEdBox_6").Drag 74,13 @@ hightlight id_;_133856_;_script infofile_;_ZIP::ssf266.xml_;_
						.ActiveX("SSTab_2").ActiveX("SSTab").VbFrame("FrameSiteSpec").Drop 192,364 @@ hightlight id_;_133978_;_script infofile_;_ZIP::ssf267.xml_;_
						.ActiveX("SSTab_2").ActiveX("SSTab").ActiveX("MaskEdBox_6").Type Datatable.value("Maxeffectheight","ASL") @@ hightlight id_;_133856_;_script infofile_;_ZIP::ssf268.xml_;_
						.ActiveX("SSTab_2").ActiveX("SSTab").ActiveX("MaskEdBox_6").Type  micTab @@ hightlight id_;_133856_;_script infofile_;_ZIP::ssf269.xml_;_
						.ActiveX("SSTab_2").ActiveX("SSTab").ActiveX("MaskEdBox_6").Drag 37,7 @@ hightlight id_;_133856_;_script infofile_;_ZIP::ssf264.xml_;_
						.ActiveX("SSTab_2").ActiveX("SSTab").VbFrame("FrameSiteSpec").Drop 247,361 @@ hightlight id_;_133978_;_script infofile_;_ZIP::ssf265.xml_;_



					ElseIf datatable.value("Equipment","ASL")="Transceiver"  and  datatable("SpecificRadio","ASL")="Cable" Then
						.ActiveX("SSTab_2").ActiveX("SSTab").VbRadioButton("Cable").Set
						.ActiveX("SSTab_2").ActiveX("SSTab").ActiveX("TCIComboBox.tcb_9").VbComboBox("cboCable").Select(0) '"ANDREW - FSJ1-50A" @@ hightlight id_;_69806_;_script infofile_;_ZIP::ssf270.xml_;_
						.ActiveX("SSTab_2").ActiveX("SSTab").ActiveX("TCIComboBox.tcb_9").VbComboBox("cboCable").Type  micTab @@ hightlight id_;_69806_;_script infofile_;_ZIP::ssf271.xml_;_
						.ActiveX("SSTab_2").ActiveX("SSTab").ActiveX("MaskEdBox_3").Type Datatable.value("FeedLen","ASL") @@ hightlight id_;_69814_;_script infofile_;_ZIP::ssf272.xml_;_
						.ActiveX("SSTab_2").ActiveX("SSTab").ActiveX("MaskEdBox_3").Type  micTab @@ hightlight id_;_69814_;_script infofile_;_ZIP::ssf273.xml_;_
						.ActiveX("SSTab_2").ActiveX("SSTab").ActiveX("MaskEdBox_2").Type  micTab @@ hightlight id_;_69816_;_script infofile_;_ZIP::ssf274.xml_;_
						.ActiveX("SSTab_2").ActiveX("SSTab").ActiveX("MaskEdBox_4").Drag 52,12 @@ hightlight id_;_69838_;_script infofile_;_ZIP::ssf275.xml_;_
						.ActiveX("SSTab_2").ActiveX("SSTab").VbFrame("FrameSiteSpec").Drop 219,365 @@ hightlight id_;_69794_;_script infofile_;_ZIP::ssf276.xml_;_
						.ActiveX("SSTab_2").ActiveX("SSTab").ActiveX("MaskEdBox_4").Type Datatable.value("Maxeffectheight","ASL") @@ hightlight id_;_69838_;_script infofile_;_ZIP::ssf277.xml_;_
						.ActiveX("SSTab_2").ActiveX("SSTab").ActiveX("MaskEdBox_4").Type  micTab 

					ElseIf datatable.value("Equipment","ASL")="Transceiver" and  datatable("SpecificRadio","ASL")="Line Attenuation (dB)" Then


						.ActiveX("SSTab_2").ActiveX("SSTab").VbRadioButton("Line Attenuation (dB)").Set @@ hightlight id_;_69812_;_script infofile_;_ZIP::ssf281.xml_;_
						.ActiveX("SSTab_2").ActiveX("SSTab").ActiveX("TCIComboBox.tcb_9").VbComboBox("cboCable").Select(0) '"ANDREW - FSJ1-50A" @@ hightlight id_;_69806_;_script infofile_;_ZIP::ssf282.xml_;_
						.ActiveX("SSTab_2").ActiveX("SSTab").ActiveX("MaskEdBox_3").Drag 125,9 @@ hightlight id_;_69814_;_script infofile_;_ZIP::ssf283.xml_;_
						.ActiveX("SSTab_2").ActiveX("SSTab").VbFrame("FrameAttenuation").Drop 234,97 @@ hightlight id_;_69796_;_script infofile_;_ZIP::ssf284.xml_;_
						.ActiveX("SSTab_2").ActiveX("SSTab").ActiveX("MaskEdBox_3").Type Datatable.value("FeedLen","ASL") @@ hightlight id_;_69814_;_script infofile_;_ZIP::ssf285.xml_;_
						.ActiveX("SSTab_2").ActiveX("SSTab").ActiveX("MaskEdBox_3").Type  micTab @@ hightlight id_;_69814_;_script infofile_;_ZIP::ssf286.xml_;_
						.ActiveX("SSTab_2").ActiveX("SSTab").ActiveX("MaskEdBox_2").Drag 39,5 @@ hightlight id_;_69816_;_script infofile_;_ZIP::ssf287.xml_;_
						.ActiveX("SSTab_2").ActiveX("SSTab").VbFrame("FrameAttenuation").Drop 220,120 @@ hightlight id_;_69796_;_script infofile_;_ZIP::ssf288.xml_;_
						.ActiveX("SSTab_2").ActiveX("SSTab").ActiveX("MaskEdBox_2").Type Datatable.value("Lineatt","ASL") @@ hightlight id_;_69816_;_script infofile_;_ZIP::ssf289.xml_;_
						.ActiveX("SSTab_2").ActiveX("SSTab").ActiveX("MaskEdBox_2").Type  micTab @@ hightlight id_;_69816_;_script infofile_;_ZIP::ssf290.xml_;_
						.ActiveX("SSTab_2").ActiveX("SSTab").ActiveX("MaskEdBox_4").Drag 43,12 @@ hightlight id_;_69838_;_script infofile_;_ZIP::ssf291.xml_;_
						.ActiveX("SSTab_2").ActiveX("SSTab").VbFrame("FrameSiteSpec").Drop 236,363 @@ hightlight id_;_69794_;_script infofile_;_ZIP::ssf292.xml_;_
						.ActiveX("SSTab_2").ActiveX("SSTab").ActiveX("MaskEdBox_4").Type Datatable.value("Maxeffectheight","ASL") @@ hightlight id_;_69838_;_script infofile_;_ZIP::ssf293.xml_;_
						.ActiveX("SSTab_2").ActiveX("SSTab").ActiveX("MaskEdBox_4").Type  micTab @@ hightlight id_;_69838_;_script infofile_;_ZIP::ssf294.xml_;_



				End If
			End If
		End If


		'..Saving Equipment
		Wait 8
		.ActiveX("SSTab_2").VbButton("Save Equipment").Click @@ hightlight id_;_134840_;_script infofile_;_ZIP::ssf166.xml_;_
	Wait 10
	If .ActiveX("SSTab_2").VbButton("Save Equipment").GetROProperty("enabled")=true Then
		.ActiveX("SSTab_2").VbButton("Save Equipment").Click
		Wait 8
	End If
		'.....closing application

		If .VbButton("Close").Exist Then
			
			.VbButton("Close").Click
		End If @@ hightlight id_;_330894_;_script infofile_;_ZIP::ssf296.xml_;_

		'excel output

		strTestCaseName = Environment.Value("TestName")
		If err.number = 0 Then
			blnFind = True
			Else
				blnFind = False
		End If

		UpdateTestCaseStatusInExcel strTestCaseName, blnFind
	End If
End With




