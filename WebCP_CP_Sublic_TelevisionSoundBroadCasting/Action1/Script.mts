﻿

blnFind=False
err.number = 0


With Browser("Customer Home Page").Page("Application")
    
    'Import data from Input data sheet  from folder
    datatable.ImportSheet ProjectFolderPath&"\Input_Data_Sheet.xls","cpTelevision","cpTelevision"
    n=datatable.GetSheet("cpTelevision").GetRowCount
    
    'For i = 1 To n Step 1
    If n>0 Then
        datatable.SetCurrentRow(1)		
          If Strcomp(Datatable.Value("Edit","cpTelevision"),"yes",1)<>0 Then
		        'Base station
		        If Datatable.value("Stationtype","cpTelevision")="Base Station"  Then			
		            Browser("name:=Application").Page("title:=Application").Link("name:=Base Station").clicks
		            fn_LoadpageInWebCP 
		            .Frame("Frame_2").WebList("tbl_site__siteCity").Selects(1) '"Aero Drome (Extension 127)"
		            .Frame("Frame_2").WebEdit("tbl_site__siteInfo16").Sets Datatable.value("Contactperson","cpTelevision")
		            .Frame("Frame_2").WebList("tbl_site__siteInfo17").Selects(1)' "A.M. Radio Broadcast"
		            'mobile stattion
		        ElseIf Datatable.value("Stationtype","cpTelevision")="Mobile Station"  Then
		             Browser("name:=Application").Page("title:=Application").Link("name:=Mobile Station").Clicks	
		            .Frame("Frame_2").WebList("tbl_site__siteInfo17").Selects(1)' "A.M. Radio Broadcast"
		            .Frame("Frame_2").WebEdit("tbl_site__VehregNum").Sets Datatable.value("Vehiclereg","cpTelevision")&fnRandomNumber(4)
		        End If
        End If
        If Strcomp(Datatable.Value("Edit","cpTelevision"),"yes",1)<>0 Then
           If Browser("name:=Application").Page("title:=Application").Webedit("html id:=tbl_site__nomRadius","name:=tbl_site__nomRadius").Exist(8) Then
              Browser("name:=Application").Page("title:=Application").Webedit("html id:=tbl_site__nomRadius","name:=tbl_site__nomRadius").sets RandomNumber(1,10)
           End If
        Else
           fn_EditSubLicenses "station",Datatable.Value("Stationtype","cpTelevision"),Datatable.Value("TypeOfStation","cpTelevision")
           fn_LoadpageInWebCP 
           If Browser("name:=Application").Page("title:=Application").Webedit("html id:=tbl_site__nomRadius","name:=tbl_site__nomRadius").Exist(8) Then
              Browser("name:=Application").Page("title:=Application").Webedit("html id:=tbl_site__nomRadius","name:=tbl_site__nomRadius").sets RandomNumber(11,20)
           End If
        End If
        	
        Browser("name:=.*Application.*").Page("title:=.*Application.*").WebElement("innertext:=.*Save.*","title:=.*Save.*").Clicks
        fn_LoadpageInWebCP 
         If Strcomp(Datatable.Value("Edit","cpTelevision"),"yes",1)<>0 Then
        	
             With Browser("name:=.*Application.*").Page("title:=.*Application.*")
	        	 If trim(Datatable.Value("Equipmenttype","cpTelevision"))="Add Receiver" Then	
	                .WebElement("innertext:=.*Add Receiver.*","title:=.*Receiver.*").Clicks
		            '====Base station transceiver 
		        ElseIf trim(Datatable.Value("Equipmenttype","cpTelevision"))="Add Transceiver" Then
		            .WebElement("innertext:=.*Add Transceiver.*","title:=.*Transceiver.*").Clicks
		            '====Base station Transmitter
		        ElseIf trim(Datatable.Value("Equipmenttype","cpTelevision"))="Add Transmitter" Then
		            .WebElement("innertext:=.*Add Transmitter.*","title:=.*Transmitter.*").Clicks
		        End If
            End With
            fn_LoadpageInWebCP
        
	        'Entring Equipment details
	        With Browser("name:=Application").Page("title:=Application")
	            If .WebList("html id:=tbl_equipment__ApprovalNum").Exist(40) Then
	                If Datatable.Value("EquipmentApprovalNum","cpTelevision")="" Then
	                    .WebList("html id:=tbl_equipment__ApprovalNum").Selects 1'"random"
	                Else 
	                    .WebList("html id:=tbl_equipment__ApprovalNum").Selects 1'Datatable.Value("EquipmentApprovalNum ","cpTelevision")
	                End If
	                For intC = 1 To 20 Step 1
	                    If Instr(1,.WebList("html id:=tbl_equipment__equipMake","name:=tbl_equipment__equipMake").GetROProperty("selection"),"Select",1)>0 Then
	                        Wait 3
	                    Else
	                        Exit For
	                    End If
	                Next
	            End If
	        End With
	     End If
        fn_LoadpageInWebCP	
        If Strcomp(Datatable.Value("Edit","cpTelevision"),"yes",1)=0 Then
           fn_EditSubLicenses "",Datatable.Value("Stationtype","cpTelevision"),Datatable.Value("TypeOfStation","cpTelevision") 
           fn_LoadpageInWebCP 
           Wait 8           
           Browser("name:=Application").Page("title:=Application").WebEdit("html id:=tbl_equipment__equipSerialNum").Sets Datatable.value("Equipserial","cpTelevision")&fnRandomNumber(4)
         Else
	        Browser("name:=Application").Page("title:=Application").WebEdit("html id:=tbl_equipment__equipSerialNum").Sets Datatable.value("Equipserial","cpTelevision")&fnRandomNumber(4)
	        With Browser("name:=Application").Page("title:=Application")
	            If .WebList("html id:=tbl_antenna__ApprovalNum").Exist(40) Then
	                If Datatable.Value("AntennaApprovalNum","cpTelevision")="" Then
	                    .WebList("html id:=tbl_antenna__ApprovalNum").Selects 1'"random"
	                Else
	                    .WebList("html id:=tbl_antenna__ApprovalNum").Select  1'Datatable.Value("AntennaApprovalNum","cpTelevision")
	                End If
	                
	                For intC = 1 To 20 Step 1
	                    If Instr(1,.WebList("html id:=tbl_antenna__equipMake","name:=tbl_antenna__equipMake").GetROProperty("selection"),"Select",1)>0 Then
	                        wait 3
	                    Else
	                        Exit For
	                        
	                    End If
	                Next
	            End If
	        End With     
	        fn_LoadpageInWebCP
	        If Datatable.value("Stationtype","cpTelevision")="Base Station" Then
	            wait 20
	            .Frame("Frame_3").WebEdit("tbl_antenna__physicHt").Sets Datatable.value("height","cpTelevision")
	            .Frame("Frame_3").WebEdit("tbl_antenna__mainLobeAzi").Sets Datatable.value("mainlobeazi","cpTelevision")
	            .Frame("Frame_3").WebEdit("tbl_antenna__tiltAngle").Sets Datatable.value("Tiltangle","cpTelevision")			
	        End If 
	        If Lcase(Datatable.Value("FeederLoss","cpTelevision"))="cable" Then
	            If Browser("name:=Application").Page("title:=Application").WebRadioGroup("html id:=tbl_antenna__By.*","name:=tbl_antenna__ByCableOrLineAttenuation").Exist(8) Then
	                Browser("name:=Application").Page("title:=Application").WebRadioGroup("html id:=tbl_antenna__By.*","name:=tbl_antenna__ByCableOrLineAttenuation").Select 1
	                .Frame("Frame_3").WebEdit("tbl_antenna__FeedLength").sets RandomNumber(1,10)
	            ElseIf Browser("name:=Application").Page("title:=Application").WebRadioGroup("html id:=tbl_antenna__By.*","name:=tbl_antenna__ByCableOrLineAttenuation").Exist(8) Then
	                Browser("name:=Application").Page("title:=Application").WebRadioGroup("html id:=tbl_antenna__By.*","name:=tbl_antenna__ByCableOrLineAttenuation").Select 2            
	                .Frame("Frame_3").WebEdit("tbl_antenna__LineAtten").sets RandomNumber(1,5)	
	            End If 
	        End If
	        If .Frame("Frame_3").WebList("tbl_antenna__CableID").Exist(5) Then
	            .Frame("Frame_3").WebList("tbl_antenna__CableID").Selects(1)' "ANDREW - FSJ-50B"
	        End If
       End If
   	    '=====Base station -Save equipment Receiver
        Browser("name:=.*Application.*").Page("title:=.*Application.*").WebElement("innertext:=.*Save.*","title:=.*save.*").Clicks		
        fn_LoadpageInWebCP
    End If
End With

strTestCaseName = Environment.Value("TestName")
'Test results dsiplayed in Excel
If err.number = 0 Then
    blnFind = True
Else
    blnFind = False
End If

'fn_SendStatusToExcelReport blnFind
UpdateTestCaseStatusInExcel strTestCaseName, blnFind	


