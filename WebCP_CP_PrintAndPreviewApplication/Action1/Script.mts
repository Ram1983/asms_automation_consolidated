﻿blnVal=False
Browser("name:=.*Application.*").Page("title:=.*Application.*").Sync

With Browser("PDF Application")
	.Page("PDF Application").Link("Home").Click @@ script infofile_;_ZIP::ssf22.xml_;_
	.Sync
	.Page("Page").Image("edit").Click @@ script infofile_;_ZIP::ssf23.xml_;_
	.Sync
End With
Browser("name:=.*Customer Home Page.*").Page("title:=.*Customer Home Page.*").Link("title:=Preview and Print Application").Click @@ script infofile_;_ZIP::ssf1.xml_;_
With Browser("name:=.*Application.*").Page("title:=.*Application.*")
	.Sync
	Set obj=.WebTable("html id:=ttbl","column names:=;;Create PDF for: Application Cover Sheet")
End With
For count = 1 To obj.RowCount Step 1
	If obj.ChildItem(count,1,"Image",0).Exist Then
		obj.ChildItem(count,1,"Image",0).click
		Browser("name:=.*Application.*").Sync
		With Browser("name:=.*webcp.*referenceNum.*")
			If .Exist(8) Then
				.Close
			End If
		End With
		Browser("name:=.*Application.*").Page("title:=.*Application.*").Sync
	End If
	With	Browser("name:=.*webcp.*referenceNum.*")
		If obj.ChildItem(count,2,"Image",0).Exist Then
			obj.ChildItem(count,2,"Image",0).click
			.Sync
			.Close
		End If
	End With

Next

If Err.Number=0 Then
	blnVal=True
End If

UpdateTestCaseStatusInExcel Environment("TestName"),blnVal
