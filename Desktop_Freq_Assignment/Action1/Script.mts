﻿
blnFind=False
err.number = 0
BOTfreqAssign=0
Wait 8


With VbWindow("frmMDI")
	With .VbWindow("frmFindApplication_2")
		If Lcase(Environment.Value("Client"))="jmc" OR Lcase(Environment.Value("Client"))="moz" Then
			If Not VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Exist Then
				
				fn_NavigateToScreen "Application Processing;For Licence;Modify"
				AppNum=GetDataFromDB("exec [sp_GetApplicationNumbersAndType] 'jmc'")
				While Not .ActiveX("TCIComboBox.tcb").Exist
				Wend
				.ActiveX("TCIComboBox.tcb").VbComboBox("cboApplicationNo").Select AppNum
				If .VbButton("Next").Exist Then
					.VbButton("Next").Click
				End If
				While Not VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Exist
				Wend
			End If
				VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").Select "Application"
				abs_x = VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").GetROProperty("abs_x")
				abs_y = VbWindow("frmMDI").VbWindow("frmNavigator").VbTreeView("navTree").GetROProperty("abs_y")
				Set deviceReplay = CreateObject("mercury.devicereplay")
				deviceReplay.MouseClick abs_x+50, abs_y+50, 2
				wait(2)
				VbWindow("frmMDI").WinMenu("ContextMenu").Select "Frequency Assignment"
				Wait 10
		Else
		
		   With VbWindow("frmMDI").VbWindow("frmBrowser")
				.Page("Application").WebElement("Application").Click @@ hightlight id_;_VbWindow("frmMDI").VbWindow("frmBrowser").Page("Application").WebElement("Application")_;_script infofile_;_ZIP::ssf172.xml_;_
				abs_x = .Page("Application").WebElement("Application").GetROProperty("abs_x")
				abs_y = .Page("Application").WebElement("Application").GetROProperty("abs_y")
				Set deviceReplay = CreateObject("mercury.devicereplay")
				deviceReplay.MouseClick abs_x+50, abs_y+50, 2
				Wait 5
				.Page("Application").WebElement("Frequency Assignment").Click
				Wait 20

			End With
		End If
	End With
	Environment.LoadFromFile("C:\ASMS_Automation\ASMS_Variables.xml")
	AppNum= GetDataFromDB("exec [stp_Automation_GetApplicationNumbersAndType] '"&Environment.Value("Client") &"','"& Environment("UserID") &"'")
	Wait 3
	If InStr(AppNum,"MCL") >0 Then
		With VbWindow("frmMDI")
			With .Dialog("#32770")
				If .WinButton("Yes").Exist Then
					.WinButton("Yes").Click @@ hightlight id_;_200738_;_script infofile_;_ZIP::ssf127.xml_;_
					wait(5)
				End If
			End With

			With .VbWindow("frmMWEquipFreq")
				While not .exist
				Wend
				wait(5)
				freqMWcount=.AcxTable("FREQUENCY ASSIGNMENT QUERY").RowCount
			End With
		End With
		wait(3)
		If freqMWcount>0 Then

			For Iterator = 1 To freqMWcount
				With	.VbWindow("frmMWEquipFreq")
					.VbRadioButton("Without Frequency").Set @@ hightlight id_;_330946_;_script infofile_;_ZIP::ssf125.xml_;_
					wait(5)
					freqMWcount_WO=.AcxTable("FREQUENCY ASSIGNMENT QUERY").RowCount
				End With
				If freqMWcount_WO=0 Then
					With .VbWindow("frmMWEquipFreq")
						wait 2
						If .VbButton("Close").Exist Then
							.VbButton("Close").Click
						End If

						wait(2)
						Exit for
					End With
					else
						With	VbWindow("frmMDI")
							With	.VbWindow("frmMWEquipFreq")
								.AcxTable("FREQUENCY ASSIGNMENT QUERY").SelectRow 1
								wait(4)
								.VbButton("Next").Click
							End With
							wait(15)

							With .VbWindow("frmFreqAllocation")
								While not .exist
								Wend
								.VbRadioButton("Simplex").Set @@ hightlight id_;_133638_;_script infofile_;_ZIP::ssf178.xml_;_
								If .ActiveX("TCIComboBox.tcb").VbComboBox("cboComplexity").Exist(10) Then
									.ActiveX("TCIComboBox.tcb").VbComboBox("cboComplexity").Select 0
								End If
								If .VbButton("Choose Band").Exist Then
									.VbButton("Choose Band").Click
								End If
							End With
							If .Dialog("#32770").WinButton("OK").Exist(10) then
								.Dialog("#32770").WinButton("OK").Click
								Print "There is no frequency band in National Frequency Plan - Simplex"
								wait(5)
								With	.VbWindow("frmFreqAllocation")
									.VbRadioButton("Duplex").Set
									wait(5)
									If .VbButton("Choose Band").exist Then
										.VbButton("Choose Band").Click
										wait(5)
									End If
								End With

								With .Dialog("#32770")
									If .WinButton("OK").Exist then
										.WinButton("OK").Click
										Print "There is no frequency band in National Frequency Plan - Duplex"
										ExitTest
									End If
								End With
							End If

							With .VbWindow("frmListNationalFreq")
								While not .exist
								Wend
								If .AcxTable("Available Frequency").Exist(10) Then
									ListNationalFreq=.AcxTable("Available Frequency").RowCount
								End If

							End With
						End With
						If ListNationalFreq>0 Then
							For Iterator1 = 1 To ListNationalFreq

								With VbWindow("frmMDI")
									With .VbWindow("frmFreqAllocation")
										If Iterator1>1 and .VbButton("Choose Band").Exist Then
											.VbButton("Choose Band").Click
											wait(5)
										End If
									End With


									'===Moving to left
									With .VbWindow("frmListNationalFreq")
										.Move 35,139 @@ hightlight id_;_592778_;_script infofile_;_ZIP::ssf159.xml_;_
										'===
										.AcxTable("Available Frequency").SelectRow Iterator1
										If .VbButton("Next").Exist Then
											.VbButton("Next").Click
											wait(5)

										End If
									End With
								End With

								if	.VbWindow("frmFreqAllocation").AcxTable("Available Frequency").RowCount>0 then
									wait(2)

									With	.VbWindow("frmFreqAllocation")
										.AcxTable("Available Frequency").SelectCell 1,1
										.AcxTable("Available Frequency").ActivateCell 1,1 @@ hightlight id_;_200916_;_script infofile_;_ZIP::ssf173.xml_;_
									End With

									With VbWindow("frmMDI")
										With .VbWindow("frmFreqAllocation")
											If .VbButton("Assign Frequency").exist Then

												wait(5)

												.VbButton("Assign Frequency").Click
												wait(5)
											End If
										End With

										With .Dialog("#32770")
											If .WinButton("Yes").Exist then
												.WinButton("Yes").Click
												wait(5)
												Exit for
											End If
										End With

										If .Dialog("#32770").WinButton("OK").Exist then
											.Dialog("#32770").WinButton("OK").Click						
											wait(5)
											.VbWindow("frmFreqAllocation").VbButton("Assign Frequency").Click
											wait(5)
											Exit for
										End If

										With .VbWindow("frmITUNotify")
											If .Exist And .VbButton("Close").Exist Then
												.VbButton("Close").Click
												wait(5)
											End If
										End With
									End With
								End If
							Next
						End If	
				end if	

			next
			With .VbWindow("frmMWEquipFreq")
	'			If .VbButton("Close").exist Then
	'				.VbButton("Close").Click	
	'				wait(5)
	'			End If
			End With
		End If

		wait 5

		With .VbWindow("frmBrowser")
			While not .Page("Application").WebElement("Application").Exist

			Wend
			.Page("Application").WebElement("Application").Click
			abs_x = .Page("Application").WebElement("Application").GetROProperty("abs_x")
			abs_y = .Page("Application").WebElement("Application").GetROProperty("abs_y")


			Set deviceReplay = CreateObject("mercury.devicereplay")
			deviceReplay.MouseClick abs_x+50, abs_y+50, 2

			.Page("Application").WebElement("Frequency Assignment").Click

		End With
		With VbWindow("frmMDI")
			If len(AppNum) > 13 Then 

				With .Dialog("#32770")
					If InStr(AppNum,"MCL") >0 Then
						If .WinButton("No").Exist Then
							.WinButton("No").Click @@ hightlight id_;_200738_;_script infofile_;_ZIP::ssf127.xml_;_
							wait(5)
						End If

					End If
				End With
			End IF
		End With
	End If

	If InStr(AppNum,"MCL") >0 and len(AppNum)<12 Then
		ExitTest
	end if

	' Other Sublicenses frequency assignents
	With .VbWindow("frmEquipFreq")
		While not .exist
		Wend

		freqcount=.AcxTable("FREQUENCY ASSIGNMENT QUERY").RowCount
	End With
	wait(5)
	If freqcount>0 Then

		For Iterator = 1 To freqcount

			With	.VbWindow("frmEquipFreq")
				.VbRadioButton("Without Frequency").Set
				freqwithout=.AcxTable("FREQUENCY ASSIGNMENT QUERY").RowCount
			End With

			If freqwithout=0 Then
				With	.VbWindow("frmEquipFreq")
					If BOTfreqAssign>0 and Datatable.value("Client","Frequency_Assignment")="BOT" Then
						wait(5)
						.Activate
						.Move 591,26 @@ hightlight id_;_670722_;_script infofile_;_ZIP::ssf157.xml_;_
					End If
					If .VbButton("Close").Exist Then
						.VbButton("Close").Click
					End If

					wait(5)
					Exit for
				End With
				else

					With VbWindow("frmMDI")
						With .VbWindow("frmEquipFreq")
							.Move 70,16 @@ hightlight id_;_1248430_;_script infofile_;_ZIP::ssf160.xml_;_
							.AcxTable("FREQUENCY ASSIGNMENT QUERY").SelectRow 1
							wait(5)
							If BOTfreqAssign>0 and Datatable.value("Client","Frequency_Assignment")="BOT" Then
								wait(5)
								.Activate
								.Move 591,26
							End If
							If .VbButton("Next").exist(10) Then
								.VbButton("Next").Click
								wait(10)
							End If
						End With
						If .VbWindow("frmFreqAllocation").ActiveX("TCIComboBox.tcb").VbComboBox("cboComplexity").Exist Then
							If .VbWindow("frmFreqAllocation").ActiveX("TCIComboBox.tcb").VbComboBox("cboComplexity").GetROProperty("disabled")=1 Then
								.VbWindow("frmFreqAllocation").ActiveX("TCIComboBox.tcb").VbComboBox("cboComplexity").Select 0
							End If
						End If

						With .VbWindow("frmFreqAllocation")
							While not .exist
							Wend
							If BOTfreqAssign>0 and Datatable.value("Client","Frequency_Assignment")="BOT" Then
								wait(5)
								.Activate @@ hightlight id_;_1061862_;_script infofile_;_ZIP::ssf152.xml_;_
								.Move 454,-23 @@ hightlight id_;_1061862_;_script infofile_;_ZIP::ssf153.xml_;_
							End If
							BOTfreqAssign=BOTfreqAssign+1
							If .VbButton("Choose Band").Exist Then
								.VbButton("Choose Band").Click
								wait(5)
							End If
						End With

						wait(5)

						If .Dialog("#32770").WinButton("OK").Exist then
							.Dialog("#32770").WinButton("OK").Click
							Print "There is no frequency band in National Frequency Plan - Simplex"
							wait(5)
							With	.VbWindow("frmFreqAllocation")
								.VbRadioButton("Duplex").Set
								If .VbButton("Choose Band").exist Then
									.VbButton("Choose Band").Click
									wait(5)
								End If
							End With

							With .Dialog("#32770")
								If .WinButton("OK").Exist then
									.WinButton("OK").Click
									wait(5)
									Print "There is no frequency band in National Frequency Plan - Duplex"
									ExitTest
								End If
							End With
						End If
						Wait(10)

						With .VbWindow("frmListNationalFreq")
							While not .exist
							Wend

							ListNationalFreq=.AcxTable("Available Frequency").RowCount
						End With
					End With

					If ListNationalFreq>0 Then

						For Iterator3 = 1 To ListNationalFreq

							With VbWindow("frmMDI")
								With .VbWindow("frmFreqAllocation")
									If Iterator3>1 and .VbButton("Choose Band").Exist Then
										.VbButton("Choose Band").Click
										wait(5)
									End If
								End With

								'===Moving to left
								With .VbWindow("frmListNationalFreq")
									.Move 35,139 @@ hightlight id_;_592778_;_script infofile_;_ZIP::ssf159.xml_;_
									'===

									.AcxTable("Available Frequency").SelectRow Iterator3
									If .VbButton("Next").Exist Then
										.VbButton("Next").Click
										wait(5)

									End If
								End With
							End With
'							.........Selecting Availablity Frequency

							if	.VbWindow("frmFreqAllocation").AcxTable("Available Frequency").RowCount>0 then

								wait(2)

								With	.VbWindow("frmFreqAllocation")
									.AcxTable("Available Frequency").SelectCell 1,1
									.AcxTable("Available Frequency").ActivateCell 1,1 @@ hightlight id_;_200916_;_script infofile_;_ZIP::ssf173.xml_;_
			
								End With

								With VbWindow("frmMDI")
									With .VbWindow("frmFreqAllocation")
										If .VbButton("Assign Frequency").exist Then

											wait(5)

											.VbButton("Assign Frequency").Click

										End If
									End With
									With .Dialog("#32770")
										If .WinButton("Yes").Exist then
											.WinButton("Yes").Click
											wait(5)
											Exit for
										End If
									End With

									If .Dialog("#32770").WinButton("OK").Exist then
										.Dialog("#32770").WinButton("OK").Click
										wait(5)
										.VbWindow("frmFreqAllocation").VbButton("Assign Frequency").Click

										Exit for
									End If

									With .VbWindow("frmITUNotify")
										If .Exist And .VbButton("Close").Exist Then
											.VbButton("Close").Click
											wait(5)
										End If
									End With
								End With
							End If
						Next
					End If	
			End if	

		Next	

		else
			'VbWindow("frmMDI").VbWindow("frmEquipFreq").VbButton("Close").Click
	End If
End With
With VbWindow("frmMDI").VbWindow("frmEquipFreq")
	If .Exist Then
		If BOTfreqAssign>0 and Datatable.value("Client","Frequency_Assignment")="BOT" Then
			.Activate
			.Move 591,26
		End If
		If .VbButton("Close").Exist Then
			.VbButton("Close").Click
		End If
		wait(5)

		'elseIf VbWindow("frmMDI").VbWindow("frmMWEquipFreq").exist Then
		'	VbWindow("frmMDI").VbWindow("frmMWEquipFreq").VbButton("Close").Click
	End If
End With


strTestCaseName = Environment.Value("TestName")
If err.number = 0 Then
	blnFind = True
Else
	blnFind = False
End If

UpdateTestCaseStatusInExcel strTestCaseName, blnFind
'End If
wait(2)


								


