﻿If Browser("Browser_AdminHomePage").Page("Page_AdminHomePage").Image("Image_SearchCustomerORApplication").Exist(40) Then
	Browser("Browser_AdminHomePage").Page("Page_AdminHomePage").Image("Image_SearchCustomerORApplication").Click
End If
If Browser("Browser_AdminHomePage").Page("Page_AdminHomePage").Link("Link_AddNewCustomer").Exist Then
	Browser("Browser_AdminHomePage").Page("Page_AdminHomePage").Link("Link_AddNewCustomer").Click
End If
While Not Browser("Browser_AdminHomePage").Page("Page_AdminHomePage").WebElement("WebEle_Person").Exist
Wend
Browser("Browser_AdminHomePage").Page("Page_AdminHomePage").WebElement("WebEle_Person").Click
While Not Browser("Browser_AdminHomePage").Page("Page_AdminHomePage").Image("Image_EditCustomer").Exist
Wend
Browser("Browser_AdminHomePage").Page("Page_AdminHomePage").Image("Image_EditCustomer").Click
While False Browser("Browser_AdminHomePage").Page("Page_AdminHomePage").WebEdit("WebEdit_Fname").Exist
Wend

Browser("Browser_AdminHomePage").Page("Page_AdminHomePage").WebEdit("WebEdit_Lname").Sets

Browser("Browser_AdminHomePage").Page("Edit").WebEdit("client__clientFname").Set "Newtest Person 8/26/2020 11:37:07" @@ script infofile_;_ZIP::ssf2.xml_;_
Browser("Browser_AdminHomePage").Page("Edit").WebEdit("client__clientLname").Set "New Petestrson 8/26/2020 11:37:07"

' Filed Level validation with Empty 
Browser("Browser_AdminHomePage").Page("Page_AdminHomePage").WebElement("WebEle_Information").Click
Set clickOnUpdate=Browser("Browser_AdminHomePage").Page("Page_AdminHomePage").WebElement("WebEle_Update")
Set firstName=Browser("Browser_AdminHomePage").Page("Page_AdminHomePage").WebEdit("WebEdit_Fname")
If fn_FieldLevelValidation(firstName,"Empty","",clickOnUpdate,customerNum,"1","innertext")  Then
    blnValOne = True
   Reporter.ReportEvent micPass,"Validate First Name with empty Value","Successfully validated First Name with empty Value"
Else
   Reporter.ReportEvent micFail,"Validate First Name with empty Value","Failed to validated First Name with empty Value"
End If

Set companyName=Browser("Browser_AdminHomePage").Page("Page_AdminHomePage").WebEdit("WebEdit_Company")
If fn_FieldLevelValidation(companyName,"Empty","",clickOnUpdate,Browser("Browser_AdminHomePage").Page("Page_AdminHomePage").WebElement("innertext:=Customer Name.*","index:=1"),"Customer Name","innertext")  Then
   blnValTwo = True
   Reporter.ReportEvent micPass,"Validate First Name with empty Value","Successfully validated First Name with empty Value"
Else
   Reporter.ReportEvent micFail,"Validate First Name with empty Value","Failed to validated First Name with empty Value"
End If

Set companyName=Browser("Browser_AdminHomePage").Page("Page_AdminHomePage").WebEdit("WebEdit_Company")
If fn_FieldLevelValidation(companyName,"numeric","777777777777777777777777777777777777777777777777777777777",clickOnUpdate,Browser("Browser_AdminHomePage").Page("Page_AdminHomePage").WebElement("innertext:=Customer Name.*","index:=1"),"Customer Name","innertext")  Then
   blnValThree = True
   Reporter.ReportEvent micPass,"Validate First Name with empty Value","Successfully validated First Name with empty Value"
Else
   Reporter.ReportEvent micFail,"Validate First Name with empty Value","Failed to validated First Name with empty Value"
End If

Set companyName=Browser("Browser_AdminHomePage").Page("Page_AdminHomePage").WebEdit("WebEdit_Company")
If fn_FieldLevelValidation(companyName,"alphanumeric","250",clickOnUpdate,Browser("Browser_AdminHomePage").Page("Page_AdminHomePage").WebElement("innertext:=Customer Name.*","index:=1"),"Customer Name","innertext")  Then
   blnValFour = True
   Reporter.ReportEvent micPass,"Validate First Name with empty Value","Successfully validated First Name with empty Value"
Else
   Reporter.ReportEvent micFail,"Validate First Name with empty Value","Failed to validated First Name with empty Value"
End If

Set telephoneNum=Browser("Browser_AdminHomePage").Page("Page_AdminHomePage").WebEdit("WebEdit_TelNum")
Set clickOnUpdate=Browser("Browser_AdminHomePage").Page("Page_AdminHomePage").WebElement("WebEle_Update")
If fn_FieldLevelValidation(telephoneNum,"alphanumeric","55",clickOnUpdate,telephoneNum,"Telephone No","innertext")  Then
	blnValFive= True
   Reporter.ReportEvent micPass,"Validate Telephone Number with empty Value","Successfully validated Telephone Number with empty Value"
Else
   Reporter.ReportEvent micFail,"Validate Telephone Number with empty Value","Failed to validated Telephone Number with empty Value"
End If


Set telephoneNum=Browser("Browser_AdminHomePage").Page("Page_AdminHomePage").WebEdit("WebEdit_TelNum")
Set clickOnUpdate=Browser("Browser_AdminHomePage").Page("Page_AdminHomePage").WebElement("WebEle_Update")
If fn_FieldLevelValidation(telephoneNum,"numeric","5588888888888888888888888888888888888888",clickOnUpdate,telephoneNum,"Telephone No","innertext")  Then
	blnValSix= True
   Reporter.ReportEvent micPass,"Validate Telephone Number with empty Value","Successfully validated Telephone Number with empty Value"
Else
   Reporter.ReportEvent micFail,"Validate Telephone Number with empty Value","Failed to validated Telephone Number with empty Value"
End If


Set telephoneNum=Browser("Browser_AdminHomePage").Page("Page_AdminHomePage").WebEdit("WebEdit_TelNum")
Set clickOnUpdate=Browser("Browser_AdminHomePage").Page("Page_AdminHomePage").WebElement("WebEle_Update")
If fn_FieldLevelValidation(telephoneNum,"Empty","",clickOnUpdate,Browser("Browser_AdminHomePage").Page("Page_AdminHomePage").WebElement("innertext:=Telephone No.*","index:=1"),"Telephone No","innertext")  Then
	blnValSeven= True
   Reporter.ReportEvent micPass,"Validate Telephone Number with empty Value","Successfully validated Telephone Number with empty Value"
Else
   Reporter.ReportEvent micFail,"Validate Telephone Number with empty Value","Failed to validated Telephone Number with empty Value"
End If


Set companyNameOrOrg=Browser("Browser_AdminHomePage").Page("Page_AdminHomePage").WebEdit("WebEdit_CompanyNameOROrg")
If fn_FieldLevelValidation(companyNameOrOrg,"Empty","",clickOnUpdate,Browser("Browser_AdminHomePage").Page("Page_AdminHomePage").WebElement("innertext:=Customer Name.*","index:=1"),"Customer Name","innertext")  Then
   blnValEight = True
   Reporter.ReportEvent micPass,"Validate First Name with empty Value","Successfully validated First Name with empty Value"
Else
   Reporter.ReportEvent micFail,"Validate First Name with empty Value","Failed to validated First Name with empty Value"
End If
Set companyNameOrOrg=Browser("Browser_AdminHomePage").Page("Page_AdminHomePage").WebEdit("WebEdit_CompanyNameOROrg")
If fn_FieldLevelValidation(companyNameOrOrg,"numeric","777777777777777777777777777777777777777777777777777777777",clickOnUpdate,Browser("Browser_AdminHomePage").Page("Page_AdminHomePage").WebElement("innertext:=Customer Name.*","index:=1"),"Customer Name","innertext")  Then
   blnValNine = True
   Reporter.ReportEvent micPass,"Validate First Name with empty Value","Successfully validated First Name with empty Value"
Else
   Reporter.ReportEvent micFail,"Validate First Name with empty Value","Failed to validated First Name with empty Value"
End If

Set companyNameOrOrg=Browser("Browser_AdminHomePage").Page("Page_AdminHomePage").WebEdit("WebEdit_CompanyNameOROrg")
If fn_FieldLevelValidation(companyNameOrOrg,"alphanumeric","250",clickOnUpdate,Browser("Browser_AdminHomePage").Page("Page_AdminHomePage").WebElement("innertext:=Customer Name.*","index:=1"),"Customer Name","innertext")  Then
   blnValTen = True
   Reporter.ReportEvent micPass,"Validate First Name with empty Value","Successfully validated First Name with empty Value"
Else
   Reporter.ReportEvent micFail,"Validate First Name with empty Value","Failed to validated First Name with empty Value"
End If


If blnValOne And blnValTwo And blnValThree And blnValFour And blnValFive And blnValSix And blnValSeven And blnValEight And blnValNine And blnValTen Then
   	UpdateTestCaseStatusInExcel Environment.Value("TestName"), True
Else
   UpdateTestCaseStatusInExcel Environment.Value("TestName"), False
End If





