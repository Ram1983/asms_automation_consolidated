﻿blnFind=True
err.number = 0

datatable.ImportSheet ProjectFolderPath &"\Desktop_Input_Data_Sheet\Input_Data_Sheet.xls","Int-ExportFee","Int-ExportFee"
n=datatable.GetSheet("Int-ExportFee").GetRowCount

'For i = 1 To n Step 1 
With VbWindow("frmMDI")
  If n>0 Then
    datatable.SetCurrentRow(1) 	
    .WinMenu("Menu").Select "Accounting;Interface;Export Fee File"
   With .VbWindow("frmAccountingInterface")
      While Not .Exist
      Wend
      wait 5
      
      .VbEdit("txtFilePath").SetSelection 0,9
      .VbEdit("txtFilePath").Set Datatable.Value("FilePath","Int-ExportFee")
      .VbEdit("txtFilePath").Type  micTab
        .ActiveX("MaskEdBox").Type Datatable.Value("StartDate","Int-ExportFee")
      .ActiveX("MaskEdBox").Type  micTab
      wait 5
      .ActiveX("MaskEdBox_2").Type Datatable.Value("EndDate","Int-ExportFee")
      wait 5
      
      If Datatable.Value("IgnorePrevious","Int-ExportFee")="Yes" Then
        .VbCheckBox("Ignore previously Exported").Set "ON"
      Else
        .VbCheckBox("Ignore previously Exported").Set "OFF"
      End If
      
      wait(1)
      
      .VbButton("Next").Click
    End With
    wait(1)
    If .Dialog("#32770").WinButton("OK").Exist Then
    	.Dialog("#32770").WinButton("OK").Click
    End If
    If .VbWindow("frmAccountingInterface").VbButton("Close").Exist Then
    	 .VbWindow("frmAccountingInterface").VbButton("Close").Click
    End If
    
    strTestCaseName = Environment.Value("TestName")
    If err.number = 0 Then
      blnFind = True
    Else
      blnFind = False
    End If
    
    UpdateTestCaseStatusInExcel strTestCaseName, blnFind
    'End If
    wait(2)
  End If
End With

