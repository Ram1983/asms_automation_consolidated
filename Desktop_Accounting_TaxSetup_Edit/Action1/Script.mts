﻿wait(2)
blnFind=True
err.number = 0

datatable.ImportSheet ProjectFolderPath &"\Desktop_Input_Data_Sheet\Input_Data_Sheet.xls","TaxSetup","TaxSetup"
n=datatable.GetSheet("TaxSetup").GetRowCount

'For i = 1 To n Step 1
If n>0 Then
	datatable.SetCurrentRow(1)
	
	fn_NavigateToScreen "Accounting;Setup;Tax Setup"
	wait(10)

	JurisdictionName=Datatable.value("Jurisdiction","TaxSetup") & "" & fnRandomNumber(3)
	TaxName=Datatable.value("TaxName","TaxSetup") & "" & fnRandomNumber(3)
	VbWindow("frmMDI").VbWindow("frmSystemTables").AcxTable("Taxes").SelectCell 1,3 @@ hightlight id_;_68882_;_script infofile_;_ZIP::ssf8.xml_;_
	VbWindow("frmMDI").VbWindow("frmSystemTables").AcxTable("Taxes").Type JurisdictionName @@ hightlight id_;_68882_;_script infofile_;_ZIP::ssf9.xml_;_
	VbWindow("frmMDI").VbWindow("frmSystemTables").AcxTable("Taxes").Type  micTab @@ hightlight id_;_68882_;_script infofile_;_ZIP::ssf10.xml_;_
	VbWindow("frmMDI").VbWindow("frmSystemTables").AcxTable("Taxes").SetCellData 1,3,JurisdictionName
	VbWindow("frmMDI").VbWindow("frmSystemTables").AcxTable("Taxes").Type TaxName @@ hightlight id_;_68882_;_script infofile_;_ZIP::ssf12.xml_;_
	VbWindow("frmMDI").VbWindow("frmSystemTables").AcxTable("Taxes").Type  micTab @@ hightlight id_;_68882_;_script infofile_;_ZIP::ssf13.xml_;_
	VbWindow("frmMDI").VbWindow("frmSystemTables").AcxTable("Taxes").SetCellData 1,4,TaxName @@ hightlight id_;_68882_;_script infofile_;_ZIP::ssf14.xml_;_
	VbWindow("frmMDI").VbWindow("frmSystemTables").AcxTable("Taxes").Type Datatable.value("Value","TaxSetup") @@ hightlight id_;_68882_;_script infofile_;_ZIP::ssf15.xml_;_
	VbWindow("frmMDI").VbWindow("frmSystemTables").AcxTable("Taxes").Type  micTab @@ hightlight id_;_68882_;_script infofile_;_ZIP::ssf16.xml_;_
	VbWindow("frmMDI").VbWindow("frmSystemTables").AcxTable("Taxes").Type  micTab @@ hightlight id_;_68882_;_script infofile_;_ZIP::ssf17.xml_;_
	VbWindow("frmMDI").VbWindow("frmSystemTables").AcxTable("Taxes").SetCellData 1,5,Datatable.value("Value","TaxSetup")
	VbWindow("frmMDI").VbWindow("frmSystemTables").VbButton("Save").Click @@ hightlight id_;_462028_;_script infofile_;_ZIP::ssf19.xml_;_
	wait(10)
	VbWindow("frmMDI").VbWindow("frmSystemTables").VbButton("Close").Click
	
	strTestCaseName = Environment.Value("TestName")
	If err.number = 0 Then
		blnFind = True
	  Else
		blnFind = False
	End If
	
	UpdateTestCaseStatusInExcel strTestCaseName, blnFind
End If
