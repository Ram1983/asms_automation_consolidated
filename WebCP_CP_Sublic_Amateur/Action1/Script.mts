﻿

blnFind=True
err.number = 0


'Import data from Input data sheet  from folder
datatable.ImportSheet ProjectFolderPath&"\Input_Data_Sheet.xls","cpAmateur","cpAmateur"
n=datatable.GetSheet("cpAmateur").GetRowCount

With Browser("Application").Page("Application")
    'For i = 1 To n Step 1
    If n>0 Then
        datatable.SetCurrentRow(1)
        
        With  Browser("title:=.*Application.*").Page("title:=.*Application.*")
            If datatable.value("Stationtype","cpAmateur")="Base Station" And datatable.value("Amatuertype","cpAmateur")="Novice"  And Strcomp(Datatable.Value("Edit","cpAmateur"),"yes",1)<>0 Then
                
                .Link("text:=Base Station","html tag:=A").Clicks
                wait(3)
                
                '----Full
            ElseIf datatable.value("Stationtype","cpAmateur")="Base Station" And datatable.value("Amatuertype","cpAmateur")="Full" And Strcomp(Datatable.Value("Edit","cpAmateur"),"yes",1)<>0 Then  
                .Link("text:=Base Station","html tag:=A").Clicks
                
            End If
        End With

        '======= Base station
       
        If Strcomp(Datatable.Value("Edit","cpAmateur"),"yes",1)=0 Then
            fn_EditSubLicenses "station",  Datatable.Value("typeOfStation","cpAmateur"),"Amateur"
            Browser("title:=.*Application.*").Page("title:=.*Application.*").WebEdit("name:=tbl_site__siteInfo1").Sets ""
        Else 
            If .Frame("Frame_2").WebEdit("tbl_site__siteInfo16").Exist Then
            	.Frame("Frame_2").WebEdit("tbl_site__siteInfo16").Set Datatable.value("Contactperson","cpAmateur")&fnRandomNumber(4)' "contact person"
            End If
            .Frame("Frame_2").WebEdit("tbl_site__siteInfo1").Set Datatable.value("Barscerti","cpAmateur")&fnRandomNumber(4)
            .Frame("Frame_2").WebEdit("tbl_site__siteInfo12").Set Datatable.value("Purpose","cpAmateur")			
            .Frame("Frame_2").WebEdit("tbl_site__siteStreet").Set Datatable.value("Street","cpAmateur")
             If Browser("Customer Home Page").Page("Application_2").Frame("Frame").WebList("tbl_site__siteCity").Exist(5) Then
             	Browser("Customer Home Page").Page("Application_2").Frame("Frame").WebList("tbl_site__siteCity").Select 2
             End If @@ script infofile_;_ZIP::ssf41.xml_;_
            
        End If
        
        '======Save station
        .Frame("Frame_2").WebElement("img_0").Clicks
       
       '=========Add Equipmwet transceiver
        
        If Datatable.value("Equiptype","cpAmateur")="Add Transceiver" And Strcomp(Datatable.Value("Edit","cpAmateur"),"yes",1)<>0 Then			  
            .Frame("Frame_2").WebElement("img_1").Clicks
            fn_LoadpageInWebCP
            .Frame("Frame_3").WebList("tbl_equipment__ApprovalNum").Select(1)' "BOT/TA/TRANSMITTER/1"
        ElseIf datatable.value("Equiptype","cpAmateur")="Add Transmitter" And Strcomp(Datatable.Value("Edit","cpAmateur"),"yes",1)<>0 Then
          
          '=====adding equipment -transmitter
            .Frame("Frame_2").WebElement("img_2").Clicks
            fn_LoadpageInWebCP
        End If	
        
        
        If Strcomp(Datatable.Value("Edit","cpAmateur"),"yes",1)<>0 Then

           With Browser("name:=Application").Page("title:=Application")
            If .WebList("html id:=tbl_equipment__ApprovalNum").Exist(3) Then
               If Datatable.value("EquipmentApprovalNum","cpAmateur")="" Then
               	   If .WebList("html id:=tbl_equipment__ApprovalNum").Exist(40) Then
               	   	  .WebList("html id:=tbl_equipment__ApprovalNum").Selects 1'"random"
               	   End If
               Else 
                   If .WebList("html id:=tbl_equipment__ApprovalNum").Exist(40) Then
                   	  .WebList("html id:=tbl_equipment__ApprovalNum").Selects 1'"random"
                   End If
               End If
               
                For intC = 1 To 20 Step 1
                    If Instr(1,.WebList("html id:=tbl_equipment__equipMake","name:=tbl_equipment__equipMake").GetROProperty("selection"),"Select",1)>0 Then
                        Wait 3
                    Else
                        Exit For
                    End If
                Next
            End If
           End With
         
            If .Frame("Frame_3").WebEdit("tbl_equipment__equipSerialNum").Exist(10) Then
                .Frame("Frame_3").WebEdit("tbl_equipment__equipSerialNum").Set Datatable.value("Equipserail","cpAmateur")&fnRandomNumber(4)
            End If
            If .Frame("Frame_3").WebList("tbl_antenna__AntennaCode").Exist(10) Then
                .Frame("Frame_3").WebList("tbl_antenna__AntennaCode").Select(1)' "Curtain Antenna"
            End If
            If .Frame("Frame_3").WebList("tbl_equipment__EmissionClass").Exist(20) Then
               If  Datatable.Value("EmissionClass","cpAmateur")="" Then
               	   .Frame("Frame_3").WebList("tbl_equipment__EmissionClass").Selects 1'"random"
               Else 
                  .Frame("Frame_3").WebList("tbl_equipment__EmissionClass").Select  1'Datatable.Value("EmissionClass","cpAmateur")
               End If
           End If
        Else
            fn_EditSubLicenses "",  Datatable.Value("typeOfStation","cpAmateur"),"Amateur"
            If .Frame("Frame_3").WebEdit("tbl_equipment__equipSerialNum").Exist(10) Then
                .Frame("Frame_3").WebEdit("tbl_equipment__equipSerialNum").Sets""
            End If
        End If
        fn_LoadpageInWebCP

        '=======Save equipment
        .Frame("Frame_3").WebElement("img_0").Clicks
       
        
    End If
    
End With


'Test results dsiplayed in Excel
If err.number = 0 Then
    blnFind = True
Else
    blnFind = False
End If

'fn_SendStatusToExcelReport blnFind

UpdateTestCaseStatusInExcel Environment("TestName"),blnFind



