﻿wait(2)
blnFind=False
blnVal=False
err.number = 0

datatable.ImportSheet ProjectFolderPath &"\Desktop_Input_Data_Sheet\Input_Data_Sheet.xls","Licence_Edit","Licence_Edit"
n=datatable.GetSheet("Licence_Edit").GetRowCount


'For i = 1 To n Step 1 
With VbWindow("frmMDI")
  If n>0 Then
    datatable.SetCurrentRow(1)	
    .WinMenu("Menu").Select "Licence;Edit Info"
    With .VbWindow("frmModLicense")
      While Not .Exist(15)
      Wend
      wait(3)
      
      .ActiveX("TCIComboBox.tcb").VbComboBox("cboLicType").Select Datatable.Value("LicType","Licence_Edit") '"Licence for Frequency"
      wait(2)
      .ActiveX("TCIComboBox.tcb").VbComboBox("cboLicType").Type micTab
      wait(5)
      If .ActiveX("TCIComboBox.tcb_2").VbComboBox("cboApplicationNo").Exist Then
        Set send=CreateObject("Wscript.Shell")
        .ActiveX("TCIComboBox.tcb_2").VbComboBox("cboApplicationNo").Type RandomNumber(1,999)
        If .VbButton("Next").GetROProperty("enabled")=False Then
          .ActiveX("TCIComboBox.tcb_2").VbComboBox("cboApplicationNo").Select Datatable.Value("LicenceNum","Licence_Edit")
          send.SendKeys "{DEL}"
          If .VbButton("Next").GetROProperty("enabled")=False Then
            blnVal=True
          End If
        End If
        
      End If
      .ActiveX("TCIComboBox.tcb_2").VbComboBox("cboApplicationNo").Select Datatable.Value("LicenceNum","Licence_Edit") '(8) '"02621-1 / SAT"
      '	VbWindow("frmMDI").VbWindow("frmModLicense").ActiveX("TCIComboBox.tcb_3").VbComboBox("cboLicense").Select 1'"Aeronautical Licence"
      '	VbWindow("frmMDI").VbWindow("frmModLicense").ActiveX("TCIComboBox.tcb_4").VbComboBox("cboClientNo").Select 1'"01304-1"
      
      .VbButton("Next").Click
    End With
    
    With .VbWindow("frmEditLicense")
      While Not .Exist
      Wend
      ' Issue Date
      
      
      .ActiveX("MaskEdBox").Drag 84,7
      .Drop 125,164
      .ActiveX("MaskEdBox").Drag 66,8
      .Drop 155,162
      
      '	VbWindow("frmMDI").VbWindow("frmEditLicense").ActiveX("MaskEdBox").Drag 74,4
      '	VbWindow("frmMDI").VbWindow("frmEditLicense").Drop 500,400
      .ActiveX("MaskEdBox").Type Datatable.Value("IssueDate","Licence_Edit") '"05/01/2005"
      .ActiveX("MaskEdBox").Type  micTab
    End With
    
    If .Dialog("#32770").WinButton("OK").Exist(5) Then
      .Dialog("#32770").WinButton("OK").Click
    Else
      'Operation Start date
      With	.VbWindow("frmEditLicense")
        .ActiveX("MaskEdBox_2").Drag 73,6
        .Drop 170,239
        .ActiveX("MaskEdBox_2").Type Datatable.Value("OperationStartDate","Licence_Edit") '"06/01/2006"
      End With
      
      If .Dialog("#32770").WinButton("OK").Exist(5) Then
        .Dialog("#32770").WinButton("OK").Click
      Else
        'Valid From
        With	.VbWindow("frmEditLicense")
          .ActiveX("MaskEdBox_3").Drag 74,7
          .Drop 162,286
          .ActiveX("MaskEdBox_3").Type Datatable.Value("ValidFrom","Licence_Edit")'"07/01/2019"
        End With
        
        If .Dialog("#32770").WinButton("OK").Exist(5) Then
          .Dialog("#32770").WinButton("OK").Click
        Else
          
          'Valid To
          With	.VbWindow("frmEditLicense")
            .ActiveX("MaskEdBox_4").Drag 66,1
            .Drop 188,322
            .ActiveX("MaskEdBox_4").Type Datatable.Value("ValidTo","Licence_Edit") '"06/01/2020"
          End With
          
          If .Dialog("#32770").WinButton("OK").Exist(5) Then
            .Dialog("#32770").WinButton("OK").Click
          Else
            With	.VbWindow("frmEditLicense")
              .VbEdit("txtRenewalFrequency").SetSelection 0,1
              .VbEdit("txtRenewalFrequency").SetSelection 0,2
              .VbEdit("txtRenewalFrequency").Set Datatable.Value("RenewalFrequency","Licence_Edit") '"12"
            End With
            
            If .Dialog("#32770").WinButton("OK").Exist(5) Then
              .Dialog("#32770").WinButton("OK").Click
            Else
              
              With	.VbWindow("frmEditLicense")
                .VbEdit("txtLicenseTerm").SetSelection 0,2
                .VbEdit("txtLicenseTerm").Set Datatable.Value("LicenseTerm","Licence_Edit") '"15"
                .VbEdit("txtLicenseTerm").Type  micTab
              End With
              
              If .Dialog("#32770").WinButton("OK").Exist(5) Then
                .Dialog("#32770").WinButton("OK").Click
              Else
                With	.VbWindow("frmEditLicense")
                  .VbEditor("txtNote").SetCaretPos 0,0 '2,33
                  .VbEditor("txtNote").Type Datatable.Value("Note","Licence_Edit")
                  
                  
                  '					VbWindow("frmMDI").VbWindow("frmEditLicense").VbButton("Print Certificate").Click
                  '					If VbWindow("frmReportViewer").Exist(10) Then
                  '						VbWindow("frmReportViewer").Minimize
                  '					End If
                  
                  
                  .VbButton("Save").Click
                  wait(5)
                  .VbButton("Close").Click
                End With
              End If
            End If
          End If
        End If
      End If
    End If
    
    strTestCaseName = Environment.Value("TestName")
    If err.number = 0 Then
      blnFind = True
    Else
      blnFind = False
    End If
    
    UpdateTestCaseStatusInExcel strTestCaseName, blnFind
    
  End If
End With


