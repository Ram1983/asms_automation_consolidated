﻿wait(4)
blnFind=True
err.number = 0
datatable.ImportSheet ProjectFolderPath &"\Desktop_Input_Data_Sheet\Input_Data_Sheet.xls","TypeApproval","Global"
n=datatable.GetSheet("Global").GetRowCount
datatable.SetCurrentRow(1)	
With VbWindow("frmMDI")
	.WinMenu("Menu").Select "Application Processing;Type Approval;Modify"
	With .VbWindow("frmFindRadio")
		While Not .ActiveX("TCIComboBox.tcb").WinEdit("Edit").Exist
		Wend
		.ActiveX("TCIComboBox.tcb").WinEdit("Edit").Set datatable.value("AName","Global") @@ hightlight id_;_460762_;_script infofile_;_ZIP::ssf1.xml_;_
		.ActiveX("TCIComboBox.tcb").VbComboBox("cboDealer").Select datatable.value("AName","Global") @@ hightlight id_;_1771194_;_script infofile_;_ZIP::ssf2.xml_;_
		.VbButton("Next").Click @@ hightlight id_;_264922_;_script infofile_;_ZIP::ssf3.xml_;_
		Wait 10
	End With
	With .VbWindow("frmTypeApprRadioDetail")
		If VbWindow("frmMDI").Dialog("#32770").WinEdit("Please describe why you").Exist(40) Then
			VbWindow("frmMDI").Dialog("#32770").WinEdit("Please describe why you").Set GenerateRandomString(5)
			If VbWindow("frmMDI").Dialog("#32770").WinButton("OK").Exist Then
				VbWindow("frmMDI").Dialog("#32770").WinButton("OK").Click
			End If
		End If
		.ActiveX("TCIComboBox.tcb").VbComboBox("cboStatus").Select datatable.value("LicenseStatus","Global") @@ hightlight id_;_592706_;_script infofile_;_ZIP::ssf4.xml_;_
		.VbButton("Save").Click @@ hightlight id_;_330074_;_script infofile_;_ZIP::ssf5.xml_;_
		Wait 5 @@ hightlight id_;_330074_;_script infofile_;_ZIP::ssf6.xml_;_
		.VbButton("Print").Click @@ hightlight id_;_396250_;_script infofile_;_ZIP::ssf7.xml_;_
		Wait 15
	End With
End With
With VbWindow("frmReportViewer")
		If .Dialog("Print").WinButton("OK").Exist Then
			.Dialog("Print").WinButton("OK").Click
			Wait 5
		End If
End With
With Dialog("Save Print Output As")
	
	.WinEdit("File name:").Set fn_report @@ hightlight id_;_461512_;_script infofile_;_ZIP::ssf10.xml_;_
	.WinButton("Save").Click @@ hightlight id_;_461100_;_script infofile_;_ZIP::ssf11.xml_;_
	Wait 5
End With
VbWindow("frmReportViewer").VbButton("Close").Click @@ hightlight id_;_1313154_;_script infofile_;_ZIP::ssf12.xml_;_
With VbWindow("frmMDI")
	.VbWindow("frmTypeApprRadioDetail").VbButton("Close").Click @@ hightlight id_;_395682_;_script infofile_;_ZIP::ssf13.xml_;_
End With

If Err.number=0 Then
	blnFind=true
End If

UpdateTestCaseStatusInExcel Environment("TestName"),blnFind
