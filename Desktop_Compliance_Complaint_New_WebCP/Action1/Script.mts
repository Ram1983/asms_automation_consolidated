﻿blnVal=False
VbWindow("frmMDI").WinMenu("Menu").Select "Compliance;Complaint;New"

While Not VbWindow("frmMDI").VbWindow("frmBrowser").Page("Complaint or Enquiry").WebList("from_ClientID").Exist
Wend
If DataTable("AsmsClient","Global")<>"" Then
	VbWindow("frmMDI").VbWindow("frmBrowser").Page("Complaint or Enquiry").WebList("from_ClientID").Select DataTable.Value("Client","GLobal")
Else
	VbWindow("frmMDI").VbWindow("frmBrowser_7").Page("Complaint or Enquiry").WebEdit("from_FirstName").Set GenerateRandomString(6)
	VbWindow("frmMDI").VbWindow("frmBrowser_7").Page("Complaint or Enquiry").WebEdit("from_LastName").Set GenerateRandomString(7)
End If
VbWindow("frmMDI").VbWindow("frmBrowser").Page("Complaint or Enquiry").WebEdit("from_emailAddress").Set DataTable.Value("Email","GLobal")&RandomNumber(1,99999)&"@gmail.com"
VbWindow("frmMDI").VbWindow("frmBrowser").Page("Complaint or Enquiry").WebEdit("Password").Set DataTable.Value("Password","GLobal")
If DataTable("AsmsClient","Global")<>"" Then
   VbWindow("frmMDI").VbWindow("frmBrowser").Page("Complaint or Enquiry").WebList("about_ClientID").Select DataTable.Value("Client","GLobal")
End If
If VbWindow("frmMDI").VbWindow("frmBrowser").Page("Complaint or Enquiry").WebElement("img_1").Exist Then
	VbWindow("frmMDI").VbWindow("frmBrowser").Page("Complaint or Enquiry").WebElement("img_1").Click
End If
If VbWindow("frmMDI").VbWindow("frmBrowser_2").Page("Complaint or Enquiry").WebElement("img_2_0").Exist Then
	VbWindow("frmMDI").VbWindow("frmBrowser_2").Page("Complaint or Enquiry").WebElement("img_2_0").Click
End If
If VbWindow("frmMDI").Dialog("Message from webpage").Static("Complaint has been Submitted").Exist Then
	If VbWindow("frmMDI").Dialog("Message from webpage").WinButton("OK").Exist Then
		VbWindow("frmMDI").Dialog("Message from webpage").WinButton("OK").Click
		If VbWindow("frmMDI").VbWindow("frmBrowser_3").Page("Complaint or Enquiry").WebElement("Complaint or Enquiry Status").Exist Then
			VbWindow("frmMDI").VbWindow("frmBrowser_3").Page("Complaint or Enquiry").WebElement("Complaint or Enquiry Status").Click
			If VbWindow("frmMDI").VbWindow("frmBrowser_3").Page("Complaint or Enquiry").WebElement("WebTable").Exist Then
				If Instr(1,Trim(VbWindow("frmMDI").VbWindow("frmBrowser_3").Page("Complaint or Enquiry").WebElement("WebTable").GetROProperty("innertext")),DataTable.Value("SubmitStatus","Global"),1)>0 Then
					If VbWindow("frmMDI").VbWindow("frmBrowser_3").Page("Complaint or Enquiry").WebElement("img_6_1").Exist Then
						VbWindow("frmMDI").VbWindow("frmBrowser_3").Page("Complaint or Enquiry").WebElement("img_6_1").Click
						If VbWindow("frmMDI").VbWindow("frmBrowser_3").Page("Complaint or Enquiry").WebElement("Complaint or Enquiry Status").Exist Then
							VbWindow("frmMDI").VbWindow("frmBrowser_3").Page("Complaint or Enquiry").WebElement("Complaint or Enquiry Status").Click
							If Instr(1,Trim(VbWindow("frmMDI").VbWindow("frmBrowser_4").Page("Complaint or Enquiry").WebElement("WebTable").GetROProperty("innertext")),DataTable.Value("ClosedStatus","Global"),1)>0 Then
								VbWindow("frmMDI").VbWindow("frmBrowser_4").Page("Complaint or Enquiry").WebElement("img_5_1").Click
								If VbWindow("frmMDI").VbWindow("frmBrowser_5").Page("Complaint or Enquiry").WebElement("Complaint or Enquiry Status").Exist Then
									VbWindow("frmMDI").VbWindow("frmBrowser_5").Page("Complaint or Enquiry").WebElement("Complaint or Enquiry Status").Click
									If Instr(1,Trim(VbWindow("frmMDI").VbWindow("frmBrowser_5").Page("Complaint or Enquiry").WebElement("WebTable").GetROProperty("innertext")),DataTable("Reopendstatus","Global"),1)>0 Then
										blnVal=True
									End If
								End If
							End If
						End If
					End If
				End If
			End If
		End If
	End If
End If


UpdateTestCaseStatusInExcel Environment("TestName"),blnVal
