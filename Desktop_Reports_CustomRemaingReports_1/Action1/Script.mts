﻿
Public Function GetItemIndex(ByRef test_object, ByRef itemText)
       content = test_object.GetContent()
       lines = split(content, vbLf)
       linesCount = uBound(lines) + 1
       For index = 0 To linesCount
       		text = lines(index)
       		if text = itemText Then
       			GetItemIndex = index
       			Exit Function
       		end if
       Next
       Reporter.ReportEvent micFail, "GetItemIndex", "Cannot find the specified item"
       GetItemIndex = -1
       
End Function
RegisterUserFunc "VbComboBox", "GetItemIndex", "GetItemIndex", True

Public Function ASMSComboBox(ByRef objectName, ByRef objectValue)

Set objD=Description.Create
objD("micclass").value="VbComboBox"
objD("vbname").value=objectName
Set obj=VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ChildObjects(objD)
'msgbox obj.count
'obj(0).highlight
'print obj(0).GetSelection
'print objectValue
'msgbox len(obj(0).GetSelection)
'msgbox len(objectValue)

If not obj(0).GetSelection = objectValue Then
	obj(0).Select objectValue
End If


End Function

'ASMSComboBox "cboAppType","WEBCP"
'ASMSComboBox "cboAppStatus","*"

'ASMSComboBox "cboApplicantLinked","AABB TEl   (# MP9)"
'ASMSComboBox "cboServiceClassSingle","AERONÁUTICA MÓVEL"

'ASMSComboBox "cboApplicantLinked",Datatable.Value("ApplicantName","CustomReportsRemaining") '"AABB TEl   (# MP9)"

'cboAppType.Select "*" @@ hightlight id_;_1250178_;_script infofile_;_ZIP::ssf608.xml_;_
'cboAppStatus
'cboApplicant
			
blnFind=True
err.number = 0
datatable.ImportSheet ProjectFolderPath &"\Input_Data_Sheet\Input_Data_Sheet.xls","CustomReportsRemaining","CustomReportsRemaining"
n=datatable.GetSheet("CustomReportsRemaining").GetRowCount()

'For i = 1 To n Step 1
If n>0 Then
	datatable.SetCurrentRow(1)	
	Environment.LoadFromFile(ProjectFolderPath &"\ASMS_Variables.xml")
	proj = Datatable.Value("Client","CustomReportsRemaining")
	
	'If InStr(Environment("Portuguese_Language"),proj)>0 Then @@ hightlight id_;_11_;_script infofile_;_ZIP::ssf317.xml_;_
		'VbWindow("frmMDI").InsightObject("InsightObject_18").Click @@ hightlight id_;_3_;_script infofile_;_ZIP::ssf331.xml_;_
		'VbWindow("frmMDI").InsightObject("InsightObject_19").Click @@ hightlight id_;_10_;_script infofile_;_ZIP::ssf332.xml_;_
	If Datatable.value("Client","CustomReportsRemaining")="JMC" Then
		VbWindow("frmMDI").InsightObject("InsightObject").Click @@ hightlight id_;_1_;_script infofile_;_ZIP::ssf1.xml_;_
		VbWindow("frmMDI").InsightObject("InsightObject_2").Click @@ hightlight id_;_9_;_script infofile_;_ZIP::ssf2.xml_;_
	ElseIf Datatable.value("Client","CustomReportsRemaining")="BOT" Then
		VbWindow("frmMDI").InsightObject("InsightObject_7").Click @@ hightlight id_;_3_;_script infofile_;_ZIP::ssf316.xml_;_
		VbWindow("frmMDI").InsightObject("InsightObject_11").Click
	End If
	
	wait(7)
	
	While Not VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").Exist(50)
	Wend
	'Report Tabs - JMC
'	VbWindow("frmMDI").InsightObject("InsightObject_3").Click @@ hightlight id_;_199914_;_script infofile_;_ZIP::ssf25.xml_;_
'	VbWindow("frmMDI").InsightObject("InsightObject_4").Click @@ hightlight id_;_10_;_script infofile_;_ZIP::ssf27.xml_;_
'	VbWindow("frmMDI").InsightObject("InsightObject_5").Click @@ hightlight id_;_41_;_script infofile_;_ZIP::ssf28.xml_;_
'	VbWindow("frmMDI").InsightObject("InsightObject_10").Click @@ hightlight id_;_15_;_script infofile_;_ZIP::ssf291.xml_;_
'	VbWindow("frmMDI").InsightObject("InsightObject_6").Click @@ hightlight id_;_8_;_script infofile_;_ZIP::ssf292.xml_;_
'	VbWindow("frmMDI").InsightObject("InsightObject_8").Click @@ hightlight id_;_6_;_script infofile_;_ZIP::ssf297.xml_;_
'	VbWindow("frmMDI").InsightObject("InsightObject_9").Click
	
	'	'for MOZ
'	VbWindow("frmMDI").InsightObject("InsightObject_20").Click @@ hightlight id_;_38_;_script infofile_;_ZIP::ssf333.xml_;_
'	VbWindow("frmMDI").InsightObject("InsightObject_21").Click @@ hightlight id_;_46_;_script infofile_;_ZIP::ssf334.xml_;_
'	VbWindow("frmMDI").InsightObject("InsightObject_22").Click @@ hightlight id_;_50_;_script infofile_;_ZIP::ssf335.xml_;_
'	VbWindow("frmMDI").InsightObject("InsightObject_23").Click @@ hightlight id_;_55_;_script infofile_;_ZIP::ssf336.xml_;_
'	VbWindow("frmMDI").InsightObject("InsightObject_24").Click @@ hightlight id_;_60_;_script infofile_;_ZIP::ssf337.xml_;_
'	VbWindow("frmMDI").InsightObject("InsightObject_25").Click @@ hightlight id_;_64_;_script infofile_;_ZIP::ssf338.xml_;_
'	VbWindow("frmMDI").InsightObject("InsightObject_26").Click @@ hightlight id_;_72_;_script infofile_;_ZIP::ssf339.xml_;_
	'
		
	AssignInputs="No"
	ReportTab=""
	ReportTabCount=0
	For Iterator = 1 To datatable.GetSheet("CustomReportsRemaining").GetRowCount
		DataTable.LocalSheet.SetCurrentRow(Iterator)
	
		AssignInputs="No"
		
		If Datatable.Value("ReportTab","CustomReportsRemaining")="" and Datatable.Value("ReportName","CustomReportsRemaining")="" and Datatable.Value("RunReport","CustomReportsRemaining")="" Then
			Exit for
		End If
	
		If Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then	
			
			If ReportTab<>Datatable.Value("ReportTab","CustomReportsRemaining") Then
				wait(3)
				ReportTab=Datatable.Value("ReportTab","CustomReportsRemaining")
				ReportTabCount=0
				AssignInputs="No"
			End If
			If Iterator > 1 and SelectedReport="Yes"	Then
				VbWindow("frmMDI").WinObject("MDIClient").VScroll micSetPos, -7
			End If
			SelectedReport="No" @@ hightlight id_;_922138_;_script infofile_;_ZIP::ssf58.xml_;_
			Wait(1)
			If Datatable.Value("ReportTab","CustomReportsRemaining")="Application Analysis" and Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
				If ReportTabCount=0 Then
					ReportTabCount=1	
					'If InStr(Environment("Portuguese_Language"),proj)>0 Then
						'VbWindow("frmMDI").InsightObject("InsightObject_21").Click
					Wait(5)
					If Datatable.value("Client","CustomReportsRemaining")="JMC" Then
						VbWindow("frmMDI").InsightObject("InsightObject_4").Click
					ElseIf Datatable.value("Client","CustomReportsRemaining")="BOT" Then
						VbWindow("frmMDI").InsightObject("InsightObject_12").Click	
					End If
				End If
				Wait(5)
				If Datatable.Value("ReportName","CustomReportsRemaining")="Status of application for a specified customer" and Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("SSTab").VbRadioButton("Status of an application").Set
					SelectedReport="Yes"
				ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="Number of applications of a particular type and status over a defined period of time" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("SSTab").VbRadioButton("Number of applications").Set
					SelectedReport="Yes"
			    ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="No. Of type approval appl submitted day, week and month" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
			    	VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("SSTab").VbRadioButton("Number of Type approval").Set
					SelectedReport="Yes"
				End If

			ElseIf Datatable.Value("ReportTab","CustomReportsRemaining")="Licence Analysis" and Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
				If ReportTabCount=0 Then
					ReportTabCount=1	
					'If InStr(Environment("Portuguese_Language"),proj)>0 Then
						'VbWindow("frmMDI").InsightObject("InsightObject_22").Click
					If Datatable.value("Client","CustomReportsRemaining")="JMC" Then
						VbWindow("frmMDI").InsightObject("InsightObject_5").Click
					ElseIf Datatable.value("Client","CustomReportsRemaining")="BOT" Then	
					    VbWindow("frmMDI").InsightObject("InsightObject_13").Click	
					End If
				End If
			
				'If proj<>"MOZ" Then
					Wait(5)
					If Datatable.Value("ReportName","CustomReportsRemaining")="Applications ready to be licensed" and Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("SSTab").VbRadioButton("Applications ready to").Set
						SelectedReport="Yes"
					ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="Licenses status over a defined period of time" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("SSTab").VbRadioButton("Licenses status over a").Set @@ hightlight id_;_264228_;_script infofile_;_ZIP::ssf361.xml_;_
						SelectedReport="Yes"
					ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="Licenses status over a defined period of time with customers contact information" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("SSTab").VbRadioButton("Licenses status over a_2").Set
						SelectedReport="Yes"
					ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="Systems" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("SSTab").VbRadioButton("Systems").Set
						SelectedReport="Yes"
					End If
					
				'End If
			ElseIf Datatable.Value("ReportTab","CustomReportsRemaining")="Financial 1" and Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
				If ReportTabCount=0 Then
					ReportTabCount=1	
					'If InStr(Environment("Portuguese_Language"),proj)>0 Then
						'VbWindow("frmMDI").InsightObject("InsightObject_24").Click @@ hightlight id_;_60_;_script infofile_;_ZIP::ssf337.xml_;_
					If Datatable.value("Client","CustomReportsRemaining")="JMC" Then
						VbWindow("frmMDI").InsightObject("InsightObject_6").Click
					ElseIf Datatable.value("Client","CustomReportsRemaining")="BOT" Then	
						VbWindow("frmMDI").InsightObject("InsightObject_14").Click
					End If @@ hightlight id_;_8_;_script infofile_;_ZIP::ssf320.xml_;_
				End If
				Wait(5)
				If Datatable.Value("ReportName","CustomReportsRemaining")="List of Customers" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("SSTab").VbRadioButton("List of Customers").Set @@ hightlight id_;_198626_;_script infofile_;_ZIP::ssf364.xml_;_
					SelectedReport="Yes"
				ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="Account Statement" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("SSTab").VbRadioButton("Account Statement").Set @@ hightlight id_;_198648_;_script infofile_;_ZIP::ssf365.xml_;_
					SelectedReport="Yes"
				ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="Annual Billing" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("SSTab").VbRadioButton("Annual Billing").Set @@ hightlight id_;_199924_;_script infofile_;_ZIP::ssf366.xml_;_
					SelectedReport="Yes"
				ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="Fee collected for a particular type of service over a defined period of time" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("SSTab").VbRadioButton("Fees collected for a particula").Set @@ hightlight id_;_264070_;_script infofile_;_ZIP::ssf367.xml_;_
					SelectedReport="Yes"
				ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="License fee calculated and collected over a defined period of time" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("SSTab").VbRadioButton("Licence fee calculated").Set @@ hightlight id_;_1904068_;_script infofile_;_ZIP::ssf368.xml_;_
					SelectedReport="Yes"
				End If
			ElseIf Datatable.Value("ReportTab","CustomReportsRemaining")="Financial 2" and Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
				If ReportTabCount=0 Then
					ReportTabCount=1	
					'If InStr(Environment("Portuguese_Language"),proj)>0 Then
						'VbWindow("frmMDI").InsightObject("InsightObject_25").Click
					If Datatable.value("Client","CustomReportsRemaining")="JMC" Then
						VbWindow("frmMDI").InsightObject("InsightObject_8").Click
					ElseIf Datatable.value("Client","CustomReportsRemaining")="BOT" Then	
					    VbWindow("frmMDI").InsightObject("InsightObject_15").Click
					End If
	            End If	
	            
	            
				If Datatable.Value("ReportName","CustomReportsRemaining")="Journal Report" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
		            VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("SSTab").VbRadioButton("Journal Report").Set
					SelectedReport="Yes"
				ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="Payment Gateway Response" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then	
                     Wait(3)
                     VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("SSTab_2").VbRadioButton("Payment Gateway Response").Set @@ hightlight id_;_67614_;_script infofile_;_ZIP::ssf862.xml_;_
				     SelectedReport="Yes"
				End If
			ElseIf Datatable.Value("ReportTab","CustomReportsRemaining")="Others" and Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
				If ReportTabCount=0 Then
					ReportTabCount=1
					'If InStr(Environment("Portuguese_Language"),proj)>0 Then
						'VbWindow("frmMDI").InsightObject("InsightObject_26").Click
					If Datatable.value("Client","CustomReportsRemaining")="JMC" Then
						VbWindow("frmMDI").InsightObject("InsightObject_9").Click
					ElseIf Datatable.value("Client","CustomReportsRemaining")="BOT" Then	
					    VbWindow("frmMDI").InsightObject("InsightObject_16").Click
					End If
				End If
				If Datatable.Value("ReportName","CustomReportsRemaining")="Customer Transactions" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("SSTab").VbRadioButton("Customer Transactions").Set @@ hightlight id_;_198628_;_script infofile_;_ZIP::ssf370.xml_;_
					SelectedReport="Yes"
				ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="Payments Received" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("SSTab").VbRadioButton("Payments Received").Set @@ hightlight id_;_198634_;_script infofile_;_ZIP::ssf371.xml_;_
					SelectedReport="Yes"
				ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="Forecast of fee collection over a defined period of time" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("SSTab").VbRadioButton("Forecast of fee collection").Set @@ hightlight id_;_198646_;_script infofile_;_ZIP::ssf372.xml_;_
					SelectedReport="Yes"
				ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="Customer License Types" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("SSTab").VbRadioButton("Customers License Types").Set
					SelectedReport="Yes"
				End If
			ElseIf Datatable.Value("ReportTab","CustomReportsRemaining")="Wireless Station Analysis" and Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
				If ReportTabCount=0 Then
					ReportTabCount=1
					'If InStr(Environment("Portuguese_Language"),proj)>0 Then
						'VbWindow("frmMDI").InsightObject("InsightObject_20").Click
					If Datatable.value("Client","CustomReportsRemaining")="JMC" Then
						VbWindow("frmMDI").InsightObject("InsightObject_3").Click
						'ElseIf Datatable.value("Client","CustomReportsRemaining")="BOT" Then
                        'VbWindow("frmMDI").InsightObject("InsightObject_28").Click @@ hightlight id_;_2_;_script infofile_;_ZIP::ssf838.xml_;_
					End If
				End If
				Wait(3)
				If Datatable.Value("ReportName","CustomReportsRemaining")="Number of wireless stations operating in given frequency band" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("SSTab").VbRadioButton("Number of wireless stations").Set
					SelectedReport="Yes"
				ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="Number of channels, associated bandwidth, and service type supported, Available for assignment in a particular area" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("SSTab").VbRadioButton("Number of channels, associated").Set
					SelectedReport="Yes"
				ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="Number of wireless stations operating in particular area" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
					Wait(3)
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("SSTab").VbRadioButton("Number of wireless stations_2").Set
					SelectedReport="Yes"
					Wait(3)
				ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="Complete details of all networks, irrespective of Band and location, of a  particular user" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("SSTab").VbRadioButton("Complete details of all").Set
					SelectedReport="Yes"
				ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="The number and types of equipment that has been Certified for operation" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("SSTab").VbRadioButton("The number and types of").Set
					SelectedReport="Yes"
				ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="Users by band report" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("SSTab").VbRadioButton("Users by band Report").Set @@ hightlight id_;_593078_;_script infofile_;_ZIP::ssf409.xml_;_
					SelectedReport="Yes"
				ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="Frequencies available in a particular area" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
					Wait(3)
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("SSTab").VbRadioButton("Frequencies available").Set @@ hightlight id_;_985108_;_script infofile_;_ZIP::ssf410.xml_;_
					SelectedReport="Yes"
				ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="Frequency Information on Microwave Links" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("SSTab").VbRadioButton("Frequency Information").Set
					SelectedReport="Yes"	
				ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="New frequency assignments made versus frequency Band and service over a defined period of time" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
                    Wait(5)
                    VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("SSTab_2").VbRadioButton("New frequency assignments").Set @@ hightlight id_;_723180_;_script infofile_;_ZIP::ssf839.xml_;_
					SelectedReport="Yes"
				End If
			ElseIf Datatable.Value("ReportTab","CustomReportsRemaining")="Complaints" and Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
				If ReportTabCount=0 Then
					ReportTabCount=1
					'If InStr(Environment("Portuguese_Language"),proj)>0 Then
						'VbWindow("frmMDI").InsightObject("InsightObject_23").Click
					If Datatable.value("Client","CustomReportsRemaining")="JMC" Then
						VbWindow("frmMDI").InsightObject("InsightObject_10").Click
	                ElseIf Datatable.Value("Client","CustomReportsRemaining")="BOT" Then @@ hightlight id_;_332168_;_script infofile_;_ZIP::ssf820.xml_;_
                         VbWindow("frmMDI").InsightObject("InsightObject_27").Click @@ hightlight id_;_3_;_script infofile_;_ZIP::ssf821.xml_;_
					End If
				End If
				If Datatable.Value("ReportName","CustomReportsRemaining")="Number of Interface complaints cleared and pending in a particular area over a defined period of time" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("SSTab").VbRadioButton("Number of interference").Set
					SelectedReport="Yes"
				ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="Trend of complaints" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("SSTab").VbRadioButton("Trend of complaints").Set
					SelectedReport="Yes"
				ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="Number of days to resolve complaints over defined period of time" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
					 VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("SSTab_2").VbRadioButton("Number of days to resolve").Set @@ hightlight id_;_464500_;_script infofile_;_ZIP::ssf870.xml_;_
                     SelectedReport="Yes"
				ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="Aging of currently open complaints" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then	
					 VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("SSTab_2").VbRadioButton("Aging of currently open").Set
					 SelectedReport="Yes"
				End If
			End If
 @@ hightlight id_;_134600_;_script infofile_;_ZIP::ssf159.xml_;_
 			'Input parameters
			If SelectedReport="Yes" and AssignInputs="No" Then  
				
				If Datatable.Value("ReportTab","CustomReportsRemaining")="Wireless Station Analysis" Then

					If Datatable.Value("ReportName","CustomReportsRemaining")="Number of wireless stations operating in given frequency band" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
						
						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("FrequencyStart").Object.Text=""
						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("FrequencyStart").Type Datatable.Value("FreqStart","CustomReportsRemaining") @@ hightlight id_;_267768_;_script infofile_;_ZIP::ssf516.xml_;_
						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("FrequencyStart").Type  micTab @@ hightlight id_;_267768_;_script infofile_;_ZIP::ssf517.xml_;_
						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("FrequencyEnd").Object.Text=""
						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("FrequencyEnd").Type Datatable.Value("FreqEnd","CustomReportsRemaining") @@ hightlight id_;_1051982_;_script infofile_;_ZIP::ssf520.xml_;_
						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("FrequencyEnd").Type  micTab @@ hightlight id_;_1051982_;_script infofile_;_ZIP::ssf521.xml_;_
		
					ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="Number of channels, associated bandwidth, and service type supported, Available for assignment in a particular area" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then

						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb").VbComboBox("cboCity").Select Datatable.Value("City","CustomReportsRemaining") '"Alto Molocue" @@ hightlight id_;_593422_;_script infofile_;_ZIP::ssf522.xml_;_
						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbEdit("txtDistance").Set Datatable.Value("Distance","CustomReportsRemaining")
						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("FrequencyStart").Object.Text=""
						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("FrequencyStart").Type Datatable.Value("FreqStart","CustomReportsRemaining") @@ hightlight id_;_267768_;_script infofile_;_ZIP::ssf516.xml_;_
						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("FrequencyEnd").Object.Text=""
						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("FrequencyEnd").Type Datatable.Value("FreqEnd","CustomReportsRemaining") @@ hightlight id_;_1051982_;_script infofile_;_ZIP::ssf520.xml_;_
						'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_2").VbComboBox("cboServiceClassSingle").Select Datatable.Value("ServiceClass","CustomReportsRemaining") '"AERONÁUTICA MÓVEL"
						ASMSComboBox "cboServiceClassSingle",Datatable.Value("ServiceClass","CustomReportsRemaining") '"AERONÁUTICA MÓVEL"
						
					ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="Number of wireless stations operating in particular area" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then

						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_3").VbComboBox("cboCity").Select Datatable.Value("City","CustomReportsRemaining") '"Alto Molocue" @@ hightlight id_;_593422_;_script infofile_;_ZIP::ssf532.xml_;_
						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbEdit("txtDistance").Set Datatable.Value("Distance","CustomReportsRemaining") @@ hightlight id_;_724490_;_script infofile_;_ZIP::ssf534.xml_;_
						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbEdit("txtDistance").Type  micTab 
		
					ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="Complete details of all networks, irrespective of Band and location, of a  particular user" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
						'ASMSComboBox "cboApplicantLinked",Datatable.Value("ApplicantName","CustomReportsRemaining") '"AABB TEl   (# MP9)"
						'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb").VbComboBox("cboApplicantLinked").Select Datatable.Value("ApplicantName","CustomReportsRemaining") '"AABB TEl   (# MP9)"
						'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_4").VbComboBox("cboDistrict").Select "*" @@ hightlight id_;_1249312_;_script infofile_;_ZIP::ssf537.xml_;_
						'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_5").VbComboBox("cboCityOnly").Select "*" @@ hightlight id_;_1376550_;_script infofile_;_ZIP::ssf538.xml_;_
						'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_6").VbComboBox("cboRegion").Select "*" @@ hightlight id_;_724490_;_script infofile_;_ZIP::ssf535.xml_;_
		               
		            ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="New frequency assignments made versus frequency Band and service over a defined period of time" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then   
 @@ hightlight id_;_854344_;_script infofile_;_ZIP::ssf844.xml_;_
						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_14").Drag 116,5 @@ hightlight id_;_1117170_;_script infofile_;_ZIP::ssf845.xml_;_
						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbFrame("panOption(1)").Drop 141,25 @@ hightlight id_;_854344_;_script infofile_;_ZIP::ssf846.xml_;_
						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_14").Type Datatable.Value("FreqStart","CustomReportsRemaining") '"80" @@ hightlight id_;_1117170_;_script infofile_;_ZIP::ssf847.xml_;_
						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_23").Drag 65,6 @@ hightlight id_;_919862_;_script infofile_;_ZIP::ssf848.xml_;_
						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbFrame("panOption(1)").Drop 493,32 @@ hightlight id_;_854344_;_script infofile_;_ZIP::ssf849.xml_;_
						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_23").Type Datatable.Value("FreqEnd","CustomReportsRemaining") '"100" @@ hightlight id_;_919862_;_script infofile_;_ZIP::ssf850.xml_;_
						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_24").Drag 76,12 @@ hightlight id_;_1182482_;_script infofile_;_ZIP::ssf851.xml_;_
						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbFrame("panOption(4)").Drop 293,32 @@ hightlight id_;_1313652_;_script infofile_;_ZIP::ssf852.xml_;_
						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_24").Type Datatable.Value("ReportingDateStart","CustomReportsRemaining") '"02022020" @@ hightlight id_;_1182482_;_script infofile_;_ZIP::ssf853.xml_;_
						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_25").Drag 78,10 @@ hightlight id_;_2756512_;_script infofile_;_ZIP::ssf854.xml_;_
						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbFrame("panOption(4)").Drop 450,34 @@ hightlight id_;_1313652_;_script infofile_;_ZIP::ssf855.xml_;_
						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_25").Type Datatable.Value("ReportingDateEnd","CustomReportsRemaining") '"12312021"
						'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_3").VbComboBox("cboServiceClass").Select Datatable.Value("ServiceClass","CustomReportsRemaining") '"AERONAUTICAL MOBILE"
		                Wait(3)
					ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="The number and types of equipment that has been Certified for operation" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
						
						'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_3").VbComboBox("cboEquipmentType").Select "*"
					
					ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="Users by band report" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
						
						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("FrequencyStart").Object.Text=""
						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("FrequencyStart").Type Datatable.Value("FreqStart","CustomReportsRemaining") @@ hightlight id_;_267768_;_script infofile_;_ZIP::ssf516.xml_;_
						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("FrequencyStart").Type  micTab @@ hightlight id_;_267768_;_script infofile_;_ZIP::ssf517.xml_;_
						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("FrequencyEnd").Object.Text=""
						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("FrequencyEnd").Type Datatable.Value("FreqEnd","CustomReportsRemaining") @@ hightlight id_;_1051982_;_script infofile_;_ZIP::ssf520.xml_;_
						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("FrequencyEnd").Type  micTab @@ hightlight id_;_2164456_;_script infofile_;_ZIP::ssf764.xml_;_
'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_21").VbComboBox("cboServiceClass").Select "*" @@ hightlight id_;_1315610_;_script infofile_;_ZIP::ssf765.xml_;_
'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_21").VbComboBox("cboServiceClass").Type  micTab @@ hightlight id_;_1315610_;_script infofile_;_ZIP::ssf766.xml_;_
						
					ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="Frequencies available in a particular area" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
						
						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb").VbComboBox("cboCity").Select Datatable.Value("City","CustomReportsRemaining") '"Alto Molocue" @@ hightlight id_;_593422_;_script infofile_;_ZIP::ssf550.xml_;_
						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbEdit("txtDistance").Set Datatable.Value("Distance","CustomReportsRemaining") @@ hightlight id_;_724490_;_script infofile_;_ZIP::ssf552.xml_;_
						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_3").Object.Text=""
						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_3").Type Datatable.Value("FreqStart","CustomReportsRemaining") @@ hightlight id_;_267768_;_script infofile_;_ZIP::ssf555.xml_;_
						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_4").Object.Text=""
						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_4").Type Datatable.Value("FreqEnd","CustomReportsRemaining") @@ hightlight id_;_1051982_;_script infofile_;_ZIP::ssf558.xml_;_
						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_2").VbComboBox("cboServiceClassSingle").Select Datatable.Value("ServiceClass","CustomReportsRemaining") '"AERONÁUTICA MÓVEL" @@ hightlight id_;_394516_;_script infofile_;_ZIP::ssf559.xml_;_
'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_5").Drag 23,5 @@ hightlight id_;_1248694_;_script infofile_;_ZIP::ssf560.xml_;_
'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbFrame("panOption(22)").Drop 269,25 @@ hightlight id_;_328950_;_script infofile_;_ZIP::ssf561.xml_;_
						'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_5").Object.Text=""
						'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_5").Type Datatable.Value("Lat1","CustomReportsRemaining") @@ hightlight id_;_1248694_;_script infofile_;_ZIP::ssf562.xml_;_
'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_6").Drag 26,3 @@ hightlight id_;_1707330_;_script infofile_;_ZIP::ssf563.xml_;_
'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbFrame("panOption(22)").Drop 309,17 @@ hightlight id_;_328950_;_script infofile_;_ZIP::ssf564.xml_;_
'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_6").Object.Text=""
'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_6").Type Datatable.Value("Lat2","CustomReportsRemaining") @@ hightlight id_;_1707330_;_script infofile_;_ZIP::ssf565.xml_;_
'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_7").Drag 13,9 @@ hightlight id_;_1313886_;_script infofile_;_ZIP::ssf566.xml_;_
'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_6").Drop 49,11 @@ hightlight id_;_1707330_;_script infofile_;_ZIP::ssf567.xml_;_
'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_7").Object.Text=""
'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_7").Type Datatable.Value("Lat3","CustomReportsRemaining") @@ hightlight id_;_1313886_;_script infofile_;_ZIP::ssf568.xml_;_
'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_4").VbComboBox("cboLatHem").Select Datatable.Value("Lat4","CustomReportsRemaining") @@ hightlight id_;_1707668_;_script infofile_;_ZIP::ssf569.xml_;_
'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_8").Drag 19,14 @@ hightlight id_;_2361506_;_script infofile_;_ZIP::ssf570.xml_;_
'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbFrame("panOption(22)").Drop 288,71 @@ hightlight id_;_328950_;_script infofile_;_ZIP::ssf571.xml_;_
						'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_8").Object.Text=""
						'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_8").Type Datatable.Value("Long1","CustomReportsRemaining") @@ hightlight id_;_2361506_;_script infofile_;_ZIP::ssf572.xml_;_
'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_9").Drag 20,7 @@ hightlight id_;_1967128_;_script infofile_;_ZIP::ssf573.xml_;_
'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbFrame("panOption(22)").Drop 362,64 @@ hightlight id_;_328950_;_script infofile_;_ZIP::ssf574.xml_;_
'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_9").Object.Text=""
'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_9").Type Datatable.Value("Long2","CustomReportsRemaining") @@ hightlight id_;_1967128_;_script infofile_;_ZIP::ssf575.xml_;_
'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_10").Drag 23,9 @@ hightlight id_;_328834_;_script infofile_;_ZIP::ssf576.xml_;_
'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_9").Drop 6,5 @@ hightlight id_;_1967128_;_script infofile_;_ZIP::ssf577.xml_;_
'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_10").Object.Text=""
'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_10").Type Datatable.Value("Long3","CustomReportsRemaining") @@ hightlight id_;_328834_;_script infofile_;_ZIP::ssf578.xml_;_
'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_5").VbComboBox("cboLongHem").Select Datatable.Value("Long4","CustomReportsRemaining") @@ hightlight id_;_985108_;_script infofile_;_ZIP::ssf410.xml_;_
		
						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbEdit("txtDist").Set Datatable.Value("Distance","CustomReportsRemaining") @@ hightlight id_;_139410_;_script infofile_;_ZIP::ssf812.xml_;_

					ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="Frequency Information on Microwave Links" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
						
'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("FrequencyStart").Drag 62,13
'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbFrame("panOption(1)").Drop 105,39 @@ hightlight id_;_1640392_;_script infofile_;_ZIP::ssf515.xml_;_
						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("FrequencyStart").Object.Text=""
						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("FrequencyStart").Type Datatable.Value("FreqStart","CustomReportsRemaining") @@ hightlight id_;_267768_;_script infofile_;_ZIP::ssf516.xml_;_
						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("FrequencyStart").Type  micTab @@ hightlight id_;_267768_;_script infofile_;_ZIP::ssf517.xml_;_
'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("FrequencyEnd").Drag 30,10 @@ hightlight id_;_1051982_;_script infofile_;_ZIP::ssf518.xml_;_
'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbFrame("panOption(1)").Drop 439,26 @@ hightlight id_;_1640392_;_script infofile_;_ZIP::ssf519.xml_;_
						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("FrequencyEnd").Object.Text=""
						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("FrequencyEnd").Type Datatable.Value("FreqEnd","CustomReportsRemaining") @@ hightlight id_;_1051982_;_script infofile_;_ZIP::ssf520.xml_;_
						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("FrequencyEnd").Type  micTab 
						
						'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_7").VbComboBox("cboRegion").Select "*" @@ hightlight id_;_2165436_;_script infofile_;_ZIP::ssf581.xml_;_
						'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb").VbComboBox("cboCity").Select Datatable.Value("City","CustomReportsRemaining") '"Alto Molocue" @@ hightlight id_;_593422_;_script infofile_;_ZIP::ssf550.xml_;_
 @@ hightlight id_;_724490_;_script infofile_;_ZIP::ssf552.xml_;_
''						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_3").Drag 136,12 @@ hightlight id_;_267768_;_script infofile_;_ZIP::ssf553.xml_;_
''						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbFrame("panOption(1)").Drop 133,22 @@ hightlight id_;_1640392_;_script infofile_;_ZIP::ssf554.xml_;_
'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_3").Object.Text=""
'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_3").Type Datatable.Value("FreqStart","CustomReportsRemaining") @@ hightlight id_;_267768_;_script infofile_;_ZIP::ssf555.xml_;_
''						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_4").Drag 53,10 @@ hightlight id_;_1051982_;_script infofile_;_ZIP::ssf556.xml_;_
''						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbFrame("panOption(1)").Drop 434,30 @@ hightlight id_;_1640392_;_script infofile_;_ZIP::ssf557.xml_;_
'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_4").Object.Text=""
'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_4").Type Datatable.Value("FreqStart","CustomReportsRemaining") @@ hightlight id_;_1051982_;_script infofile_;_ZIP::ssf558.xml_;_
'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_2").VbComboBox("cboServiceClassSingle").Select Datatable.Value("ServiceClass","CustomReportsRemaining") '"AERONÁUTICA MÓVEL" @@ hightlight id_;_394516_;_script infofile_;_ZIP::ssf559.xml_;_

''						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_5").Drag 23,5 @@ hightlight id_;_1248694_;_script infofile_;_ZIP::ssf560.xml_;_
''						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbFrame("panOption(22)").Drop 269,25 @@ hightlight id_;_328950_;_script infofile_;_ZIP::ssf561.xml_;_
'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_5").Object.Text=""
'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_5").Type Datatable.Value("Lat1","CustomReportsRemaining") @@ hightlight id_;_1248694_;_script infofile_;_ZIP::ssf562.xml_;_
''						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_6").Drag 26,3 @@ hightlight id_;_1707330_;_script infofile_;_ZIP::ssf563.xml_;_
''						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbFrame("panOption(22)").Drop 309,17 @@ hightlight id_;_328950_;_script infofile_;_ZIP::ssf564.xml_;_
'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_6").Object.Text=""
'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_6").Type Datatable.Value("Lat2","CustomReportsRemaining") @@ hightlight id_;_1707330_;_script infofile_;_ZIP::ssf565.xml_;_
''						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_7").Drag 13,9 @@ hightlight id_;_1313886_;_script infofile_;_ZIP::ssf566.xml_;_
''						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_6").Drop 49,11 @@ hightlight id_;_1707330_;_script infofile_;_ZIP::ssf567.xml_;_
'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_7").Object.Text=""
'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_7").Type Datatable.Value("Lat3","CustomReportsRemaining") @@ hightlight id_;_1313886_;_script infofile_;_ZIP::ssf568.xml_;_
'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_4").VbComboBox("cboLatHem").Select Datatable.Value("Lat4","CustomReportsRemaining") '"S" @@ hightlight id_;_1707668_;_script infofile_;_ZIP::ssf569.xml_;_
'						
''						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_8").Drag 19,14 @@ hightlight id_;_2361506_;_script infofile_;_ZIP::ssf570.xml_;_
''						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbFrame("panOption(22)").Drop 288,71 @@ hightlight id_;_328950_;_script infofile_;_ZIP::ssf571.xml_;_
'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_8").Object.Text=""
'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_8").Type Datatable.Value("Long1","CustomReportsRemaining") @@ hightlight id_;_2361506_;_script infofile_;_ZIP::ssf572.xml_;_
''						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_9").Drag 20,7 @@ hightlight id_;_1967128_;_script infofile_;_ZIP::ssf573.xml_;_
''						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbFrame("panOption(22)").Drop 362,64 @@ hightlight id_;_328950_;_script infofile_;_ZIP::ssf574.xml_;_
'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_9").Object.Text=""
'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_9").Type Datatable.Value("Long2","CustomReportsRemaining") @@ hightlight id_;_1967128_;_script infofile_;_ZIP::ssf575.xml_;_
''						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_10").Drag 23,9 @@ hightlight id_;_328834_;_script infofile_;_ZIP::ssf576.xml_;_
''						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_9").Drop 6,5 @@ hightlight id_;_1967128_;_script infofile_;_ZIP::ssf577.xml_;_
'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_10").Object.Text=""
'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_10").Type Datatable.Value("Long3","CustomReportsRemaining") @@ hightlight id_;_328834_;_script infofile_;_ZIP::ssf578.xml_;_
'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_5").VbComboBox("cboLongHem").Select Datatable.Value("Long4","CustomReportsRemaining") '"E"
'
'						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbEdit("txtDistance").SetSelection 0,2 @@ hightlight id_;_724490_;_script infofile_;_ZIP::ssf551.xml_;_
						'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbEdit("txtDistance").Set Datatable.Value("Distance","CustomReportsRemaining")						
						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbEdit("txtDist").Set Datatable.Value("Distance","CustomReportsRemaining")

						'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_4").VbComboBox("cboAppStatus").Select "*"
		
					End If
			ElseIf Datatable.Value("ReportTab","CustomReportsRemaining")="Complaints" and Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
				If Datatable.Value("ReportName","CustomReportsRemaining")="Number of Interface complaints cleared and pending in a particular area over a defined period of time" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
					
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("ReportDateStart").Click 79,6
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("ReportDateStart").Drag 79,6 @@ hightlight id_;_264310_;_script infofile_;_ZIP::ssf784.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbFrame("panOption(4)").Drop 277,42 @@ hightlight id_;_264312_;_script infofile_;_ZIP::ssf785.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("ReportDateStart").Type Datatable.Value("ReportingDateStart","CustomReportsRemaining") @@ hightlight id_;_264310_;_script infofile_;_ZIP::ssf793.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("ReportDateStart").Type  micTab @@ hightlight id_;_264310_;_script infofile_;_ZIP::ssf794.xml_;_
					
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("ReportDateEnd").Click 80,7 @@ hightlight id_;_264348_;_script infofile_;_ZIP::ssf795.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("ReportDateEnd").Drag 80,7 @@ hightlight id_;_264348_;_script infofile_;_ZIP::ssf796.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbFrame("panOption(4)").Drop 453,32 @@ hightlight id_;_264312_;_script infofile_;_ZIP::ssf797.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("ReportDateEnd").Type Datatable.Value("ReportingDateEnd","CustomReportsRemaining") @@ hightlight id_;_264348_;_script infofile_;_ZIP::ssf798.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("ReportDateEnd").Type  micTab 
					
					'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbRadioButton("Região").Set @@ hightlight id_;_2364210_;_script infofile_;_ZIP::ssf591.xml_;_
					'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_8").VbComboBox("cboArea(1)").Select "*" @@ hightlight id_;_1577812_;_script infofile_;_ZIP::ssf592.xml_;_
					
					'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbRadioButton("Distrito").Set @@ hightlight id_;_2953014_;_script infofile_;_ZIP::ssf593.xml_;_
					'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_9").VbComboBox("cboArea(0)").Select "*" @@ hightlight id_;_1315338_;_script infofile_;_ZIP::ssf594.xml_;_
				ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="Trend of complaints" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
					
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_11").Drag 51,9 @@ hightlight id_;_787468_;_script infofile_;_ZIP::ssf595.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbFrame("panOption(9)").Drop 273,26 @@ hightlight id_;_724500_;_script infofile_;_ZIP::ssf596.xml_;_
					'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_11").Object.Text=""
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_11").Type Datatable.Value("ReportYear","CustomReportsRemaining") '"2020" @@ hightlight id_;_787468_;_script infofile_;_ZIP::ssf597.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_11").Type  micTab @@ hightlight id_;_787468_;_script infofile_;_ZIP::ssf598.xml_;_
				
				ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="Number of days to resolve complaints over defined period of time" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
 @@ hightlight id_;_65868_;_script infofile_;_ZIP::ssf877.xml_;_
						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_14").Drag 80,8 @@ hightlight id_;_464814_;_script infofile_;_ZIP::ssf878.xml_;_
						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbFrame("panOption(4)").Drop 278,38 @@ hightlight id_;_530022_;_script infofile_;_ZIP::ssf879.xml_;_
						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_14").Type Datatable.Value("ReportingDateStart","CustomReportsRemaining") '"01012020" @@ hightlight id_;_464814_;_script infofile_;_ZIP::ssf880.xml_;_
						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_24").Drag 78,11 @@ hightlight id_;_595550_;_script infofile_;_ZIP::ssf881.xml_;_
						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_24").Drop 6,20 @@ hightlight id_;_595550_;_script infofile_;_ZIP::ssf882.xml_;_
						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_24").Type Datatable.Value("ReportingDateEnd","CustomReportsRemaining") '"12312020"
                    
				End If
			
			ElseIf Datatable.Value("ReportTab","CustomReportsRemaining")="Application Analysis" and Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
				
				If Datatable.Value("ReportName","CustomReportsRemaining")="Status of application for a specified customer" and Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
					'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_10").VbComboBox("cboApplicant").Select "*" @@ hightlight id_;_788210_;_script infofile_;_ZIP::ssf599.xml_;_
					'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_9").VbComboBox("cboAppStatus").Select "*" @@ hightlight id_;_1904252_;_script infofile_;_ZIP::ssf600.xml_;_
					'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_9").VbComboBox("cboAppStatus").Type  micTab @@ hightlight id_;_1904252_;_script infofile_;_ZIP::ssf601.xml_;_
				ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="Number of applications of a particular type and status over a defined period of time" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
					
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("ReportDateStart").Click 79,6
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("ReportDateStart").Drag 79,6 @@ hightlight id_;_264310_;_script infofile_;_ZIP::ssf784.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbFrame("panOption(4)").Drop 277,42 @@ hightlight id_;_264312_;_script infofile_;_ZIP::ssf785.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("ReportDateStart").Type Datatable.Value("ReportingDateStart","CustomReportsRemaining") @@ hightlight id_;_264310_;_script infofile_;_ZIP::ssf793.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("ReportDateStart").Type  micTab @@ hightlight id_;_264310_;_script infofile_;_ZIP::ssf794.xml_;_
					
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("ReportDateEnd").Click 80,7 @@ hightlight id_;_264348_;_script infofile_;_ZIP::ssf795.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("ReportDateEnd").Drag 80,7 @@ hightlight id_;_264348_;_script infofile_;_ZIP::ssf796.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbFrame("panOption(4)").Drop 453,32 @@ hightlight id_;_264312_;_script infofile_;_ZIP::ssf797.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("ReportDateEnd").Type Datatable.Value("ReportingDateEnd","CustomReportsRemaining") @@ hightlight id_;_264348_;_script infofile_;_ZIP::ssf798.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("ReportDateEnd").Type  micTab 
					
					'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_9").VbComboBox("cboAppType").Select "*" @@ hightlight id_;_1250178_;_script infofile_;_ZIP::ssf608.xml_;_
					'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_11").VbComboBox("cboAppStatus").Select "*" @@ hightlight id_;_1904252_;_script infofile_;_ZIP::ssf609.xml_;_
					'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_11").VbComboBox("cboAppStatus").Type  micTab @@ hightlight id_;_1904252_;_script infofile_;_ZIP::ssf610.xml_;_
			    ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="No. Of type approval appl submitted day, week and month" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
									
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("ReportDateStart").Click 79,6
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("ReportDateStart").Drag 79,6 @@ hightlight id_;_264310_;_script infofile_;_ZIP::ssf784.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbFrame("panOption(4)").Drop 277,42 @@ hightlight id_;_264312_;_script infofile_;_ZIP::ssf785.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("ReportDateStart").Type Datatable.Value("ReportingDateStart","CustomReportsRemaining") @@ hightlight id_;_264310_;_script infofile_;_ZIP::ssf793.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("ReportDateStart").Type  micTab @@ hightlight id_;_264310_;_script infofile_;_ZIP::ssf794.xml_;_
					
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("ReportDateEnd").Click 80,7 @@ hightlight id_;_264348_;_script infofile_;_ZIP::ssf795.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("ReportDateEnd").Drag 80,7 @@ hightlight id_;_264348_;_script infofile_;_ZIP::ssf796.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbFrame("panOption(4)").Drop 453,32 @@ hightlight id_;_264312_;_script infofile_;_ZIP::ssf797.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("ReportDateEnd").Type Datatable.Value("ReportingDateEnd","CustomReportsRemaining") @@ hightlight id_;_264348_;_script infofile_;_ZIP::ssf798.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("ReportDateEnd").Type  micTab 
					
					'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_9").VbComboBox("cboAppStatus").Select "*" @@ hightlight id_;_1904252_;_script infofile_;_ZIP::ssf617.xml_;_
					'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_9").VbComboBox("cboAppStatus").Type  micTab @@ hightlight id_;_1904252_;_script infofile_;_ZIP::ssf618.xml_;_
				End If
			ElseIf Datatable.Value("ReportTab","CustomReportsRemaining")="Licence Analysis" and Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
				If Datatable.Value("ReportName","CustomReportsRemaining")="Applications ready to be licensed" and Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
				

					'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_10").VbComboBox("cboAppType").Select "*" @@ hightlight id_;_1250178_;_script infofile_;_ZIP::ssf619.xml_;_
					'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_10").VbComboBox("cboAppType").Type  micTab @@ hightlight id_;_1250178_;_script infofile_;_ZIP::ssf620.xml_;_
				ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="Licenses status over a defined period of time" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
				
					'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_10").WinEdit("Edit").SetSelection 0,1 @@ hightlight id_;_329076_;_script infofile_;_ZIP::ssf621.xml_;_
					'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_10").VbComboBox("cboApplicant").Select "*" @@ hightlight id_;_788210_;_script infofile_;_ZIP::ssf622.xml_;_
					
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("ReportDateStart").Click 79,6
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("ReportDateStart").Drag 79,6 @@ hightlight id_;_264310_;_script infofile_;_ZIP::ssf784.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbFrame("panOption(4)").Drop 277,42 @@ hightlight id_;_264312_;_script infofile_;_ZIP::ssf785.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("ReportDateStart").Type Datatable.Value("ReportingDateStart","CustomReportsRemaining") @@ hightlight id_;_264310_;_script infofile_;_ZIP::ssf793.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("ReportDateStart").Type  micTab @@ hightlight id_;_264310_;_script infofile_;_ZIP::ssf794.xml_;_
					
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("ReportDateEnd").Click 80,7 @@ hightlight id_;_264348_;_script infofile_;_ZIP::ssf795.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("ReportDateEnd").Drag 80,7 @@ hightlight id_;_264348_;_script infofile_;_ZIP::ssf796.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbFrame("panOption(4)").Drop 453,32 @@ hightlight id_;_264312_;_script infofile_;_ZIP::ssf797.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("ReportDateEnd").Type Datatable.Value("ReportingDateEnd","CustomReportsRemaining") @@ hightlight id_;_264348_;_script infofile_;_ZIP::ssf798.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("ReportDateEnd").Type  micTab 
					
					'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_11").VbComboBox("cboAppStatus").Select "*" @@ hightlight id_;_1904252_;_script infofile_;_ZIP::ssf629.xml_;_
					'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_12").VbComboBox("cboAppType").Select "*" @@ hightlight id_;_1250178_;_script infofile_;_ZIP::ssf630.xml_;_
					'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_12").VbComboBox("cboAppType").Type  micTab @@ hightlight id_;_1250178_;_script infofile_;_ZIP::ssf631.xml_;_
				ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="Licenses status over a defined period of time with customers contact information" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
				
					'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_10").WinEdit("Edit").SetSelection 0,1 @@ hightlight id_;_329076_;_script infofile_;_ZIP::ssf632.xml_;_
					'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_10").VbComboBox("cboApplicant").Select "*" @@ hightlight id_;_788210_;_script infofile_;_ZIP::ssf633.xml_;_
					
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("ReportDateStart").Click 79,6
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("ReportDateStart").Drag 79,6 @@ hightlight id_;_264310_;_script infofile_;_ZIP::ssf784.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbFrame("panOption(4)").Drop 277,42 @@ hightlight id_;_264312_;_script infofile_;_ZIP::ssf785.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("ReportDateStart").Type Datatable.Value("ReportingDateStart","CustomReportsRemaining") @@ hightlight id_;_264310_;_script infofile_;_ZIP::ssf793.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("ReportDateStart").Type  micTab @@ hightlight id_;_264310_;_script infofile_;_ZIP::ssf794.xml_;_
					
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("ReportDateEnd").Click 80,7 @@ hightlight id_;_264348_;_script infofile_;_ZIP::ssf795.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("ReportDateEnd").Drag 80,7 @@ hightlight id_;_264348_;_script infofile_;_ZIP::ssf796.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbFrame("panOption(4)").Drop 453,32 @@ hightlight id_;_264312_;_script infofile_;_ZIP::ssf797.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("ReportDateEnd").Type Datatable.Value("ReportingDateEnd","CustomReportsRemaining") @@ hightlight id_;_264348_;_script infofile_;_ZIP::ssf798.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("ReportDateEnd").Type  micTab 
					
					'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_11").VbComboBox("cboAppType").Select "*" @@ hightlight id_;_1250178_;_script infofile_;_ZIP::ssf641.xml_;_
					'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_12").VbComboBox("cboAppStatus").Select "*" @@ hightlight id_;_1904252_;_script infofile_;_ZIP::ssf642.xml_;_
					'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_12").VbComboBox("cboAppStatus").Type  micTab @@ hightlight id_;_1904252_;_script infofile_;_ZIP::ssf643.xml_;_
				ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="Systems" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
				
					'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_10").VbComboBox("cboApplicant").Select "*" @@ hightlight id_;_788210_;_script infofile_;_ZIP::ssf644.xml_;_
				End If
			ElseIf Datatable.Value("ReportTab","CustomReportsRemaining")="Financial 1" and Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
				If Datatable.Value("ReportName","CustomReportsRemaining")="List of Customers" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
				
					'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_13").VbComboBox("cboApplicant").Select "*" @@ hightlight id_;_788210_;_script infofile_;_ZIP::ssf647.xml_;_
					'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_14").VbComboBox("cboLegalType").Select "*" @@ hightlight id_;_2953250_;_script infofile_;_ZIP::ssf648.xml_;_
					'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbCheckBox("Sinalizado").Set "ON" @@ hightlight id_;_395344_;_script infofile_;_ZIP::ssf649.xml_;_
					'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_15").VbComboBox("cboCityOnly").Select "*" @@ hightlight id_;_1376550_;_script infofile_;_ZIP::ssf650.xml_;_
					'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_16").VbComboBox("cboAppType").Select "*" @@ hightlight id_;_1250178_;_script infofile_;_ZIP::ssf651.xml_;_
					'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_17").VbComboBox("cboServiceType").Select "*" @@ hightlight id_;_197830_;_script infofile_;_ZIP::ssf652.xml_;_
					If Datatable.Value("Client","CustomReportsRemaining")="MOZ" Then
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbCheckBox("Sistema (s)").Set "ON" @@ hightlight id_;_852336_;_script infofile_;_ZIP::ssf653.xml_;_
					End If
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbCheckBox("Factura").Set "ON" @@ hightlight id_;_263430_;_script infofile_;_ZIP::ssf654.xml_;_
				ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="Account Statement" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
				
					'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_13").VbComboBox("cboApplicant").Select "*" @@ hightlight id_;_788210_;_script infofile_;_ZIP::ssf655.xml_;_
					'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbRadioButton("Não").Set @@ hightlight id_;_4459442_;_script infofile_;_ZIP::ssf656.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbRadioButton("Sim").Set @@ hightlight id_;_1902346_;_script infofile_;_ZIP::ssf657.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbCheckBox("incluir todos os aliases").Set "ON" @@ hightlight id_;_4786596_;_script infofile_;_ZIP::ssf658.xml_;_
					
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("ReportDateStart").Click 79,6
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("ReportDateStart").Drag 79,6 @@ hightlight id_;_264310_;_script infofile_;_ZIP::ssf784.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbFrame("panOption(4)").Drop 277,42 @@ hightlight id_;_264312_;_script infofile_;_ZIP::ssf785.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("ReportDateStart").Type Datatable.Value("ReportingDateStart","CustomReportsRemaining") @@ hightlight id_;_264310_;_script infofile_;_ZIP::ssf793.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("ReportDateStart").Type  micTab @@ hightlight id_;_264310_;_script infofile_;_ZIP::ssf794.xml_;_
					
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("ReportDateEnd").Click 80,7 @@ hightlight id_;_264348_;_script infofile_;_ZIP::ssf795.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("ReportDateEnd").Drag 80,7 @@ hightlight id_;_264348_;_script infofile_;_ZIP::ssf796.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbFrame("panOption(4)").Drop 453,32 @@ hightlight id_;_264312_;_script infofile_;_ZIP::ssf797.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("ReportDateEnd").Type Datatable.Value("ReportingDateEnd","CustomReportsRemaining") @@ hightlight id_;_264348_;_script infofile_;_ZIP::ssf798.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("ReportDateEnd").Type  micTab 
					
					'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_16").VbComboBox("cboServiceType").Select "*" @@ hightlight id_;_197830_;_script infofile_;_ZIP::ssf665.xml_;_
				ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="Annual Billing" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
				
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_17").Drag 43,8
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbFrame("panOption(9)").Drop 287,29 @@ hightlight id_;_724500_;_script infofile_;_ZIP::ssf667.xml_;_
					'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_17").Object.Text=""
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_17").Type Datatable.Value("ReportYear","CustomReportsRemaining")'"2020" @@ hightlight id_;_787468_;_script infofile_;_ZIP::ssf668.xml_;_
					'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_14").VbComboBox("cboLegalType").Select "*" @@ hightlight id_;_2953250_;_script infofile_;_ZIP::ssf669.xml_;_
					'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbCheckBox("Sinalizado").Set "OFF" @@ hightlight id_;_395344_;_script infofile_;_ZIP::ssf670.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbCheckBox("Sinalizado").Set "ON" @@ hightlight id_;_395344_;_script infofile_;_ZIP::ssf671.xml_;_
					'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbRadioButton("Não").Set @@ hightlight id_;_4459442_;_script infofile_;_ZIP::ssf672.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbRadioButton("Sim").Set @@ hightlight id_;_1902346_;_script infofile_;_ZIP::ssf673.xml_;_
					'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbCheckBox("incluir todos os aliases").Set "OFF" @@ hightlight id_;_4786596_;_script infofile_;_ZIP::ssf674.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbCheckBox("incluir todos os aliases").Set "ON" @@ hightlight id_;_4786596_;_script infofile_;_ZIP::ssf675.xml_;_
				ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="Fee collected for a particular type of service over a defined period of time" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
				
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_17").Drag 52,14 @@ hightlight id_;_787468_;_script infofile_;_ZIP::ssf676.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbFrame("panOption(9)").Drop 281,26 @@ hightlight id_;_724500_;_script infofile_;_ZIP::ssf677.xml_;_
				'	VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_17").Object.Text=""
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_17").Type Datatable.Value("ReportYear","CustomReportsRemaining")'"2020" @@ hightlight id_;_787468_;_script infofile_;_ZIP::ssf678.xml_;_
					'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_14").VbComboBox("cboLegalType").Select "*" @@ hightlight id_;_2953250_;_script infofile_;_ZIP::ssf679.xml_;_
					'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbCheckBox("Sinalizado").Set "OFF" @@ hightlight id_;_395344_;_script infofile_;_ZIP::ssf680.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbCheckBox("Sinalizado").Set "ON" @@ hightlight id_;_395344_;_script infofile_;_ZIP::ssf681.xml_;_
					'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbRadioButton("Não").Set @@ hightlight id_;_4459442_;_script infofile_;_ZIP::ssf682.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbRadioButton("Sim").Set @@ hightlight id_;_1902346_;_script infofile_;_ZIP::ssf683.xml_;_
					'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbCheckBox("incluir todos os aliases").Set "OFF" @@ hightlight id_;_4786596_;_script infofile_;_ZIP::ssf684.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbCheckBox("incluir todos os aliases").Set "ON" @@ hightlight id_;_4786596_;_script infofile_;_ZIP::ssf685.xml_;_
				ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="License fee calculated and collected over a defined period of time" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
				
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("ReportDateStart").Click 79,6
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("ReportDateStart").Drag 79,6 @@ hightlight id_;_264310_;_script infofile_;_ZIP::ssf784.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbFrame("panOption(4)").Drop 277,42 @@ hightlight id_;_264312_;_script infofile_;_ZIP::ssf785.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("ReportDateStart").Type Datatable.Value("ReportingDateStart","CustomReportsRemaining") @@ hightlight id_;_264310_;_script infofile_;_ZIP::ssf793.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("ReportDateStart").Type  micTab @@ hightlight id_;_264310_;_script infofile_;_ZIP::ssf794.xml_;_
					
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("ReportDateEnd").Click 80,7 @@ hightlight id_;_264348_;_script infofile_;_ZIP::ssf795.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("ReportDateEnd").Drag 80,7 @@ hightlight id_;_264348_;_script infofile_;_ZIP::ssf796.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbFrame("panOption(4)").Drop 453,32 @@ hightlight id_;_264312_;_script infofile_;_ZIP::ssf797.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("ReportDateEnd").Type Datatable.Value("ReportingDateEnd","CustomReportsRemaining") @@ hightlight id_;_264348_;_script infofile_;_ZIP::ssf798.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("ReportDateEnd").Type  micTab 
					
					'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbRadioButton("Não").Set @@ hightlight id_;_4459442_;_script infofile_;_ZIP::ssf693.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbRadioButton("Sim").Set @@ hightlight id_;_1902346_;_script infofile_;_ZIP::ssf694.xml_;_
					'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbCheckBox("incluir todos os aliases").Set "OFF" @@ hightlight id_;_4786596_;_script infofile_;_ZIP::ssf695.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbCheckBox("incluir todos os aliases").Set "ON" @@ hightlight id_;_4786596_;_script infofile_;_ZIP::ssf696.xml_;_
				End If
			ElseIf Datatable.Value("ReportTab","CustomReportsRemaining")="Financial 2" and Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
				If Datatable.Value("ReportName","CustomReportsRemaining")="Journal Report" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
					
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("ReportDateStart").Click 79,6
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("ReportDateStart").Drag 79,6 @@ hightlight id_;_264310_;_script infofile_;_ZIP::ssf784.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbFrame("panOption(4)").Drop 277,42 @@ hightlight id_;_264312_;_script infofile_;_ZIP::ssf785.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("ReportDateStart").Type Datatable.Value("ReportingDateStart","CustomReportsRemaining") @@ hightlight id_;_264310_;_script infofile_;_ZIP::ssf793.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("ReportDateStart").Type  micTab @@ hightlight id_;_264310_;_script infofile_;_ZIP::ssf794.xml_;_
					
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("ReportDateEnd").Click 80,7 @@ hightlight id_;_264348_;_script infofile_;_ZIP::ssf795.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("ReportDateEnd").Drag 80,7 @@ hightlight id_;_264348_;_script infofile_;_ZIP::ssf796.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbFrame("panOption(4)").Drop 453,32 @@ hightlight id_;_264312_;_script infofile_;_ZIP::ssf797.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("ReportDateEnd").Type Datatable.Value("ReportingDateEnd","CustomReportsRemaining") @@ hightlight id_;_264348_;_script infofile_;_ZIP::ssf798.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("ReportDateEnd").Type  micTab 
					
			ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="Payment Gateway Response" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
					
			VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_14").Drag 79,10 @@ hightlight id_;_67564_;_script infofile_;_ZIP::ssf863.xml_;_
			VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbFrame("panOption(4)").Drop 282,44 @@ hightlight id_;_67562_;_script infofile_;_ZIP::ssf864.xml_;_
			VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_14").Type Datatable.Value("ReportingDateStart","CustomReportsRemaining") '"02022020" @@ hightlight id_;_67564_;_script infofile_;_ZIP::ssf865.xml_;_
			VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_24").Drag 75,8 @@ hightlight id_;_67566_;_script infofile_;_ZIP::ssf866.xml_;_
			VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbFrame("panOption(4)").Drop 450,57 @@ hightlight id_;_67562_;_script infofile_;_ZIP::ssf867.xml_;_
			VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_24").Type Datatable.Value("ReportingDateEnd","CustomReportsRemaining") '"12312020" @@ hightlight id_;_67566_;_script infofile_;_ZIP::ssf868.xml_;_
			VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_9").VbComboBox("cboApplicant").Select Datatable.Value("ApplicantName","CustomReportsRemaining") '"Ray320   (# 5929)" @@ hightlight id_;_67824_;_script infofile_;_ZIP::ssf869.xml_;_
						
				End If
			ElseIf Datatable.Value("ReportTab","CustomReportsRemaining")="Others" and Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
				If Datatable.Value("ReportName","CustomReportsRemaining")="Customer Transactions" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
				
					'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_10").VbComboBox("cboApplicant").Select "*" @@ hightlight id_;_788210_;_script infofile_;_ZIP::ssf705.xml_;_
					
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("ReportDateStart").Click 79,6
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("ReportDateStart").Drag 79,6 @@ hightlight id_;_264310_;_script infofile_;_ZIP::ssf784.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbFrame("panOption(4)").Drop 277,42 @@ hightlight id_;_264312_;_script infofile_;_ZIP::ssf785.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("ReportDateStart").Type Datatable.Value("ReportingDateStart","CustomReportsRemaining") @@ hightlight id_;_264310_;_script infofile_;_ZIP::ssf793.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("ReportDateStart").Type  micTab @@ hightlight id_;_264310_;_script infofile_;_ZIP::ssf794.xml_;_
					
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("ReportDateEnd").Click 80,7 @@ hightlight id_;_264348_;_script infofile_;_ZIP::ssf795.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("ReportDateEnd").Drag 80,7 @@ hightlight id_;_264348_;_script infofile_;_ZIP::ssf796.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbFrame("panOption(4)").Drop 453,32 @@ hightlight id_;_264312_;_script infofile_;_ZIP::ssf797.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("ReportDateEnd").Type Datatable.Value("ReportingDateEnd","CustomReportsRemaining") @@ hightlight id_;_264348_;_script infofile_;_ZIP::ssf798.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("ReportDateEnd").Type  micTab 
					
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbCheckBox("Factura_2").Set "OFF" @@ hightlight id_;_1052048_;_script infofile_;_ZIP::ssf713.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbCheckBox("Nota De Débito").Set "OFF" @@ hightlight id_;_920960_;_script infofile_;_ZIP::ssf714.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbCheckBox("Nota de Crédito").Set "OFF" @@ hightlight id_;_855068_;_script infofile_;_ZIP::ssf715.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbCheckBox("Pagamento").Set "OFF" @@ hightlight id_;_1902114_;_script infofile_;_ZIP::ssf716.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbCheckBox("Pagamentos Antecipados").Set "OFF" @@ hightlight id_;_920350_;_script infofile_;_ZIP::ssf717.xml_;_
					
					'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbCheckBox("Pagamentos Antecipados").Set "ON" @@ hightlight id_;_920350_;_script infofile_;_ZIP::ssf718.xml_;_
					'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbCheckBox("Pagamento").Set "ON" @@ hightlight id_;_1902114_;_script infofile_;_ZIP::ssf719.xml_;_
					'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbCheckBox("Nota de Crédito").Set "ON" @@ hightlight id_;_855068_;_script infofile_;_ZIP::ssf720.xml_;_
					'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbCheckBox("Nota De Débito").Set "ON" @@ hightlight id_;_920960_;_script infofile_;_ZIP::ssf721.xml_;_
					'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbCheckBox("Factura_2").Set "ON" @@ hightlight id_;_1052048_;_script infofile_;_ZIP::ssf722.xml_;_
					
					 VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbCheckBox("ThunderRT6CheckBox").Set "OFF" @@ hightlight id_;_525496_;_script infofile_;_ZIP::ssf723.xml_;_
					'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbCheckBox("ThunderRT6CheckBox").Set "ON" @@ hightlight id_;_525496_;_script infofile_;_ZIP::ssf724.xml_;_
				ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="Payments Received" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
				
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("ReportDateStart").Click 79,6
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("ReportDateStart").Drag 79,6 @@ hightlight id_;_264310_;_script infofile_;_ZIP::ssf784.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbFrame("panOption(4)").Drop 277,42 @@ hightlight id_;_264312_;_script infofile_;_ZIP::ssf785.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("ReportDateStart").Type Datatable.Value("ReportingDateStart","CustomReportsRemaining") @@ hightlight id_;_264310_;_script infofile_;_ZIP::ssf793.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("ReportDateStart").Type  micTab @@ hightlight id_;_264310_;_script infofile_;_ZIP::ssf794.xml_;_
					
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("ReportDateEnd").Click 80,7 @@ hightlight id_;_264348_;_script infofile_;_ZIP::ssf795.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("ReportDateEnd").Drag 80,7 @@ hightlight id_;_264348_;_script infofile_;_ZIP::ssf796.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbFrame("panOption(4)").Drop 453,32 @@ hightlight id_;_264312_;_script infofile_;_ZIP::ssf797.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("ReportDateEnd").Type Datatable.Value("ReportingDateEnd","CustomReportsRemaining") @@ hightlight id_;_264348_;_script infofile_;_ZIP::ssf798.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("ReportDateEnd").Type  micTab 
					
					'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_9").VbComboBox("cbopmttype").Select "*" @@ hightlight id_;_264170_;_script infofile_;_ZIP::ssf731.xml_;_
					'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_9").VbComboBox("cbopmttype").Type  micTab @@ hightlight id_;_264170_;_script infofile_;_ZIP::ssf732.xml_;_
				ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="Forecast of fee collection over a defined period of time" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
				
'					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_10").VbComboBox("cboMonth(0)").Select Datatable.Value("StartDate-Month","CustomReportsRemaining")'"Janeiro" @@ hightlight id_;_2427478_;_script infofile_;_ZIP::ssf733.xml_;_
''					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_19").Drag 50,3 @@ hightlight id_;_528274_;_script infofile_;_ZIP::ssf734.xml_;_
''					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_10").VbComboBox("cboMonth(0)").Drop 168,5 @@ hightlight id_;_2427478_;_script infofile_;_ZIP::ssf735.xml_;_
'					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_19").Object.Text=""
'					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_19").Type Datatable.Value("StartDate-Year","CustomReportsRemaining")'"2020" @@ hightlight id_;_528274_;_script infofile_;_ZIP::ssf736.xml_;_
'					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_18").VbComboBox("cboMonth(1)").Select Datatable.Value("EndDate-Month","CustomReportsRemaining")'"Dezembro" @@ hightlight id_;_1117580_;_script infofile_;_ZIP::ssf737.xml_;_
''					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_20").Drag 49,5 @@ hightlight id_;_659264_;_script infofile_;_ZIP::ssf738.xml_;_
''					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_18").VbComboBox("cboMonth(1)").Drop 157,7 @@ hightlight id_;_1117580_;_script infofile_;_ZIP::ssf739.xml_;_
'					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_20").Object.Text=""
'					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("MaskEdBox_20").Type Datatable.Value("EndDate-Year","CustomReportsRemaining")'"2021" @@ hightlight id_;_659264_;_script infofile_;_ZIP::ssf740.xml_;_
					'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_19").VbComboBox("cboLegalType").Select "*" @@ hightlight id_;_2953250_;_script infofile_;_ZIP::ssf741.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbCheckBox("Sinalizado").Set "OFF" @@ hightlight id_;_395344_;_script infofile_;_ZIP::ssf742.xml_;_
					'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbCheckBox("Sinalizado").Set "ON" @@ hightlight id_;_395344_;_script infofile_;_ZIP::ssf743.xml_;_
					'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_20").VbComboBox("cboAppType").Select "*" @@ hightlight id_;_1250178_;_script infofile_;_ZIP::ssf744.xml_;_
				ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="Customer License Types" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
				
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("ReportDateStart").Click 79,6
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("ReportDateStart").Drag 79,6 @@ hightlight id_;_264310_;_script infofile_;_ZIP::ssf784.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbFrame("panOption(4)").Drop 277,42 @@ hightlight id_;_264312_;_script infofile_;_ZIP::ssf785.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("ReportDateStart").Type Datatable.Value("ReportingDateStart","CustomReportsRemaining") @@ hightlight id_;_264310_;_script infofile_;_ZIP::ssf793.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("ReportDateStart").Type  micTab @@ hightlight id_;_264310_;_script infofile_;_ZIP::ssf794.xml_;_
					
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("ReportDateEnd").Click 80,7 @@ hightlight id_;_264348_;_script infofile_;_ZIP::ssf795.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("ReportDateEnd").Drag 80,7 @@ hightlight id_;_264348_;_script infofile_;_ZIP::ssf796.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbFrame("panOption(4)").Drop 453,32 @@ hightlight id_;_264312_;_script infofile_;_ZIP::ssf797.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("ReportDateEnd").Type Datatable.Value("ReportingDateEnd","CustomReportsRemaining") @@ hightlight id_;_264348_;_script infofile_;_ZIP::ssf798.xml_;_
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("ReportDateEnd").Type  micTab 
					
					'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_9").VbComboBox("cboApplicant").Select "*" @@ hightlight id_;_788210_;_script infofile_;_ZIP::ssf752.xml_;_
					'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_11").VbComboBox("cboAppType").Select "*" @@ hightlight id_;_1250178_;_script infofile_;_ZIP::ssf753.xml_;_
					'VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("TCIComboBox.tcb_12").VbComboBox("cboAppStatus").Select "*" @@ hightlight id_;_1904252_;_script infofile_;_ZIP::ssf754.xml_;_
				End If
			
			End If 
			
			AssignInputs="Yes"
			End if
			 
			wait(3)
				
			If SelectedReport="Yes" Then
			
			VbWindow("frmMDI").WinObject("MDIClient").VScroll micSetPos, 10
			
			VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbButton("View").Click @@ hightlight id_;_69160_;_script infofile_;_ZIP::ssf7.xml_;_
			
			If VbWindow("frmReportViewer").Dialog("Crystal Report Viewer").WinButton("OK").Exist(10) Then
				VbWindow("frmReportViewer").Dialog("Crystal Report Viewer").WinButton("OK").Click @@ hightlight id_;_1772388_;_script infofile_;_ZIP::ssf353.xml_;_
				wait(2)
				VbWindow("frmReportViewer").VbButton("Close").Click
			Else
				Set fso = CreateObject("Scripting.FileSystemObject")
					
				file_location = ProjectFolderPath &"\Reports\"& GetCurrentDate
					
				If not fso.FolderExists(file_location) Then
					fso.CreateFolder file_location  
				End If
				wait(5)
				
				If VbWindow("frmMDI").Dialog("#32770").WinButton("OK").Exist(5) Then
					VbWindow("frmMDI").Dialog("#32770").WinButton("OK").Click
				Else
				
					If Datatable.Value("ReportName","CustomReportsRemaining")="Forecast of fee collection over a defined period of time" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
						
						filename=file_location & "\" & Datatable.Value("ReportName","CustomReportsRemaining")
						
						While fso.FileExists(filename&".pdf")
							filename=file_location & "\" & Datatable.Value("ReportName","CustomReportsRemaining")&"_"&fnRandomNumber(2)
						Wend
						
						'===2016 Excel
					
					   While Not UIAWindow("rptFeeForecast3  [Compatibilit").UIAButton("File Tab").Exist(30)
					   Wend
					   UIAWindow("rptFeeForecast3  [Compatibilit").UIAButton("File Tab").Click
		               Wait(2)
		               UIAWindow("rptFeeForecast3  [Compatibilit").UIAList("File").UIAObject("Print").Select
		               Wait(5)
		               While Not Window("Excel").WinObject("WinObject").WinButton("Print").Exist(10)
		               Wend
		               Window("Excel").WinObject("WinObject").WinButton("Print").Click
		               Wait(6)
		               VbWindow("frmPrintConfig").ActiveX("TimoSoft TabStrip Control").ActiveX("TimoSoft Frame Control").ActiveX("TimoSoft TextBox Control").Object.Text=""
					   VbWindow("frmPrintConfig").ActiveX("TimoSoft TabStrip Control").ActiveX("TimoSoft Frame Control").ActiveX("TimoSoft TextBox Control").Type filename &".pdf" @@ hightlight id_;_134420_;_script infofile_;_ZIP::ssf349.xml_;_
					   VbWindow("frmPrintConfig").ActiveX("TimoSoft TabStrip Control").ActiveX("TimoSoft Frame Control").ActiveX("TimoSoft CheckBox Control").Click 16,7 @@ hightlight id_;_134412_;_script infofile_;_ZIP::ssf350.xml_;_
					   VbWindow("frmPrintConfig").ActiveX("TimoSoft CommandButton").Click 39,6
		              ' Window("Excel").Window("Printing").Dialog("Save Print Output As").WinEdit("File name:").Set filename &".pdf"'"qwerty" @@ hightlight id_;_526514_;_script infofile_;_ZIP::ssf831.xml_;_
		              ' Wait(3)
		              ' Window("Excel").Window("Printing").Dialog("Save Print Output As").WinButton("Save").Click @@ hightlight id_;_527390_;_script infofile_;_ZIP::ssf824.xml_;_
                       Wait(5)
					   Window("Window").Close
                       Wait(3)					   
                       Window("Window").Window("Microsoft Excel").WinObject("Microsoft Excel").WinButton("Don't Save").Click @@ hightlight id_;_2026992048_;_script infofile_;_ZIP::ssf835.xml_;_
 @@ hightlight id_;_2033144_;_script infofile_;_ZIP::ssf836.xml_;_
                       
						'===2007 Excel
						'Window("rptFeeForecast1  [Compatibilit").WinObject("Ribbon").WinButton("Office Button").Click
						'Window("rptFeeForecast1  [Compatibilit").Window("Window").WinObject("WinObject").WinButton("Print").Click @@ hightlight id_;_2104459072_;_script infofile_;_ZIP::ssf803.xml_;_
						'Window("rptFeeForecast1  [Compatibilit").Window("Print").Click 441,370
						'VbWindow("frmPrintConfig").ActiveX("TimoSoft TabStrip Control_2").ActiveX("TimoSoft Frame Control").ActiveX("TimoSoft TextBox Control").Click 347,6 @@ hightlight id_;_200122_;_script infofile_;_ZIP::ssf805.xml_;_
						'VbWindow("frmPrintConfig").ActiveX("TimoSoft TabStrip Control_2").ActiveX("TimoSoft Frame Control").ActiveX("TimoSoft TextBox Control").Type filename &".pdf" @@ hightlight id_;_200122_;_script infofile_;_ZIP::ssf806.xml_;_
						'VbWindow("frmPrintConfig").ActiveX("TimoSoft TabStrip Control_2").ActiveX("TimoSoft Frame Control").ActiveX("TimoSoft CheckBox Control").Click 13,10 @@ hightlight id_;_200120_;_script infofile_;_ZIP::ssf807.xml_;_
						'VbWindow("frmPrintConfig").ActiveX("TimoSoft CommandButton_2").Click 53,10
						'Window("rptFeeForecast1  [Compatibilit").Close
						'==
					Else
					
						While Not VbWindow("frmReportViewer").Exist
						Wend
						wait(2)
						
						If Datatable.Value("ReportName","CustomReportsRemaining")="Annual Billing" Then
							Wait(6)
						ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="Account Statement" and Datatable.Value("Client","CustomReportsRemaining")="BOT" Then
						    Wait(25)
						ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="List of Customers" Then
							Wait(7)
						End If 
						
						If Datatable.Value("ReportName","CustomReportsRemaining")="List of Customers" and Datatable.Value("Client","CustomReportsRemaining")="BOT" Then
							Wait(670)
						ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="License fee calculated and collected over a defined period of time" and Datatable.Value("Client","CustomReportsRemaining")="BOT" Then	
							Wait(290)
						End If
						
						If Datatable.Value("ReportName","CustomReportsRemaining")="The number and types of equipment that has been Certified for operation" and Datatable.Value("Client","CustomReportsRemaining")="BOT" Then
						    Wait(446)
						End If
						
						If Datatable.Value("ReportName","CustomReportsRemaining")="Number of wireless stations operating in particular area" and Datatable.Value("Client","CustomReportsRemaining")="BOT" Then
						    Wait(5)
						End If
						
						If Datatable.Value("ReportName","CustomReportsRemaining")="Licenses status over a defined period of time with customers contact information" and Datatable.Value("Client","CustomReportsRemaining")="BOT" Then
						Wait(10)
						End If
						
						If Datatable.Value("ReportName","CustomReportsRemaining")="No. Of type approval appl submitted day, week and month" and Datatable.Value("Client","CustomReportsRemaining")="BOT" Then
						Wait(10)
						ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="Number of applications of a particular type and status over a defined period of time" and Datatable.Value("Client","CustomReportsRemaining")="BOT" Then
						Wait(7)
						ElseIf Datatable.Value("ReportName","CustomReportsRemaining")="Status of application for a specified customer" and Datatable.Value("Client","CustomReportsRemaining")="BOT" Then
						Wait(12)
						End If
						If Datatable.Value("ReportName","CustomReportsRemaining")="Number of Interface complaints cleared and pending in a particular area over a defined period of time" and Datatable.Value("Client","CustomReportsRemaining")="BOT" Then
						Wait(6)
						End If
						Wait(4)
						VbWindow("frmReportViewer").VbButton("Print").Click
						wait(2)
						VbWindow("frmReportViewer").Dialog("Print").WinButton("OK").Click
						wait(2)	
						filename=file_location & "\" & Datatable.Value("ReportName","CustomReportsRemaining")
						
						While fso.FileExists(filename&".pdf")
							filename=file_location & "\" & Datatable.Value("ReportName","CustomReportsRemaining")&"_"&fnRandomNumber(2)
						Wend
						wait(5)
						'If InStr(Environment("Portuguese_Language"),proj)>0 Then
						
							If Datatable.Value("ReportName","CustomReportsRemaining")="Type approval log report" and  Datatable.Value("RunReport","CustomReportsRemaining")="Yes" Then
								Dialog("Printing Records").Dialog("Save Print Output As").WinEdit("File name:").Set filename &".pdf"
								Dialog("Printing Records").Dialog("Save Print Output As").WinEdit("File name:").Type  micTab @@ hightlight id_;_135778_;_script infofile_;_ZIP::ssf53.xml_;_
								Dialog("Printing Records").Dialog("Save Print Output As").WinButton("Save").Click
							Else
							  If Datatable.Value("ReportName","CustomReportsRemaining")="Account Statement" and Datatable.Value("Client","CustomReportsRemaining")="BOT" Then
							   Wait(15)
							  End If
							  Wait(3)
								VbWindow("frmPrintConfig").ActiveX("TimoSoft TabStrip Control").ActiveX("TimoSoft Frame Control").ActiveX("TimoSoft TextBox Control").Object.Text="" @@ hightlight id_;_134420_;_script infofile_;_ZIP::ssf340.xml_;_
								VbWindow("frmPrintConfig").ActiveX("TimoSoft TabStrip Control").ActiveX("TimoSoft Frame Control").ActiveX("TimoSoft TextBox Control").Type filename &".pdf" @@ hightlight id_;_134420_;_script infofile_;_ZIP::ssf349.xml_;_
								VbWindow("frmPrintConfig").ActiveX("TimoSoft TabStrip Control").ActiveX("TimoSoft Frame Control").ActiveX("TimoSoft CheckBox Control").Click 16,7 @@ hightlight id_;_134412_;_script infofile_;_ZIP::ssf350.xml_;_
								VbWindow("frmPrintConfig").ActiveX("TimoSoft CommandButton").Click 39,6 @@ hightlight id_;_134118_;_script infofile_;_ZIP::ssf351.xml_;_
							End If 
						'Else
							'Dialog("Printing Records").Dialog("Save Print Output As").WinEdit("File name:").Set filename &".pdf" @@ hightlight id_;_135778_;_script infofile_;_ZIP::ssf52.xml_;_
							'Dialog("Printing Records").Dialog("Save Print Output As").WinEdit("File name:").Type  micTab @@ hightlight id_;_135778_;_script infofile_;_ZIP::ssf53.xml_;_
							'Dialog("Printing Records").Dialog("Save Print Output As").WinButton("Save").Click @@ hightlight id_;_201448_;_script infofile_;_ZIP::ssf54.xml_;_
						'End If 
						
						wait(5)
						VbWindow("frmReportViewer").VbButton("Close").Click
						End If
					End If
				End If 
			End If
		End If
	Next
	
	If AssignInputs="No" Then
		VbWindow("frmMDI").WinObject("MDIClient").VScroll micSetPos, 10
	End If

	VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbButton("Close").Click
	
	strTestCaseName = Environment.Value("TestName")
		If err.number = 0 Then
			blnFind = True
		Else
			blnFind = False
		End If
		
		UpdateTestCaseStatusInExcel strTestCaseName, blnFind
End If @@ hightlight id_;_133080_;_script infofile_;_ZIP::ssf884.xml_;_
