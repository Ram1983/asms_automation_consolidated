﻿wait(2)
blnFind=True
err.number = 0

datatable.ImportSheet ProjectFolderPath &"\Desktop_Input_Data_Sheet\Input_Data_Sheet.xls","FootNotes","FootNotes"
n=datatable.GetSheet("FootNotes").GetRowCount
With VbWindow("frmMDI")
	If n>0 Then
		datatable.SetCurrentRow(1)
		fn_NavigateToScreen "Administrative;System Table;Footnotes"
		With .VbWindow("frmSystemTables")
			While not .Exist
			Wend
			Wait 8
			.Activate @@ hightlight id_;_2169230_;_script infofile_;_ZIP::ssf4.xml_;_
			.AcxTable("Footnotes").WinScrollBar("ScrollBar").Set 32767 @@ hightlight id_;_2363632_;_script infofile_;_ZIP::ssf22.xml_;_
			Wait 10
			.AcxTable("Footnotes").ActivateCell 5,4
			.AcxTable("Footnotes").SetCellData 5,4, Datatable.value("FootNote","FootNotes")&RandomNumber(111,99999) @@ hightlight id_;_3214160_;_script infofile_;_ZIP::ssf8.xml_;_
			.AcxTable("Footnotes").Type  micTab @@ hightlight id_;_3214160_;_script infofile_;_ZIP::ssf10.xml_;_
		End With

		If .Dialog("#32770").WinButton("OK").Exist(8) Then
			.Dialog("#32770").WinButton("OK").Click
		End If @@ hightlight id_;_265584_;_script infofile_;_ZIP::ssf25.xml_;_
		.Type  micTab  @@ hightlight id_;_722586_;_script infofile_;_ZIP::ssf26.xml_;_
		With .VbWindow("frmSystemTables")
			.AcxTable("Footnotes").WinObject("FootNote138961").Click 191,16 @@ hightlight id_;_1117488_;_script infofile_;_ZIP::ssf27.xml_;_
			.AcxTable("Footnotes").WinObject("FootNote138961").Type  micTab @@ hightlight id_;_1117488_;_script infofile_;_ZIP::ssf28.xml_;_
			Wait 10
			.AcxTable("Footnotes").SetCellData 5,5,Datatable.value("Description","FootNotes")&RandomNumber(111,99999) @@ hightlight id_;_3214160_;_script infofile_;_ZIP::ssf11.xml_;_
			.VbButton("Save").Click @@ hightlight id_;_3343556_;_script infofile_;_ZIP::ssf12.xml_;_
			Wait(3)
			.VbButton("Close").Click @@ hightlight id_;_2688200_;_script infofile_;_ZIP::ssf18.xml_;_
		End With
		strTestCaseName = Environment.Value("TestName")
		If err.number = 0 Then
			blnFind = True
			Else
				blnFind = False
		End If

		UpdateTestCaseStatusInExcel strTestCaseName, blnFind
	End If
End With
