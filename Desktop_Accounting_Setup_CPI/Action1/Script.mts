﻿

datatable.ImportSheet ProjectFolderPath &"\Desktop_Input_Data_Sheet\Input_Data_Sheet.xls","asms_CPI","Global"
n=datatable.GetSheet("Global").GetRowCount

'For i = 1 To n Step 1
With VbWindow("frmMDI")
	If n>0 Then
		datatable.SetCurrentRow(1)	

		'Screen Navigation
		If Strcomp(DataTable.Value("Client","Global"),"swz",1)=0  OR Strcomp(DataTable.Value("Client","Global"),"BOT",1)=0 Then
			fn_NavigateToScreen "Accounting;Setup;CPI"
			Else
				.InsightObject("InsightObject").Click
				.InsightObject("InsightObject_2").Click @@ hightlight id_;_14_;_script infofile_;_ZIP::ssf2.xml_;_
				.InsightObject("InsightObject_3").Click
		End If


		'Entering the CPI values
		With .VbWindow("frmCPI")
			While not .ActiveX("TCIComboBox.tcb").VbComboBox("cmbFiscalYear").Exist

			Wend
			Set send=CreateObject("Wscript.Shell")

			.ActiveX("TCIComboBox.tcb").VbComboBox("cmbFiscalYear").Type Datatable.value("Year","Global")'"2021-2022"

			send.SendKeys "{TAB}"
			.ActiveX("TCIComboBox.tcb").VbComboBox("cmbFiscalYear").Select Datatable.value("Year","Global")'"2021-2022"
			wait(20)
			.VbEdit("txtCPI").Set Datatable.value("CPI","Global") @@ hightlight id_;_985032_;_script infofile_;_ZIP::ssf14.xml_;_
			.VbEdit("txtRemarks").Set Datatable.value("Remarks","Global") @@ hightlight id_;_722972_;_script infofile_;_ZIP::ssf15.xml_;_
			'Save
			.VbButton("Save").Click
			'Close
			'.VbButton("Close").Click

			.VbButton("Close").Click @@ hightlight id_;_724270_;_script infofile_;_ZIP::ssf19.xml_;_
		End With

		strTestCaseName = Environment.Value("TestName")
		If err.number = 0 Then
			blnFind = True
			Else
				blnFind = False
		End If

		UpdateTestCaseStatusInExcel strTestCaseName, blnFind
	End If
End With

