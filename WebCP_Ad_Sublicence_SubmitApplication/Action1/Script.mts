﻿
blnFind=True
err.number = 0

Browser("Application").Page("Application").Link("Manage Application").Click @@ script infofile_;_ZIP::ssf2.xml_;_
Browser("Manage Application").Page("Manage Application").Sync

'Submit the Admin portal created Sublicence 
Browser("Manage Application").Page("Manage Application").Link("Submit this Application").Click @@ script infofile_;_ZIP::ssf1.xml_;_
Browser("Manage Application").HandleDialog micOK
Browser("Manage Application").Page("Manage Application").Sync
strTestCaseName = Environment.Value("TestName")
	
'Test results dsiplayed in Excel
If err.number = 0 Then
    blnFind = True
Else
    blnFind = False
End If

UpdateTestCaseStatusInExcel strTestCaseName, blnFind 
