﻿

blnFind=True
err.number = 0
'import data
datatable.ImportSheet ProjectFolderpath&"\Input_Data_Sheet.xls","cpTypeapproval","cpTypeapproval"
n=datatable.GetSheet("cpTypeapproval").GetRowCount

'For i = 1 To n Step 1
If n>0 Then
	datatable.SetCurrentRow(1)
	With	Browser("Customer Home Page")
		With	.Page("Page")
			.Link("Home").Click @@ script infofile_;_ZIP::ssf162.xml_;_
			.Sync
		End With
		With .Page("Customer Home Page")
			If .Link("Equipment Type Approval").Exist Then
				.Link("Equipment Type Approval").Click
			End If
		End With
		With	.Page("Page")
			.Sync

			If Not .WebTable("Table").Exist Then
				Wait 10
			End If
			If Datatable.value("CustomerId","cpTypeapproval")<>"" Then
				rc=.WebTable("Table").GetRowWithCellText(Datatable.value("CustomerId","cpTypeapproval"))
				If rc<>-1 Then
					If .WebTable("Table").ChildItem(rc,1,"WebElement",1).exist Then
						.WebTable("Table").ChildItem(rc,1,"WebElement",1).Click
					End If
				End If
			End If
			
		End With
		'Selecting type approval	
	
		If Browser("Application").Page("Type Approval").Link("Add Type Approval").Exist Then
			Browser("Application").Page("Type Approval").Link("Add Type Approval").Click
		End If

		'=======Selecting New add Type approval
		With	.Page("Type Approval")
			If Datatable.Value("Functionalitytype","cpTypeapproval")="ADD" Then
				If .Link("Add Type Approval").Exist(8) Then
					.Link("Add Type Approval").Click
				End If
				'========Selecting edit the exist Type approval
				ElseIf Lcase(Datatable.Value("Functionalitytype","cpTypeapproval"))="edit"  Then
					'Browser("Customer Home Page").Page("Customer Home Page").Link("Equipment Type Approval").Click
					.WebList("select").Select Datatable.value("EditcustomerId","cpTypeapproval") @@ script infofile_;_ZIP::ssf119.xml_;_
					.Image("edit").Click
			End If
		End With

		'	 Manufacturer 	
		With	.Page("Type Approval_2")
			.Image("combo_select_dhx_skyblue").Click @@ script infofile_;_ZIP::ssf6.xml_;_
			wait(5)
			.WebElement("xpath:=//div[@class='dhx_combo_list dhx_skyblue_list'][3]//div[2]").Click @@ script infofile_;_ZIP::ssf7.xml_;_
			wait(2)
			.WebEdit("SupplierTelnum2").Set Datatable.value("Tel","cpTypeapproval") @@ script infofile_;_ZIP::ssf8.xml_;_
			.WebEdit("SupplierFaxnum2").Set Datatable.value("Fax","cpTypeapproval") @@ script infofile_;_ZIP::ssf9.xml_;_
			.WebEdit("contact2").Set Datatable.value("Contact","cpTypeapproval") @@ hightlight id_;_197284_;_script infofile_;_ZIP::ssf11.xml_;_
			If .WebEdit("SupplierEmailid2").Exist(5) Then @@ script infofile_;_ZIP::ssf12.xml_;_
				.WebEdit("SupplierEmailid2").Set Datatable.value("supllierEmail","cpTypeapproval")
			End If
			If .WebEdit("SupplierWebAddress2").Exist(5) Then @@ script infofile_;_ZIP::ssf13.xml_;_
				.WebEdit("SupplierWebAddress2").Set Datatable.value("Supllierweb","cpTypeapproval")
			End If

			'	update
			.WebElement("img_1").Click @@ script infofile_;_ZIP::ssf116.xml_;_
		End With

		wait(5)
		Set send=CreateObject("Wscript.Shell") @@ script infofile_;_ZIP::ssf51.xml_;_
		.Page("Type Approval_3").WebEdit("WebEdit_2").Set "Test" @@ script infofile_;_ZIP::ssf131.xml_;_
		send.SendKeys "{TAB}" @@ script infofile_;_ZIP::ssf132.xml_;_
		send.SendKeys "ENTER"

		wait(2) @@ script infofile_;_ZIP::ssf15.xml_;_
		If Browser("Application").Page("Type Approval_2").WebEdit("Address3").Exist Then
			Browser("Application").Page("Type Approval_2").WebEdit("Address3").Set GenerateRandomString(5)
		End If
		If Browser("Application").Page("Type Approval_2").WebEdit("SupplierFaxNum3").Exist(10) Then
			Browser("Application").Page("Type Approval_2").WebEdit("SupplierFaxNum3").Set RandomNumber(5)
		End If
		If Browser("Application").Page("Type Approval_2").WebEdit("SupplierWebAddress2").Exist(10) Then
			Browser("Application").Page("Type Approval_2").WebEdit("SupplierWebAddress2").Set GenerateRandomString(5)
		End If
		If Browser("Application").Page("Type Approval_2").WebElement("img_1").Exist Then
			Browser("Application").Page("Type Approval_2").WebElement("img_1").Click
			Wait 8
			Browser("Application").Page("Type Approval_2").Sync
		End If
		With	.Page("Type Approval_2")
			.WebEdit("Contact3").Set Datatable.value("Repaircontact","cpTypeapproval") @@ script infofile_;_ZIP::ssf16.xml_;_
			If .WebEdit("SupplierEmailid3").Exist(5) Then @@ script infofile_;_ZIP::ssf17.xml_;_
				.WebEdit("SupplierEmailid3").Set Datatable.value("Repairemail","cpTypeapproval")
			End If
			If .WebEdit("SupplierWebAddress3").Exist(5) Then @@ script infofile_;_ZIP::ssf18.xml_;_
				.WebEdit("SupplierWebAddress3").Set Datatable.value("Repiarweb","cpTypeapproval")
			End If

			'update
			.WebElement("img_1").Click
			wait(2)
			'Requested approval
			.WebElement("Requested Approval").Click @@ script infofile_;_ZIP::ssf20.xml_;_
			wait(2) @@ hightlight id_;_197284_;_script infofile_;_ZIP::ssf21.xml_;_
			.WebCheckBox("attachment1").Set "ON" @@ script infofile_;_ZIP::ssf22.xml_;_
			.WebEdit("technical1").Set Datatable.value("metreportnum","cpTypeapproval") @@ script infofile_;_ZIP::ssf23.xml_;_
			.WebEdit("intention1").Set Datatable.value("metcarriedout","cpTypeapproval") @@ script infofile_;_ZIP::ssf24.xml_;_
			.WebCheckBox("attachment2").Set "ON" @@ script infofile_;_ZIP::ssf25.xml_;_
			.WebEdit("technical2").Set Datatable.value("Notmetreportnum","cpTypeapproval") @@ script infofile_;_ZIP::ssf26.xml_;_
			.WebEdit("intention2").Set Datatable.value("Notmetreportnum","cpTypeapproval") @@ script infofile_;_ZIP::ssf27.xml_;_
			.WebCheckBox("attachment3").Set "ON" @@ script infofile_;_ZIP::ssf28.xml_;_
			.WebEdit("technical3").Set Datatable.value("Notmetreportnum","cpTypeapproval") @@ script infofile_;_ZIP::ssf29.xml_;_
			.WebCheckBox("attachment4").Set "ON" @@ hightlight id_;_197284_;_script infofile_;_ZIP::ssf31.xml_;_
			.WebEdit("technical4").Set Datatable.value("Suplementryregreportnum","cpTypeapproval") @@ script infofile_;_ZIP::ssf32.xml_;_
			.WebEdit("intention4").Set Datatable.value("SupplimentryNB","cpTypeapproval") @@ script infofile_;_ZIP::ssf33.xml_;_
			.WebCheckBox("attachment5").Set "ON" @@ script infofile_;_ZIP::ssf34.xml_;_
			.WebEdit("technical5").Set Datatable.value("Approvalrefapprovalnum","cpTypeapproval") @@ script infofile_;_ZIP::ssf35.xml_;_
			.WebEdit("intention5").Set Datatable.value("issuedby","cpTypeapproval") @@ script infofile_;_ZIP::ssf36.xml_;_
			.WebEdit("intention3").Set Datatable.value("Country","cpTypeapproval") @@ script infofile_;_ZIP::ssf37.xml_;_
			'Browser("Customer Home Page").Page("Type Approval_2").WebElement("Telephone number of issuer").Click
			.WebEdit("intention9").Set Datatable.value("approvalTel","cpTypeapproval") @@ script infofile_;_ZIP::ssf39.xml_;_
			.WebEdit("intention10").Set Datatable.value("approvalfax","cpTypeapproval") @@ script infofile_;_ZIP::ssf40.xml_;_
			.WebCheckBox("attachment6").Set "ON" @@ hightlight id_;_197284_;_script infofile_;_ZIP::ssf42.xml_;_
			.WebEdit("technical6").Set Datatable.value("Radioapprovalnum","cpTypeapproval") @@ script infofile_;_ZIP::ssf43.xml_;_
			.WebEdit("intention6").Set Datatable.value("RadioCarriedout","cpTypeapproval") @@ script infofile_;_ZIP::ssf44.xml_;_
			.WebCheckBox("attachment7").Set "ON" @@ script infofile_;_ZIP::ssf45.xml_;_
			.WebEdit("technical7").Set Datatable.value("Radioapprvalcerticenum","cpTypeapproval") @@ script infofile_;_ZIP::ssf46.xml_;_
			.WebEdit("intention7").Set Datatable.value("Radioaprovalceriissuedby","cpTypeapproval") @@ script infofile_;_ZIP::ssf47.xml_;_
			.WebCheckBox("attachment8").Set "ON" @@ script infofile_;_ZIP::ssf48.xml_;_
			.WebEdit("technical8").Set Datatable.value("approvalequipmentrefnum","cpTypeapproval") @@ script infofile_;_ZIP::ssf49.xml_;_
			.WebEdit("intention8").Set Datatable.value("NB","cpTypeapproval") @@ script infofile_;_ZIP::ssf50.xml_;_
			'update
			.WebElement("img_2").Click
		End With
	End With
	wait(2)
	'======Equipment information
	
	Browser("Type Approval").Page("Type Approval").WebElement("Equipment Information").Click @@ script infofile_;_ZIP::ssf122.xml_;_
'	If Browser("Customer Home Page").Page("Type Approval_3").Exist(5) Then
'		Browser("Customer Home Page").Page("Type Approval_3").WebCheckBox("2").Set "ON"
'	End If
'	If Browser("Customer Home Page").Page("Type Approval_3").WebCheckBox("3_2").Exist(5) Then
'		Browser("Customer Home Page").Page("Type Approval_3").WebCheckBox("3_2").Set "ON"
'	End If
'
'
'If Browser("Customer Home Page").Page("Type Approval_3").WebCheckBox("5").Exist(5) Then
'Browser("Customer Home Page").Page("Type Approval_3").WebCheckBox("5").Set "ON"
'End If
'If Browser("Customer Home Page").Page("Type Approval_3").WebCheckBox("2_2").Exist(5) Then
'Browser("Customer Home Page").Page("Type Approval_3").WebCheckBox("2_2").Set "ON"
'End If


	
	
	'Browser("Customer Home Page").Page("Type Approval_2").Image("combo_select_dhx_skyblue_3").Click
	wait(5)
	With Browser("Customer Home Page")
		With .Page("Type Approval_2")
			If .WebElement("AC4486 868 OEM").Exist(5) Then
				.WebElement("AC4486 868 OEM").Click
			End If
			wait(2)
			.WebEdit("version").Set Datatable.value("Version","cpTypeapproval") @@ script infofile_;_ZIP::ssf91.xml_;_
			.WebEdit("modulation").Set Datatable.value("Version","cpTypeapproval") @@ script infofile_;_ZIP::ssf92.xml_;_
			.WebEdit("emissionclass").Set Datatable.value("emission","cpTypeapproval") @@ script infofile_;_ZIP::ssf93.xml_;_
			If .WebEdit("frequencyList").Exist(5) Then @@ script infofile_;_ZIP::ssf94.xml_;_
				.WebEdit("frequencyList").Set RandomNumber(1,3)
			End If
		End With
		'	If Browser("Customer Home Page").Page("Type Approval_3").WebCheckBox("3").Exist(5) Then
		'		Browser("Customer Home Page").Page("Type Approval_3").WebCheckBox("3").Set "ON"
		'	End If
		'If Browser("Customer Home Page").Page("Type Approval_3").WebCheckBox("4").Exist(5) Then
		'	Browser("Customer Home Page").Page("Type Approval_3").WebCheckBox("4").Set "ON"
		'End If
		With .Page("Type Approval_3")
			If .WebEdit("manufacturer").Exist(5) Then
				.WebEdit("manufacturer").Set Datatable.value("Make","cpTypeapproval")
			End If
			If .WebEdit("model").Exist(5) Then
				.WebEdit("model").Set Datatable.value("Model","cpTypeapproval")
			End If

			If .WebEdit("lowerfrequency").Exist(5) Then
				.WebEdit("lowerfrequency").Set RandomNumber(1,10)
			End If
			If .WebEdit("upperfrequency").Exist(5) Then
				.WebEdit("upperfrequency").Set RandomNumber(1,10)
			End If
		End With
		path=fileCreate @@ script infofile_;_ZIP::ssf142.xml_;_
		Wait 5
		'Browser("Customer Home Page").Page("Type Approval_2").WebElement("F. Description of Equipment").Click
		'update
		With	.Page("Type Approval_2")
			.WebElement("img_3").Click @@ script infofile_;_ZIP::ssf121.xml_;_
			wait(2)
			'Attachment tab
			.WebElement("Attached Documents").Click @@ script infofile_;_ZIP::ssf96.xml_;_
		End With
	End With
	wait 10
	If Not Strcomp(Datatable.value("Client","cpTypeapproval"),"eswatini",1)=0 Then     
		With	Browser("Customer Home Page").Page("Type Approval_2").Frame("Frame")
			.WebList("DocumentType").Select "* Standards_EMC" @@ script infofile_;_ZIP::ssf106.xml_;_
			.WebFile("BrowserHidden").Set path 'Datatable.value("path","cpTypeapproval") @@ script infofile_;_ZIP::ssf107.xml_;_
			.Image("Pressing Upload, the document").Click @@ script infofile_;_ZIP::ssf108.xml_;_
		End With
		With Browser("name:=Type Approval").Page("title:=Type Approval")
			If .WebTable("column names:=File Name;File Size;Type.*","visible:=true").Exist(60) Then
				.WebTable("column names:=File Name;File Size;Type.*","visible:=true").Highlight
			End If
		End With
		With	Browser("Customer Home Page").Page("Type Approval_2").Frame("Frame")
			.WebList("DocumentType").Select "* Health and Safety" @@ script infofile_;_ZIP::ssf109.xml_;_
			.WebFile("BrowserHidden").Set path 'fileCreate 'Datatable.value("path","cpTypeapproval") @@ script infofile_;_ZIP::ssf110.xml_;_
			.Image("Pressing Upload, the document").Click @@ script infofile_;_ZIP::ssf111.xml_;_
		End With
		With Browser("name:=Type Approval").Page("title:=Type Approval")
			If .WebTable("column names:=File Name;File Size;Type.*","visible:=true").Exist(60) Then
				.WebTable("column names:=File Name;File Size;Type.*","visible:=true").Highlight
			End If
		End With
		With	Browser("Customer Home Page").Page("Type Approval_2").Frame("Frame")
			.WebList("DocumentType").Select "* RF Test Report" @@ script infofile_;_ZIP::ssf112.xml_;_
			.WebFile("BrowserHidden").Set path 'fileCreate 'Datatable.value("path","cpTypeapproval") @@ script infofile_;_ZIP::ssf113.xml_;_
			If Not .Image("Pressing Upload, the document").Exist Then
				.WebFile("BrowserHidden").Set path 'fileCreate 'Datatable.value("path","cpTypeapproval")
			End If
			.Image("Pressing Upload, the document").Click @@ script infofile_;_ZIP::ssf114.xml_;_
		End With
		With Browser("name:=Type Approval").Page("title:=Type Approval")
			If .WebTable("column names:=File Name;File Size;Type.*","visible:=true").Exist(60) Then
				.WebTable("column names:=File Name;File Size;Type.*","visible:=true").Highlight
			End If
		End With
	End If
	 
	With	Browser("Customer Home Page").Page("Type Approval_2")
		With	.Frame("Frame")
			If Lcase(Environment("TestName"))="bot" Then
				.WebList("DocumentType").Select "Test report"
			Else
			   .WebList("DocumentType").Select "* Test report"
			End If
			
			'	Wait 8
			'	Browser("Customer Home Page").Page("Type Approval_2").Frame("Frame").WebFile("BrowserHidden").Set path 'fileCreate 'Datatable.value("path","cpTypeapproval")
			'	Wait 8
			'	If Not Browser("Customer Home Page").Page("Type Approval_2").Frame("Frame").Image("Pressing Upload, the document").Exist Then
			'		Browser("Customer Home Page").Page("Type Approval_2").Frame("Frame").WebFile("BrowserHidden").Set path 'fileCreate 'Datatable.value("path","cpTypeapproval")
			'	End If
			With Window("Google Chrome")
				.WinObject("Chrome Legacy Window").Click 571,187 @@ hightlight id_;_198194_;_script infofile_;_ZIP::ssf167.xml_;_
				With .Window("Open")
					.WinObject("File name:").Click 23,9 @@ hightlight id_;_1443206_;_script infofile_;_ZIP::ssf168.xml_;_
					.WinObject("File name:").Type path @@ hightlight id_;_1443206_;_script infofile_;_ZIP::ssf169.xml_;_
					.WinObject("Open").Click 58,10
				End With
			End With
			Wait 3
			.Image("Pressing Upload, the document").Click @@ script infofile_;_ZIP::ssf99.xml_;_
			Wait 5
			With Browser("name:=Type Approval").Page("title:=Type Approval")
				If .WebTable("column names:=File Name;File Size;Type.*","visible:=true").Exist(60) Then
					.WebTable("column names:=File Name;File Size;Type.*","visible:=true").Highlight
				End If
			End With
			.WebList("DocumentType").Select "* Declaration of conformity/certificate" @@ script infofile_;_ZIP::ssf100.xml_;_
			Wait 5
			'	Browser("Customer Home Page").Page("Type Approval_2").Frame("Frame").WebFile("BrowserHidden").Set path 'fileCreate 'Datatable.value("path","cpTypeapproval")
			Wait 8
			'	If Not Browser("Customer Home Page").Page("Type Approval_2").Frame("Frame").Image("Pressing Upload, the document").Exist Then
			'		Browser("Customer Home Page").Page("Type Approval_2").Frame("Frame").WebFile("BrowserHidden").Set path 'fileCreate 'Datatable.value("path","cpTypeapproval")
			'	End If
			With Window("Google Chrome")
				.WinObject("Chrome Legacy Window").Click 571,187 @@ hightlight id_;_198194_;_script infofile_;_ZIP::ssf167.xml_;_
				With .Window("Open")
					.WinObject("File name:").Click 23,9 @@ hightlight id_;_1443206_;_script infofile_;_ZIP::ssf168.xml_;_
					.WinObject("File name:").Type path @@ hightlight id_;_1443206_;_script infofile_;_ZIP::ssf169.xml_;_
					.WinObject("Open").Click 58,10
				End With
			End With
			Wait 3
			.Image("Pressing Upload, the document").Click @@ script infofile_;_ZIP::ssf102.xml_;_
			With Browser("name:=Type Approval").Page("title:=Type Approval")
				If .WebTable("column names:=File Name;File Size;Type.*","visible:=true").Exist(60) Then
					.WebTable("column names:=File Name;File Size;Type.*","visible:=true").Highlight
				End If
			End With
			.WebList("DocumentType").Select "* Brief technical description (Product sheet)" @@ script infofile_;_ZIP::ssf103.xml_;_
			'	Browser("Customer Home Page").Page("Type Approval_2").Frame("Frame").WebFile("BrowserHidden").Set path 'fileCreate 'Datatable.value("path","cpTypeapproval")
			'	Wait 8
			'	If Not Browser("Customer Home Page").Page("Type Approval_2").Frame("Frame").Image("Pressing Upload, the document").Exist Then
			'		Browser("Customer Home Page").Page("Type Approval_2").Frame("Frame").WebFile("BrowserHidden").Set path 'fileCreate 'Datatable.value("path","cpTypeapproval")
			'	End If
			With Window("Google Chrome")
				.WinObject("Chrome Legacy Window").Click 571,187 @@ hightlight id_;_198194_;_script infofile_;_ZIP::ssf167.xml_;_
				With .Window("Open")
					.WinObject("File name:").Click 23,9 @@ hightlight id_;_1443206_;_script infofile_;_ZIP::ssf168.xml_;_
					.WinObject("File name:").Type path @@ hightlight id_;_1443206_;_script infofile_;_ZIP::ssf169.xml_;_
					.WinObject("Open").Click 58,10
				End With
			End With
			Wait 3
			.Image("Pressing Upload, the document").Click @@ script infofile_;_ZIP::ssf105.xml_;_
		End With
		With Browser("name:=Type Approval").Page("title:=Type Approval")
			If .WebTable("column names:=File Name;File Size;Type.*","visible:=true").Exist(60) Then
				.WebTable("column names:=File Name;File Size;Type.*","visible:=true").Highlight
			End If
		End With
		With	.Frame("Frame_2")
			If Lcase(Environment("Client"))="bot" Then

				Browser("Customer Home Page").Page("Type Approval_2").WebElement("Attached Documents").Click @@ script infofile_;_ZIP::ssf171.xml_;_
				With Browser("Application")
					.Page("Application").Sync
					.Back @@ hightlight id_;_6031844_;_script infofile_;_ZIP::ssf172.xml_;_
				End With
				With Browser("Customer Home Page").Page("Type Approval_2").Frame("Frame_2")
					.WebList("DocumentType").Select "* Health and Safety" @@ script infofile_;_ZIP::ssf173.xml_;_
					.WebFile("BrowserHidden").Set path @@ script infofile_;_ZIP::ssf174.xml_;_
					Wait 9
				End With
				With Browser("Application")
					.Page("Application").Sync
				End With
				.Image("Pressing Upload, the document").Click 33,10 @@ script infofile_;_ZIP::ssf176.xml_;_

				.WebList("DocumentType").Select "* RF Test Report" @@ script infofile_;_ZIP::ssf177.xml_;_
				.WebFile("BrowserHidden").Set path @@ script infofile_;_ZIP::ssf178.xml_;_
				Wait 9
				.Image("Pressing Upload, the document").Click 27,9 @@ script infofile_;_ZIP::ssf179.xml_;_
				Wait 6
				Browser("Application").Page("Type Approval_2").Frame("Frame").WebList("DocumentType").Select "* Standards_EMC" @@ script infofile_;_ZIP::ssf187.xml_;_
				Wait 4
				Browser("Application").Page("Type Approval_2").Frame("Frame").WebFile("BrowserHidden").Set path @@ script infofile_;_ZIP::ssf185.xml_;_
				Browser("Application").Page("Type Approval_2").Frame("Frame").Image("Pressing Upload, the document").Click 38,10 @@ script infofile_;_ZIP::ssf186.xml_;_
				Wait 4
			End If
		End With
		.WebElement("Equipment Information").Click
	End With
	'Update
	With Browser("Type Approval")
		With .Page("Type Approval")
			If .WebElement("img_1").Exist Then
				.WebElement("img_1").Click
			End If
			'Submit application
			If .WebElement("img_2_0").Exist Then
				.WebElement("img_2_0").Click
			End If
		End With

		Wait 5

		ID=	Trim(.Page("Type Approval_2").WebElement("keyTypeApprovalID").GetROProperty("innertext"))
	End With
	Set objExcel = CreateObject("Excel.Application")
    Set objWB = objExcel.Workbooks.open("C:\ASMS_Automation\WebCP_Input_Data_Sheet\Input_Data_Sheet.xls")
    Set obj=objWB.Worksheets("Ad_TypeApproval_Add")
    rowc=obj.usedrange.rows.count
    obj.cells(2,3).value=ID
    Wait 10
    objExcel.ActiveWorkbook.Save
    objExcel.Quit
    Set objExcel= nothing
    Set objWB =Nothing
	strTestCaseName = Environment.Value("TestName")
				
	'Test results dsiplayed in Excel
	 If err.number = 0 Then
	       blnFind = True
	 Else
		   blnFind = False
	 End If
	
		 UpdateTestCaseStatusInExcel strTestCaseName, blnFind 

End if





