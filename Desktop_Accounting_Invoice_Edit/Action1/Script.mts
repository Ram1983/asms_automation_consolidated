﻿blnFind=False
err.number = 0

datatable.ImportSheet ProjectFolderPath &"\Desktop_Input_Data_Sheet\Input_Data_Sheet.xls","Invoice_Edit","Invoice_Edit"
n=datatable.GetSheet("Invoice_Edit").GetRowCount

'For i = 1 To n Step 1
If n>0 Then
	datatable.SetCurrentRow(1)
	
'	=====Navigate to Invoice
	With VbWindow("frmMDI")
		.WinMenu("Menu").Select "Accounting;Invoices/Credit Memos/Debit Memos;Edit"
		Wait 15
	End With
	If Datatable.value("Client","Invoice_Edit")="JMC" Then
		With	VbWindow("frmMDI").VbWindow("frmFindInvoice")
			If Lcase(DataTable.Value("AccountType", "Invoice_Edit"))="invoice" Then
				.VbRadioButton("Invoice").Set
				ElseIf Lcase(DataTable.Value("AccountType", "Invoice_Edit"))="credit memo" Then
					.VbRadioButton("Credit Memo").Set
				Else
					With VbWindow("frmMDI").VbWindow("frmFindInvoice")
						.VbRadioButton("Debit Memo").Set
					End With
			End If

		End With
		ElseIf Datatable.value("Client","Invoice_Edit")="BOT" Then
			VbWindow("VbWindow").VbWindow("VbWindow").VbRadioButton("Invoice").Set
	End If
	wait 20
	If Datatable.value("Client","Invoice_Edit")="JMC" Then
	   VbWindow("frmMDI").VbWindow("frmFindInvoice").ActiveX("TCIComboBox.tcb").VbComboBox("cboClient").Select 3'Datatable.value("Company_Name","Invoice_Edit") @@ hightlight id_;_461164_;_script infofile_;_ZIP::ssf4.xml_;_
	ElseIf VbWindow("VbWindow").VbWindow("VbWindow").ActiveX("TCIComboBox.tcb").VbComboBox("VbComboBox").Exist Then
		VbWindow("VbWindow").VbWindow("VbWindow").ActiveX("TCIComboBox.tcb").VbComboBox("VbComboBox").Select Datatable.value("Company_Name","Invoice_Edit")
	End If
	wait 5
	With VbWindow("VbWindow").VbWindow("VbWindow_2")
		If Datatable.value("Client","Invoice_Edit")="JMC" Then
			With VbWindow("frmMDI").VbWindow("frmFindInvoice")
				If .VbButton("Next").Exist(10) Then
					.VbButton("Next").Click
				End If

			End With
			ElseIf .VbButton("Next").Exist Then
				.VbButton("Next").Click
		End If
	End With
	Wait 15
	With VbWindow("frmMDI").VbWindow("frmListInvoice")
		If Not .Exist Then
			Wait 20
		End If

		If .AcxTable("Invoices").Exist Then
			.AcxTable("Invoices").SelectRow 1
			If .VbButton("Next").Exist Then
				wait 5
				.VbButton("Next").Click
			End If
		End If
	End With
	With VbWindow("frmInvoice")
		While Not .AcxTable("Detail Information").Exist
		Wend
'		
	End With
	With VbWindow("frmInvoice")
		With VbWindow("VbWindow_2")
		
		
			
			VbWindow("frmInvoice").AcxTable("Detail Information").SelectCell 1,1 @@ hightlight id_;_200282_;_script infofile_;_ZIP::ssf148.xml_;_
			With Window("ASMS").Window("Credit Memo")
				.WinObject("Detail Information").Click 77,95 @@ hightlight id_;_200282_;_script infofile_;_ZIP::ssf149.xml_;_
				.WinObject("Detail Information").Click 91,96 @@ hightlight id_;_200282_;_script infofile_;_ZIP::ssf150.xml_;_
			End With
			Window("SSDropDownFrame").WinObject("SSChildDropDown").Click 33,61 @@ hightlight id_;_134768_;_script infofile_;_ZIP::ssf151.xml_;_
			With	VbWindow("frmInvoice")
				.AcxTable("Detail Information").SelectCell 1,1 @@ hightlight id_;_200282_;_script infofile_;_ZIP::ssf152.xml_;_
				.AcxTable("Detail Information").Type  micTab @@ hightlight id_;_200282_;_script infofile_;_ZIP::ssf153.xml_;_
			End With
      
			VbWindow("frmInvoice").ActiveX("SSTab").Click 294,17 @@ hightlight id_;_461354_;_script infofile_;_ZIP::ssf154.xml_;_
			VbWindow("frmInvoice").ActiveX("SSTab").ActiveX("TCIComboBox.tcb_2").VbComboBox("cboDebitGL").Select 0 @@ hightlight id_;_461206_;_script infofile_;_ZIP::ssf155.xml_;_
			VbWindow("frmInvoice").ActiveX("SSTab").ActiveX("TCIComboBox.tcb_3").VbComboBox("cboFeeType").Select 0 @@ hightlight id_;_1445022_;_script infofile_;_ZIP::ssf156.xml_;_
		End With
		.VbButton("Post").Click @@ hightlight id_;_198326_;_script infofile_;_ZIP::ssf47.xml_;_
	End With
	wait(5)
	With	VbWindow("frmInvoice")
		If Datatable.value("Client","Invoice_Edit")="JMC" Then
			If .VbButton("Close").Exist(8) Then
				.VbButton("Close").Click
			End If
			With VbWindow("frmMDI")
				If .VbWindow("frmListInvoice").VbButton("Close").Exist(8) Then
					.VbWindow("frmListInvoice").VbButton("Close").Click
				End If
			End With
			ElseIf .VbButton("Close").Exist Then
				If .VbButton("Close").Exist(8) Then
					.VbButton("Close").Click
				End If
				With VbWindow("frmMDI").VbWindow("frmFindInvoice")
					If .VbButton("Close").Exist Then
						.VbButton("Close").Click
					End If
				End With
		End If
	End With
	If VbWindow("frmMDI").VbWindow("frmListInvoice").VbButton("Close").Exist Then
		VbWindow("frmMDI").VbWindow("frmListInvoice").VbButton("Close").Click
	End If @@ hightlight id_;_329506_;_script infofile_;_ZIP::ssf157.xml_;_
	strTestCaseName = Environment.Value("TestName")
	If err.number = 0 Then
		blnFind = True
	Else
		blnFind = False
	End If
	
	UpdateTestCaseStatusInExcel strTestCaseName, blnFind
End If

