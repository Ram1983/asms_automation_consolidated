﻿
blnFind=False
err.number = 0

datatable.ImportSheet ProjectFolderPath &"\Desktop_Input_Data_Sheet\Input_Data_Sheet.xls","Invoice_Cancel","Invoice_Cancel"
n=datatable.GetSheet("Invoice_Cancel").GetRowCount
If n>0 Then
datatable.SetCurrentRow(1) @@ hightlight id_;_20_;_script infofile_;_ZIP::ssf3.xml_;_
 
fn_NavigateToScreen "Accounting;Invoices/Credit Memos/Debit Memos;Cancel"
 
 '=====Navigation for Cancel invoices
While not VbWindow("frmMDI").VbWindow("frmFindClient").exist(15)
Wend
'======Selecting customer
Wait 8
VbWindow("frmMDI").VbWindow("frmFindClient").ActiveX("TCIComboBox.tcb").VbComboBox("cboClientNo").Select Datatable.value("CutomerNumber","Invoice_Cancel") @@ hightlight id_;_723364_;_script infofile_;_ZIP::ssf7.xml_;_
If VbWindow("frmMDI").VbWindow("frmFindClient").VbButton("Next").Exist Then
	VbWindow("frmMDI").VbWindow("frmFindClient").VbButton("Next").Click
End If @@ hightlight id_;_985844_;_script infofile_;_ZIP::ssf32.xml_;_
While Not VbWindow("frmMDI").VbWindow("frmARCancelInvoices").AcxTable("SSDBGrid Control 3.1 -").Exist
Wend
Wait 5
intTableRowCount = VbWindow("frmMDI").VbWindow("frmARCancelInvoices").AcxTable("SSDBGrid Control 3.1 -").RowCount
For intIteration = 1 To intTableRowCount
	strDocType = VbWindow("frmMDI").VbWindow("frmARCancelInvoices").AcxTable("SSDBGrid Control 3.1 -").GetCellData(intIteration,2)

	strDocId = VbWindow("frmMDI").VbWindow("frmARCancelInvoices").AcxTable("SSDBGrid Control 3.1 -").GetCellData(intIteration,3)
	
	If strDocType = datatable.value("strDocType","Invoice_cancel") and strDocId = datatable.value("strDocId","Invoice_Cancel")   Then
	   VbWindow("frmMDI").VbWindow("frmARCancelInvoices").AcxTable("SSDBGrid Control 3.1 -").SetCellData intIteration,1,True
		Wait(1)		
	Exit For
	End If

Next
If VbWindow("frmMDI").VbWindow("frmARCancelInvoices").VbButton("Cancel").Exist Then
	Wait 5
	VbWindow("frmMDI").VbWindow("frmARCancelInvoices").VbButton("Cancel").Click
End If
'=====Confirmation
If VbWindow("frmMDI").Dialog("#32770").WinButton("Yes").Exist Then
	VbWindow("frmMDI").Dialog("#32770").WinButton("Yes").Click
End If @@ hightlight id_;_591238_;_script infofile_;_ZIP::ssf12.xml_;_
wait(3)
If VbWindow("frmMDI").Dialog("#32770").WinButton("OK").Exist Then
	VbWindow("frmMDI").Dialog("#32770").WinButton("OK").Click
End If @@ hightlight id_;_788910_;_script infofile_;_ZIP::ssf13.xml_;_
wait(5)
'====Closing application
If VbWindow("frmMDI").VbWindow("frmARCancelInvoices").VbButton("Close").Exist Then
	VbWindow("frmMDI").VbWindow("frmARCancelInvoices").VbButton("Close").Click
End If @@ hightlight id_;_395724_;_script infofile_;_ZIP::ssf14.xml_;_


strTestCaseName = Environment.Value("TestName")
If err.number = 0 Then
	blnFind = True
Else
	blnFind = False
End If
End If
 @@ hightlight id_;_1446734_;_script infofile_;_ZIP::ssf31.xml_;_
UpdateTestCaseStatusInExcel strTestCaseName, blnFind
