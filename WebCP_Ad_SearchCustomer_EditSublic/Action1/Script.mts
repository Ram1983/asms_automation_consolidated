﻿


blnFind=True
err.number = 0

'Import data from Input data sheet  from folder
datatable.ImportSheet ProjectFolderPath&"\Input_Data_Sheet.xls","AdAdminSearchCustomerEditSublic","AdAdminSearchCustomerEditSublic"
n=datatable.GetSheet("AdAdminSearchCustomerEditSublic").GetRowCount

'For i = 1 To n Step 1
If n>0 Then
	datatable.SetCurrentRow(1)	

'	'Navigate to Home page
	Browser("Browser").Page("Page").Link("Home").Click @@ script infofile_;_ZIP::ssf1.xml_;_
	'Search Customer Number
	If Browser("Browser").Page("Admin Home Page").Link("Search Customer or Application").Exist Then
		Browser("Browser").Page("Admin Home Page").Link("Search Customer or Application").Click
	End If @@ script infofile_;_ZIP::ssf2.xml_;_
	
	'Customer Number Selection
	CustomerNumber=GetDataFromDB("select top 1  RTRIM(ccNum) from Client C where C.proj='"& Environment("Client")&"' order by c.clientid desc")
	If Instr(1,CustomerNumber,"-",1)>0 Then
		val=Split((Trim(CustomerNumber)),"-")
		CustomerNumber=val(Lbound(val))
	End If
	Browser("Browser").Page("Page").Sync
	If Browser("Browser").Page("Page").WebList("ccnum").Exist(50) Then
		Browser("Browser").Page("Page").WebList("ccnum").Select CustomerNumber
	End If @@ script infofile_;_ZIP::ssf3.xml_;_
	
	'Submit Search
	Browser("Browser").Page("Page").Link("Submit Search").Click @@ script infofile_;_ZIP::ssf4.xml_;_
	Browser("Browser").Page("Page").Sync
	'Clicking edit the Application
	If Browser("Browser").Page("Page").Link("A").Exist Then
		Browser("Browser").Page("Page").Link("A").Click
	End If @@ script infofile_;_ZIP::ssf5.xml_;_
	
End If


strTestCaseName = Environment.Value("TestName")
'Test results dsiplayed in Excel
    If err.number = 0 Then
		blnFind = True
	Else
		blnFind = False
	End If
		    
	UpdateTestCaseStatusInExcel strTestCaseName, blnFind
