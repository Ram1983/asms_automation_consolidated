﻿'SystemUtil.Run "C:\Program Files (x86)\TCI\ASMS\ASMS.exe"	' Launch Application
'''================= Launch AUT completed
''
'WaitForForm(VbWindow("vbname:=frmLogin"))		'Function to login to Application
'
'VbWindow("vbname:=frmLogin").VbEdit("vbname:=txtLoginID").Set "ram"
'VbWindow("vbname:=frmLogin").VbEdit("vbname:=txtPassword").SetSecure "5df376ea1c12047af5111808"
'VbWindow("vbname:=frmLogin").VbButton("vbname:=cmdOK").Click
''================= Login completed
'
Wait(1)
WaitForForm(VbWindow("frmMDI").InsightObject("InsightObject"))
VbWindow("frmMDI").InsightObject("InsightObject").Click

WaitForForm(VbWindow("frmMDI").InsightObject("InsightObject_2"))
VbWindow("frmMDI").InsightObject("InsightObject_2").Click
Wait(2)
'================= Menu Navigation completed

If ((VbWindow("vbname:=frmMDI").VbWindow("vbname:=frmFindClient").Exist(60))) Then
	Print "Wait completed, continue with test execution"
End If

If (VbWindow("vbname:=frmMDI").VbWindow("vbname:=frmFindClient").VbButton("vbname:=cmdAdd").Exist(10)) Then
	Print "Wait is completed..."
End If

WaitForForm(VbWindow("vbname:=frmMDI").VbWindow("vbname:=frmFindClient"))
Wait(3)
VbWindow("vbname:=frmMDI").VbWindow("vbname:=frmFindClient").VbButton("vbname:=cmdAdd").Click
'================= Find screen completed

WaitForForm(VbWindow("vbname:=frmMDI").VbWindow("vbname:=frmClientEntity"))

subType = "text:=" & Datatable.Value("CustType")	'Person / Company / Government / Non-Governmental Organization

VbWindow("vbname:=frmMDI").VbWindow("vbname:=frmClientEntity").VbRadioButton(subType).Set

isDealer = Datatable.Value("Dealer")				'Yes / No
If (isDealer = "Yes") Then
	VbWindow("vbname:=frmMDI").VbWindow("vbname:=frmClientEntity").VbCheckBox("text:=Dealer").Set "ON"
End If
Wait(2)
VbWindow("vbname:=frmMDI").VbWindow("vbname:=frmClientEntity").VbButton("vbname:=cmdOK").Click
'================== Customer Type screen completed
WaitForForm(VbWindow("vbname:=frmMDI").VbWindow("vbname:=frmClientInfo").ActiveX("progid:=TabDlg.SSTab.1"))

'Setting Customer Details
VbWindow("vbname:=frmMDI").VbWindow("vbname:=frmClientInfo").ActiveX("progid:=TabDlg.SSTab.1").VbEdit("vbname:=txtFirstName").Set Datatable.Value("Fname")
VbWindow("vbname:=frmMDI").VbWindow("vbname:=frmClientInfo").ActiveX("progid:=TabDlg.SSTab.1").VbEdit("vbname:=txtLastName").Set Datatable.Value("Lname")
VbWindow("vbname:=frmMDI").VbWindow("vbname:=frmClientInfo").ActiveX("progid:=TabDlg.SSTab.1").VbEdit("vbname:=txtNationalID").Set Datatable.Value("Nid")

'Setting Physical Address
VbWindow("vbname:=frmMDI").VbWindow("vbname:=frmClientInfo").ActiveX("progid:=TabDlg.SSTab.1").VbEdit("vbname:=txtPlotNo").Set Datatable.Value("Phy_PtNo")
VbWindow("vbname:=frmMDI").VbWindow("vbname:=frmClientInfo").ActiveX("progid:=TabDlg.SSTab.1").VbEdit("vbname:=txtStreet").Set Datatable.Value("Phy_St")
VbWindow("vbname:=frmMDI").VbWindow("vbname:=frmClientInfo").ActiveX("progid:=TabDlg.SSTab.1").VbComboBox("vbname:=cboCity").Select Datatable.Value("Phy_City")

'Setting Postal Address
VbWindow("vbname:=frmMDI").VbWindow("vbname:=frmClientInfo").ActiveX("progid:=TabDlg.SSTab.1").VbEdit("vbname:=txtPostalLocation").Set Datatable.Value("Po_Loc")
VbWindow("vbname:=frmMDI").VbWindow("vbname:=frmClientInfo").ActiveX("progid:=TabDlg.SSTab.1").VbEdit("vbname:=txtPostStreet").Set Datatable.Value("Po_St")
VbWindow("vbname:=frmMDI").VbWindow("vbname:=frmClientInfo").ActiveX("progid:=TabDlg.SSTab.1").VbEdit("vbname:=txtPostal").Set Datatable.Value("Po_Pbox")
If Datatable.Value("Client")="JMC" Then
    VbWindow("vbname:=frmMDI").VbWindow("vbname:=frmClientInfo").ActiveX("progid:=TabDlg.SSTab.1").VbEdit("vbname:=txtTA").Set Datatable.Value("Po_Zip")
    VbWindow("vbname:=frmMDI").VbWindow("vbname:=frmClientInfo").ActiveX("progid:=TabDlg.SSTab.1").VbComboBox("vbname:=cboPostCity").Select Datatable.Value("Po_City")
    VbWindow("vbname:=frmMDI").VbWindow("vbname:=frmClientInfo").ActiveX("progid:=TabDlg.SSTab.1").VbComboBox("vbname:=cboPostRegion").Select Datatable.Value("Po_Reg")
End If
'Setting Communication Details
VbWindow("vbname:=frmMDI").VbWindow("vbname:=frmClientInfo").ActiveX("progid:=TabDlg.SSTab.1").VbEdit("vbname:=txtTel1").Set Datatable.Value("Tel")
VbWindow("vbname:=frmMDI").VbWindow("vbname:=frmClientInfo").ActiveX("progid:=TabDlg.SSTab.1").VbEdit("vbname:=txtMobile").Set Datatable.Value("Mob")
VbWindow("vbname:=frmMDI").VbWindow("vbname:=frmClientInfo").ActiveX("progid:=TabDlg.SSTab.1").VbEdit("vbname:=txtEmail").Set Datatable.Value("Email")
VbWindow("vbname:=frmMDI").VbWindow("vbname:=frmClientInfo").ActiveX("progid:=TabDlg.SSTab.1").VbEdit("vbname:=txtWebSite").Set Datatable.Value("Web")
VbWindow("vbname:=frmMDI").VbWindow("vbname:=frmClientInfo").ActiveX("progid:=TabDlg.SSTab.1").VbEdit("vbname:=txtCertifyingOrganization").Type Datatable.Value("Corg")
Wait(1)		'Needed because Type operation is taking time 
VbWindow("vbname:=frmMDI").VbWindow("vbname:=frmClientInfo").ActiveX("progid:=TabDlg.SSTab.1").VbEdit("vbname:=txtFax").Set Datatable.Value("Fax")
VbWindow("vbname:=frmMDI").VbWindow("vbname:=frmClientInfo").ActiveX("progid:=TabDlg.SSTab.1").Click 300,13		' Switch to Second Tab
If Datatable.Value("Client")="JMC" Then
    VbWindow("vbname:=frmMDI").VbWindow("vbname:=frmClientInfo").ActiveX("progid:=TabDlg.SSTab.1").VbCheckBox("vbname:=chkNeedToPay").Set "ON"
End If
VbWindow("vbname:=frmMDI").VbWindow("vbname:=frmClientInfo").ActiveX("progid:=TabDlg.SSTab.1").VbEdit("vbname:=txtEmployerName").Set "Emp1"
VbWindow("vbname:=frmMDI").VbWindow("vbname:=frmClientInfo").ActiveX("progid:=TabDlg.SSTab.1").VbEditor("vbname:=txtEmployerAddress").Type "Emp Address Line 1"

VbWindow("vbname:=frmMDI").VbWindow("vbname:=frmClientInfo").ActiveX("progid:=TabDlg.SSTab.1").Click 135,11		' Switch to Primary Tab
VbWindow("vbname:=frmMDI").VbWindow("vbname:=frmClientInfo").ActiveX("progid:=TabDlg.SSTab.1").VbButton("vbname:=cmdOK").Click

Set oParent = VbWindow("vbname:=frmMDI").VbWindow("vbname:=frmClientInfo").ActiveX("progid:=TabDlg.SSTab.1").ActiveX("vbName:=txtClientNum")
Do
	If(oParent.exist) Then
		Exit Do
	End If
Loop While (1)
customerID = oParent.GetROProperty("text")

If (VbWindow("vbname:=frmMDI").Dialog("nativeclass:=#32770").Exist) Then
	VbWindow("vbname:=frmMDI").Dialog("nativeclass:=#32770").WinButton("text:=&Yes").Click		
	Set oParent = VbWindow("vbname:=frmMDI").VbWindow("vbname:=frmClientInfo").ActiveX("progid:=TabDlg.SSTab.1").ActiveX("vbName:=txtClientNum")
	Do
		If(oParent.exist) Then
			Exit Do
		End If
	Loop While (1)
	customerID = oParent.GetROProperty("text")
End If

Print "Customer ID : " & customerID & " added successfully"
'VbWindow("vbname:=frmMDI").VbWindow("vbname:=frmClientInfo").ActiveX("progid:=TabDlg.SSTab.1").VbButton("vbname:=cmdCancel").Click

If (VbWindow("vbname:=frmMDI").Dialog("nativeclass:=#32770").WinButton("text:=&Yes").Exist) Then
	VbWindow("vbname:=frmMDI").Dialog("nativeclass:=#32770").WinButton("text:=&Yes").Click
End  If

'VbWindow("vbname:=frmMDI").close		'Move this out of this action
