﻿blnVal=False
datatable.ImportSheet ProjectFolderPath &"\Input_Data_Sheet.xls","Admin_SystemAndServices","Global"
datatable.SetCurrentRow(1)	
With Browser("Admin Home Page")
	.Page("Admin Home Page").Link("System and Services").Click @@ script infofile_;_ZIP::ssf1.xml_;_
	If Lcase(DataTable("Edit"))="yes" Then
		With .Page("System and Services")
			.WebList("select").Select DataTable("ClientName") @@ script infofile_;_ZIP::ssf2.xml_;_
			.Image("edit").Click @@ script infofile_;_ZIP::ssf3.xml_;_
			.Sync
		End With
	End If
	
End With
With Browser("Admin Home Page")
	If Lcase(DataTable("Add"))="yes" Then
		.Page("System and Services").Link("Add System and Services").Click @@ script infofile_;_ZIP::ssf20.xml_;_

		With .Page("System and Services_2")
			.WebRadioGroup("licenseTyperadio").Select "1" @@ script infofile_;_ZIP::ssf21.xml_;_
			.WebElement("img_1").Click @@ script infofile_;_ZIP::ssf22.xml_;_
			Wait 4
			With .Frame("Frame")
				path=DataTable.Value("Path")
				Setting.WebPackage("ReplayType")=2
				.WebFile("BrowserHidden").Set path @@ script infofile_;_ZIP::ssf29.xml_;_
				Setting.WebPackage("ReplayType")=2
				Wait 5
				.Image("Pressing Upload, the document").Click @@ script infofile_;_ZIP::ssf24.xml_;_
			End With
			If .WebElement("img_1").Exist Then
				.WebElement("img_1").Click
				.Sync
			End If
			.Sync
			If .WebElement("img_1_0").Exist Then
				.WebElement("img_1_0").Click
				.Sync
			End If
		End With
	End If
End With
With Browser("Admin Home Page").Page("System and Services_2")
	.WebElement("Official Use").Click @@ script infofile_;_ZIP::ssf5.xml_;_
	.Sync
	.WebEdit("licenseNumber").Set RandomNumber(1111,999999) @@ script infofile_;_ZIP::ssf6.xml_;_
	.WebEdit("issueDate").Set Year(Date)&"/"&Month(Date)&"/"&Day(Date) @@ script infofile_;_ZIP::ssf8.xml_;_
	.WebEdit("placeofIssue").Set GenerateRandomString(7) @@ script infofile_;_ZIP::ssf9.xml_;_
	.WebEdit("DecisionDate").Set Year(Date)&"/"&Month(Date)&"/"&Day(Date) @@ script infofile_;_ZIP::ssf10.xml_;_
	.WebEdit("SentDate").Set Year(Date)&"/"&Month(Date)&"/"&Day(Date) @@ script infofile_;_ZIP::ssf13.xml_;_
	.WebElement("System and Services").Click @@ script infofile_;_ZIP::ssf14.xml_;_
	.Sync @@ script infofile_;_ZIP::ssf15.xml_;_
	If .WebElement("img_1").Exist Then
		.WebElement("img_1").Click
		.Sync
	End If
	If Lcase(DataTable.Value("Approve"))="yes" Then
		.WebElement("img_21_0").Click @@ script infofile_;_ZIP::ssf16.xml_;_
		.Sync
		Wait 3
		Set obj=CreateObject("Wscript.Shell") @@ hightlight id_;_3671192_;_script infofile_;_ZIP::ssf28.xml_;_
		obj.SendKeys "{ENTER}"
		obj.SendKeys "{ENTER}"
		.Sync
	End If
	If Lcase(DataTable.Value("License"))="yes" Then
		.WebElement("img_10_0").Click @@ script infofile_;_ZIP::ssf17.xml_;_
		.Sync
		If Browser("Admin Home Page").Page("System and Services_2").WebElement("Licenced").Exist Then
				blnVal=true
		End If

	End If
	If Lcase(DataTable.Value("Terminate"))="yes" Then
		.WebElement("img_30_0").Click @@ script infofile_;_ZIP::ssf19.xml_;_
		.Sync
	End If
End With
If Err.Number=0 OR blnVal Then
	blnVal=true
End If

UpdateTestCaseStatusInExcel Environment("TestName"),blnVal




