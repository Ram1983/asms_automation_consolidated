﻿blnVal=False
With VbWindow("frmMDI")
	.WinMenu("Menu").Select "Accounting;Adjustments;Journal Entry;Add Journal Entry"
	With .VbWindow("frmARJournal_Entry")
		While Not .VbEdit("txtJournalNo").Exist
		Wend
		.VbEdit("txtJournalNo").Set RandomNumber(9,99990) @@ hightlight id_;_592170_;_script infofile_;_ZIP::ssf13.xml_;_
		.AcxTable("SSDBGrid Control 3.1 -").SelectCell 1,3 @@ hightlight id_;_461180_;_script infofile_;_ZIP::ssf14.xml_;_
	End With
End With
With Window("ASMS").Window("Automated Spectrum Management")
	.WinObject("Afx:27800000:8").Click 127,58 @@ hightlight id_;_461152_;_script infofile_;_ZIP::ssf24.xml_;_
	.WinObject("Afx:27800000:8").Click 145,55 @@ hightlight id_;_461152_;_script infofile_;_ZIP::ssf25.xml_;_
End With
Window("SSDropDownFrame").WinObject("SSChildDropDown").Click 37,28 @@ hightlight id_;_1704662_;_script infofile_;_ZIP::ssf26.xml_;_
With VbWindow("frmMDI")
	With .VbWindow("frmARJournal_Entry")
		.AcxTable("SSDBGrid Control 3.1 -").SelectCell 1,4 @@ hightlight id_;_461180_;_script infofile_;_ZIP::ssf16.xml_;_
		val=GenerateRandomString(5)
		.AcxTable("SSDBGrid Control 3.1 -").SetCellData 1,4,val @@ hightlight id_;_461180_;_script infofile_;_ZIP::ssf17.xml_;_
		.AcxTable("SSDBGrid Control 3.1 -").SelectCell 1,5 @@ hightlight id_;_461180_;_script infofile_;_ZIP::ssf18.xml_;_
		.AcxTable("SSDBGrid Control 3.1 -").Type val&RandomNumber(1,10) @@ hightlight id_;_461180_;_script infofile_;_ZIP::ssf19.xml_;_
		.AcxTable("SSDBGrid Control 3.1 -").Type  micTab @@ hightlight id_;_461180_;_script infofile_;_ZIP::ssf20.xml_;_
		amount=RandomNumber(1,20)
		.AcxTable("SSDBGrid Control 3.1 -").SetCellData 1,5,amount @@ hightlight id_;_461180_;_script infofile_;_ZIP::ssf21.xml_;_
		.VbButton("Post").Click @@ hightlight id_;_395730_;_script infofile_;_ZIP::ssf22.xml_;_
	End With
	With .Dialog("#32770")
		If .WinButton("OK").Exist Then
			.WinButton("OK").Click
		End If
	End With
End With

With Window("ASMS").Window("Automated Spectrum Management")
	.WinObject("Afx:27800000:8").Click 59,81 @@ hightlight id_;_1771316_;_script infofile_;_ZIP::ssf27.xml_;_
	.WinObject("Afx:27800000:8").Click 147,91 @@ hightlight id_;_1771316_;_script infofile_;_ZIP::ssf28.xml_;_
End With
WinObject("SSChildDropDown").Click 63,26 @@ hightlight id_;_395950_;_script infofile_;_ZIP::ssf48.xml_;_
With VbWindow("frmMDI").VbWindow("frmARJournal_Entry")
	.AcxTable("SSDBGrid Control 3.1 -").SelectCell 2,4 @@ hightlight id_;_395888_;_script infofile_;_ZIP::ssf49.xml_;_
	.AcxTable("SSDBGrid Control 3.1 -").SetCellData 2,4,GenerateRandomString(7) @@ hightlight id_;_395888_;_script infofile_;_ZIP::ssf50.xml_;_
	.AcxTable("SSDBGrid Control 3.1 -").SetCellData 2,6,amount @@ hightlight id_;_395888_;_script infofile_;_ZIP::ssf51.xml_;_
End With

Window("ASMS").Window("Automated Spectrum Management").WinObject("Afx:27800000:8").Click 566,96 @@ hightlight id_;_1771316_;_script infofile_;_ZIP::ssf30.xml_;_
With VbWindow("frmMDI")
	.VbWindow("frmARJournal_Entry").VbButton("Post").Click
	.Dialog("#32770").WinButton("OK").Click @@ hightlight id_;_1509090_;_script infofile_;_ZIP::ssf36.xml_;_
	.VbWindow("frmARJournal_Entry").VbButton("Preview").Click @@ hightlight id_;_2098686_;_script infofile_;_ZIP::ssf37.xml_;_
	.Activate @@ hightlight id_;_591342_;_script infofile_;_ZIP::ssf38.xml_;_
End With
With VbWindow("frmReportViewer")
	.Activate @@ hightlight id_;_3147082_;_script infofile_;_ZIP::ssf39.xml_;_
	Wait 10
	.VbButton("Print").Click @@ hightlight id_;_264734_;_script infofile_;_ZIP::ssf40.xml_;_
	.Dialog("Print").WinButton("OK").Click @@ hightlight id_;_264724_;_script infofile_;_ZIP::ssf41.xml_;_
End With
With Dialog("Save Print Output As")
	.WinEdit("File name:").Set Environment("ResultDir")&RandomNumber(1,9999) @@ hightlight id_;_199060_;_script infofile_;_ZIP::ssf43.xml_;_
	.WinButton("Save").Click @@ hightlight id_;_330264_;_script infofile_;_ZIP::ssf44.xml_;_
	If .Exist Then
		blVal=True
	End If
End With
VbWindow("frmReportViewer").VbButton("Close").Click @@ hightlight id_;_854344_;_script infofile_;_ZIP::ssf45.xml_;_
VbWindow("frmMDI").VbWindow("frmARJournal_Entry").VbButton("Close").Click @@ hightlight id_;_1181342_;_script infofile_;_ZIP::ssf46.xml_;_
UpdateTestCaseStatusInExcel Environment("TestName"),blnVal

