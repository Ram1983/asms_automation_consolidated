﻿
blnFind=False
err.number = 0

'Selecting the view status of the appplication

With Browser("Application Status")
	.Page("Application Status").Link("Home").Click @@ script infofile_;_ZIP::ssf2.xml_;_
	.Sync
	.Page("Page").Image("edit").Click @@ script infofile_;_ZIP::ssf3.xml_;_
	.Sync
End With
Browser("name:=.*Customer Home Page.*").Page("title:=.*Customer Home Page*").Link("name:=View Status of Application").Clicks @@ script infofile_;_ZIP::ssf1.xml_;_
Browser("name:=.*Customer Home Page.*").Page("title:=.*Customer Home Page*").Sync
If Browser("name:=.*Application.*").Page("title:=.*Application.*").WebTable("text:=.*Application.*","column names:=.*Application.*").Exist Then
	blnFind = True
End If

strTestCaseName = Environment.Value("TestName")
			
'Test results dsiplayed in Excel

 UpdateTestCaseStatusInExcel strTestCaseName, blnFind 

