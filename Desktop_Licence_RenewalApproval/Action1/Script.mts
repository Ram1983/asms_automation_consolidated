﻿wait(2)
blnFind=True
err.number = 0

datatable.ImportSheet ProjectFolderPath &"\Desktop_Input_Data_Sheet\Input_Data_Sheet.xls","Renewalapproval","Renewalapproval"
n=datatable.GetSheet("Renewalapproval").GetRowCount

'For i = 1 To n Step 1
If n>0 Then
	datatable.SetCurrentRow(1)
  
	With VbWindow("frmMDI")
		.WinMenu("Menu").Select "Licence;Renew Licence;Renewal Approval"

		'====Month
		With .VbWindow("frmLicRenew")
			While Not .ActiveX("TCIComboBox.tcb").VbComboBox("cboMonth").Exist
			Wend
			.ActiveX("TCIComboBox.tcb").VbComboBox("cboMonth").Select Datatable.value("Month","Renewalapproval") '"May" @@ hightlight id_;_460978_;_script infofile_;_ZIP::ssf5.xml_;_
			.ActiveX("TCIComboBox.tcb").WinEdit("Edit").Type  micTab @@ hightlight id_;_264340_;_script infofile_;_ZIP::ssf6.xml_;_
			wait(3)
			.ActiveX("TCIComboBox.tcb_2").VbComboBox("cboYear").Select Datatable.value("Year","Renewalapproval") '"2020" @@ hightlight id_;_134192_;_script infofile_;_ZIP::ssf7.xml_;_
			wait(10)
			If Datatable.value("CheckboxOptions","Renewalapproval") = "Select All" Then
				.VbButton("Select All").Click
				Else
					strCellData = 1
					strTemp = 0
					intRowCount = 1
					Do while strTemp <> strCellData
						strTemp = strCellData
						strCellData = .AcxTable("List of Licences to be").GetCellData(intRowCount,2)

						If strCellData = Datatable.value("InvoiceNumber","Renewalapproval")  Then   '"00529-1"
							'VbWindow("frmMDI").VbWindow("frmLicRenew").AcxTable("List of Licences to be").SelectColumn 1
							'VbWindow("frmMDI").VbWindow("frmLicRenew").AcxTable("List of Licences to be").SelectRow intRowCount
							Wait(2)
							.AcxTable("List of Licences to be").ActivateCell intRowCount,1
							Wait(2)
							.AcxTable("List of Licences to be").SetCellData intRowCount,1,"ON"
							'VbWindow("frmMDI").VbWindow("frmLicRenew").AcxTable("List of Licences to be").SelectCell intRowCount,1
							'VbWindow("frmMDI").VbWindow("frmLicRenew").AcxTable("List of Licences to be").ActivateRow intRowCount
							Exit Do
						End If
						intRowCount = intRowCount + 1
					Loop 

			End If
			wait(3)

			'VbWindow("frmMDI").VbWindow("frmLicRenew").AcxTable("List of Licences to be").SelectColumn 1
			'VbWindow("frmMDI").VbWindow("frmLicRenew").VbButton("Next").Click

			.VbButton("Next").Click @@ hightlight id_;_133188_;_script infofile_;_ZIP::ssf12.xml_;_
		End With
		wait(10)

		With .Dialog("#32770")
			If .WinButton("OK").Exist(15) Then
				.WinButton("OK").Click	
			End If
		End With
	End With

'	
'	With VbWindow("frmReportViewer")
'		If .Exist(5) Then
'			.Minimize
'		End If
'	End With
	'If VbWindow("frmReportViewer_1").Exist(5) Then
	'	VbWindow("frmReportViewer_1").Minimize
	'End If
'	With VbWindow("frmReportViewer_2")
'		If .Exist(5) Then
'			.Minimize
'		End If
'	End With
'	With VbWindow("frmReportViewer_3")
'		If .Exist(5) Then
'			.Minimize
'		End If
'	End With

	'VbWindow("frmReportViewer_4").Minimize
	'VbWindow("frmReportViewer").Minimize
	'VbWindow("frmReportViewer_2").Minimize
	
	'VbWindow("frmReportViewer").VbButton("Close").Click
	'Window("Window_2").Click 911,207
	'VbWindow("frmReportViewer_2").VbButton("Close").Click
	
Wait 120
Set fso = CreateObject("Scripting.FileSystemObject")
file_location = ProjectFolderPath &"\Desktop_Reports\"& GetCurrentDate
If not fso.FolderExists(file_location) Then
    fso.CreateFolder file_location  
End If
filename=file_location & "\" & fnRandomNumber(5)

While fso.FileExists(filename&".pdf")
filename=file_location & "\"&"_"&fnRandomNumber(10)
Wend
	
	With VbWindow("frmReportViewer_9")
		.VbButton("Print").Click @@ hightlight id_;_199144_;_script infofile_;_ZIP::ssf56.xml_;_
		.Dialog("Print").WinButton("OK").Click @@ hightlight id_;_592100_;_script infofile_;_ZIP::ssf57.xml_;_
	End With
	
	With Dialog("Save Print Output As")
		.WinEdit("File name:").Set filename @@ hightlight id_;_788474_;_script infofile_;_ZIP::ssf66.xml_;_
		.WinButton("Save").Click @@ hightlight id_;_592136_;_script infofile_;_ZIP::ssf68.xml_;_
	End With
	wait(5)
	VbWindow("frmMDI").VbWindow("frmLicRenew").VbButton("Close").Click @@ hightlight id_;_461622_;_script infofile_;_ZIP::ssf16.xml_;_
	
	With VbWindow("frmReportViewer_9")
		 @@ hightlight id_;_3213762_;_script infofile_;_ZIP::ssf70.xml_;_
		If .VbButton("Close").Exist Then
			.VbButton("Close").Click
		End If @@ hightlight id_;_1118508_;_script infofile_;_ZIP::ssf71.xml_;_
	End With

	strTestCaseName = Environment.Value("TestName")
	If err.number = 0 Then
		blnFind = True
	  Else
		blnFind = False
	End If
	
	UpdateTestCaseStatusInExcel strTestCaseName, blnFind
End If


