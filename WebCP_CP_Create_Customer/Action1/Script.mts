﻿	
blnFind=false
err.number = 0

Datatable.ImportSheet ProjectFolderPath&"\Input_Data_Sheet.xls","CP_CreateCustomer","CP_CreateCustomer"
rowC=datatable.GetSheet("CP_CreateCustomer").GetRowCount
For count = 1 To rowC Step 1
    DataTable.SetCurrentRow(count)
    If StrComp(Datatable.GetSheet("CP_CreateCustomer").GetParameter("ClientExecution").Value,"YES",1)=0 And StrComp(Datatable.GetSheet("CP_CreateCustomer").GetParameter("ClientTypeExecution").Value,"YES",1)=0 Then
		userName="customer"&RandomNumber(1,9999999)&"@gmail.com"
		password="DwpPass@"&RandomNumber(1,10000)
		strExcelPath=ProjectFolderPath&"\Input_Data_Sheet.xls"
		
		'Update the UserName in CP Login sheets based on Country
'	If datatable.value("UserName","CP_CreateCustomer")="" Then
			fnUpdateCPUserName strExcelPath,userName,password
'		End If
			'----Launch application------
    	If Lcase(Environment("Browser"))="microsoftedge" Then
    		SystemUtil.Run "msedge.exe",Datatable.GetSheet("CP_CreateCustomer").GetParameter("URL").Value
    	ElseIf Lcase(Environment("Browser"))="firefox" Then
    		 SystemUtil.Run "Firefox.exe", Datatable.GetSheet("CP_CreateCustomer").GetParameter("URL").Value
    	ElseIf Lcase(Environment("Browser"))="chrome" Then
    		Systemutil.Run "chrome.exe",Datatable.GetSheet("CP_CreateCustomer").GetParameter("URL").Value
    	End If
    				
'	    With  Browser("openurl:=http:/.*webcp.*").Page("url:=http://.*webcp.*")
			With  Browser("name:=.*Sign In.*|.*Customer.*").Page("url:=http://.*webcp.*")
'	    	  .Sync
'	    	  Browser("openurl:=http:/.*webcp.*").Maximize()
	    	  While Not .Link("text:=here","html tag:=A").Exist
	    	  Wend
	    	  .Link("text:=here","html tag:=A").Click
		     .WebList("name:=ClientTypeID","html tag:=Select").Selects datatable.value("ClientType","CP_CreateCustomer")
		     .Sync
		     While Not .Link("text:=.*Next.*").Exist
		     Wend
		     .Link("text:=.*Next.*").Click
		     .Sync
		     While Not .Link("text:=.*Next.*").Exist
		     Wend
		     Wait 5
		     
		     If .Link("text:=.*Next.*").Exist Then
		     	.Link("text:=.*Next.*").Click
		     ElseIf .WebElement("xpath:=//span[contains(text(),'Customer Entity - Information')]").Exist Then
		     	  .WebElement("xpath:=//span[contains(text(),'Customer Entity - Information')]").click
		     End If
		    	     	
			If Datatable.value("ClientType","CP_CreateCustomer")="Person" Then	
					While Not .WebEdit("name:=Eclient__clientFname").Exist
					Wend
					.WebEdit("name:=Eclient__clientFname").Sets "Firstname"&fnRandomNumber(5)
					.WebEdit("name:=Eclient__clientLname").Sets "Lastname"&fnRandomNumber(5)
					If .WebList("name:=Eclient__clientIDType").Exist(5) Then
						.WebList("name:=Eclient__clientIDType").Selects "National Id"
					End If
					.WebEdit("name:=Eclient__ROT").Sets fnRandomNumber(6)			
				If Datatable.value("Dealer","CP_CreateCustomer")="Dealer" Then
					.WebCheckBox("name:=Eclient__Dealer").Sets "ON"
				ElseIf Datatable.value("Director","CP_CreateCustomer")="Director" Then
					.WebCheckBox("name:=Eclient__Director").Sets "ON"
				End If
			
			ElseIf datatable.value("ClientType","CP_CreateCustomer")="Company"  or datatable.value("ClientType","CP_CreateCustomer")="Government" or datatable.value("ClientType","CP_CreateCustomer")="Non-Governmental Organization"  or datatable.value("ClientType","CP_CreateCustomer")="Parastatal" Or datatable.value("ClientType","CP_CreateCustomer")="Corporation" Then  
					While Not  .WebEdit("name:=Eclient__clientCompany","index:=0").Exist(5)						
					Wend
					If .WebEdit("name:=Eclient__clientCompany","index:=0").Exist(5) Then
						.WebEdit("name:=Eclient__clientCompany","index:=0").Sets "Customer"&fnRandomNumber(4)
						If Browser("openurl:=http:/.*webcp.*").Page("url:=http://.*webcp.*").WebEdit("html id:=Eclient__clientCompany","name:=Eclient__clientCompany","index:=1").Exist(5) Then
							Browser("openurl:=http:/.*webcp.*").Page("url:=http://.*webcp.*").WebEdit("html id:=Eclient__clientCompany","name:=Eclient__clientCompany","index:=1").Sets "Customer"&fnRandomNumber(5)
						End If
					End If

				
				If .WebEdit("name:=Eclient__ClientWebAddress").Exist(5) Then
					.WebEdit("name:=Eclient__ClientWebAddress").Sets Datatable.value("WebAddress","CP_CreateCustomer")
				End If
				If .WebEdit("name:=Eclient__ROT").Exist(5) Then
				   .WebEdit("name:=Eclient__ROT").Sets fnRandomNumber(4)
				End If
				If Datatable.value("Dealer","CP_CreateCustomer")="Dealer" Then
					.WebCheckBox("name:=Eclient__Dealer").SetCheckBoxOnOff "ON"
				ElseIf Datatable.value("Director","CP_CreateCustomer")="Director" Then
					.WebCheckBox("name:=Eclient__Director").SetCheckBoxOnOff "ON"
				End If
				
			End If	
				If .WebEdit("name:=Eclient__clientTelNum").Exist(30) Then
					.WebEdit("name:=Eclient__clientTelNum").Sets fnRandomNumber(8)
				End If
					.WebEdit("name:=Eclient__ClientCellPhone").Sets fnRandomNumber(8)
				If .WebEdit("name:=.*companyRegnum","html id:=.*companyRegnum").Exist(5) Then
					.WebEdit("name:=.*companyRegnum","html id:=.*companyRegnum").Sets fnRandomNumber(8)
				End If
				.WebEdit("name:=Eclient__clientFaxNum").Sets fnRandomNumber(8)
				If .WebEdit("name:=Eclient__clientEmail").Exist(5) Then
					.WebEdit("name:=Eclient__clientEmail").Sets "Clientemail"&fnRandomNumber(4)&"@gmail.com"
				End If
				.WebEdit("name:=Eclient__PlotNo").Sets Chr(84)&Chr(69)&Chr(83)&Chr(84)&" "&GenerateRandomString(4)
				.WebEdit("name:=Eclient__clientStreet1").Sets Chr(84)&Chr(69)&Chr(83)&Chr(84)&" "&GenerateRandomString(3)
				.WebList("name:=Eclient__clientCity1").Selects 1
				.WebEdit("name:=Eclient__PostalLocation").Sets Chr(84)&Chr(69)&Chr(83)&Chr(84)&" "&GenerateRandomString(4)
				.WebEdit("name:=Eclient__clientStreet2").Sets Chr(84)&Chr(69)&Chr(83)&Chr(84)&" "&GenerateRandomString(4)
				.WebEdit("name:=Eclient__clientZipCode1").Sets fnRandomNumber(8)
				If .WebEdit("name:=Eclient__clientDistrict2").Exist(5) Then
					.WebEdit("name:=Eclient__clientDistrict2").Sets Chr(84)&Chr(69)&Chr(83)&Chr(84)&" "&GenerateRandomString(4)
				End If
				.WebList("name:=Eclient__clientCity2").Selects 2
				.Link("name:=Next","visible:=true").Click	
				.WebEdit("name:=Rclient__clientFname").Sets "FirstName"&fnRandomNumber(4)
				.WebEdit("name:=Rclient__clientLname").Sets "LastName"&fnRandomNumber(4)
				If .WebList("name:=Rclient__clientIDType").Exist(5) Then
					.WebList("name:=Rclient__clientIDType").Selects "National Id"
				End If
				.WebEdit("name:=Rclient__ROT").Sets "NID"&fnRandomNumber(4)
				.WebEdit("name:=Rclient__clientTelNum").Sets fnRandomNumber(8)
				 fn_GetCPUserNameAndPassword UserName,Password
				 If Not datatable.value("Email","CP_CreateCustomer")="" Then
				 	UserName=datatable.value("Email","CP_CreateCustomer")
				 	Password=datatable.value("EmailPassword","CP_CreateCustomer")
				 End If
				 Domain=datatable.value("Domain","CP_CreateCustomer")
				.WebEdit("name:=Rclient__clientEmail").Sets UserName
				.WebEdit("name:=Rclient__Password").Sets Password
				.Link("name:=Next","visible:=true").Click
				.WebCheckBox("html id:=aggrement","type:=checkbox").SetCheckBoxOnOff "ON"
				.Link("name:=Request Account","visible:=true").Click
				Wait 5
'				If WaitForObject(Browser("openurl:=http:/.*webcp.*").Page("url:=http://.*webcp.*").WebTable("innertext:=Thank you for submitting your request.*")) Then
				If Browser("openurl:=http:/.*webcp.*").Page("url:=http://.*webcp.*").WebTable("innertext:=Thank you for submitting your request.*").Exist Then
				   Browser("openurl:=.*webc.*").Close
				   
				ElseIf Browser("name:=.*Sign In.*|.*Customer Portal.*").Page("url:=http://.*webcp.*").WebTable("innertext:=Thank you for submitting your request.*").Exist Then
					Browser("name:=.*Sign In.*|.*Customer Portal.*").Close
				Else
				  Reporter.ReportEvent micFail,"Create Customer","Failed to create customer in WebCP"
				End If
				
				
			   End With
		   Exit For
		End If
 Next
 
 
Wait 5		
 
' fn_VerifyEmail UserName,Password,Domain
 
 strTestCaseName = Environment.Value("TestName")
	
'Test results dsiplayed in Excel
If err.number = 0 Then
    blnFind = True
End If

UpdateTestCaseStatusInExcel strTestCaseName, blnFind 



 
 
 
 
