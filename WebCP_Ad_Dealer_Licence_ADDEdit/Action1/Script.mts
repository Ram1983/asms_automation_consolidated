﻿

blnFind=False
err.number = 0

'import data
datatable.ImportSheet ProjectFolderPath&"\Input_Data_Sheet.xls","Ad_Dealer_Licence_AddEdit","Ad_Dealer_Licence_AddEdit"
n=datatable.GetSheet("Ad_Dealer_Licence_AddEdit").GetRowCount

'For i = 1 To n Step 1
If n>0 Then
    datatable.SetCurrentRow(1)	
    'Selecting Dealer Licence
	With Browser("Admin Home Page").Page("Admin Home Page")
		.Link("Dealer").Click
		.Sync
	End With
	'Add Dealer Licence
    If Datatable.Value("FunctionalityType","Ad_Dealer_Licence_AddEdit")="ADD" Then
        Browser("opentitle:=Administration Login").Page("title:=Dealer").Link("name:=Add.*Dealer.*").click
        'Edit Dealer Licence
    ElseIf Datatable.Value("FunctionalityType","Ad_Dealer_Licence_AddEdit")="Edit"  Then
			With Browser("Admin Home Page").Page("Dealer_5")
				.WebEdit("WebEdit").Set Datatable.Value("DealerID","Ad_Dealer_Licence_AddEdit")
				Wait 3
				rc=.WebTable("1").GetRowWithCellText(Datatable.Value("DealerID","Ad_Dealer_Licence_AddEdit"))
				msgbox rc
				.WebTable("1").ChildItem(rc,1,"WebElement",1).highlight
				.WebTable("1").ChildItem(rc,1,"WebElement",1).Click
			End With
	End If
	With Browser("Admin Home Page").Page("Dealer_4")
		If Datatable.Value("Modify","Ad_Dealer_Licence_AddEdit")="yes" Then
			.WebElement("img_12_0").Click
			.Sync
		End If
	End With
	If Datatable.Value("FunctionalityType","Ad_Dealer_Licence_AddEdit")="ADD" Then
		'Dealer Information1
		With Browser("Admin Home Page").Page("Dealer_2")
			If .WebList("clientId").Exist(8) Then
				.WebList("clientId").Select Datatable.Value("ClientID","Ad_Dealer_Licence_AddEdit")
			End If

			'Dealer information2
			.WebCheckBox("Wholesaling").Set "ON"
			'If Browser("Admin Home Page").Page("Dealer_2").WebEdit("storesQty").Exist(5) Then
			'
			'Browser("Admin Home Page").Page("Dealer_2").WebEdit("storesQty").Set Datatable.value("Storesqty","Ad_Dealer_Licence_AddEdit")
			'End If

			'Service Support
			If Datatable.value("Onlyinworkshop","Ad_Dealer_Licence_AddEdit")="Yes" Then
				.WebCheckBox("supplying").Set "ON"
			End If
			If Datatable.value("FieldService","Ad_Dealer_Licence_AddEdit")="Yes" Then
				.WebCheckBox("importing").Set "ON"
			End If
			If Datatable.value("ByManufacure","Ad_Dealer_Licence_AddEdit")="Yes" Then
				.WebCheckBox("dealing").Set "ON"
			End If
			If Datatable.value("ByUser","Ad_Dealer_Licence_AddEdit")="Yes" Then
				.WebCheckBox("repairing").Set "ON"
			End If

			'Main Activites Dealer
			.WebList("activity1").Select(1)
			.WebList("activity2").Select(2)
			.WebList("activity3").Select(3)
			.WebList("activity4").Select(4)
			.WebList("activity5").Select(5)
			If .WebList("activity1").GetROProperty("items count")>6 Then
				.WebList("activity6").Select(6)
			End If

			.WebElement("WebElement").Click
			.WebCheckBox("assembling").Set "ON"

			'Dealer ownership(client)

			.WebEdit("owner1").Set Datatable.value("Owner1","Ad_Dealer_Licence_AddEdit")
			.WebEdit("owner2").Set Datatable.value("Owner2","Ad_Dealer_Licence_AddEdit")
			.WebEdit("owner3").Set Datatable.value("Owner3","Ad_Dealer_Licence_AddEdit")
			.WebEdit("owner4").Set Datatable.value("Owner4","Ad_Dealer_Licence_AddEdit")
			.WebEdit("owner5").Set Datatable.value("Owner5","Ad_Dealer_Licence_AddEdit")
			.WebEdit("owner6").Set Datatable.value("Owner6","Ad_Dealer_Licence_AddEdit")
			If .WebEdit("owner7").GetROProperty("items count")>7 Then
				.WebEdit("owner7").Set Datatable.value("Owner7","Ad_Dealer_Licence_AddEdit")
			End If

			'Dealer Ownership(ASMS)
			If .WebList("ownerclientid1").GetROProperty("items count")>1 Then
				.WebList("ownerclientid1").Select(1)
			End If

			If .WebList("ownerclientid2").GetROProperty("items count")>2 Then
				.WebList("ownerclientid2").Select(2)' "adlname1, adfname1"
			End If
			If .WebList("ownerclientid3").GetROProperty("items count")>3 Then
				.WebList("ownerclientid3").Select(3)' "dwp7654, dwp456"
			End If
			If .WebList("ownerclientid4").GetROProperty("items count")>4 Then
				.WebList("ownerclientid4").Select(4)' "Ferguson, Lorato"
			End If
			If .WebList("ownerclientid5").GetROProperty("items count")>5 Then
				.WebList("ownerclientid5").Select 5
			End If
			If .WebList("ownerclientid6").GetROProperty("items count")>6 Then

				.WebList("ownerclientid6").Select(6)' "Kavishwar, Ashish"
			End If

			If .WebList("ownerclientid7").GetROProperty("items count")>=7 Then
				.WebList("ownerclientid7").Select(7)' "Luke, Luke"
			End If

			'update
			.WebElement("img_1").Click
		End With
		With Browser("Dealer").Page("Dealer")
			.Sync
			'official use/Attachments
			If .WebElement("UpdateMoveToPending").Exist(8) Then
				.WebElement("UpdateMoveToPending").Click
			End If
			.Sync
			If .WebEdit("issuedate").Exist(6) Then
				.WebEdit("issuedate").Set Datatable.value("IssueDate","Ad_Dealer_Licence_AddEdit")
			End If
			If .WebEdit("expiryDate").Exist(6) Then
				.WebEdit("expiryDate").Set Datatable.value("Expirydate","Ad_Dealer_Licence_AddEdit")
			End If
		End With
		'update

		Browser("Admin Home Page").Page("Dealer_3").WebElement("img_1").Click
		Browser("Dealer").Page("Dealer").Sync
		'Navigate to attachments

		With Browser("Admin Home Page")
			With .Page("Dealer_4")
				.WebElement("Official Use / Attachments").Click
				.Sync
				wait 5
				'    '
				'    'AIUtil.SetContext Browser("Admin Home Page")
				'    'AIUtil("button", "", micFromTop, 1).Click
				'    'If Not Window("Google Chrome").Dialog("Open").WinEdit("File name:").Exist Then
				'    Browser("Admin Home Page").Page("Dealer_4").Frame("Frame").WebFile("BrowserHidden").Click
				'    Wait 30
				'    'End If

				val=fileCreate
				wait 8
				With .Frame("Frame")
					.WebFile("BrowserHidden").Set val
					Wait 8
					.Image("Pressing Upload, the document").Click
				End With
			End With
			'    'Update

			.Page("Dealer_3").WebElement("img_1").Click
		End With
	End If
    Browser("Admin Home Page").Sync
    Browser("Dealer").Page("Dealer_3").WebElement("img_1_0").Click @@ script infofile_;_ZIP::ssf90.xml_;_
    Browser("Admin Home Page").Sync
    If Lcase(Datatable.Value("Approve","Ad_Dealer_Licence_AddEdit"))="yes" Then
        If Browser("Admin Home Page").Page("Dealer_3").WebElement("img_21_0").Exist Then
        	Browser("Admin Home Page").Page("Dealer_3").WebElement("img_21_0").Click
        	If Instr(1,Trim(Browser("Dealer").Page("Dealer_2").WebElement("Rejected").GetROProperty("innertext")),DataTable.Value("ApproveStatus","Ad_Dealer_Licence_AddEdit"),1)>0 Then
      		  Status=DataTable.Value("ApproveStatus","Ad_Dealer_Licence_AddEdit")
      		   blnFind=True
   			 End If
        	
        End If
    ElseIf  Lcase(Datatable.Value("Reject","Ad_Dealer_Licence_AddEdit"))="yes" Then
			With Browser("Dealer").Page("Dealer_2")
				If .WebElement("img_23_1").Exist Then
					.WebElement("img_23_1").Click
					Status=DataTable.Value("RejectStatus","Ad_Dealer_Licence_AddEdit")
				End If
			End With
	End If
    
    Browser("Admin Home Page").Sync
    If Instr(1,Trim(Browser("Dealer").Page("Dealer_2").WebElement("Rejected").GetROProperty("innertext")),Datatable.Value("Status","Ad_Dealer_Licence_AddEdit"),1)>0 Then
        blnFind=True
    End If
    dealerID=Trim(Browser("Admin Home Page").Page("Dealer_4").WebElement("ID").GetROProperty("innertext"))
    Set objExcel = CreateObject("Excel.Application")
    Set objWB = objExcel.Workbooks.open(ProjectFolderPath&"\Input_Data_Sheet.xls")
    Set obj=objWB.Worksheets("Ad_Dealer_Licence_AddEdit")
    rowc=obj.usedrange.rows.count
    obj.cells(2,2).value=dealerID
     Wait 10
    objExcel.ActiveWorkbook.Save
    objExcel.Quit
    Set objExcel= nothing
    Set objWB =Nothing
    strTestCaseName = Environment.Value("TestName")

fn_EmailLogin DataTable.Value("Email","Ad_Dealer_Licence_AddEdit"),DataTable.Value("Password","Ad_Dealer_Licence_AddEdit"),DataTable.Value("URL","Ad_Dealer_Licence_AddEdit")

Set obj=Browser("version:=internet explorer.*","openurl:=https://.*mail.google.com/mail.*").Page("url:=https://mail.google.com/mail.*")
Set odesc=Description.Create
odesc("micclass").value="Link"
odesc("innertext").value=".*Dealer application has been.*"
Set Val=obj.ChildObjects(odesc)
For i = 0 To Val.count-1 Step 1
    Val(i).Click
    Wait 2
    If Val(i).exist(5) Then
    	Val(i).Click
    	Wait 10
    	If Val(i).exist Then
    		Val(i).Click
    	End If
    End If
    Exit For
Next

Set verify=Description.Create
'verify("micclass").value="Link"	
verify("innertext").value=".*"&Status&".*"
Set verifyEmail=Browser("version:=internet explorer.*","openurl:=https://.*mail.google.com/mail.*").Page("url:=https://mail.google.com/mail.*").ChildObjects(verify)
For count = 0 To verifyEmail.count-1 Step 1
    verifyEmail(count).highlight
Next
Wait 3
    
    
'Test results dsiplayed in Excel
If err.number = 0 Then
    blnFind = True
Else
    blnFind = False
End If
UpdateTestCaseStatusInExcel strTestCaseName, blnFind 
    
End If


