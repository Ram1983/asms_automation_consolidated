﻿wait(2)
blnFind=True
err.number = 0

datatable.ImportSheet ProjectFolderPath &"\Desktop_Input_Data_Sheet\Input_Data_Sheet.xls","ManageGroup","ManageGroup"
n=datatable.GetSheet("ManageGroup").GetRowCount

'For i = 1 To n Step 1
If n>0 Then
	datatable.SetCurrentRow(1)
	fn_NavigateToScreen "Administrative;SECURITY;Manage Groups"
	If Datatable.Value("Operation","ManageGroup")="Add Group" Then
		'Add Group
		VbWindow("frmMDI").VbWindow("frmManageGroups").ActiveX("SSTab").Click 101,13 @@ hightlight id_;_17238686_;_script infofile_;_ZIP::ssf5.xml_;_
		VbWindow("frmMDI").VbWindow("frmManageGroups").ActiveX("SSTab").VbEdit("txtNewGroups").Set Datatable.Value("GroupName","ManageGroup") @@ hightlight id_;_1118098_;_script infofile_;_ZIP::ssf6.xml_;_
		VbWindow("frmMDI").VbWindow("frmManageGroups").ActiveX("SSTab").VbEditor("txtNotes").SetCaretPos 0,0 @@ hightlight id_;_1116896_;_script infofile_;_ZIP::ssf7.xml_;_
		VbWindow("frmMDI").VbWindow("frmManageGroups").ActiveX("SSTab").VbEditor("txtNotes").Type Datatable.Value("Notes","ManageGroup") @@ hightlight id_;_1116896_;_script infofile_;_ZIP::ssf8.xml_;_
		VbWindow("frmMDI").VbWindow("frmManageGroups").ActiveX("SSTab").ActiveX("TCIComboBox.tcb").VbComboBox("cboGroup").Select Datatable.Value("Permission","ManageGroup") @@ hightlight id_;_2231636_;_script infofile_;_ZIP::ssf9.xml_;_
		VbWindow("frmMDI").VbWindow("frmManageGroups").ActiveX("SSTab").VbButton("Save").Click @@ hightlight id_;_1051388_;_script infofile_;_ZIP::ssf10.xml_;_
	
	ElseIf Datatable.Value("Operation","ManageGroup")="Modify Group" Then
		'Modify Group
		VbWindow("frmMDI").VbWindow("frmManageGroups").ActiveX("SSTab").Click 267,6 @@ hightlight id_;_17238686_;_script infofile_;_ZIP::ssf11.xml_;_
		VbWindow("frmMDI").VbWindow("frmManageGroups").ActiveX("SSTab").ActiveX("TCIComboBox.tcb").VbComboBox("cboGroupModify").Select Datatable.Value("GroupName","ManageGroup") @@ hightlight id_;_463454_;_script infofile_;_ZIP::ssf12.xml_;_
	    VbWindow("frmMDI").VbWindow("frmManageGroups").ActiveX("SSTab").VbEditor("txtNoteModify").Object.Text =""
		VbWindow("frmMDI").VbWindow("frmManageGroups").ActiveX("SSTab").VbEditor("txtNoteModify").Type Datatable.Value("Notes","ManageGroup") @@ hightlight id_;_1053272_;_script infofile_;_ZIP::ssf14.xml_;_
		VbWindow("frmMDI").VbWindow("frmManageGroups").ActiveX("SSTab").VbButton("Save_2").Click @@ hightlight id_;_723696_;_script infofile_;_ZIP::ssf15.xml_;_
	
	ElseIf Datatable.Value("Operation","ManageGroup")="Delete Group" Then
		'Delete Group
		VbWindow("frmMDI").VbWindow("frmManageGroups").ActiveX("SSTab").Click 473,11 @@ hightlight id_;_17238686_;_script infofile_;_ZIP::ssf16.xml_;_
		VbWindow("frmMDI").VbWindow("frmManageGroups").ActiveX("SSTab").ActiveX("TCIComboBox.tcb").VbComboBox("cboGroupRemove").Select Datatable.Value("GroupName","ManageGroup") @@ hightlight id_;_1642456_;_script infofile_;_ZIP::ssf17.xml_;_
		VbWindow("frmMDI").VbWindow("frmManageGroups").ActiveX("SSTab").ActiveX("TCIComboBox.tcb").VbComboBox("cboGroupRemove").Type  micTab @@ hightlight id_;_1642456_;_script infofile_;_ZIP::ssf18.xml_;_
		VbWindow("frmMDI").VbWindow("frmManageGroups").ActiveX("SSTab").VbButton("Delete").Click @@ hightlight id_;_789178_;_script infofile_;_ZIP::ssf19.xml_;_
		VbWindow("frmMDI").Dialog("#32770").WinButton("Yes").Click @@ hightlight id_;_727312_;_script infofile_;_ZIP::ssf20.xml_;_
	End If
	'VbWindow("frmMDI").VbWindow("frmManageGroups").ActiveX("SSTab").Click 469,10 @@ hightlight id_;_17238686_;_script infofile_;_ZIP::ssf21.xml_;_
	wait(5)
	VbWindow("frmMDI").VbWindow("frmManageGroups").VbButton("Close").Click @@ hightlight id_;_1444562_;_script infofile_;_ZIP::ssf22.xml_;_
	
	strTestCaseName = Environment.Value("TestName")
	If err.number = 0 Then
		blnFind = True
	  Else
		blnFind = False
	End If
	
	UpdateTestCaseStatusInExcel strTestCaseName, blnFind
End If




