﻿
'Search application number
Browser("Admin Home Page").Page("Admin Home Page").Link("Search Customer or Application").Click @@ script infofile_;_ZIP::ssf1.xml_;_
Browser("Admin Home Page").Page("Page").WebList("ReferenceNum").Select Datatable.value("AppNumber","AdReviewCpCitizenBand") @@ script infofile_;_ZIP::ssf2.xml_;_
'Submit search
Browser("Admin Home Page").Page("Page").Link("Submit Search").Click @@ script infofile_;_ZIP::ssf3.xml_;_
'Mange application
Browser("Admin Home Page").Page("Page").Link("M").Click @@ script infofile_;_ZIP::ssf4.xml_;_
'Change status to Reviewing
Browser("Admin Home Page").Page("Manage Application").WebElement("img_1").Click @@ script infofile_;_ZIP::ssf5.xml_;_
'Review the application
Browser("Admin Home Page").Page("Manage Application").WebElement("img_E").Click @@ script infofile_;_ZIP::ssf6.xml_;_
'Navigate to base station
Browser("Admin Home Page").Page("Application").WebElement("Base Station").Click @@ script infofile_;_ZIP::ssf7.xml_;_
Browser("Admin Home Page").Page("Application").Image("plus2").Click @@ script infofile_;_ZIP::ssf8.xml_;_
Browser("Admin Home Page").Page("Application").Image("plus2_2").Click @@ script infofile_;_ZIP::ssf9.xml_;_
Browser("Admin Home Page").Page("Application").WebElement("Station : New Base Station").Click @@ script infofile_;_ZIP::ssf10.xml_;_
'Station class and service class
Browser("Admin Home Page").Page("Application").Frame("Frame").WebList("tbl_site__stationPurpose").Select "MOBILE" @@ script infofile_;_ZIP::ssf11.xml_;_
Browser("Admin Home Page").Page("Application").Frame("Frame").WebList("tbl_site__stationClass").Select "Mobile Stn." @@ script infofile_;_ZIP::ssf12.xml_;_
'Save Station
Browser("Admin Home Page").Page("Application").Frame("Frame").WebElement("img_0").Click @@ script infofile_;_ZIP::ssf13.xml_;_
'Equipment Click
Browser("Admin Home Page").Page("Application").WebElement("Trx : b5-681022").Click @@ script infofile_;_ZIP::ssf14.xml_;_
 @@ hightlight id_;_2034310_;_script infofile_;_ZIP::ssf15.xml_;_
'Browser("Admin Home Page").Page("Application").Frame("Frame_2").WebList("tbl_equipment__ApprovalNum").Select "BOT/TA/TRANSCEIVER/5" @@ script infofile_;_ZIP::ssf16.xml_;_

Browser("Admin Home Page").Page("Application").Frame("Frame_2").WebList("tbl_equipment__relateEquipType").Select(1)' "Citizen Band 27 MHz National" @@ script infofile_;_ZIP::ssf17.xml_;_
'Call sign click
Browser("Admin Home Page").Page("Application").Frame("Frame_2").WebElement("img_GCStbl_equipment__callSign").Click @@ script infofile_;_ZIP::ssf18.xml_;_
'Save station
Browser("Admin Home Page").Page("Application").Frame("Frame_2").WebElement("img_0").Click @@ script infofile_;_ZIP::ssf19.xml_;_
'Mange application
Browser("Admin Home Page").Page("Application").Link("Manage Application").Click @@ script infofile_;_ZIP::ssf20.xml_;_
'Accepting beside sublicnce button
Browser("Admin Home Page").Page("Manage Application").WebElement("img_Accept_7").Click @@ script infofile_;_ZIP::ssf21.xml_;_
'Accept the application
Browser("Admin Home Page").Page("Manage Application").WebElement("img_0").Click @@ script infofile_;_ZIP::ssf22.xml_;_
Browser("Admin Home Page").HandleDialog micOK @@ hightlight id_;_2034310_;_script infofile_;_ZIP::ssf23.xml_;_
