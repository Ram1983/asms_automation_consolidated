﻿blnFind=True
err.number = 0

datatable.ImportSheet ProjectFolderPath &"\Desktop_Input_Data_Sheet\Input_Data_Sheet.xls","AddUser","AddUser"
n=datatable.GetSheet("AddUser").GetRowCount

'For i = 1 To n Step 1 
If n>0 Then
	datatable.SetCurrentRow(1) 	
	
fn_NavigateToScreen "Administrative;SECURITY;Add User"
	While not VbWindow("frmMDI").VbWindow("frmAddUser").Exist
	Wend
	VbWindow("frmMDI").VbWindow("frmAddUser").VbEdit("txtUserID").Set Datatable.Value("UserID","AddUser") @@ hightlight id_;_1121290_;_script infofile_;_ZIP::ssf4.xml_;_
	VbWindow("frmMDI").VbWindow("frmAddUser").VbEdit("txtUserID").Type  micTab @@ hightlight id_;_1121290_;_script infofile_;_ZIP::ssf5.xml_;_
	VbWindow("frmMDI").VbWindow("frmAddUser").VbEdit("txtPassword").SetSecure Datatable.Value("Password","AddUser") @@ hightlight id_;_6686776_;_script infofile_;_ZIP::ssf6.xml_;_
	VbWindow("frmMDI").VbWindow("frmAddUser").VbEdit("txtPassword").Type  micTab @@ hightlight id_;_6686776_;_script infofile_;_ZIP::ssf7.xml_;_
	VbWindow("frmMDI").VbWindow("frmAddUser").VbEdit("txtRepassword").SetSecure Datatable.Value("Password","AddUser") @@ hightlight id_;_3544814_;_script infofile_;_ZIP::ssf8.xml_;_
	VbWindow("frmMDI").VbWindow("frmAddUser").VbEdit("txtRepassword").Type  micTab @@ hightlight id_;_3544814_;_script infofile_;_ZIP::ssf9.xml_;_
	VbWindow("frmMDI").VbWindow("frmAddUser").VbEdit("txtFirstName").Set Datatable.Value("FirstName","AddUser") @@ hightlight id_;_1901896_;_script infofile_;_ZIP::ssf10.xml_;_
	VbWindow("frmMDI").VbWindow("frmAddUser").VbEdit("txtFirstName").Type  micTab @@ hightlight id_;_1901896_;_script infofile_;_ZIP::ssf11.xml_;_
	VbWindow("frmMDI").VbWindow("frmAddUser").VbEdit("txtLastname").Set Datatable.Value("LastName","AddUser") @@ hightlight id_;_3082118_;_script infofile_;_ZIP::ssf12.xml_;_
	VbWindow("frmMDI").VbWindow("frmAddUser").VbEdit("txtLastname").Type  micTab @@ hightlight id_;_3082118_;_script infofile_;_ZIP::ssf13.xml_;_
	VbWindow("frmMDI").VbWindow("frmAddUser").VbEdit("txtAddress").Set Datatable.Value("Address","AddUser") @@ hightlight id_;_1050180_;_script infofile_;_ZIP::ssf14.xml_;_
	VbWindow("frmMDI").VbWindow("frmAddUser").VbEdit("txtAddress").Type  micTab
	VbWindow("frmMDI").VbWindow("frmAddUser").VbEdit("txtTitle").SetSelection 0,1 @@ hightlight id_;_2229594_;_script infofile_;_ZIP::ssf16.xml_;_
	VbWindow("frmMDI").VbWindow("frmAddUser").VbEdit("txtTitle").Type Datatable.Value("Title","AddUser")
	VbWindow("frmMDI").VbWindow("frmAddUser").VbEdit("txtDepartment").Type Datatable.Value("Dept","AddUser") @@ hightlight id_;_2361860_;_script infofile_;_ZIP::ssf20.xml_;_
	VbWindow("frmMDI").VbWindow("frmAddUser").VbEdit("txtExtNum").Type Datatable.Value("ExtNum","AddUser") @@ hightlight id_;_2164006_;_script infofile_;_ZIP::ssf21.xml_;_
	Wait 5
	VbWindow("frmMDI").VbWindow("frmAddUser").VbEdit("txtEmailAddress").Set Datatable.Value("EmailAddress","AddUser") @@ hightlight id_;_2757388_;_script infofile_;_ZIP::ssf22.xml_;_
	
	if Datatable.Value("ChangePassword","AddUser")="No" then
		VbWindow("frmMDI").VbWindow("frmAddUser").VbCheckBox("User must change password").Set "OFF" @@ hightlight id_;_1705842_;_script infofile_;_ZIP::ssf23.xml_;_
	else
		VbWindow("frmMDI").VbWindow("frmAddUser").VbCheckBox("User must change password").Set "ON" @@ hightlight id_;_1705842_;_script infofile_;_ZIP::ssf24.xml_;_
	End If
	
	For Iterator = 1 To VbWindow("frmMDI").VbWindow("frmAddUser").AcxTable("Group").RowCount
		'VbWindow("frmMDI").VbWindow("frmAddUser").AcxTable("Group").SelectCell Iterator, 3
		VbWindow("frmMDI").VbWindow("frmAddUser").AcxTable("Group").SetCellData Iterator, 3, True
	Next
	wait(2)
	VbWindow("frmMDI").VbWindow("frmAddUser").VbButton("Add").Click @@ hightlight id_;_1509100_;_script infofile_;_ZIP::ssf40.xml_;_
	Wait 3
	If VbWindow("frmMDI").VbWindow("frmAddUser").VbButton("Close").Exist(6) Then
	   VbWindow("frmMDI").VbWindow("frmAddUser").VbButton("Close").Click @@ hightlight id_;_3147654_;_script infofile_;_ZIP::ssf41.xml_;_
	End If
	strTestCaseName = Environment.Value("TestName")
	
	If err.number = 0 Then
		blnFind = True
	  Else
		blnFind = False
	End If
	
	UpdateTestCaseStatusInExcel strTestCaseName, blnFind
	'End If
	wait(2)
End If @@ hightlight id_;_3147654_;_script infofile_;_ZIP::ssf41.xml_;_

