﻿
blnFind=True
err.number = 0

'Import data from Input data sheet  from folder
datatable.ImportSheet ProjectFolderPath&"\Input_Data_Sheet.xls","Ad_SublicAero","Ad_SublicAero"
n=datatable.GetSheet("Ad_SublicAero").GetRowCount


With Browser("Admin Home Page").Page("Application")
    
    'For i = 1 To n Step 1
    If n>0 Then
        datatable.SetCurrentRow(1)	
        
        'Review and Edit 
        If Datatable.value("FunctionType","Ad_SublicAero")="Review" Or Datatable.value("FunctionType","Ad_SublicAero")="Edit" Then
            
            
            If Datatable.value("FunctionType","Ad_SublicAero")="Edit" Then
             	Browser("Admin Home Page").Page("Page").Link("A").Click
            End If
            '-------------------------------Station Details-------------------------------------- Add station details below
            fn_EditSubLicenses "station",Datatable.Value("Stationtype","Ad_SublicAero"),Datatable.Value("SubLicType","Ad_SublicAero")
            fn_LoadpageInWebCP
            If Browser("Admin Home Page").Page("Application").Frame("Frame_5").Image("Edit").Exist Then
            	Browser("Admin Home Page").Page("Application").Frame("Frame_5").Image("Edit").Click
            	fn_LoadpageInWebCP
            End If
			If .Frame("Frame_2").WebList("tbl_site__stationPurpose").Exist(20) Then
			   .Frame("Frame_2").WebList("tbl_site__stationPurpose").Select Datatable.Value("ServiceClass","Ad_SublicAero")
			End If
	        If .Frame("Frame_2").WebList("tbl_site__stationClass").Exist(8) Then
	        	 .Frame("Frame_2").WebList("tbl_site__stationClass").Select Datatable.Value("StationClass","Ad_SublicAero")
	        End If
           
            .Frame("Frame_2").WebElement("img_0").Click
            fn_LoadpageInWebCP
            '====Call sign
            If .Frame("Frame_2").WebElement("img_GCStbl_site__siteCallSign").Exist Then
            	.Frame("Frame_2").WebElement("img_GCStbl_site__siteCallSign").Click
            End If
            fn_LoadpageInWebCP
            '=======Save station 
            .Frame("Frame_2").WebElement("img_0").Click
            fn_LoadpageInWebCP
            fn_EditSubLicenses "",Datatable.Value("Stationtype","Ad_SublicAero"),Datatable.Value("SubLicType","Ad_SublicAero")
            fn_LoadpageInWebCP			
        End If 
        
        
        If Datatable.value("FunctionType","Ad_SublicAero")="New"  Then
            
            
            'Aeronautical Station
            If Datatable.Value("Stationtype","Ad_SublicAero")="Aeronautical Station" Then
                .Frame("Frame").Link("Aeronautical Station").Click
                fn_LoadpageInWebCP
                '=====Station information
                .Frame("Frame_2").WebList("tbl_site__siteCity").Select(1)
                .Frame("Frame_2").WebEdit("tbl_site__remark").Set datatable.value("siteremark","Ad_SublicAero")
                If .Frame("Frame_2").WebList("tbl_site__stationPurpose").Exist(40) Then
                	.Frame("Frame_2").WebList("tbl_site__stationPurpose").Select datatable.value("ServiceClass","Ad_SublicAero")
                End If
                 If .Frame("Frame_2").WebList("tbl_site__stationClass").Exist Then
                 	.Frame("Frame_2").WebList("tbl_site__stationClass").Select datatable.value("StationClass","Ad_SublicAero")
                 End If
                fn_LoadpageInWebCP
                '====Save Station
                Wait 5
                .Frame("Frame_2").WebElement("img_0").Click
                fn_LoadpageInWebCP
                '====Call sign
                If .Frame("Frame_2").WebElement("img_GCStbl_site__siteCallSign").Exist(5) Then
                	.Frame("Frame_2").WebElement("img_GCStbl_site__siteCallSign").Click
                End If
                Wait 4
                '=======Save station aeronautical
                .Frame("Frame_2").WebElement("img_0").Click
                fn_LoadpageInWebCP
                
                '---Aircraft station
            ElseIf Datatable.Value("Stationtype","Ad_SublicAero")="Aircraft Station" Then 
                .Frame("Frame").Link("Aircraft Station").Click
                fn_LoadpageInWebCP
                If .Frame("Frame_2").WebEdit("tbl_site__siteInfo13").Exist(5) Then
                    .Frame("Frame_2").WebEdit("tbl_site__siteInfo13").Set Datatable.Value("AircraftRegmark","Ad_SublicAero")
                End If
                If .Frame("Frame_2").WebList("tbl_site__siteInfo1").Exist(5) Then
                    .Frame("Frame_2").WebList("tbl_site__siteInfo1").Select(1)' "Aérospatiale SN.601 Corvette"
                End If
                If .Frame("Frame_2").WebEdit("tbl_site__siteInfo12").Exist(5) Then
                    .Frame("Frame_2").WebEdit("tbl_site__siteInfo12").Set Datatable.Value("takeoffweight","Ad_SublicAero")
                End If
                If .Frame("Frame_2").WebEdit("tbl_site__remark").Exist(5) Then
                    .Frame("Frame_2").WebEdit("tbl_site__remark").Set "remark"
                End If
                If .Frame("Frame_2").WebList("tbl_site__stationPurpose").Exist(20) Then
                    .Frame("Frame_2").WebList("tbl_site__stationPurpose").Select Datatable.Value("ServiceClass","Ad_SublicAero")
                End If
                If .Frame("Frame_2").WebList("tbl_site__stationClass").Exist(5) Then
                    .Frame("Frame_2").WebList("tbl_site__stationClass").Select Datatable.Value("StationClass","Ad_SublicAero")
                End If
                fn_LoadpageInWebCP
                '===Save Station
                .Frame("Frame_2").WebElement("img_0").Click
                fn_LoadpageInWebCP
                'Clicking Call sign for aircrft station
                .Frame("Frame_2").WebElement("img_0").Click
                wait 8
                If .Frame("Frame_2").WebElement("img_GCStbl_site__siteCallSign").Exist(5) Then
                	.Frame("Frame_2").WebElement("img_GCStbl_site__siteCallSign").Click
                End If
                fn_LoadpageInWebCP
                '====Save station
                .Frame("Frame_2").WebElement("img_0").Click
                fn_LoadpageInWebCP
            End If
        End If
        
        
        If Datatable.value("FunctionType","Ad_SublicAero")="New"  Then
            '===Add receiver- aeronautical station
            If Datatable.Value("Equipmenttype","Ad_SublicAero")="Add Receiver" Then
                .Frame("Frame_2").WebElement("img_1").Click
                fn_LoadpageInWebCP
            ElseIf Datatable.Value("Equipmenttype","Ad_SublicAero")="Add Transceiver" Then
                .Frame("Frame_2").WebElement("img_2").Click
                fn_LoadpageInWebCP
                If .Frame("Frame_3").WebEdit("tbl_equipment__antennaPower").Exist(5) Then
                    .Frame("Frame_3").WebEdit("tbl_equipment__antennaPower").Set datatable.value("Outputdbpower","Ad_SublicAero")
                End If
                
            ElseIf Datatable.Value("Equipmenttype","Ad_SublicAero")="Add Transmitter" Then
                .Frame("Frame_2").WebElement("img_3").Click
                fn_LoadpageInWebCP
                .Frame("Frame_3").WebEdit("tbl_equipment__antennaPower").Set datatable.value("Outputdbpower","Ad_SublicAero")	
                
            ElseIf Datatable.Value("Equipmenttype","Ad_SublicAero")="Add Other" Then
                .Frame("Frame_2").WebElement("img_4").Click
                fn_LoadpageInWebCP
                .Frame("Frame_3").WebEdit("tbl_equipment__antennaPower").Set datatable.value("Outputdbpower","Ad_SublicAero")
                
            End If	
        End If	
        If .Frame("Frame_3").WebEdit("tbl_equipment__lowerFreqRange").Exist(5) Then
            .Frame("Frame_3").WebEdit("tbl_equipment__lowerFreqRange").Set datatable.value("Lowerfreq","Ad_SublicAero")
        End If
        If .Frame("Frame_3").WebEdit("tbl_equipment__upperFreqRange").Exist(5) Then
            .Frame("Frame_3").WebEdit("tbl_equipment__upperFreqRange").Set datatable.value("Upperfreq","Ad_SublicAero")
        End If
        If Browser("name:=Application").Page("title:=Application").WebList("html id:=tbl_equipment__ApprovalNum").Exist(1) Then
            With Browser("name:=Application").Page("title:=Application")
                If .WebList("html id:=tbl_equipment__ApprovalNum").Exist(40) Then
                    If Datatable.Value("EquipmentApprovalNum","Ad_SublicAero")="" Then
                        .WebList("html id:=tbl_equipment__ApprovalNum").Selects 1'"random"
                    Else 
                        .WebList("html id:=tbl_equipment__ApprovalNum").Selects 1'Datatable.Value("EquipmentApprovalNum ","Ad_SublicAero")
                    End If
                    For intC = 1 To 20 Step 1
                        If Instr(1,.WebList("html id:=tbl_equipment__equipMake","name:=tbl_equipment__equipMake").GetROProperty("selection"),"Select",1)>0 Then
                            Wait 3
                        Else
                            Exit For
                        End If
                    Next
                End If
            End With
        End If
        '===Enter the equipment details
        Browser("Admin Home Page").Sync
        fn_LoadpageInWebCP
        If .Frame("Frame_3").WebList("tbl_equipment__equipType").Exist(5) Then
            .Frame("Frame_3").WebList("tbl_equipment__equipType").Select(1)
        End If
        
        If .Frame("Frame_3").WebEdit("tbl_equipment__equipMake").Exist(5) Then
            .Frame("Frame_3").WebEdit("tbl_equipment__equipMake").Set datatable.value("make","Ad_SublicAero")&fnRandomNumber(4)
        End If
        If .Frame("Frame_3").WebEdit("tbl_equipment__modelNum").Exist(5) Then
            .Frame("Frame_3").WebEdit("tbl_equipment__modelNum").Set datatable.value("model","Ad_SublicAero")&fnRandomNumber(4)
        End If
        fn_LoadpageInWebCP
        If .Frame("Frame_3").WebList("tbl_equipment__EmissionClass").Exist(20) Then
            .Frame("Frame_3").WebList("tbl_equipment__EmissionClass").Highlight
            .Frame("Frame_3").WebList("tbl_equipment__EmissionClass").Select(1)
        End If
        If Browser("name:=.*Application.*").Page("title:=.*Application.*").WebElement("html id:=img_GCStbl_equipment__callSign").Exist(20) Then
            Browser("name:=.*Application.*").Page("title:=.*Application.*").WebElement("html id:=img_GCStbl_equipment__callSign").Click
        End If
        If Browser("name:=.*Application.*").Page("title:=.*Application.*").WebEdit("html id:=tbl_equipment__equipSerialNum").Exist(5) Then
            Browser("name:=.*Application.*").Page("title:=.*Application.*").WebEdit("html id:=tbl_equipment__equipSerialNum").Set RandomNumber(9999,999999)
        End If
        If Browser("name:=.*Application.*").Page("title:=.*Application.*").WebList("html id:=tbl_equipment__relateEquipType").Exist(5) Then
            Browser("name:=.*Application.*").Page("title:=.*Application.*").WebList("html id:=tbl_equipment__relateEquipType").Select 1
        End If
        
        .Frame("Frame_3").WebElement("img_0").Click
        fn_LoadpageInWebCP
        If .Frame("Frame_4").WebElement("Antenna").Exist(5) Then
            .Frame("Frame_4").WebElement("Antenna").Click
            fn_LoadpageInWebCP
        End If
        fn_LoadpageInWebCP
        Browser("name:=.*Application.*").Page("title:=.*Application.*").Sync
        If Browser("name:=.*Application.*").Page("title:=.*Application.*").WebList("html id:=tbl_antenna__ApprovalNum").Exist(5) Then
            With Browser("name:=.*Application.*").Page("title:=.*Application.*")
                If .WebList("html id:=tbl_antenna__ApprovalNum").Exist(40) Then
                    If Datatable.Value("AntennaApprovalNum","Ad_SublicAero")="" Then
                        .WebList("html id:=tbl_antenna__ApprovalNum").Selects 1'"random"
                    Else
                        .WebList("html id:=tbl_antenna__ApprovalNum").Select 1' Datatable.Value("AntennaApprovalNum","Ad_SublicAero")
                    End If
                    
                    For intC = 1 To 20 Step 1
                        If Instr(1,.WebList("html id:=tbl_antenna__equipMake","name:=tbl_antenna__equipMake").GetROProperty("selection"),"Select",1)>0 Then
                            wait 3
                        Else
                            Exit For
                            
                        End If
                    Next
                End If
            End With     
            fn_LoadpageInWebCP
        End If
        fn_LoadpageInWebCP
        'Save receiver Equipment
        
        Browser("Admin Home Page").Page("Application_3").Frame("Frame").WebElement("Equipment").Click @@ script infofile_;_ZIP::ssf127.xml_;_
        fn_LoadpageInWebCP
        If  Browser("Admin Home Page").Page("Application_3").Frame("Frame").WebList("tbl_equipment__relateEquipType").Exist(8) Then
        	If Browser("Admin Home Page").Page("Application_3").Frame("Frame").WebList("tbl_equipment__relateEquipType").GetROProperty("selected item index")=0 Then
	        	Browser("Admin Home Page").Page("Application_3").Frame("Frame").WebList("tbl_equipment__relateEquipType").Select "#1"
	        	fn_LoadpageInWebCP
            End If
        End If
        
		 If Browser("Admin Home Page").Page("Application_3").Frame("Frame").WebEdit("tbl_equipment__lowerFrequency").Exist(20) Then
		    Browser("Admin Home Page").Page("Application_3").Frame("Frame").WebEdit("tbl_equipment__lowerFrequency").Set RandomNumber(1,10)
		 End If
		If Browser("Admin Home Page").Page("Application_3").Frame("Frame").WebEdit("tbl_equipment__upperFrequency").Exist(20) Then
		    Browser("Admin Home Page").Page("Application_3").Frame("Frame").WebEdit("tbl_equipment__upperFrequency").Set RandomNumber(11,20)
		End If
		Wait 4
		If Browser("name:=.*Application.*").Page("title:=.*Application.*").WebEdit("html id:=.*equipMake","name:=.*equipMake").Exist Then
			Browser("name:=.*Application.*").Page("title:=.*Application.*").WebEdit("html id:=.*equipMake","name:=.*equipMake").Set GenerateRandomString(7)
		End If
 		If Browser("name:=.*Application.*").Page("title:=.*Application.*").WebEdit("name:=.*modelNum","html id:=.*modelNum").Exist Then
 			Browser("name:=.*Application.*").Page("title:=.*Application.*").WebEdit("name:=.*modelNum","html id:=.*modelNum").Set GenerateRandomString(7)
 		End If
        Browser("name:=.*Application.*").Page("title:=.*Application.*").WebElement("innertext:=.*Save.*","title:=.*save.*").Click
        Wait 5
        Browser("name:=.*Application.*").Page("title:=.*Application.*").Sync
        fn_LoadpageInWebCP 
    End If
    
End With

strTestCaseName = Environment.Value("TestName")
'Test results dsiplayed in Excel
If err.number = 0 Then
    blnFind = True
Else
    blnFind = False
End If

UpdateTestCaseStatusInExcel strTestCaseName, blnFind
'fn_SendStatusToExcelReport blnFind	



 @@ script infofile_;_ZIP::ssf129.xml_;_

