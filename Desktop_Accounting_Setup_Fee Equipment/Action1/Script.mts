﻿wait(2)
blnFind=True
err.number = 0

datatable.ImportSheet ProjectFolderPath &"\Desktop_Input_Data_Sheet\Input_Data_Sheet.xls","Setup_FeeEquipment","Setup_FeeEquipment"
n=datatable.GetSheet("Setup_FeeEquipment").GetRowCount

'For i = 1 To n Step 1
If n>0 Then
  datatable.SetCurrentRow(1)
  
  '=========Fee equipment Screen naviation
  With	VbWindow("frmMDI")
    
    fn_NavigateToScreen "Accounting;Setup;Fee Equipment"
    
    
    With .VbWindow("frmFeeEquipment")
      While Not .AcxTable("Equipment Fee").Exist(5)
      Wend
      
      If Datatable.value("Client","Setup_FeeEquipment")="JMC" Then
        .AcxTable("Equipment Fee").WinScrollBar("ScrollBar").NextPage 1
        .AcxTable("Equipment Fee").WinScrollBar("ScrollBar").Set 3056
        .AcxTable("Equipment Fee").WinScrollBar("ScrollBar").NextLine 565
        VbWindow("frmMDI").VbWindow("frmFeeEquipment").AcxTable("Equipment Fee").WinScrollBar("ScrollBar_2").NextLine 1
        For i = 1 To 8 Step 1
          .AcxTable("Equipment Fee").WinScrollBar("ScrollBar_2").NextLine 1
        Next
      ElseIf Datatable.value("Client","Setup_FeeEquipment")="BOT" Or Datatable.value("Client","Setup_FeeEquipment")="SWZ" Then
        .AcxTable("Equipment Fee").WinScrollBar("ScrollBar_2").NextPage 1
        .AcxTable("Equipment Fee").WinScrollBar("ScrollBar_2").Set 3056
        .AcxTable("Equipment Fee").WinScrollBar("ScrollBar_2").NextLine 565
        wait(6)
      End If
      On Error Resume Next
      If Datatable.value("Client","Setup_FeeEquipment")="JMC" Then
        
        .AcxTable("Equipment Fee").SetCellData 14,2,Datatable.value("Code","Setup_FeeEquipment") & GenerateRandomString(4) & RandomNumber(111,99999)
        .AcxTable("Equipment Fee").Type  micTab
        .AcxTable("Equipment Fee").SetCellData 14,3,Datatable.value("Name","Setup_FeeEquipment") & "" & RandomNumber(111,99999)
        .AcxTable("Equipment Fee").Type  micTab
        .AcxTable("Equipment Fee").SetCellData 14,4,Datatable.value("Category","Setup_FeeEquipment")'"tbl"
        .AcxTable("Equipment Fee").Type  micTab 
        .AcxTable("Equipment Fee").SetCellData 14,5,Datatable.value("Method","Setup_FeeEquipment")'"Fixed"
        .AcxTable("Equipment Fee").Type  micTab
        .AcxTable("Equipment Fee").SetCellData 14,6,Datatable.value("Fixed","Setup_FeeEquipment")'"345"
        .AcxTable("Equipment Fee").Type  micTab 
        .AcxTable("Equipment Fee").SetCellData 14,"Type",Datatable.value("Type","Setup_FeeEquipment")'"Other"
        .AcxTable("Equipment Fee").Type  micTab 
        .AcxTable("Equipment Fee").SetCellData 14,"Currency",Datatable.value("Currency","Setup_FeeEquipment")'"USD"
        .AcxTable("Equipment Fee").Type  micTab
        .AcxTable("Equipment Fee").SetCellData 14,"Foreign Fee",Datatable.value("ForeignFee","Setup_FeeEquipment")'"23.6"
        .AcxTable("Equipment Fee").Type  micTab
      ElseIf Datatable.value("Client","Setup_FeeEquipment")="BOT" Or Datatable.value("Client","Setup_FeeEquipment")="SWZ" Then
        wait(3)
        
     
        Set send=CreateObject("Wscript.Shell")
        'Window("ASMS").Window("Automated Spectrum Management").WinObject("SSDataWidgetsEdit").Click 33,8
        .AcxTable("Equipment Fee").SetCellData 14,2,Datatable.value("Code","Setup_FeeEquipment") & "" & RandomNumber(111,99999)
        
        send.SendKeys "{TAB}"
        '		Window("ASMS").Window("Automated Spectrum Management").WinObject("SSDataWidgetsEdit").Type Datatable.value("Name","Setup_FeeEquipment") & "" & fnRandomNumber(3)
        send.SendKeys Datatable.value("Name","Setup_FeeEquipment") & "" & RandomNumber(111,99999)
        send.SendKeys "{TAB}"
        '		Window("ASMS").Window("Automated Spectrum Management").WinObject("SSDataWidgetsEdit").Type Datatable.value("Category","Setup_FeeEquipment")
        send.SendKeys Datatable.value("Category","Setup_FeeEquipment")
        send.SendKeys "{TAB}"
        '		Window("ASMS").Window("Automated Spectrum Management").WinObject("SSDataWidgetsEdit").Type datatable.value("Method","Setup_FeeEquipment")'
        send.SendKeys datatable.value("Method","Setup_FeeEquipment")
        send.SendKeys "{TAB}"
        '		Window("ASMS").Window("Automated Spectrum Management").WinObject("SSDataWidgetsEdit").Type Datatable.value("Fixed","Setup_FeeEquipment")
        send.SendKeys Datatable.value("Fixed","Setup_FeeEquipment")
        send.SendKeys "{TAB}"
        send.SendKeys Datatable.value("Type","Setup_FeeEquipment")
        '		Window("ASMS").Window("Automated Spectrum Management").WinObject("SSDataWidgetsEdit").Type Datatable.value("Type","Setup_FeeEquipment")
        send.SendKeys "{TAB}"
        '		Window("ASMS").Window("Automated Spectrum Management").WinObject("SSDataWidgetsEdit").Type Datatable.value("Duration","Setup_FeeEquipment")'"60"
        If Datatable.value("Client","Setup_FeeEquipment")<>"SWZ" Then
          send.SendKeys Datatable.value("Duration","Setup_FeeEquipment")
        End If
        
      End If
    End With
  End With
  If Datatable.value("Client","Setup_FeeEquipment")="JMC" Then
    
    With	Window("ASMS").Window("Automated Spectrum Management")
      .Activate
      .WinObject("SSDataWidgetsEdit").Click 9,14
      .WinObject("SSDataWidgetsEdit").Type Datatable.value("Accounttype","Setup_FeeEquipment")'"I Q"
    End With
  End If
  Wait 8
  With	VbWindow("frmMDI")
    .VbWindow("frmFeeEquipment").VbButton("Save").Click
    With .Dialog("#32770")
      If .WinButton("OK").Exist Then
        .WinButton("OK").Click
      End If
    End With
    With .VbWindow("frmFeeEquipment")
      
      If .VbButton("Close_2").Exist Then
        .VbButton("Close_2").Click
      End If
    End With
  End With
  
  strTestCaseName = Environment.Value("TestName")
  If err.number = 0 Then
    blnFind = True
  Else
    blnFind = False
  End If
  
  UpdateTestCaseStatusInExcel strTestCaseName, blnFind
End If
A
