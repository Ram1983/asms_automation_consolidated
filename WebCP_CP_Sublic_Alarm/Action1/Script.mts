﻿
blnFind=True
err.number = 0


With Browser("Customer Home Page").Page("Application")
    
    'Import data from Input data sheet  from folder
    datatable.ImportSheet ProjectFolderPath&"\Input_Data_Sheet.xls","cpalarm","cpalarm"
    n=datatable.GetSheet("cpalarm").GetRowCount
    
    'For i = 1 To n Step 1
    If n>0 Then
        datatable.SetCurrentRow(1)
        If Strcomp(Datatable.Value("Edit","cpalarm"),"yes",1)<>0 Then
            With Browser("name:=.*Application.*").Page("title:=.*Application.*")
                '-----Base station
                If datatable.value("Stationtype","cpalarm")="Base Station"  Then
                    .Link("name:=Base Station").Clicks	
                    
                    '-----Adding Mobile station
                ElseIf datatable.value("Stationtype","cpalarm")="Mobile Station" Then	
                    .Link("name:=Mobile Station").Clicks
                    '----Portable station
                ElseIf datatable.value("Stationtype","cpalarm")="Portable Station" Then		
                    .Link("name:=Portable Station").Clicks
                    '------Reciver Station
                ElseIf datatable.value("Stationtype","cpalarm")="Receiver Station" Then		
                    .Link("name:=Receiver Station").Clicks
                    '					
                    '====Repeater Station
                ElseIf datatable.value("Stationtype","cpalarm")="Repeater Station" Then		
                    .Link("name:=Repeater Station").Clicks
                End If
            End With	
            fn_LoadpageInWebCP
        Else
            fn_EditSubLicenses "station",Datatable.Value("Stationtype","cpalarm"),"ALARM Radio"
        End If
        
        fn_LoadpageInWebCP	
        If Strcomp(Datatable.Value("Edit","cpalarm"),"yes",1)<>0 Then
            If .Frame("Frame_2").WebList("tbl_site__siteCity").Exist  Then
                If  .Frame("Frame_2").WebList("tbl_site__siteCity").GetROProperty("items count")>1 Then
                    .Frame("Frame_2").WebList("tbl_site__siteCity").Select(1)' "Aero Drome (Extension 127)"
                    .Sync
                    Wait 10
                End If
                
            End If
            If .Frame("Frame_2").WebEdit("tbl_site__siteInfo16").Exist(5) Then
                .Frame("Frame_2").WebEdit("tbl_site__siteInfo16").Set datatable.value("Contactperson","cpalarm")
            End If
            
            If .Frame("Frame_2").WebEdit("tbl_site__numOfSites").Exist(5) Then
                .Frame("Frame_2").WebEdit("tbl_site__numOfSites").Set datatable.value("numofsites","cpalarm")
            End If	
        End If
        
        If Browser("Application").Page("Application_2").Frame("Frame_2").WebEdit("tbl_site__nomRadius").Exist(5) And Strcomp(Datatable.Value("Edit","cpalarm"),"yes",1)<>0 Then
            Browser("Application").Page("Application_2").Frame("Frame_2").WebEdit("tbl_site__nomRadius").Set RandomNumber(1,10)
        ElseIf Browser("Application").Page("Application_2").Frame("Frame_2").WebEdit("tbl_site__nomRadius").Exist(5) Then
            
            Browser("Application").Page("Application_2").Frame("Frame_2").WebEdit("tbl_site__nomRadius").Set RandomNumber(10,20)
        End If
        
        Browser("Application").Page("Application").Frame("Frame").WebElement("img_1").Clicks	
        
        'Selection Equipment Adding
        
        If datatable.value("Stationtype","cpalarm")="Base Station" Or datatable.value("Stationtype","cpalarm")="Receiver Station" Or  datatable.value("Stationtype","cpalarm")="Repeater Station"   Then
            If Strcomp(Datatable.Value("Edit","cpalarm"),"yes",1)<>0 Then
                If datatable.value("Equipmenttype","cpalarm")="Add Receiver"  Then		
                    .Frame("Frame_2").WebElement("img_1").Clicks
                    
                    '=====Adding equipment-Base station -Transceiver
                ElseIf datatable.value("Equipmenttype","cpalarm")="Add Transceiver" Then 
                    .Frame("Frame_2").WebElement("img_2").Clicks
                    
                    '===========Adding Equipment-Base station -Transmitter	
                ElseIf datatable.value("Equipmenttype","cpalarm")="Add Transmitter" Then	
                    .Frame("Frame_2").WebElement("img_3").Clicks
                    
                End If
            End If
            
            fn_LoadpageInWebCP
            'Base station,reciver ,Repeater Station have same code 3 equipments
            
            If Strcomp(Datatable.Value("Edit","cpalarm"),"yes",1)<>0 Then
                
                With Browser("name:=Application").Page("title:=Application")
                    If .WebList("html id:=tbl_equipment__ApprovalNum").Exist(40) Then
                        If Datatable.Value("EquipmentApprovalNum","cpalarm")="" Then
                          .WebList("html id:=tbl_equipment__ApprovalNum").Selects 1'"random"
                        Else 
                            .WebList("html id:=tbl_equipment__ApprovalNum").Selects 1'Datatable.Value("EquipmentApprovalNum ","cpalarm")
                        End If
                        For intC = 1 To 20 Step 1
                            If Instr(1,.WebList("html id:=tbl_equipment__equipMake","name:=tbl_equipment__equipMake").GetROProperty("selection"),"Select",1)>0 Then
                                Wait 3
                            Else
                                Exit For
                            End If
                        Next
                    End If
                End With
                fn_LoadpageInWebCP
                .Frame("Frame_3").WebEdit("tbl_equipment__equipSerialNum").Set datatable.value("Equipserial","cpalarm")&fnRandomNumber(4)
                fn_LoadpageInWebCP
                If Not .Frame("Frame_3").WebList("tbl_antenna__ApprovalNum").GetROProperty("items count")>1 Then
                    Wait 8
                End If
                With Browser("name:=Application").Page("title:=Application")
                    If .WebList("html id:=tbl_antenna__ApprovalNum").Exist(40) Then
                        If Datatable.Value("AntennaApprovalNum","cpalarm")="" Then
                           .WebList("html id:=tbl_antenna__ApprovalNum").Selects 1'"random"
                        Else
                            .WebList("html id:=tbl_antenna__ApprovalNum").Select  1'Datatable.Value("AntennaApprovalNum","cpalarm")
                        End If
                        
                        For intC = 1 To 20 Step 1
                            If Instr(1,.WebList("html id:=tbl_antenna__equipMake","name:=tbl_antenna__equipMake").GetROProperty("selection"),"Select",1)>0 Then
                                wait 3
                            Else
                                Exit For
                                
                            End If
                        Next
                    End If
                End With     
                fn_LoadpageInWebCP
                .Frame("Frame_3").WebEdit("tbl_antenna__physicHt").Set datatable.value("height","cpalarm")
                .Frame("Frame_3").WebEdit("tbl_antenna__mainLobeAzi").Set datatable.value("mainlobeazi","cpalarm")
                .Frame("Frame_3").WebEdit("tbl_antenna__tiltAngle").Set datatable.value("Tiltangle","cpalarm")
                fn_LoadpageInWebCP
                If Lcase(Datatable.Value("FeederLoss","cpalarm"))="cable" And Strcomp(Datatable.Value("Edit","cpalarm"),"yes",1)<>0 Then
                    If Browser("name:=Application").Page("title:=Application").WebRadioGroup("html id:=tbl_antenna__By.*","name:=tbl_antenna__ByCableOrLineAttenuation").Exist(5) Then
                        Browser("name:=Application").Page("title:=Application").WebRadioGroup("html id:=tbl_antenna__By.*","name:=tbl_antenna__ByCableOrLineAttenuation").Select 1
                        .Frame("Frame_3").WebEdit("tbl_antenna__FeedLength").Set Datatable.Value("Feedlength","cpalarm")
                    ElseIf Browser("name:=Application").Page("title:=Application").WebRadioGroup("html id:=tbl_antenna__By.*","name:=tbl_antenna__ByCableOrLineAttenuation").Exist(5) Then
                        Browser("name:=Application").Page("title:=Application").WebRadioGroup("html id:=tbl_antenna__By.*","name:=tbl_antenna__ByCableOrLineAttenuation").Select 2            
                    End If 
                End If
                If .Frame("Frame_3").WebList("tbl_antenna__CableID").Exist Then
                	.Frame("Frame_3").WebList("tbl_antenna__CableID").Select(1)' "ANDREW - FSJ-50B"
                End If
                .Sync
            End If
            If Strcomp(Datatable.Value("Edit","cpalarm"),"yes",1)=0 Then
                fn_EditSubLicenses "",Datatable.Value("typeOfStation","cpalarm"),"ALARM Radio"
                If .Frame("Frame_3").WebEdit("tbl_antenna__LineAtten").Exist(5) Then
                    .Frame("Frame_3").WebEdit("tbl_antenna__LineAtten").Sets""
                End If
            Else
                If .Frame("Frame_3").WebEdit("tbl_antenna__LineAtten").Exist(5) Then
                    .Frame("Frame_3").WebEdit("tbl_antenna__LineAtten").Set datatable.value("Lineattten","cpalarm")	
                End If
                
            End If
            
            'Save -equipment- Reciver
           If  .Frame("Frame_3").WebElement("img_0").Exist Then
           	    .Frame("Frame_3").WebElement("img_0").Clicks
           End If			
            
        End If
        
        'mobile,portable- same reciver,tranceivr and diffrent for transmiitter
        
        If datatable.value("Stationtype","cpalarm")="Portable Station" Or  datatable.value("Stationtype","cpalarm")="Mobile Station" Then
            
            If Strcomp(Datatable.Value("Edit","cpalarm"),"yes",1)<>0 Then
                '=====Adding equipment-mobile station-Add reciver
                If datatable.value("Equipmenttype","cpalarm")="Add Receiver" Then				
                    .Frame("Frame_2").WebElement("img_1").Clicks
                    
                    '=====Adding equipment-mobile station-Add Transciever
                ElseIf datatable.value("Equipmenttype","cpalarm")="Add Transceiver" Then 
                    .Frame("Frame_2").WebElement("img_2").Clicks
                    
                    '=====Adding equipment-mobile station-Add TTransmitter
                ElseIf datatable.value("Equipmenttype","cpalarm")="Add Transmitter"  Then
                    Browser("Application").Page("Application").Frame("Frame").WebElement("img_0").Clicks	
                    
                End If
                fn_LoadpageInWebCP
            End If	
            If datatable.value("Equipmenttype","cpalarm")="Add Receiver" Or datatable.value("Equipmenttype","cpalarm")="Add Transceiver"  Then
                If Strcomp(Datatable.Value("Edit","cpalarm"),"yes",1)<>0 Then
                    With Browser("name:=Application").Page("title:=Application")
                        If .WebList("html id:=tbl_equipment__ApprovalNum").Exist(40) Then
                            If Datatable.Value("EquipmentApprovalNum","cpalarm")="" Then
                              .WebList("html id:=tbl_equipment__ApprovalNum").Selects 1'"random"
                            Else 
                                .WebList("html id:=tbl_equipment__ApprovalNum").Selects 1'Datatable.Value("EquipmentApprovalNum ","cpalarm")
                            End If
                            For intC = 1 To 20 Step 1
                                If Instr(1,.WebList("html id:=tbl_equipment__equipMake","name:=tbl_equipment__equipMake").GetROProperty("selection"),"Select",1)>0 Then
                                    Wait 3
                                Else
                                    Exit For
                                End If
                            Next
                        End If
                    End With
                    fn_LoadpageInWebCP
                    .Frame("Frame_3").WebEdit("tbl_equipment__equipSerialNum").Set datatable.value("Equipserial","cpalarm")&fnRandomNumber(4)
                    fn_LoadpageInWebCP
                    If datatable.value("Stationtype","cpalarm")="Mobile Station" Then						
                        .Frame("Frame_3").WebEdit("tbl_equipment__regno").Set datatable.value("VehicleReg","cpalarm")&fnRandomNumber(4)
                        fn_LoadpageInWebCP
                    End If
                End If
                
            ElseIf datatable.value("Equipmenttype","cpalarm")="Add Transmitter" Then
                If Strcomp(Datatable.Value("Edit","cpalarm"),"yes",1)<>0 Then	 			
                    Browser("Application").Page("Application").Frame("Frame_2").WebEdit("tbl_equipment__equipSerialNum").Set datatable.value("Equipserial","cpalarm")&fnRandomNumber(4)
                    With Browser("name:=Application").Page("title:=Application")
                        If .WebList("html id:=tbl_equipment__ApprovalNum").Exist(40) Then
                            If Datatable.Value("EquipmentApprovalNum","cpalarm")="" Then
                           	  .WebList("html id:=tbl_equipment__ApprovalNum").Selects 1'"random"
                            Else 
                                .WebList("html id:=tbl_equipment__ApprovalNum").Selects 1'Datatable.Value("EquipmentApprovalNum ","cpalarm")
                            End If
                            For intC = 1 To 20 Step 1
                                If Instr(1,.WebList("html id:=tbl_equipment__equipMake","name:=tbl_equipment__equipMake").GetROProperty("selection"),"Select",1)>0 Then
                                    Wait 3
                                Else
                                    Exit For
                                End If
                            Next
                        End If
                    End With
                End If
                
            End If
            If Strcomp(Datatable.Value("Edit","cpalarm"),"yes",1)=0 Then
                fn_EditSubLicenses "",Datatable.Value("Stationtype","cpalarm"),"ALARM"
                fn_LoadpageInWebCP	
            End If
            With Browser("name:=Application").Page("title:=Application")
                If .WebList("html id:=tbl_antenna__ApprovalNum").Exist(40) Then
                    If Datatable.Value("AntennaApprovalNum","cpalarm")="" Then
                      .WebList("html id:=tbl_equipment__ApprovalNum").Selects 1'"random"
                    Else
                        .WebList("html id:=tbl_antenna__ApprovalNum").Select  1'Datatable.Value("AntennaApprovalNum","cpalarm")
                    End If
                    
                    For intC = 1 To 20 Step 1
                        If Instr(1,.WebList("html id:=tbl_antenna__equipMake","name:=tbl_antenna__equipMake").GetROProperty("selection"),"Select",1)>0 Then
                            wait 3
                        Else
                            Exit For
                            
                        End If
                    Next
                End If
            End With     
            fn_LoadpageInWebCP
            
            'Save Equipment
            If .Frame("Frame_3").WebElement("img_0").Exist(5) Then
                .Frame("Frame_3").WebElement("img_0").Clicks
            End If
        End If
    End If	
End With

strTestCaseName = Environment.Value("TestName")
'Test results dsiplayed in Excel
If err.number = 0 Then
    blnFind = True
Else
    blnFind = False
End If

UpdateTestCaseStatusInExcel strTestCaseName, blnFind
'fn_SendStatusToExcelReport blnFind












