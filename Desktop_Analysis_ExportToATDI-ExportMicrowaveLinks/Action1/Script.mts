﻿
 wait(2)
blnFind=True
err.number = 0

datatable.ImportSheet ProjectFolderPath &"\Desktop_Input_Data_Sheet\Input_Data_Sheet.xls","ExportMWlinks","ExportMWlinks"
n=datatable.GetSheet("ExportMWlinks").GetRowCount

'For i = 1 To n Step 1
With VbWindow("frmMDI")
	If n>0 Then
		datatable.SetCurrentRow(1)

'		.WinMenu("Menu").Select "Analysis;Export to ATDI;Export Microwave Links"

		With .VbWindow("frmMwLinks")
			While not .Exist
			Wend
			.ActiveX("TCIComboBox.tcb").VbComboBox("cboLicenseNo").Select datatable.Value("LicenseNo","ExportMWlinks") @@ hightlight id_;_1706364_;_script infofile_;_ZIP::ssf127.xml_;_
			.ActiveX("TCIComboBox.tcb").WinEdit("Edit").Type  micTab @@ hightlight id_;_1314174_;_script infofile_;_ZIP::ssf128.xml_;_
			.ActiveX("MaskEdBox").Drag 18,5 @@ hightlight id_;_265094_;_script infofile_;_ZIP::ssf77.xml_;_
			.VbFrame("Frame1").Drop 728,31 @@ hightlight id_;_265206_;_script infofile_;_ZIP::ssf78.xml_;_
			.ActiveX("MaskEdBox").Type datatable.Value("Lat1","ExportMWlinks") @@ hightlight id_;_265094_;_script infofile_;_ZIP::ssf79.xml_;_
			.ActiveX("MaskEdBox").Type  micTab @@ hightlight id_;_265094_;_script infofile_;_ZIP::ssf80.xml_;_

			.ActiveX("MaskEdBox_2").Drag 20,10 @@ hightlight id_;_265092_;_script infofile_;_ZIP::ssf81.xml_;_
			.VbFrame("Frame1").Drop 787,32 @@ hightlight id_;_265206_;_script infofile_;_ZIP::ssf82.xml_;_
			.ActiveX("MaskEdBox_2").Type datatable.Value("Lat2","ExportMWlinks") @@ hightlight id_;_265092_;_script infofile_;_ZIP::ssf83.xml_;_
			.ActiveX("MaskEdBox_2").Type  micTab @@ hightlight id_;_265092_;_script infofile_;_ZIP::ssf84.xml_;_

			.ActiveX("MaskEdBox_3").Drag 16,5 @@ hightlight id_;_656632_;_script infofile_;_ZIP::ssf85.xml_;_
'			.VbFrame("Frame1").Drop 850,30
			.ActiveX("MaskEdBox_3").Type datatable.Value("Lat3","ExportMWlinks") @@ hightlight id_;_656632_;_script infofile_;_ZIP::ssf87.xml_;_
			.ActiveX("MaskEdBox_3").Type  micTab @@ hightlight id_;_656632_;_script infofile_;_ZIP::ssf88.xml_;_

			.ActiveX("TCIComboBox.tcb_4").VbComboBox("cboLatHem").Select datatable.Value("LatHem","ExportMWlinks") @@ hightlight id_;_265098_;_script infofile_;_ZIP::ssf89.xml_;_
			.ActiveX("TCIComboBox.tcb_4").VbComboBox("cboLatHem").Type  micTab @@ hightlight id_;_265098_;_script infofile_;_ZIP::ssf90.xml_;_

			.ActiveX("MaskEdBox_4").Drag 15,4 @@ hightlight id_;_1180990_;_script infofile_;_ZIP::ssf91.xml_;_
			.VbFrame("Frame1").Drop 727,60 @@ hightlight id_;_265206_;_script infofile_;_ZIP::ssf92.xml_;_
			.ActiveX("MaskEdBox_4").Type datatable.Value("Long1","ExportMWlinks") @@ hightlight id_;_1180990_;_script infofile_;_ZIP::ssf93.xml_;_
			.ActiveX("MaskEdBox_4").Type  micTab @@ hightlight id_;_1180990_;_script infofile_;_ZIP::ssf94.xml_;_

			.ActiveX("MaskEdBox_5").Drag 19,6 @@ hightlight id_;_1443828_;_script infofile_;_ZIP::ssf95.xml_;_
			.VbFrame("Frame1").Drop 792,60 @@ hightlight id_;_265206_;_script infofile_;_ZIP::ssf96.xml_;_
			.ActiveX("MaskEdBox_5").Type datatable.Value("Long2","ExportMWlinks") @@ hightlight id_;_1443828_;_script infofile_;_ZIP::ssf97.xml_;_
			.ActiveX("MaskEdBox_5").Type  micTab @@ hightlight id_;_1443828_;_script infofile_;_ZIP::ssf98.xml_;_

			.ActiveX("MaskEdBox_6").Drag 12,2 @@ hightlight id_;_395556_;_script infofile_;_ZIP::ssf99.xml_;_
'			.VbFrame("Frame1").Drop 852,57
			.ActiveX("MaskEdBox_6").Type datatable.Value("Long3","ExportMWlinks") @@ hightlight id_;_395556_;_script infofile_;_ZIP::ssf101.xml_;_
			.ActiveX("MaskEdBox_6").Type  micTab @@ hightlight id_;_395556_;_script infofile_;_ZIP::ssf102.xml_;_

			.ActiveX("TCIComboBox.tcb_5").VbComboBox("cboLongHem").Select datatable.Value("LongHem","ExportMWlinks") @@ hightlight id_;_396098_;_script infofile_;_ZIP::ssf103.xml_;_
			.ActiveX("TCIComboBox.tcb_5").VbComboBox("cboLongHem").Type  micTab @@ hightlight id_;_396098_;_script infofile_;_ZIP::ssf104.xml_;_

			.ActiveX("MaskEdBox_7").Drag 51,9 @@ hightlight id_;_460836_;_script infofile_;_ZIP::ssf105.xml_;_
			.VbFrame("Frame1").Drop 718,92 @@ hightlight id_;_265206_;_script infofile_;_ZIP::ssf106.xml_;_
			.ActiveX("MaskEdBox_7").Type datatable.Value("Radius","ExportMWlinks") @@ hightlight id_;_460836_;_script infofile_;_ZIP::ssf107.xml_;_
			.ActiveX("MaskEdBox_7").Type  micTab @@ hightlight id_;_460836_;_script infofile_;_ZIP::ssf108.xml_;_

			.ActiveX("MaskEdBox_8").Drag 44,13 @@ hightlight id_;_1443682_;_script infofile_;_ZIP::ssf109.xml_;_
			.VbFrame("Frame1").Drop 718,124 @@ hightlight id_;_265206_;_script infofile_;_ZIP::ssf110.xml_;_
			.ActiveX("MaskEdBox_8").Type datatable.Value("Frequency","ExportMWlinks") @@ hightlight id_;_1443682_;_script infofile_;_ZIP::ssf111.xml_;_
			.ActiveX("MaskEdBox_8").Type  micTab @@ hightlight id_;_1443682_;_script infofile_;_ZIP::ssf112.xml_;_

			.ActiveX("MaskEdBox_9").Drag 28,6 @@ hightlight id_;_395562_;_script infofile_;_ZIP::ssf113.xml_;_
			.VbFrame("Frame1").Drop 727,151 @@ hightlight id_;_265206_;_script infofile_;_ZIP::ssf114.xml_;_
			.ActiveX("MaskEdBox_9").Type datatable.Value("FreqSeparate","ExportMWlinks") @@ hightlight id_;_395562_;_script infofile_;_ZIP::ssf115.xml_;_
			
			.ActiveX("MaskEdBox_3").Click 17,9 @@ hightlight id_;_526164_;_script infofile_;_ZIP::ssf132.xml_;_
			.ActiveX("MaskEdBox_3").Type RandomNumber(1,100) @@ hightlight id_;_526164_;_script infofile_;_ZIP::ssf133.xml_;_
			.ActiveX("MaskEdBox_6").Click 16,12 @@ hightlight id_;_133054_;_script infofile_;_ZIP::ssf134.xml_;_
			.ActiveX("MaskEdBox_6").Type  RandomNumber(1,100) @@ hightlight id_;_133054_;_script infofile_;_ZIP::ssf135.xml_;_
			.ActiveX("MaskEdBox_7").Click 48,5 @@ hightlight id_;_133072_;_script infofile_;_ZIP::ssf136.xml_;_
			wait(1)
			.VbButton("Find").Click @@ hightlight id_;_395468_;_script infofile_;_ZIP::ssf116.xml_;_
		End With
		wait(5)
		If .VbWindow("frmMwLinks").AcxTable("Select Microwave links").RowCount>0 Then
			wait(3)
			.InsightObject("InsightObject_4").Click
			wait(2)

			.VbWindow("frmMwLinks").VbButton("Export to ATDI").Click
			wait(2)
			With	.Dialog("#32770")
				.WinButton("No").Click @@ hightlight id_;_592900_;_script infofile_;_ZIP::ssf124.xml_;_
				wait(2)
				.WinButton("OK").Click
			End With

			else
				Print "microwave Links are not available for selected criteria"
		End If
		wait(2)

		.VbWindow("frmMwLinks").VbButton("Close").Click @@ hightlight id_;_722432_;_script infofile_;_ZIP::ssf126.xml_;_

		strTestCaseName = Environment.Value("TestName")
		If err.number = 0 Then
			blnFind = True
			Else
				blnFind = False
		End If

		UpdateTestCaseStatusInExcel strTestCaseName, blnFind
	End If
End With
