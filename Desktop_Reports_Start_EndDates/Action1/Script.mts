﻿blnFind=True
err.number = 0
datatable.ImportSheet ProjectFolderPath &"\Desktop_Input_Data_Sheet\Input_Data_Sheet.xls","Reports_StartEndDates","Reports_StartEndDates"
n=datatable.GetSheet("Reports_StartEndDates").GetRowCount()

'For i = 1 To n Step 1
If n>0 Then
	datatable.SetCurrentRow(1)	
	'VbWindow("frmMDI").InsightObject("InsightObject").Click
	'VbWindow("frmMDI").InsightObject("InsightObject_2").Click
	fn_NavigateToScreen "Report;Custom Reports"
	
	wait(7)
	
	While Not VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").Exist
	Wend
	'Report Tabs
	'VbWindow("frmMDI").InsightObject("InsightObject_3").Click
	'VbWindow("frmMDI").InsightObject("InsightObject_4").Click
	'VbWindow("frmMDI").InsightObject("InsightObject_5").Click
	'VbWindow("frmMDI").InsightObject("InsightObject_6").Click
	'VbWindow("frmMDI").InsightObject("InsightObject_7").Click
	'VbWindow("frmMDI").InsightObject("InsightObject_8").Click
	'VbWindow("frmMDI").InsightObject("InsightObject_9").Click
	
	For Iterator = 1 To datatable.GetSheet("Reports_StartEndDates").GetRowCount
		AssignInputs="No"
		DataTable.LocalSheet.SetCurrentRow(Iterator)
	
	If Iterator = 1 Then 
	Wait(2)
		StartMonth=Datatable.Value("StartMonth","Reports_StartEndDates")
		StartYear=Datatable.Value("StartYear","Reports_StartEndDates")
		EndMonth=Datatable.Value("EndMonth","Reports_StartEndDates")
		EndYear=Datatable.Value("EndYear","Reports_StartEndDates")
		
	End If
	
	If Datatable.Value("ReportTab","Reports_StartEndDates")="" and Datatable.Value("ReportName","Reports_StartEndDates")="" and Datatable.Value("RunReport","Reports_StartEndDates")="" Then
		Exit for
	End If
	
	If Datatable.Value("RunReport","Reports_StartEndDates")="Yes" Then
	wait(3)
	
			With VbWindow("frmMDI")
				If Iterator > 1 and SelectedReport="Yes" and .WinObject("MDIClient").Exist(10) Then
					'VbWindow("frmMDI").WinObject("MDIClient").VScroll micSetPos, -7
				End If

				SelectedReport="No" @@ hightlight id_;_922138_;_script infofile_;_ZIP::ssf58.xml_;_
				With	.VbWindow("frmRptMonthlyBilling")
					If Datatable.Value("ReportTab","Reports_StartEndDates")="Application Analysis" and Datatable.Value("RunReport","Reports_StartEndDates")="Yes" Then
					.Activate
					wait(3)
					VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("SSTab").VirtualObject("ApplicationAnalysis").Click 0,0
					wait(3)
'						If VbWindow("frmMDI").InsightObject("InsightObject_4").Exist(10) Then
'							VbWindow("frmMDI").InsightObject("InsightObject_4").Click	
'							ElseIf VbWindow("frmMDI").InsightObject("InsightObject_7").Exist(10) Then
'								VbWindow("frmMDI").InsightObject("InsightObject_7").Click
'							ElseIf VbWindow("frmMDI").InsightObject("InsightObject_15").Exist(10) Then
'								VbWindow("frmMDI").InsightObject("InsightObject_15").Click
'						End If

						If Datatable.Value("ReportName","Reports_StartEndDates")="Number of applications received per Service Class per month over a defined period of time" and Datatable.Value("RunReport","Reports_StartEndDates")="Yes" Then
							.ActiveX("SSTab").VbRadioButton("Number of applications").Set @@ hightlight id_;_1124414_;_script infofile_;_ZIP::ssf91.xml_;_
							ElseIf Datatable.Value("ReportName","Reports_StartEndDates")="Number of applications received per Service Class per Island over a defined period of time" and  Datatable.Value("RunReport","Reports_StartEndDates")="Yes" Then
								.ActiveX("SSTab").VbRadioButton("Number of applications_2").Set
							ElseIf Datatable.Value("ReportName","Reports_StartEndDates")="Statistical report on applications over a defined period of time" and  Datatable.Value("RunReport","Reports_StartEndDates")="Yes" Then
								.ActiveX("SSTab").VbRadioButton("Statistical report on").Set
						End If
						SelectedReport="Yes"

					ElseIf Datatable.Value("ReportTab","Reports_StartEndDates")="Licence Analysis" and Datatable.Value("RunReport","Reports_StartEndDates")="Yes" Then
						.Activate
						wait(3)
						VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("SSTab").VirtualObject("LicenseAnalysis").Click 0,0
						wait(3)
'							If VbWindow("frmMDI").InsightObject("InsightObject_5").Exist(10) Then
'								VbWindow("frmMDI").InsightObject("InsightObject_5").Click
'								ElseIf VbWindow("frmMDI").InsightObject("InsightObject_14").Exist(10) Then @@ hightlight id_;_3_;_script infofile_;_ZIP::ssf191.xml_;_
'									VbWindow("frmMDI").InsightObject("InsightObject_14").Click @@ hightlight id_;_2_;_script infofile_;_ZIP::ssf195.xml_;_
'								ElseIf VbWindow("frmMDI").InsightObject("InsightObject_16").Exist(10) Then
'									VbWindow("frmMDI").InsightObject("InsightObject_16").Click
'							End If

							If Datatable.Value("ReportName","Reports_StartEndDates")="Number of licenses issued per month by type over a defined period of time" and Datatable.Value("RunReport","Reports_StartEndDates")="Yes" Then
								.ActiveX("SSTab").VbRadioButton("Number of licences issued").Set	
							ElseIf Datatable.Value("ReportName","Reports_StartEndDates")="Number of licenses issued per month by service class over a defined period of time" and  Datatable.Value("RunReport","Reports_StartEndDates")="Yes" Then
								.ActiveX("SSTab").VbRadioButton("Number of licences issued_2").Set	
							ElseIf Datatable.Value("ReportName","Reports_StartEndDates")="Number of licenses issued per month by service nature over a defined period of time" and  Datatable.Value("RunReport","Reports_StartEndDates")="Yes" Then
								.ActiveX("SSTab").VbRadioButton("Number of licences issued_3").Set	
							ElseIf Datatable.Value("ReportName","Reports_StartEndDates")="Licenses renewed over a defined period of time" and  Datatable.Value("RunReport","Reports_StartEndDates")="Yes" Then
								.ActiveX("SSTab").VbRadioButton("Licences renewed over").Set	
							ElseIf Datatable.Value("ReportName","Reports_StartEndDates")="Licenses cancelled over a defined period of time" and  Datatable.Value("RunReport","Reports_StartEndDates")="Yes" Then
								.ActiveX("SSTab").VbRadioButton("Licences cancelled over").Set	
							ElseIf Datatable.Value("ReportName","Reports_StartEndDates")="Statistical report on sub-licenses over a defined period of time" and  Datatable.Value("RunReport","Reports_StartEndDates")="Yes" Then
									.ActiveX("SSTab").VbRadioButton("Statistical report on_2").Set	
							End If
							SelectedReport="Yes"

						ElseIf Datatable.Value("ReportTab","Reports_StartEndDates")="Complaints" and Datatable.Value("RunReport","Reports_StartEndDates")="Yes" Then
							.Activate
							wait(3)
							VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("SSTab").VirtualObject("Complaints").Click 0,0
							wait(3)

'							If VbWindow("frmMDI").InsightObject("InsightObject_10").Exist(10) Then
'								VbWindow("frmMDI").InsightObject("InsightObject_10").Click
'								ElseIf VbWindow("frmMDI").InsightObject("InsightObject_12").Exist(10) Then @@ hightlight id_;_407896_;_script infofile_;_ZIP::ssf192.xml_;_
'									VbWindow("frmMDI").InsightObject("InsightObject_12").Click @@ hightlight id_;_2_;_script infofile_;_ZIP::ssf193.xml_;_
'								ElseIf VbWindow("frmMDI").InsightObject("InsightObject_17").Exist(10) Then
'									VbWindow("frmMDI").InsightObject("InsightObject_17").Click
'							End If

							If Datatable.Value("ReportName","Reports_StartEndDates")="Number of Interface complaints received per month over a defined period of time" and  Datatable.Value("RunReport","Reports_StartEndDates")="Yes" Then
								.ActiveX("SSTab").VbRadioButton("Number of interference").Set	
								ElseIf Datatable.Value("ReportName","Reports_StartEndDates")="Number of unresolved Interface complaints per month over a defined period of time" and  Datatable.Value("RunReport","Reports_StartEndDates")="Yes" Then
									.ActiveX("SSTab").VbRadioButton("Number of unresolved interfere").Set
								ElseIf Datatable.Value("ReportName","Reports_StartEndDates")="Statistical report on complaints over a defined period of time" and  Datatable.Value("RunReport","Reports_StartEndDates")="Yes" Then
									.ActiveX("SSTab").VbRadioButton("Statistical report on_3").Set
							End If
							SelectedReport="Yes"
						ElseIf Datatable.Value("ReportTab","Reports_StartEndDates")="Financial 2" and Datatable.Value("RunReport","Reports_StartEndDates")="Yes" Then
							.Activate
							wait(3)
							VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").ActiveX("SSTab").VirtualObject("Financial2").Click 0,0
							wait(3)
'							If VbWindow("frmMDI").InsightObject("InsightObject_8").Exist(10) Then
'								VbWindow("frmMDI").InsightObject("InsightObject_8").Click
'								ElseIf VbWindow("frmMDI").InsightObject("InsightObject_13").Exist(10) Then
'									VbWindow("frmMDI").InsightObject("InsightObject_13").Click
'								ElseIf VbWindow("frmMDI").InsightObject("InsightObject_18").Exist(10) Then
'									VbWindow("frmMDI").InsightObject("InsightObject_18").Click
'							End If

							If Datatable.Value("ReportName","Reports_StartEndDates")="Number of notices with outstanding payments per month over a defined period of time" and  Datatable.Value("RunReport","Reports_StartEndDates")="Yes" Then
								.ActiveX("SSTab").VbRadioButton("Number of notices with").Set @@ hightlight id_;_854252_;_script infofile_;_ZIP::ssf172.xml_;_
							End If
							SelectedReport="Yes"
					End If

					If SelectedReport="Yes" and AssignInputs="No" Then
						Wait(3)
						.ActiveX("TCIComboBox.tcb").VbComboBox("cboMonth(0)").Select StartMonth

						.ActiveX("MaskEdBox").Click 41,7
						.ActiveX("MaskEdBox").Drag 41,7 @@ hightlight id_;_2494166_;_script infofile_;_ZIP::ssf181.xml_;_
						.VbFrame("panOption(15)").Drop 491,30 @@ hightlight id_;_921176_;_script infofile_;_ZIP::ssf182.xml_;_
						.ActiveX("MaskEdBox").Type StartYear @@ hightlight id_;_2494166_;_script infofile_;_ZIP::ssf183.xml_;_
						.ActiveX("MaskEdBox").Type  micTab @@ hightlight id_;_2494166_;_script infofile_;_ZIP::ssf184.xml_;_
						Wait(3)
						.ActiveX("TCIComboBox.tcb_2").VbComboBox("cboMonth(1)").Select EndMonth

						.ActiveX("MaskEdBox_2").Click 41,6 @@ hightlight id_;_986840_;_script infofile_;_ZIP::ssf185.xml_;_
						.ActiveX("MaskEdBox_2").Drag 41,6 @@ hightlight id_;_986840_;_script infofile_;_ZIP::ssf186.xml_;_
						.VbFrame("panOption(15)").Drop 495,73 @@ hightlight id_;_921176_;_script infofile_;_ZIP::ssf187.xml_;_
						.ActiveX("MaskEdBox_2").Type EndYear @@ hightlight id_;_2494166_;_script infofile_;_ZIP::ssf183.xml_;_
						.ActiveX("MaskEdBox_2").Type  micTab @@ hightlight id_;_731046_;_script infofile_;_ZIP::ssf114.xml_;_

						AssignInputs="Yes"

					End If
				End With
			End With

	wait(2)
		
	If SelectedReport="Yes" Then
	
				With VbWindow("frmMDI")
					If .WinObject("MDIClient").Exist(10) Then
						'			VbWindow("frmMDI").WinObject("MDIClient").VScroll micSetPos, 10
					End If


					.VbWindow("frmRptMonthlyBilling").VbButton("View").Click @@ hightlight id_;_69160_;_script infofile_;_ZIP::ssf7.xml_;_
				End With

		Set fso = CreateObject("Scripting.FileSystemObject")
		
		file_location = ProjectFolderPath &"\Desktop_Reports\"& GetCurrentDate
		
		If not fso.FolderExists(file_location) Then
			fso.CreateFolder file_location  
		End If
	
		wait(5)
		
		If Datatable.Value("ReportName","Reports_StartEndDates")="Statistical report on complaints over a defined period of time" and  Datatable.Value("RunReport","Reports_StartEndDates")="Yes" Then
			wait(2)
			Window("Internet Explorer").WinToolbar("Command Bar").Press "P&rint"
		Else	
						With VbWindow("frmReportViewer")
							While Not .Exist
							Wend
							wait(10)
							.VbButton("Print").Click
							wait(5)
							While not .Dialog("Print").WinButton("OK").Exist
								If .Dialog("#32770").WinButton("OK").Exist Then
									.Dialog("#32770").WinButton("OK").Click
									wait(15)
									.VbButton("Print").Click
									wait(5)
								End If
							Wend
							.Dialog("Print").WinButton("OK").Click
						End With
				End If 
		
'		If Datatable.Value("ReportName","Reports_StartEndDates")="Number of applications received per Service Class per month over a defined period of time" and Datatable.Value("RunReport","Reports_StartEndDates")="Yes" Then
'			While Not VbWindow("frmReportViewer").Exist
'			Wend
'			wait(10)
'			VbWindow("frmReportViewer").VbButton("Print").Click
'			VbWindow("frmReportViewer").Dialog("Print").WinButton("OK").Click
'	
'		ElseIf Datatable.Value("ReportName","Reports_StartEndDates")="Number of applications received per Service Class per Island over a defined period of time" and  Datatable.Value("RunReport","Reports_StartEndDates")="Yes" Then
'			While Not VbWindow("frmReportViewer_2").Exist
'			Wend
'			wait(2)
'			VbWindow("frmReportViewer_2").VbButton("Print").Click
'			VbWindow("frmReportViewer_2").Dialog("Print").WinButton("OK").Click
'	
'		ElseIf Datatable.Value("ReportName","Reports_StartEndDates")="Statistical report on applications over a defined period of time" and  Datatable.Value("RunReport","Reports_StartEndDates")="Yes" Then
'			While Not VbWindow("frmReportViewer_3").Exist
'			Wend
'			wait(20)
'			If VbWindow("frmReportViewer_3").Dialog("#32770").WinButton("OK").Exist(10) Then
'				VbWindow("frmReportViewer_3").Dialog("#32770").WinButton("OK").Click
'			End If		
'	
'			VbWindow("frmReportViewer_3").VbButton("Print").Click
'			VbWindow("frmReportViewer_3").Dialog("Print").WinButton("OK").Click
'		ElseIf Datatable.Value("ReportName","Reports_StartEndDates")="Number of licenses issued per month by type over a defined period of time" and Datatable.Value("RunReport","Reports_StartEndDates")="Yes" Then
'			While Not VbWindow("frmReportViewer_4").Exist
'			Wend
'			wait(6)		
'			VbWindow("frmReportViewer_4").VbButton("Print").Click
'			VbWindow("frmReportViewer_4").Dialog("Print").WinButton("OK").Click
'		ElseIf Datatable.Value("ReportName","Reports_StartEndDates")="Number of licenses issued per month by service class over a defined period of time" and  Datatable.Value("RunReport","Reports_StartEndDates")="Yes" Then
'			While Not VbWindow("frmReportViewer_5").Exist
'			Wend
'			wait(6)		
'			VbWindow("frmReportViewer_5").VbButton("Print").Click
'			VbWindow("frmReportViewer_5").Dialog("Print").WinButton("OK").Click
'		ElseIf Datatable.Value("ReportName","Reports_StartEndDates")="Number of licenses issued per month by service nature over a defined period of time" and  Datatable.Value("RunReport","Reports_StartEndDates")="Yes" Then
'			While Not VbWindow("frmReportViewer_6").Exist
'			Wend
'			wait(2)		
'			VbWindow("frmReportViewer_6").VbButton("Print").Click
'			VbWindow("frmReportViewer_6").Dialog("Print").WinButton("OK").Click
'		ElseIf Datatable.Value("ReportName","Reports_StartEndDates")="Licenses renewed over a defined period of time" and  Datatable.Value("RunReport","Reports_StartEndDates")="Yes" Then
'			While Not VbWindow("frmReportViewer_7").Exist
'			Wend
'			wait(2)		
'			VbWindow("frmReportViewer_7").VbButton("Print").Click
'			VbWindow("frmReportViewer_7").Dialog("Print").WinButton("OK").Click
'		ElseIf Datatable.Value("ReportName","Reports_StartEndDates")="Licenses cancelled over a defined period of time" and  Datatable.Value("RunReport","Reports_StartEndDates")="Yes" Then
'			While Not VbWindow("frmReportViewer_8").Exist
'			Wend
'			wait(2)		
'			
'			VbWindow("frmReportViewer_8").VbButton("Print").Click
'			VbWindow("frmReportViewer_8").Dialog("Print").WinButton("OK").Click
'		ElseIf Datatable.Value("ReportName","Reports_StartEndDates")="Statistical report on sub-licenses over a defined period of time" and  Datatable.Value("RunReport","Reports_StartEndDates")="Yes" Then
'			While Not VbWindow("frmReportViewer_9").Exist
'			Wend
'			wait(15)		
'			VbWindow("frmReportViewer_9").VbButton("Print").Click
'			VbWindow("frmReportViewer_9").Dialog("Print").WinButton("OK").Click
'		ElseIf Datatable.Value("ReportName","Reports_StartEndDates")="Number of Interface complaints received per month over a defined period of time" and  Datatable.Value("RunReport","Reports_StartEndDates")="Yes" Then
'			While Not VbWindow("frmReportViewer_10").Exist
'			Wend
'			wait(2)
'			VbWindow("frmReportViewer_10").VbButton("Print").Click
'			VbWindow("frmReportViewer_10").Dialog("Print").WinButton("OK").Click
'		ElseIf Datatable.Value("ReportName","Reports_StartEndDates")="Number of unresolved Interface complaints per month over a defined period of time" and  Datatable.Value("RunReport","Reports_StartEndDates")="Yes" Then
'	
'			While Not VbWindow("frmReportViewer_11").Exist
'			Wend
'			wait(2)		
'			VbWindow("frmReportViewer_11").VbButton("Print").Click
'			VbWindow("frmReportViewer_11").Dialog("Print").WinButton("OK").Click
	
'		ElseIf Datatable.Value("ReportName","Reports_StartEndDates")="Number of notices with outstanding payments per month over a defined period of time" and  Datatable.Value("RunReport","Reports_StartEndDates")="Yes" Then
'			While Not VbWindow("frmReportViewer_12").Exist
'			Wend
'			wait(2)		
'			VbWindow("frmReportViewer_12").VbButton("Print").Click
'			VbWindow("frmReportViewer_12").Dialog("Print").WinButton("OK").Click
'		End If
	End If
	
	
	filename1=Datatable.Value("ReportName","Reports_StartEndDates")
	
	If len(filename1)>70 Then
		replaceStr=Right(filename1,len(filename1)-70)
	End If
	filename1=Replace(filename1,replaceStr,"")
	
	filename=file_location & "\" & filename1
	
	While fso.FileExists(filename&".pdf")
		filename=file_location & "\" & Datatable.Value("ReportName","Reports_StartEndDates")&"_"&fnRandomNumber(2)
	Wend
	
	
	    Wait(3)
	    If Datatable.Value("ReportName","Reports_StartEndDates")="Statistical report on complaints over a defined period of time" and  Datatable.Value("RunReport","Reports_StartEndDates")="Yes" Then
				'With	Window("Internet Explorer").Dialog("Save Print Output As")
				Browser("'Generated Report'_3").Dialog("Save Print Output As").WinEdit("File name:").Set filename&".pdf" @@ hightlight id_;_462648_;_script infofile_;_ZIP::ssf222.xml_;_
				Wait(3)
				Browser("'Generated Report'_3").Dialog("Save Print Output As").WinButton("Save").Click @@ hightlight id_;_724364_;_script infofile_;_ZIP::ssf223.xml_;_
					'.WinEdit("File name:").Set filename&".pdf"
					'Wait(3)
					'.WinButton("Save").Click
				'End With
		ElseIf Dialog("Printing Records").Exist(10) Then
					With	Dialog("Printing Records").Dialog("Save Print Output As")

						.WinEdit("File name:").Set filename&".pdf"
						.WinEdit("File name:").Type  micTab @@ hightlight id_;_135778_;_script infofile_;_ZIP::ssf53.xml_;_
						Wait(3)
						.WinButton("Save").Click
					End With
		ElseIf VbWindow("frmPrintConfig").Exist(10) Then
					With	VbWindow("frmPrintConfig")
						.ActiveX("TimoSoft TabStrip Control").ActiveX("TimoSoft Frame Control").ActiveX("TimoSoft TextBox Control").Object.Text=""
						.ActiveX("TimoSoft TabStrip Control").ActiveX("TimoSoft Frame Control").ActiveX("TimoSoft TextBox Control").Type filename &".pdf"
						Wait(2)
						.ActiveX("TimoSoft TabStrip Control").ActiveX("TimoSoft Frame Control").ActiveX("TimoSoft CheckBox Control").Click 16,7
						Wait(2)
						.ActiveX("TimoSoft CommandButton").Click 39,6
					End With
			End If
	
	wait(6)

 		If Datatable.Value("ReportName","Reports_StartEndDates")="Statistical report on complaints over a defined period of time" and  Datatable.Value("RunReport","Reports_StartEndDates")="Yes" Then
			Window("Internet Explorer").Close
		Else
 			VbWindow("frmReportViewer").VbButton("Close").Click
		End If 
		
'	 	If Datatable.Value("ReportName","Reports_StartEndDates")="Number of applications received per Service Class per month over a defined period of time" and Datatable.Value("RunReport","Reports_StartEndDates")="Yes" Then
'			VbWindow("frmReportViewer").VbButton("Close").Click
'		ElseIf Datatable.Value("ReportName","Reports_StartEndDates")="Number of applications received per Service Class per Island over a defined period of time" and  Datatable.Value("RunReport","Reports_StartEndDates")="Yes" Then
'			VbWindow("frmReportViewer_2").VbButton("Close").Click
'		ElseIf Datatable.Value("ReportName","Reports_StartEndDates")="Statistical report on applications over a defined period of time" and  Datatable.Value("RunReport","Reports_StartEndDates")="Yes" Then
'			VbWindow("frmReportViewer_3").VbButton("Close").Click
'		ElseIf Datatable.Value("ReportName","Reports_StartEndDates")="Number of licenses issued per month by type over a defined period of time" and Datatable.Value("RunReport","Reports_StartEndDates")="Yes" Then
'			VbWindow("frmReportViewer_4").VbButton("Close").Click
'		ElseIf Datatable.Value("ReportName","Reports_StartEndDates")="Number of licenses issued per month by service class over a defined period of time" and  Datatable.Value("RunReport","Reports_StartEndDates")="Yes" Then
'			VbWindow("frmReportViewer_5").VbButton("Close").Click
'		ElseIf Datatable.Value("ReportName","Reports_StartEndDates")="Number of licenses issued per month by service nature over a defined period of time" and  Datatable.Value("RunReport","Reports_StartEndDates")="Yes" Then
'			VbWindow("frmReportViewer_6").VbButton("Close").Click
'		ElseIf Datatable.Value("ReportName","Reports_StartEndDates")="Licenses renewed over a defined period of time" and  Datatable.Value("RunReport","Reports_StartEndDates")="Yes" Then
'			VbWindow("frmReportViewer_7").VbButton("Close").Click
'		ElseIf Datatable.Value("ReportName","Reports_StartEndDates")="Licenses cancelled over a defined period of time" and  Datatable.Value("RunReport","Reports_StartEndDates")="Yes" Then
'			VbWindow("frmReportViewer_8").VbButton("Close").Click
'		ElseIf Datatable.Value("ReportName","Reports_StartEndDates")="Statistical report on sub-licenses over a defined period of time" and  Datatable.Value("RunReport","Reports_StartEndDates")="Yes" Then
'			VbWindow("frmReportViewer_9").VbButton("Close").Click 
'		ElseIf Datatable.Value("ReportName","Reports_StartEndDates")="Number of Interface complaints received per month over a defined period of time" and  Datatable.Value("RunReport","Reports_StartEndDates")="Yes" Then
'			VbWindow("frmReportViewer_10").VbButton("Close").Click
'		ElseIf Datatable.Value("ReportName","Reports_StartEndDates")="Number of unresolved Interface complaints per month over a defined period of time" and  Datatable.Value("RunReport","Reports_StartEndDates")="Yes" Then
'			VbWindow("frmReportViewer_11").VbButton("Close").Click
'		ElseIf Datatable.Value("ReportName","Reports_StartEndDates")="Statistical report on complaints over a defined period of time" and  Datatable.Value("RunReport","Reports_StartEndDates")="Yes" Then
'			Window("Internet Explorer").Close
'		ElseIf Datatable.Value("ReportName","Reports_StartEndDates")="Number of notices with outstanding payments per month over a defined period of time" and  Datatable.Value("RunReport","Reports_StartEndDates")="Yes" Then
'			VbWindow("frmReportViewer_12").VbButton("Close").Click
'	End If
	End If
	
	Next
	
	VbWindow("frmMDI").VbWindow("frmRptMonthlyBilling").VbButton("Close").Click
	
	strTestCaseName = Environment.Value("TestName")
		If err.number = 0 Then
			blnFind = True
		Else
			blnFind = False
		End If
		
		UpdateTestCaseStatusInExcel strTestCaseName, blnFind
End If
