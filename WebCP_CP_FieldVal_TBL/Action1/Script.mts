﻿

blnFind=True
err.number = 0

'Import data from Input data sheet  from folder
datatable.ImportSheet ProjectFolderPath&"\Input_Data_Sheet\Input_Data_Sheet.xls","cpTelevision","Global"
n=datatable.GetSheet("Global").GetRowCount
With Browser("name:=Application","title:=Application").Page("title:=Application")
    If n>0 Then
        datatable.SetCurrentRow(1)
        
        If .WebElement("innertext:=TYPE:.*"&Datatable.Value("TypeOfStation","Global")&".*","html tag:=SPAN").Exist Then
            .WebElement("innertext:=TYPE:.*"&Datatable.Value("TypeOfStation","Global")&".*","html tag:=SPAN").Click
        End If
        wait 5
        If .Link("html id:=img.*","name:="&Datatable.Value("Stationtype","Global")).Exist(20) Then
            .Link("html id:=img.*","name:="&Datatable.Value("Stationtype","Global")).Click
        End If
    End If
'    Set objRadius=.WebEdit("html id:=.*Radius.*","name:=.*nomRadius.*")
'    If objRadius.Exist Then
'        fn_WebCPSubLic_FieldLevelVal objRadius,"numeric",500,""
'        fn_WebCPSubLic_FieldLevelVal objRadius,"","","Radius"
'    End If
    Set obj=.WebEdit("html id:=.*stationName","name:=.*stationName")
    If obj.Exist Then
        fn_WebCPSubLic_FieldLevelVal obj,"alphanumeric",500,""
        fn_WebCPSubLic_FieldLevelVal obj,"","","Station Location"
    End If
    Set objVehicle=.WebEdit("html id:=tbl_site__VehregNum","name:=tbl_site__VehregNum")
    If objVehicle.Exist Then
        fn_WebCPSubLic_FieldLevelVal objVehicle,"alphanumeric",500,""
        fn_WebCPSubLic_FieldLevelVal objVehicle,"","","Vehicle Registration No"
    End If
    
    Set objRadius=.WebEdit("html id:=.*Radius.*","name:=.*nomRadius.*")
    If objRadius.Exist Then
        fn_WebCPSubLic_FieldLevelVal objRadius,"numeric",500,""
        fn_WebCPSubLic_FieldLevelVal objRadius,"","","Radius"
    End If
    If .WebElement("innertext:="&Datatable.Value("Equipmenttype","Global"),"html id:=img.*").exist Then
        .WebElement("innertext:="&Datatable.Value("Equipmenttype","Global"),"html id:=img.*").click
        .Sync
        Wait 10     
        Set objEquipSerialNum=.WebEdit("name:=.*equipSerialNum")
        If objEquipSerialNum.Exist Then
            fn_WebCPSubLic_FieldLevelVal objEquipSerialNum,"alphanumeric",500,""
            fn_WebCPSubLic_FieldLevelVal objEquipSerialNum,"","","Equipment Serial Number"
        End If
    End If
    
    
    Set objAntennaPower=.WebEdit("html id:=.*antennaPower.*","name:=.*antennaPower.*")
    If objAntennaPower.Exist Then
        fn_WebCPSubLic_FieldLevelVal objAntennaPower,"numeric",10000001,""
        fn_WebCPSubLic_FieldLevelVal objAntennaPower,"","","Output Power \(W\)"
    End If
    
    
    Set objEquipSerialNum=.WebEdit("name:=tbl_antenna__transgain")
    If objEquipSerialNum.Exist Then
        fn_WebCPSubLic_FieldLevelVal objEquipSerialNum,"alphanumeric",500,""
        '	   fn_WebCPSubLic_FieldLevelVal objEquipSerialNum,"","","Equipment Serial Number"
    End If
    
End With



