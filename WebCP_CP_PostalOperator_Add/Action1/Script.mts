﻿blnFind=False
err.number = 0

'Import data from Input data sheet  from folder
datatable.ImportSheet ProjectFolderPath&"\Input_Data_Sheet.xls","WebCP_CP_PostalOp_Add","Global"
n=datatable.GetSheet("Global").GetRowCount
If n>0 Then
    datatable.SetCurrentRow(1)
    With Browser("Browser").Page("Page")
         .Link("Home").Click
	    .Sync
	    If Not Browser("Browser").Page("Page").WebTable("Postal").Exist Then
	    	Wait 10
	    End If
	    RC=Browser("Browser").Page("Page").WebTable("Postal").GetRowWithCellText(Datatable("ClientName","Global"))
		If Browser("Browser").Page("Page").WebTable("Postal").ChildItem(RC,1,"WebElement",1).exist Then
			Browser("Browser").Page("Page").WebTable("Postal").ChildItem(RC,1,"WebElement",1).Click
		End If
	    .Sync
	    .Link("Postal Operator").Click @@ script infofile_;_ZIP::ssf19.xml_;_
	    .Sync
		.Link("Add Postal Operator License").Click
		.Sync
		.WebList("LicenseTypeId").Select Datatable("LicenseType","Global") @@ script infofile_;_ZIP::ssf21.xml_;_
		If Strcomp(Trim(.WebList("LicenseTerm").GetROProperty("innertext")),Datatable("LicenseTerm","Global") ,1)=0 Then
			.WebEdit("Ownername1").Set GenerateRandomString(8) @@ script infofile_;_ZIP::ssf22.xml_;_
			.WebEdit("OwnerPercentage1").Set RandomNumber(100,100) @@ script infofile_;_ZIP::ssf23.xml_;_
			.WebEdit("Nationality1").Set Datatable("Client","Global") @@ script infofile_;_ZIP::ssf24.xml_;_
			.WebElement("img_1").Click @@ script infofile_;_ZIP::ssf25.xml_;_
			.Sync
			.WebElement("Attached Documents").Click @@ script infofile_;_ZIP::ssf27.xml_;_
			.Sync
			 Wait 30
			.Frame("Frame").WebFile("BrowserHidden").Set fileCreate @@ script infofile_;_ZIP::ssf28.xml_;_
			.Frame("Frame").Image("Pressing Upload, the document").Click @@ script infofile_;_ZIP::ssf29.xml_;_
			.WebElement("img_2_0").Click @@ script infofile_;_ZIP::ssf30.xml_;_
			.Sync
			If Instr(1,Trim(.WebElement("Pending").GetROProperty("innertext")),Datatable("Status","Global"),1)>0 Then
					poNum=Trim(Browser("Browser").Page("Postal Operator").WebElement("OperatorsId").GetROProperty("innertext"))
					client=Trim(Browser("Browser").Page("Postal Operator").WebElement("clientName").GetROProperty("innertext"))
					fnUpdatePostalOperatorCP poNum,client
			        blnFind=True
			End If

		 End If	
    End With


End If
Browser("Browser").Close
UpdateTestCaseStatusInExcel Environment("TestName"),blnFind


