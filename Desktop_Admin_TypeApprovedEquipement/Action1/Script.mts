﻿blnVal=False

With VbWindow("frmMDI")
	.WinMenu("Menu").Select "Administrative;System Table;Type Approved Equipment"
	With .VbWindow("frmApprovedEquipFilter")
		While Not .ActiveX("TCIComboBox.tcb").VbComboBox("cboEquipType").Exist
		Wend
		Wait 5
		.ActiveX("TCIComboBox.tcb").VbComboBox("cboEquipType").Select Datatable.Value("EquipmentType","Global")
		.VbButton("OK").Click
		Wait 5
	End With
	With .VbWindow("frmApprovedRx")
		Wait 5
		If .VbButton("Add Equipment").Exist Then
			.VbButton("Add Equipment").Click
		End If
		Wait 5
		.VbEdit("txtMake").Set GenerateRandomString(5) @@ hightlight id_;_1835958_;_script infofile_;_ZIP::ssf5.xml_;_
		.VbEdit("txtModel").Set RandomNumber(9999,999999) @@ hightlight id_;_1311952_;_script infofile_;_ZIP::ssf6.xml_;_
		.ActiveX("MaskEdBox").Drag 34,11 @@ hightlight id_;_525942_;_script infofile_;_ZIP::ssf7.xml_;_
		.VbFrame("Frame1").Drop 140,126 @@ hightlight id_;_723968_;_script infofile_;_ZIP::ssf8.xml_;_
		.ActiveX("MaskEdBox").Object.Text =RandomNumber(1,1000) @@ hightlight id_;_525942_;_script infofile_;_ZIP::ssf9.xml_;_
		.ActiveX("MaskEdBox_2").Drag 20,10 @@ hightlight id_;_525488_;_script infofile_;_ZIP::ssf10.xml_;_
		.VbFrame("Frame1").Drop 185,149 @@ hightlight id_;_723968_;_script infofile_;_ZIP::ssf11.xml_;_
		.ActiveX("MaskEdBox_2").Object.Text =RandomNumber(1,1000) @@ hightlight id_;_525488_;_script infofile_;_ZIP::ssf12.xml_;_
		.ActiveX("MaskEdBox_3").Drag 25,11 @@ hightlight id_;_394216_;_script infofile_;_ZIP::ssf13.xml_;_
		.VbFrame("Frame1").Drop 157,174 @@ hightlight id_;_723968_;_script infofile_;_ZIP::ssf14.xml_;_
		.ActiveX("MaskEdBox_3").Type RandomNumber(1,1000) @@ hightlight id_;_394216_;_script infofile_;_ZIP::ssf15.xml_;_
		.VbComboBox("cboEmission").Select 2 @@ hightlight id_;_3016768_;_script infofile_;_ZIP::ssf16.xml_;_
		.ActiveX("MaskEdBox_4").Drag 16,8 @@ hightlight id_;_328684_;_script infofile_;_ZIP::ssf17.xml_;_
		.VbFrame("Frame1").Drop 194,240 @@ hightlight id_;_723968_;_script infofile_;_ZIP::ssf18.xml_;_
		.ActiveX("MaskEdBox_4").Type RandomNumber(1,100) @@ hightlight id_;_328684_;_script infofile_;_ZIP::ssf19.xml_;_
		.ActiveX("MaskEdBox_5").Drag 40,7 @@ hightlight id_;_721922_;_script infofile_;_ZIP::ssf20.xml_;_
		.VbFrame("Frame1").Drop 616,120 @@ hightlight id_;_723968_;_script infofile_;_ZIP::ssf21.xml_;_
		.ActiveX("MaskEdBox_5").Type RandomNumber(101,1000) @@ hightlight id_;_721922_;_script infofile_;_ZIP::ssf22.xml_;_
		.ActiveX("MaskEdBox_6").Drag 36,7 @@ hightlight id_;_459706_;_script infofile_;_ZIP::ssf23.xml_;_
		.VbFrame("Frame1").Drop 588,195 @@ hightlight id_;_723968_;_script infofile_;_ZIP::ssf24.xml_;_
		.ActiveX("MaskEdBox_6").Type RandomNumber(1001,10000) @@ hightlight id_;_459706_;_script infofile_;_ZIP::ssf25.xml_;_
		.ActiveX("MaskEdBox_7").Drag 19,3 @@ hightlight id_;_459750_;_script infofile_;_ZIP::ssf26.xml_;_
		.VbFrame("Frame1").Drop 605,216 @@ hightlight id_;_723968_;_script infofile_;_ZIP::ssf27.xml_;_
		.ActiveX("MaskEdBox_7").Type RandomNumber(1,1000) @@ hightlight id_;_459750_;_script infofile_;_ZIP::ssf28.xml_;_
		If .VbEdit("txtPresetChannel").Exist(8) Then
			.VbEdit("txtPresetChannel").SetSelection 0,1
		End If
		If .VbEdit("txtPresetChannel").Exist(8) Then
			.VbEdit("txtPresetChannel").Set RandomNumber(1001,10000)
		End If
		.ActiveX("MaskEdBox_8").Drag 22,7 @@ hightlight id_;_394288_;_script infofile_;_ZIP::ssf36.xml_;_
		.VbFrame("Frame1").Drop 195,263 @@ hightlight id_;_263052_;_script infofile_;_ZIP::ssf37.xml_;_
		If .ActiveX("MaskEdBox_8").Exist(8) Then
			.ActiveX("MaskEdBox_8").Type RandomNumber(1001,10000)
		End If @@ hightlight id_;_394288_;_script infofile_;_ZIP::ssf38.xml_;_
		If .ActiveX("TCIComboBox.tcb").VbComboBox("cboModulation").Exist(8) Then
			.ActiveX("TCIComboBox.tcb").VbComboBox("cboModulation").Select 2
		End If @@ hightlight id_;_589978_;_script infofile_;_ZIP::ssf39.xml_;_
		.ActiveX("MaskEdBox_9").Drag 14,10 @@ hightlight id_;_263218_;_script infofile_;_ZIP::ssf40.xml_;_
		.VbFrame("Frame1").Drop 552,318 @@ hightlight id_;_263052_;_script infofile_;_ZIP::ssf41.xml_;_
		.VbButton("Save").Click @@ hightlight id_;_459642_;_script infofile_;_ZIP::ssf29.xml_;_
		Wait 5
	End With
	If .Dialog("#32770").WinButton("OK").Exist Then
		.Dialog("#32770").WinButton("OK").Click
		Wait 5
	End If
	With .VbWindow("frmApprovedRx")
		If .VbButton("Previous").Exist Then
			.VbButton("Previous").Click
		End If
		Wait 3
		If .VbButton("Next").Exist Then
			.VbButton("Next").Click
		End If
		If .VbButton("Close").Exist Then
			.VbButton("Close").Click
		End If
		If VbWindow("frmMDI").VbWindow("frmApprovedEquipFilter").VbButton("Close").Exist Then
		    Wait 8
			VbWindow("frmMDI").VbWindow("frmApprovedEquipFilter").VbButton("Close").Click
		End If @@ hightlight id_;_854444_;_script infofile_;_ZIP::ssf42.xml_;_
	End With
	
End With
If Err.Number=0 Then
	blnVal=True
End If
UpdateTestCaseStatusInExcel Environment("TestName"),blnVal
