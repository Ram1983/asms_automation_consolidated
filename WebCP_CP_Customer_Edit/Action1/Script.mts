﻿

blnFind=True
err.number = 0
'import data
datatable.ImportSheet ProjectFolderPath&"\Input_Data_Sheet.xls","cpEditcustomer","cpEditcustomer"
n=datatable.GetSheet("cpEditcustomer").GetRowCount

'For i = 1 To n Step 1
If n>0 Then
datatable.SetCurrentRow(1)

		'====Edit Customer details
		
		If Datatable.value("Clienttype","cpEditcustomer")="Person" Then
			'Edit customer link
			Browser("Customer Home Page").Page("Customer Home Page").Link("Edit Customer Information").Click
			'Edit
			Browser("Customer Home Page").Page("bocraasmswebcp1:61081/manageme").Image("Edit").Click
			Browser("Customer Home Page").Page("Edit Customer, Requestor").WebEdit("client__clientFname").Set Datatable.value("Fname","cpEditcustomer")&fnRandomNumber(4)
			Browser("Customer Home Page").Page("Edit Customer, Requestor").WebEdit("client__clientLname").Set Datatable.value("Lname","cpEditcustomer")&fnRandomNumber(4)
			Browser("Customer Home Page").Page("Edit Customer, Requestor").WebEdit("client__ROT").Set Datatable.value("NID","cpEditcustomer")&fnRandomNumber(4)
			
			'=====Selecting Dealer or director
			If Datatable.value("Dealer","cpEditcustomer")="Dealer"  Then
				Browser("Customer Home Page").Page("Edit Customer, Requestor").WebCheckBox("client__Dealer").Set "ON"
			Else  
				Browser("Customer Home Page").Page("Edit Customer, Requestor").WebCheckBox("client__Dealer").Set "OFF"
			End If
			If Datatable.value("Dealer","cpEditcustomer")="Director" Then  
				Browser("Customer Home Page").Page("Edit Customer, Requestor").WebCheckBox("client__Director").Set "ON"
			Else
				Browser("Customer Home Page").Page("Edit Customer, Requestor").WebCheckBox("client__Director").Set "OFF"
			End If
			
			Browser("Customer Home Page").Page("Edit Customer, Requestor").WebEdit("client__clientTelNum").Set Datatable.value("Tel","cpEditcustomer")
			Browser("Customer Home Page").Page("Edit Customer, Requestor").WebEdit("client__ClientCellPhone").Set Datatable.value("Cell","cpEditcustomer")
			Browser("Customer Home Page").Page("Edit Customer, Requestor").WebEdit("client__clientFaxNum").Set Datatable.value("Fax","cpEditcustomer")
			Browser("Customer Home Page").Page("Edit Customer, Requestor").WebEdit("name:=client__clientEmail","visible:=true").Set Datatable.value("Fax","cpEditcustomer")
			'Browser("Customer Home Page").Page("Edit Customer, Requestor").WebCheckBox("client__Dealer").Set Datatable.value("Dealer","cpEditcustomer")
			'Browser("Customer Home Page").Page("Edit Customer, Requestor").WebCheckBox("client__Director").Set Datatable.value("Director","cpEditcustomer")
			'====Update
			Browser("Customer Home Page").Page("Edit Customer, Requestor").WebElement("img_0").Click
			
			'=====Address tab
			Browser("Customer Home Page").Page("Edit Customer, Requestor").WebElement("Address").Click
			Browser("Customer Home Page").Page("Edit Customer, Requestor").WebEdit("client__PlotNo").Set Datatable.value("PlotNo","cpEditcustomer")
			Browser("Customer Home Page").Page("Edit Customer, Requestor").WebEdit("client__clientStreet1").Set Datatable.value("Street","cpEditcustomer")
			Browser("Customer Home Page").Page("Edit Customer, Requestor").WebEdit("client__PostalLocation").Set Datatable.value("Postalloc","cpEditcustomer")
			Browser("Customer Home Page").Page("Edit Customer, Requestor").WebEdit("client__clientStreet2").Set Datatable.value("Village","cpEditcustomer")
			Browser("Customer Home Page").Page("Edit Customer, Requestor").WebEdit("client__clientZipCode1").Set Datatable.value("zipcode","cpEditcustomer")
			Browser("Customer Home Page").Page("Edit Customer, Requestor").WebEdit("client__clientDistrict2").Set Datatable.value("TA","cpEditcustomer")
			'===Update
			'Email Tab
			Browser("Customer Home Page").Page("Edit Customer, Requestor").WebElement("img_0").Click
			If Browser("Customer Home Page").Page("title:=Edit Customer, Requestor or Contact").webelement("innertext:=Email","html tag:=SPAN").Exist(8) Then
			   Browser("Customer Home Page").Page("title:=Edit Customer, Requestor or Contact").webelement("innertext:=Email","html tag:=SPAN").click
               Browser("Customer Home Page").Page("title:=Edit Customer, Requestor or Contact").WebEdit("name:=client__clientEmail").Set "customer@"&RandomNumber(1,1000)&"gmail.com"
               Browser("Edit Customer, Requestor").Page("Edit Customer, Requestor").WebElement("img_0").Click
			End If
			'=====Customer Information Landing			
			Browser("Customer Home Page").Page("Edit Customer, Requestor").Link("Customer Information Landing").Click
			'update
			Browser("Customer Home Page").Page("bocraasmswebcp1:61081/manageme").WebElement("img_0").Click	
			
			
			
		
		ElseIf Datatable.value("Clienttype","cpEditcustomer")="Government"  or  Datatable.value("Clienttype","cpEditcustomer")="Non-Governmental Organization"  or   Datatable.value("Clienttype","cpEditcustomer")="Parastatal" or  Datatable.value("Clienttype","cpEditcustomer")="Company"  Then

			Browser("Customer Home Page").Page("Customer Home Page").Link("Edit Customer Information").Click			
			Browser("Customer Home Page").Page("bocraasmswebcp1:61081/manageme").Image("Edit").Click			
			If Browser("Edit Customer, Requestor").Page("Edit Customer, Requestor").WebEdit("client__clientCompany").Exist(5) Then
			   Browser("Edit Customer, Requestor").Page("Edit Customer, Requestor").WebEdit("client__clientCompany").Set Datatable.value("Name","cpEditcustomer")&fnRandomNumber(4)
			End If
			If Browser("Edit Customer, Requestor").Page("Edit Customer, Requestor").WebEdit("client__ClientWebAddress").Exist(8) Then
			   Browser("Edit Customer, Requestor").Page("Edit Customer, Requestor").WebEdit("client__ClientWebAddress").Set Datatable.value("WebAddress","cpEditcustomer")
			End If

			If Browser("Edit Customer, Requestor").Page("Edit Customer, Requestor").WebEdit("client__ROT").Exist(5) Then
			   Browser("Edit Customer, Requestor").Page("Edit Customer, Requestor").WebEdit("client__ROT").Set Datatable.value("NID","cpEditcustomer")&fnRandomNumber(4)
			End If
			
			'=====Dealer			
			If Datatable.value("Dealer","cpEditcustomer")="Dealer" Then			
				Browser("Edit Customer, Requestor").Page("Edit Customer, Requestor").WebCheckBox("client__Dealer").Set "ON"		
			End If
			'Browser("Edit Customer, Requestor").Page("Edit Customer, Requestor").WebEdit("client__CompComercialName").Set Datatable.value("Activity","cpEditcustomer")

			Browser("Edit Customer, Requestor").Page("Edit Customer, Requestor").WebEdit("client__clientTelNum").Set Datatable.value("Tel","cpEditcustomer")
			Browser("Edit Customer, Requestor").Page("Edit Customer, Requestor").WebEdit("client__ClientCellPhone").Set Datatable.value("Cell","cpEditcustomer")

			Browser("Edit Customer, Requestor").Page("Edit Customer, Requestor").WebEdit("client__clientFaxNum").Set Datatable.value("Fax","cpEditcustomer")
			If Browser("Customer Home Page").Page("Edit Customer, Requestor").WebEdit("name:=client__clientEmail","visible:=true").Exist(8) Then
			   Browser("Customer Home Page").Page("Edit Customer, Requestor").WebEdit("name:=client__clientEmail","visible:=true").Click
			End If
				
			'Address tab
			Browser("Edit Customer, Requestor").Page("Edit Customer, Requestor").WebElement("Address").Click
			Browser("Edit Customer, Requestor").Page("Edit Customer, Requestor").WebEdit("client__PlotNo").Set Datatable.value("PlotNo","cpEditcustomer")
			Browser("Edit Customer, Requestor").Page("Edit Customer, Requestor").WebEdit("client__clientStreet1").Set Datatable.value("Street","cpEditcustomer")
			Browser("Edit Customer, Requestor").Page("Edit Customer, Requestor").WebEdit("client__PostalLocation").Set Datatable.value("Postalloc","cpEditcustomer")
			Browser("Edit Customer, Requestor").Page("Edit Customer, Requestor").WebEdit("client__clientStreet2").Set Datatable.value("Street","cpEditcustomer")
			Browser("Edit Customer, Requestor").Page("Edit Customer, Requestor").WebEdit("client__clientZipCode1").Set Datatable.value("zipcode","cpEditcustomer")
			'Browser("Edit Customer, Requestor").Page("Edit Customer, Requestor").WebEdit("client__clientDistrict2").Set "tapost"
			'===Update
			Browser("Edit Customer, Requestor").Page("Edit Customer, Requestor").WebElement("img_0").Click
			'Email
			If Browser("Customer Home Page").Page("title:=Edit Customer, Requestor or Contact").webelement("innertext:=Email","html tag:=SPAN").Exist(8) Then
			   Browser("Customer Home Page").Page("title:=Edit Customer, Requestor or Contact").webelement("innertext:=Email","html tag:=SPAN").Click
               Browser("Customer Home Page").Page("Edit Customer, Requestor").WebEdit("name:=client__clientEmail","visible:=true").Set "customer@"&RandomNumber(1,1000)&"gmail.com"
               Browser("Edit Customer, Requestor").Page("Edit Customer, Requestor").WebElement("img_0").Click
			End If
			'===Customer infoormation landing page			
			Browser("Customer Home Page").Page("Edit Customer, Requestor").Link("Customer Information Landing").Click			
			'update
			Browser("Customer Home Page").Page("bocraasmswebcp1:61081/manageme").WebElement("img_0").Click	
			
		End If
Browser("Edit Customer, Requestor").Page("Page").Image("Edit").Click
Browser("Edit Customer, Requestor").Page("Edit Customer, Requestor").WebEdit("client__clientFname").Set Datatable.value("Fname","cpEditcustomer")&fnRandomNumber(4)
Browser("Edit Customer, Requestor").Page("Edit Customer, Requestor").WebEdit("client__clientLname").Set Datatable.value("Lname","cpEditcustomer")&fnRandomNumber(4)
If Browser("Edit Customer, Requestor").Page("Edit Customer, Requestor").WebList("client__clientIDType").Exist(5) Then
	Browser("Edit Customer, Requestor").Page("Edit Customer, Requestor").WebList("client__clientIDType").Select 1
End If
Browser("Edit Customer, Requestor").Page("Edit Customer, Requestor").WebEdit("client__ROT").Set Datatable.value("NID","cpEditcustomer")&fnRandomNumber(4)
Browser("Edit Customer, Requestor").Page("Edit Customer, Requestor").WebEdit("client__clientTelNum").Set  Datatable.value("Tel","cpEditcustomer")
Browser("Edit Customer, Requestor").Page("Edit Customer, Requestor").WebEdit("client__ClientCellPhone").Set Datatable.value("Cell","cpEditcustomer")
Browser("Edit Customer, Requestor").Page("Edit Customer, Requestor").WebEdit("client__clientFaxNum").Set Datatable.value("Fax","cpEditcustomer")
'Address Tab
Browser("Edit Customer, Requestor").Page("Edit Customer, Requestor").WebElement("Address").Click
Browser("Edit Customer, Requestor").Page("Edit Customer, Requestor").WebEdit("client__PlotNo").Set Datatable.value("PlotNo","cpEditcustomer")
Browser("Edit Customer, Requestor").Page("Edit Customer, Requestor").WebEdit("client__clientStreet1").Set Datatable.value("Street","cpEditcustomer")
Browser("Edit Customer, Requestor").Page("Edit Customer, Requestor").WebEdit("client__PostalLocation").Set Datatable.value("Postalloc","cpEditcustomer")
Browser("Edit Customer, Requestor").Page("Edit Customer, Requestor").WebEdit("client__clientStreet2").Set Datatable.value("Street","cpEditcustomer")
Browser("Edit Customer, Requestor").Page("Edit Customer, Requestor").WebElement("WebElement_5").Click
If Browser("Edit Customer, Requestor").Page("Edit Customer, Requestor").WebEdit("client__clientZipCode1").Exist(5) Then
	Browser("Edit Customer, Requestor").Page("Edit Customer, Requestor").WebEdit("client__clientZipCode1").Set GenerateRandomString(5)
End If
If Browser("Edit Customer, Requestor").Page("Edit Customer, Requestor").WebEdit("client__clientDistrict2").Exist(5) Then
	Browser("Edit Customer, Requestor").Page("Edit Customer, Requestor").WebEdit("client__clientDistrict2").Set GenerateRandomString(6)
End If
Browser("Edit Customer, Requestor").Page("Edit Customer, Requestor").WebElement("Email").Click
Browser("Customer Home Page").Page("Edit Customer, Requestor").WebEdit("name:=client__clientEmail","visible:=true").Click
Browser("Edit Customer, Requestor").Page("Edit Customer, Requestor").WebElement("Select a password for").Click
Browser("Edit Customer, Requestor").Page("Edit Customer, Requestor").WebElement("img_0").Click
Browser("Edit Customer, Requestor").Page("Edit Customer, Requestor").WebElement("img_0").Click
If Browser("Customer Home Page").Page("Edit Customer, Requestor_2").WebElement("Requestor is not completed").Exist(4) Then
	If Instr(1,Browser("Customer Home Page").Page("Edit Customer, Requestor_2").WebElement("Requestor is not completed").GetROProperty("innertext"),"not completed",1)>0 Then
		blnFind = False
		UpdateTestCaseStatusInExcel strTestCaseName, blnFind 
		ExitTest
   End If 
End If

strTestCaseName = Environment.Value("TestName")
			
'Test results dsiplayed in Excel
If err.number = 0 Then
    blnFind = True
Else
    blnFind = False
End If

UpdateTestCaseStatusInExcel strTestCaseName, blnFind 

End if
	



