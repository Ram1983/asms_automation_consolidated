﻿Wait(1)
blnFind=True
err.number = 0

If Lcase(Environment.Value("Client"))="moz" Then
	fn_NavigateToScreen "Contabilidade;Informação do Cliente"
Else
   WaitForForm(VbWindow("frmMDI"))
   fn_NavigateToScreen "Accounting;Customer Information" 
End If
Wait(2)
'================= Menu Navigation completed

datatable.ImportSheet ProjectFolderPath &"\Desktop_Input_Data_Sheet\Input_Data_Sheet.xls","AddCustomer_Person","AddCustomer_Person"
n=datatable.GetSheet("AddCustomer_Person").GetRowCount

'For i = 1 To n Step 1
If n>0 Then
	datatable.SetCurrentRow(1)	
	
	While Not VbWindow("frmMDI").VbWindow("frmBrowser").Page("Page").Link("Add New Customer").Exist(40) @@ hightlight id_;_1772618_;_script infofile_;_ZIP::ssf3.xml_;_
	Wend
	Wait(3)
	'==Add new customer
	VbWindow("frmMDI").VbWindow("frmBrowser").Page("Page").Link("Add New Customer").Click @@ hightlight id_;_VbWindow("frmMDI").VbWindow("frmBrowser").Page("Page").Link("Add New Customer")_;_script infofile_;_ZIP::ssf4.xml_;_
	Wait(4)
	'==person
	VbWindow("frmMDI").VbWindow("frmBrowser").Page("Page_2").WebElement("img_0").Click @@ hightlight id_;_VbWindow("frmMDI").VbWindow("frmBrowser").Page("Page 2").WebElement("img 0")_;_script infofile_;_ZIP::ssf5.xml_;_
	Wait(2)
	'==Edit
	VbWindow("frmMDI").VbWindow("frmBrowser").Page("Page_3").Image("Edit").Click @@ hightlight id_;_VbWindow("frmMDI").VbWindow("frmBrowser").Page("Page 3").Image("Edit")_;_script infofile_;_ZIP::ssf6.xml_;_
	Wait(3)
	VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebEdit("client__clientFname").Set  Datatable.Value("Fname","AddCustomer_Person") & "" & fnRandomNumber(3)'"grass" @@ hightlight id_;_VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebEdit("client  clientFname")_;_script infofile_;_ZIP::ssf8.xml_;_
	VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebEdit("client__clientLname").Set Datatable.Value("Lname","AddCustomer_Person") & "" & fnRandomNumber(3)'"hoper123" @@ hightlight id_;_VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebEdit("client  clientLname")_;_script infofile_;_ZIP::ssf9.xml_;_
	If VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebList("client__clientIDType").Exist(5) Then
		VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebList("client__clientIDType").Select(1) '"Driver License"
	End If
	VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebEdit("client__ROT").Set Datatable.Value("Nid","AddCustomer_Person")'"125444" @@ hightlight id_;_VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebEdit("client  ROT")_;_script infofile_;_ZIP::ssf11.xml_;_
	'==Cust Num
	VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebElement("img_GCSclient__ccNum").Click @@ hightlight id_;_VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebElement("img GCSclient  ccNum")_;_script infofile_;_ZIP::ssf12.xml_;_

	If 	VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebElement("No").Exist(5) Then
		VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebElement("No").Click	
	End If
 @@ hightlight id_;_VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebElement("No")_;_script infofile_;_ZIP::ssf13.xml_;_
	'==Dealer
	isDealer = Datatable.Value("Dealer","AddCustomer_Person")				'Yes / No
	If (isDealer = "Yes") Then
		VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebCheckBox("client__Dealer").Set "ON"
	End If
 @@ hightlight id_;_VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebCheckBox("client  Dealer")_;_script infofile_;_ZIP::ssf14.xml_;_
	'==Director
	isDirecter = Datatable.Value("Director","AddCustomer_Person")           'Yes / No
	If (isDirecter = "Yes") Then
	    VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebCheckBox("client__Director").Set "ON" @@ hightlight id_;_VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebCheckBox("client  Director")_;_script infofile_;_ZIP::ssf15.xml_;_
	End If @@ hightlight id_;_VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebElement("No")_;_script infofile_;_ZIP::ssf34.xml_;_
	VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebEdit("client__clientTelNum").Set Datatable.Value("Tel","AddCustomer_Person")'"120548879" @@ hightlight id_;_VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebEdit("client  clientTelNum")_;_script infofile_;_ZIP::ssf35.xml_;_
	'Wait(2)
	VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebEdit("client__clientEmail").Set Datatable.Value("Email","AddCustomer_Person")'"jkhm@spx.com" @@ hightlight id_;_VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebEdit("client  clientEmail")_;_script infofile_;_ZIP::ssf36.xml_;_
	Wait(2)
	VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebElement("Financial Information").Click @@ hightlight id_;_VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebElement("Financial Information")_;_script infofile_;_ZIP::ssf37.xml_;_
	VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebElement("Address").Click @@ hightlight id_;_VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebElement("WebElement")_;_script infofile_;_ZIP::ssf38.xml_;_
	Wait(2)
	VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebList("client__clientCity1").Select(1) '"Aero Drome (Extension 127)" @@ hightlight id_;_VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebList("client  clientCity1")_;_script infofile_;_ZIP::ssf39.xml_;_
	VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebList("client__clientCity2").Select(1) '"Aero Drome (Extension 127)" @@ hightlight id_;_VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebList("client  clientCity2")_;_script infofile_;_ZIP::ssf40.xml_;_
	Wait(3)
	'==Update
	VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebElement("img_0").Click @@ hightlight id_;_VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebElement("img 0")_;_script infofile_;_ZIP::ssf41.xml_;_
	Wait(3)
	'==Refresh
	VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebElement("Information").Click @@ hightlight id_;_VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebElement("Information")_;_script infofile_;_ZIP::ssf42.xml_;_
	Wait(2)
	VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebElement("Financial Information").Click @@ hightlight id_;_VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebElement("Financial Information")_;_script infofile_;_ZIP::ssf43.xml_;_
	Wait(2)
	VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebElement("Address").Click
	Wait(3) @@ hightlight id_;_VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebElement("img 0")_;_script infofile_;_ZIP::ssf45.xml_;_
	'==cust inf landing @@ hightlight id_;_VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebElement("Financial Information")_;_script infofile_;_ZIP::ssf28.xml_;_
	VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").Link("Customer Information Landing").Click @@ hightlight id_;_VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").Link("Customer Information Landing")_;_script infofile_;_ZIP::ssf46.xml_;_
	'==Add contact
	VbWindow("frmMDI").VbWindow("frmBrowser").Page("Page_3").WebElement("img_0").Click @@ hightlight id_;_VbWindow("frmMDI").VbWindow("frmBrowser").Page("Page 3").WebElement("img 0")_;_script infofile_;_ZIP::ssf47.xml_;_
'	UIAWindow("Automated Spectrum Management").Maximize
'	UIAWindow("Automated Spectrum Management").UIAWindow("http://bocraasmswebcp1:61083//").UIAObject("Pane").UIATable("Table").UIATable("Table").UIATable("Table").UIATable("Table").UIARadioButton("requestor").Select
	VbWindow("frmMDI_2").VbWindow("frmBrowser").Page("Page").WebRadioGroup("requestor").Click
	'==update
	Wait 4
	VbWindow("frmMDI").VbWindow("frmBrowser").Page("Page_3").WebElement("img_1").Click @@ hightlight id_;_VbWindow("frmMDI").VbWindow("frmBrowser").Page("Page 3").WebElement("img 1")_;_script infofile_;_ZIP::ssf48.xml_;_
	Wait(3)
	'==Edit @@ hightlight id_;_VbWindow("frmMDI").VbWindow("frmBrowser").Page("Page 3").WebElement("img 0")_;_script infofile_;_ZIP::ssf49.xml_;_
	VbWindow("frmMDI").VbWindow("frmBrowser").Page("Page_3").Image("Edit_2").Click @@ hightlight id_;_VbWindow("frmMDI").VbWindow("frmBrowser").Page("Page 3").Image("Edit 2")_;_script infofile_;_ZIP::ssf50.xml_;_
	Wait(2)
	VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebEdit("client__clientFname").Set Datatable.Value("AcFname","AddCustomer_Person") & "" & fnRandomNumber(3)'"acperson1" @@ hightlight id_;_VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebEdit("client  clientFname")_;_script infofile_;_ZIP::ssf51.xml_;_
	Wait(2)
	VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebEdit("client__clientLname").Set Datatable.Value("AcLname","AddCustomer_Person") & "" & fnRandomNumber(3)'"aclname12" @@ hightlight id_;_VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebEdit("client  clientLname")_;_script infofile_;_ZIP::ssf52.xml_;_
	Wait(2)
	If VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebList("client__clientIDType").Exist(5) Then
		VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebList("client__clientIDType").Select(1) '"Driver License"
	End If @@ hightlight id_;_VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebList("client  clientIDType")_;_script infofile_;_ZIP::ssf53.xml_;_
	Wait(2)
	VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebEdit("client__ROT").Set Datatable.Value("Nid","AddCustomer_Person")'"125400" @@ hightlight id_;_VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebEdit("client  ROT")_;_script infofile_;_ZIP::ssf54.xml_;_
	VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebList("client__nationality").Select(32) '"Adelie Land" @@ hightlight id_;_VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebList("client  nationality")_;_script infofile_;_ZIP::ssf55.xml_;_
	Wait(3)
	VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebEdit("client__clientTelNum").Set Datatable.Value("Tel","AddCustomer_Person")'"120445669" @@ hightlight id_;_VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebEdit("client  clientTelNum")_;_script infofile_;_ZIP::ssf56.xml_;_
	VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebElement("Address").Click @@ hightlight id_;_VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebElement("Address")_;_script infofile_;_ZIP::ssf57.xml_;_
	Wait(2)
	VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebList("client__clientCity1").Select(1) '"Aero Drome (Extension 127)" @@ hightlight id_;_VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebList("client  clientCity1")_;_script infofile_;_ZIP::ssf58.xml_;_
	VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebList("client__clientCity2").Select(1) '"Aero Drome (Extension 127)" @@ hightlight id_;_VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebList("client  clientCity2")_;_script infofile_;_ZIP::ssf59.xml_;_
	VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebElement("Email").Click @@ hightlight id_;_VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebElement("Email")_;_script infofile_;_ZIP::ssf60.xml_;_
	Wait(2)
	'===Email
	VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebEdit("client__clientEmail").Set Datatable.Value("Email","AddCustomer_Person")'"gdffvf@spx.com" @@ hightlight id_;_VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebEdit("client  clientEmail")_;_script infofile_;_ZIP::ssf61.xml_;_
	VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebEdit("client__Password").SetSecure Datatable.Value("Pass","AddCustomer_Person") '"5edb8ba1b8ea126e388239bf1f9732f48953c05906c7" @@ hightlight id_;_VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebEdit("client  Password")_;_script infofile_;_ZIP::ssf62.xml_;_
	Wait(2)
	'===Update
	VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebElement("img_0").Click @@ hightlight id_;_VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebElement("img 0")_;_script infofile_;_ZIP::ssf63.xml_;_
	Wait(3)
	'==Send verification mail
	VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebElement("img_2").Click @@ hightlight id_;_VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebElement("img 2")_;_script infofile_;_ZIP::ssf65.xml_;_
	Wait(5)
	'==Force verify email
	VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebElement("img_1").Click
	Wait(5)
	'===Close
     VbWindow("frmMDI").VbWindow("frmBrowser").Close
     
	 strTestCaseName = Environment.Value("TestName")
	If err.number = 0 Then
		blnFind = True
	Else
		blnFind = False
	End If
	
	UpdateTestCaseStatusInExcel strTestCaseName, blnFind
	
	'VbWindow("vbname:=frmMDI").close		'Move this out of this action

End If @@ hightlight id_;_929740_;_script infofile_;_ZIP::ssf2.xml_;_




 @@ hightlight id_;_VbWindow("frmMDI 2").VbWindow("frmBrowser").Page("Page").WebRadioGroup("requestor")_;_script infofile_;_ZIP::ssf71.xml_;_
