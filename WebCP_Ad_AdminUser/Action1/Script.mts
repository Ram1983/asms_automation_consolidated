﻿

blnFind=True
err.number = 0

'import data
datatable.ImportSheet ProjectFolderPath&"\Input_Data_Sheet.xls","AdAdminuser","AdAdminuser"
n=datatable.GetSheet("AdAdminuser").GetRowCount


'For i = 1 To n Step 1
If n>0 Then
	datatable.SetCurrentRow(1)	
'
		
'		'=====Selecting Admin users
		Browser("Admin Home Page").Page("Admin Home Page").Link("Admin Users").Click @@ script infofile_;_ZIP::ssf1.xml_;_
		Browser("Admin Home Page").Page("Admin Users").Link("Admin Users Administration").Click @@ script infofile_;_ZIP::ssf2.xml_;_
		'====Add Admin User
		Browser("Admin Home Page").Page("Admin Users_2").WebElement("img_0").Click @@ script infofile_;_ZIP::ssf3.xml_;_
		
		Browser("Admin Home Page").Page("Admin Users_2").Image("Edit").Click
		
		' @@ script infofile_;_ZIP::ssf5.xml_;_
		 
		'Browser("Admin Home Page").Page("Admin Users_2").Frame("iframe_3508").WebEdit("AdminUserFirstName").Set "adminNew Admin User" @@ script infofile_;_ZIP::ssf23.xml_;_
		
		Browser("Admin Home Page").Page("Admin Users_2").Frame("iframe_3507").WebEdit("AdminUserFirstName").Set Datatable.Value("AdFname","AdAdminuser") @@ script infofile_;_ZIP::ssf6.xml_;_
		Browser("Admin Home Page").Page("Admin Users_2").Frame("iframe_3507").WebEdit("AdminUserLastName").Set Datatable.Value("AdLname","AdAdminuser") @@ script infofile_;_ZIP::ssf8.xml_;_
 @@ hightlight id_;_197854_;_script infofile_;_ZIP::ssf9.xml_;_
		Browser("Admin Home Page").Page("Admin Users_2").Frame("iframe_3507").WebEdit("AdminUserEmail").Set Datatable.Value("AdUsremail","AdAdminuser") @@ script infofile_;_ZIP::ssf10.xml_;_
		Browser("Admin Home Page").Page("Admin Users_2").Frame("iframe_3507").WebEdit("AdminUserPassword").SetSecure Datatable.Value("Adpwd","AdAdminuser") @@ script infofile_;_ZIP::ssf11.xml_;_
		
		'====Receive email
		Browser("Admin Home Page").Page("Admin Users_2").Frame("iframe_3507").WebCheckBox("ReceivesEmailCustomer").Set "ON" @@ script infofile_;_ZIP::ssf12.xml_;_
		Browser("Admin Home Page").Page("Admin Users_2").Frame("iframe_3507").WebCheckBox("ReceivesEmailSpectrum").Set "ON" @@ script infofile_;_ZIP::ssf13.xml_;_
 @@ script infofile_;_ZIP::ssf14.xml_;_
		Browser("Admin Home Page").Page("Admin Users_2").Frame("iframe_3507").WebCheckBox("ReceivesEmailComplaint").Set "ON" @@ script infofile_;_ZIP::ssf15.xml_;_
		Browser("Admin Home Page").Page("Admin Users_2").Frame("iframe_3507").WebCheckBox("RevievesEmailDealer").Set "ON" @@ script infofile_;_ZIP::ssf16.xml_;_
		Browser("Admin Home Page").Page("Admin Users_2").Frame("iframe_3507").WebCheckBox("ReceivesEmailTypeApproval").Set "ON" @@ script infofile_;_ZIP::ssf17.xml_;_
		If Browser("Admin Home Page").Page("Admin Users_2").Frame("iframe_3507").WebCheckBox("ReceivesEmailSystemandService").Exist(5) Then
			Browser("Admin Home Page").Page("Admin Users_2").Frame("iframe_3507").WebCheckBox("ReceivesEmailSystemandService").Set "ON"
		End If @@ script infofile_;_ZIP::ssf18.xml_;_
		If Browser("Admin Home Page").Page("Admin Users_2").Frame("iframe_3507").WebCheckBox("ReceivesEmail").Exist(6) Then
			Browser("Admin Home Page").Page("Admin Users_2").Frame("iframe_3507").WebCheckBox("ReceivesEmail").Set "ON"
		End If @@ script infofile_;_ZIP::ssf19.xml_;_
		If Browser("Admin Home Page").Page("Admin Users_2").Frame("iframe_3507").WebCheckBox("ReceivesEmailPostalOperator").Exist(7) Then
			Browser("Admin Home Page").Page("Admin Users_2").Frame("iframe_3507").WebCheckBox("ReceivesEmailPostalOperator").Clicks
		End If
 @@ script infofile_;_ZIP::ssf24.xml_;_
		'====Status
		Browser("Admin Home Page").Page("Admin Users_2").Frame("iframe_3507").WebList("AdminUserStatusID").Select(2)' "Active" @@ script infofile_;_ZIP::ssf21.xml_;_
		'Update Admin user button
		Browser("Admin Home Page").Page("Admin Users_2").Frame("iframe_3507").WebElement("img_0").Click @@ script infofile_;_ZIP::ssf22.xml_;_

End If
strTestCaseName = Environment.Value("TestName")
	
'Test results dsiplayed in Excel
    If err.number = 0 Then
        blnFind = True
    Else
        blnFind = False
    End If
    
    UpdateTestCaseStatusInExcel strTestCaseName, blnFind 
