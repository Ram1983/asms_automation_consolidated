﻿Wait(1)
blnFind=False
err.number = 0


If Lcase(Environment.Value("Client"))="moz" Then
	fn_NavigateToScreen "Contabilidade;Informação do Cliente"
Else
   fn_NavigateToScreen("Accounting;Customer Information")
End If @@ script infofile_;_ZIP::ssf78.xml_;_

Wait(2)
'================= Menu Navigation completed

datatable.ImportSheet ProjectFolderPath &"\Input_Data_Sheet\Input_Data_Sheet.xls","AddCustomer_Person","AddCustomer_Person"
n=datatable.GetSheet("AddCustomer_Person").GetRowCount

'For i = 1 To n Step 1
With VbWindow("frmMDI").VbWindow("frmBrowser")
	If n>0 Then
		datatable.SetCurrentRow(1)	

		
		While Not .Page("Page").Link("Add New Customer").Exist(40) @@ hightlight id_;_1772618_;_script infofile_;_ZIP::ssf3.xml_;_
		Wend
		Wait(3)

		'==Add new customer
		.Page("Page").Link("Add New Customer").Click @@ hightlight id_;_VbWindow("frmMDI").VbWindow("frmBrowser").Page("Page").Link("Add New Customer")_;_script infofile_;_ZIP::ssf4.xml_;_
		Wait(2)

		'==Radio Operator
		.Page("Page_2").WebElement("img_0").Click @@ hightlight id_;_VbWindow("frmMDI").VbWindow("frmBrowser").Page("Page 2").WebElement("img 0")_;_script infofile_;_ZIP::ssf71.xml_;_
		Wait(2)

		'==Edit
		.Page("Page_3").Image("Edit").Click @@ hightlight id_;_VbWindow("frmMDI").VbWindow("frmBrowser").Page("Page 3").Image("Edit")_;_script infofile_;_ZIP::ssf6.xml_;_
		Wait(3)


		Wait(2)
		With	.Page("Edit")
			.WebEdit("client__clientFname").Set Datatable.Value("AcFname","AddCustomer_Person") & "" & fnRandomNumber(3)'"acperson1" @@ hightlight id_;_VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebEdit("client  clientFname")_;_script infofile_;_ZIP::ssf51.xml_;_
			Wait(2)
			.WebEdit("client__clientLname").Set Datatable.Value("AcLname","AddCustomer_Person") & "" & fnRandomNumber(3)'"aclname12" @@ hightlight id_;_VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebEdit("client  clientLname")_;_script infofile_;_ZIP::ssf52.xml_;_
			Wait(2)
			'VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebList("client__clientIDType").Select(1) '"Driver License"
			'Wait(2)
			.WebEdit("client__ROT").Set Datatable.Value("Nid","AddCustomer_Person")'"125400" @@ hightlight id_;_VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebEdit("client  ROT")_;_script infofile_;_ZIP::ssf54.xml_;_
			'VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebList("client__nationality").Select(2) '"Adelie Land"
			Wait(2)

			'==Cust Num
			.WebElement("img_GCSclient__ccNum").Click @@ hightlight id_;_VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebElement("img GCSclient  ccNum")_;_script infofile_;_ZIP::ssf12.xml_;_
			If .WebElement("No").Exist(5) Then
				.WebElement("No").Click		
			End If



			'==Dealer
			isDealer = Datatable.Value("Dealer","AddCustomer_Person")				'Yes / No
			If (isDealer = "Yes") Then
				.WebCheckBox("client__Dealer").Set "ON"
			End If

			Wait(2)
			.WebEdit("client__clientTelNum").Set Datatable.Value("Tel","AddCustomer_Person")'"120445669" @@ hightlight id_;_VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebEdit("client  clientTelNum")_;_script infofile_;_ZIP::ssf56.xml_;_
			.WebElement("Address").Click @@ hightlight id_;_VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebElement("Address")_;_script infofile_;_ZIP::ssf57.xml_;_
			Wait(2)
			.WebList("client__clientCity1").Select(1) '"Aero Drome (Extension 127)" @@ hightlight id_;_VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebList("client  clientCity1")_;_script infofile_;_ZIP::ssf58.xml_;_
			.WebList("client__clientCity2").Select(1) '"Aero Drome (Extension 127)" @@ hightlight id_;_VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebList("client  clientCity2")_;_script infofile_;_ZIP::ssf59.xml_;_
			.WebElement("Email").Click @@ hightlight id_;_VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebElement("Email")_;_script infofile_;_ZIP::ssf60.xml_;_
			Wait(2)
			'===Email
			.WebEdit("client__clientEmail").Set Datatable.Value("Email","AddCustomer_Person")'"gdffvf@spx.com" @@ hightlight id_;_VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebEdit("client  clientEmail")_;_script infofile_;_ZIP::ssf61.xml_;_
			'VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebEdit("client__Password").SetSecure Datatable.Value("Pass","AddCustomer_Person") '"5edb8ba1b8ea126e388239bf1f9732f48953c05906c7"
			'===Update
			.WebElement("img_0").Click @@ hightlight id_;_VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebElement("img 0")_;_script infofile_;_ZIP::ssf63.xml_;_
			Wait(2)
			'==Send verification mail
			'VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebElement("img_2").Click
			'Wait(2)
			'==Force verify email
			'VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebElement("img_1").Click
			'Wait(5)

			'===Refresh

			.WebElement("Information").Click
			Wait(2)
			.WebElement("Address").Click
			Wait(3)
			.WebElement("Email").Click
			Wait(3)

			'===Update
			.WebElement("img_0").Click
			Wait(4)

			'==cust inf landing
			.Link("Customer Information Landing").Click @@ hightlight id_;_VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").Link("Customer Information Landing")_;_script infofile_;_ZIP::ssf74.xml_;_
		End With

		'==Add contact
		With	.Page("Page_3")
			.WebElement("img_0").Click

			'===Pcontact
			pcontact = Datatable.Value("Pcontact","AddCustomer_Person")
			If (pcontact = "Yes") Then
				.WebCheckBox("PrimaryContact").Set "ON" @@ hightlight id_;_VbWindow("frmMDI").VbWindow("frmBrowser").Page("Page 3").WebCheckBox("PrimaryContact")_;_script infofile_;_ZIP::ssf72.xml_;_
				.WebElement("img_1").Click @@ hightlight id_;_VbWindow("frmMDI").VbWindow("frmBrowser").Page("Page 3").WebElement("img 1")_;_script infofile_;_ZIP::ssf76.xml_;_
			End If
			'===Lrepresentative
			lrep = Datatable.Value("LRepresent","AddCustomer_Person")
			If (lrep = "Yes") Then
				.WebCheckBox("IsLegalRep").Set "ON"
				.WebElement("img_1").Click
			End If

			'==Edit
			.Image("Edit_2").Click
		End With

		With	.Page("Edit")
			.WebEdit("client__clientFname").Set  Datatable.Value("Fname","AddCustomer_Person") & "" & fnRandomNumber(3)'"grass" @@ hightlight id_;_VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebEdit("client  clientFname")_;_script infofile_;_ZIP::ssf8.xml_;_
			.WebEdit("client__clientLname").Set Datatable.Value("Lname","AddCustomer_Person") & "" & fnRandomNumber(3)'"hoper123" @@ hightlight id_;_VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebEdit("client  clientLname")_;_script infofile_;_ZIP::ssf9.xml_;_
			.WebList("client__clientIDType").Select(1) '"Driver License" @@ hightlight id_;_VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebList("client  clientIDType")_;_script infofile_;_ZIP::ssf10.xml_;_
			.WebEdit("client__ROT").Set Datatable.Value("Nid","AddCustomer_Person")'"125444" @@ hightlight id_;_VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebEdit("client  ROT")_;_script infofile_;_ZIP::ssf11.xml_;_

			'==Dealer
			isDealer = Datatable.Value("Dealer","AddCustomer_Person")				'Yes / No
			If (isDealer = "Yes") Then
				.WebCheckBox("client__Dealer").Set "ON"
			End If

			'==Director
			isDirecter = Datatable.Value("Director","AddCustomer_Person")           'Yes / No
			If (isDirecter = "Yes") Then
				.WebCheckBox("client__Director").Set "ON" @@ hightlight id_;_VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebCheckBox("client  Director")_;_script infofile_;_ZIP::ssf15.xml_;_
			End If
			.WebEdit("client__clientTelNum").Set Datatable.Value("Tel","AddCustomer_Person")'"120548879" @@ hightlight id_;_VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebEdit("client  clientTelNum")_;_script infofile_;_ZIP::ssf35.xml_;_
			Wait(2)
			.WebEdit("client__clientEmail").Set Datatable.Value("Email","AddCustomer_Person")'"jkhm@spx.com" @@ hightlight id_;_VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebEdit("client  clientEmail")_;_script infofile_;_ZIP::ssf36.xml_;_
			Wait(2)
			.WebElement("Financial Information").Click @@ hightlight id_;_VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebElement("Financial Information")_;_script infofile_;_ZIP::ssf37.xml_;_
			.WebElement("Address").Click @@ hightlight id_;_VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebElement("WebElement")_;_script infofile_;_ZIP::ssf38.xml_;_
			Wait(2)
			.WebList("client__clientCity1").Select(1) '"Aero Drome (Extension 127)" @@ hightlight id_;_VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebList("client  clientCity1")_;_script infofile_;_ZIP::ssf39.xml_;_
			.WebList("client__clientCity2").Select(1) '"Aero Drome (Extension 127)" @@ hightlight id_;_VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebList("client  clientCity2")_;_script infofile_;_ZIP::ssf40.xml_;_
			Wait(3)
			'==Update
			.WebElement("img_0").Click @@ hightlight id_;_VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebElement("img 0")_;_script infofile_;_ZIP::ssf41.xml_;_
			Wait(3)
			'==Refresh
			.WebElement("Information").Click @@ hightlight id_;_VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebElement("Information")_;_script infofile_;_ZIP::ssf42.xml_;_
			Wait(2)
			.WebElement("Financial Information").Click @@ hightlight id_;_VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebElement("Financial Information")_;_script infofile_;_ZIP::ssf43.xml_;_
			Wait(2)
			.WebElement("Address").Click
		End With
		Wait(3) @@ hightlight id_;_VbWindow("frmMDI").VbWindow("frmBrowser").Page("Edit").WebElement("img 0")_;_script infofile_;_ZIP::ssf45.xml_;_


		'UIAWindow("Automated Spectrum Management").Maximize
		'UIAWindow("Automated Spectrum Management").UIAWindow("http://bocraasmswebcp1:61083//").UIAObject("Pane").UIATable("Table").UIATable("Table").UIATable("Table").UIATable("Table").UIARadioButton("requestor").Select
		'==update
		.Page("Page_3").WebElement("img_1").Click @@ hightlight id_;_VbWindow("frmMDI").VbWindow("frmBrowser").Page("Page 3").WebElement("img 1")_;_script infofile_;_ZIP::ssf48.xml_;_
		Wait(3)

		'===Close
		.Close

		strTestCaseName = Environment.Value("TestName")
		If err.number = 0 Then
			blnFind = True
			Else
				blnFind = False
		End If

		UpdateTestCaseStatusInExcel strTestCaseName, blnFind

		'VbWindow("vbname:=frmMDI").close		'Move this out of this action

	End If
End With

