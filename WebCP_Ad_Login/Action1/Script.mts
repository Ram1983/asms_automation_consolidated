﻿

blnFind=false
Err.number=0

'import data
datatable.ImportSheet ProjectFolderPath&"\Input_Data_Sheet.xls","Adlogin","Adlogin"
rowC=datatable.GetSheet("Adlogin").GetRowCount

For count = 1 To rowC Step 1
    DataTable.SetCurrentRow(count)
    If StrComp(Datatable.GetSheet("Adlogin").GetParameter("Priority").Value,"YES",1)=0 Then
    	
    	'----Launch application------
    	If Lcase(Environment("Browser"))="microsoftedge" Then
    		SystemUtil.Run "msedge.exe",Datatable.GetSheet("Adlogin").GetParameter("URL").Value	
    	ElseIf Lcase(Environment("Browser"))="firefox" Then
    		 SystemUtil.Run "Firefox.exe", Datatable.GetSheet("Adlogin").GetParameter("URL").Value	
    	ElseIf Lcase(Environment("Browser"))="chrome" Then
    		Systemutil.Run "chrome.exe",Datatable.GetSheet("Adlogin").GetParameter("URL").Value	
    	End If
		
		With Browser("title:=Admini.*Login").Page("title:=Admin.*Login")
		     Browser("title:=Admini.*Login").Maximize
		     .Sync
			 '====Admin Portal Login
			.WebEdit("name:=userid").Set Datatable.Value("username","Adlogin")
		    .WebEdit("name:=password").SetSecure Datatable.Value("Pwd","Adlogin")
			'Clicking login
			.WebElement("innertext:=Administration Login","html tag:=A").Click
			If Browser("openurl:=.*webcp.*").Page("url:=.*webcp.*").WebElement("innertext:=Admin Home Page.*","html tag:=B").Exist(20) Then
				Exit For
			End If
		
		End With
   End If

Next

strTestCaseName = Environment.Value("TestName")
	
'Test results dsiplayed in Excel
If err.number = 0  Then
   blnFind = True
End If
    
UpdateTestCaseStatusInExcel strTestCaseName, blnFind 

