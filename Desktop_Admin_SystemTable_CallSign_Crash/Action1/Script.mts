﻿blnVal=False
fn_NavigateToScreen "Administrative;System Table;Call Sign"
While Not VbWindow("frmMDI").VbWindow("frmCallsignSetup").VbButton("Add Category").Exist
Wend
VbWindow("frmMDI").VbWindow("frmCallsignSetup").VbButton("Add Category").Click
Wait 10
Val=GenerateRandomString(5)
With VbWindow("frmMDI")
	With .VbWindow("frmCallsignSetup")
		If .VbEdit("txtDescription").Exist(10) Then
			.VbEdit("txtDescription").Set Val
		End If
		If .ActiveX("TCIComboBox.tcb").VbComboBox("cboCallsignCat").Exist(10) Then
			.ActiveX("TCIComboBox.tcb").VbComboBox("cboCallsignCat").Select 2
		End If @@ hightlight id_;_2361514_;_script infofile_;_ZIP::ssf18.xml_;_
		.VbEdit("txtMinCallsign").Set RandomNumber(1,7) @@ hightlight id_;_593582_;_script infofile_;_ZIP::ssf3.xml_;_
		.VbEdit("txtMaxCallsign").Set RandomNumber(8,17) @@ hightlight id_;_593470_;_script infofile_;_ZIP::ssf4.xml_;_
		.AcxTable("SSDBGrid Control 3.1 -").SelectCell 1,1 @@ hightlight id_;_1378534_;_script infofile_;_ZIP::ssf5.xml_;_
		.AcxTable("SSDBGrid Control 3.1 -").SetCellData 1,1,RandomNumber(1,10) @@ hightlight id_;_1378534_;_script infofile_;_ZIP::ssf6.xml_;_
		.AcxTable("SSDBGrid Control 3.1 -").SelectCell 1,2 @@ hightlight id_;_1378534_;_script infofile_;_ZIP::ssf7.xml_;_
		.AcxTable("SSDBGrid Control 3.1 -").SetCellData 1,2,RandomNumber(1,10) @@ hightlight id_;_1378534_;_script infofile_;_ZIP::ssf8.xml_;_
		.AcxTable("SSDBGrid Control 3.1 -").SelectCell 1,3 @@ hightlight id_;_1378534_;_script infofile_;_ZIP::ssf9.xml_;_
		.AcxTable("SSDBGrid Control 3.1 -").SetCellData 1,3,RandomNumber(1,10) @@ hightlight id_;_1378534_;_script infofile_;_ZIP::ssf10.xml_;_
		.VbButton("Save").Click @@ hightlight id_;_1247574_;_script infofile_;_ZIP::ssf14.xml_;_
	End With
	With .Dialog("#32770")
		If .WinButton("Yes").Exist Then
			.WinButton("Yes").Click
		End If
	End With
	.VbWindow("frmCallsignSetup").ActiveX("TCIComboBox.tcb").VbComboBox("cboCallsignCat").Select val @@ hightlight id_;_593536_;_script infofile_;_ZIP::ssf17.xml_;_
End With
Wait 10
If Err.number=0 Then
	blnVal=True
End If

UpdateTestCaseStatusInExcel Environment("TestName"),blnVal
