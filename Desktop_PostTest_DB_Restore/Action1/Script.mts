﻿blnFind=True
err.number = 0

datatable.ImportSheet ProjectFolderPath &"\Input_Data_Sheet\Input_Data_Sheet.xls","Config","Config"
n=datatable.GetSheet("Config").GetRowCount

'For i = 1 To n Step 1 
If n>0 Then

datatable.SetCurrentRow(1)	

DB_Server=datatable.Value("DB_Server","Config")
DB_UserID=datatable.Value("UserID","Config")
DB_Password=datatable.Value("Password","Config")
DB_RestorePath=datatable.Value("DB_RestorePath","Config")

DB_Restore DB_Server, DB_UserID, DB_Password, DB_RestorePath

strTestCaseName = Environment.Value("TestName")
If err.number = 0 Then
	blnFind = True
Else
	blnFind = False
End If


UpdateTestCaseStatusInExcel strTestCaseName, blnFind
End If
