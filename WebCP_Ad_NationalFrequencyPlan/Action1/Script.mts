﻿ @@ hightlight id_;_591358_;_script infofile_;_ZIP::ssf1.xml_;_
 
blnFind=True
err.number = 0

'import data
datatable.ImportSheet ProjectFolderPath&"\Input_Data_Sheet.xls","Ad_NationalFrequencyplan","Ad_NationalFrequencyplan"
n=datatable.GetSheet("Ad_NationalFrequencyplan").GetRowCount


'For i = 1 To n Step 1
If n>0 Then
	datatable.SetCurrentRow(1)	
	
 	'Clicking National frequency plan
	With	Browser("Admin Home Page")
		.Page("Admin Home Page").Link("National Frequency Plan").Click @@ script infofile_;_ZIP::ssf2.xml_;_
		.Sync
		.Page("National Frequency Landing").Link("Full Plan").Click
		.Sync
	End With
	Browser("TCI Administration - National").Page("TCI Administration - National").Sync
	With Browser("Admin Home Page")
		If .Page("TCI Administration - National").WebTable("WebTable").Exist Then
			.Page("National Frequency Full").Link("VLF").Click
			.Sync
			If .Page("TCI Administration - National").WebTable("WebTable").Exist Then
				.Page("TCI Administration - National").Link("LF").Click
				.Sync
				If .Page("TCI Administration - National").WebTable("WebTable").Exist Then
					.Page("TCI Administration - National_2").Link("MF").Click
					.Sync
					If .Page("TCI Administration - National").WebTable("WebTable").Exist Then
						.Page("TCI Administration - National_3").Link("HF").Click
						.Sync
						If .Page("TCI Administration - National").WebTable("WebTable").Exist Then
							.Page("TCI Administration - National_4").Link("VHF").Click
							.Sync
							If .Page("TCI Administration - National").WebTable("WebTable").Exist Then
								.Page("TCI Administration - National_5").Link("UHF").Click
								.Sync
							End If
							If .Page("TCI Administration - National").WebTable("WebTable").Exist Then
								.Page("TCI Administration - National_6").Link("SHF").Click
								.Sync
							End If
							If .Page("TCI Administration - National").WebTable("WebTable").Exist Then
								.Page("TCI Administration - National_7").Link("EHF").Click
								.Sync
								If .Page("TCI Administration - National").WebTable("WebTable").Exist Then
								.Sync
									blnFind = True
								End If
							End If
						End If
					End If
				End If
			End If

		End If
	End With


	strTestCaseName = Environment.Value("TestName")
    UpdateTestCaseStatusInExcel strTestCaseName, blnFind 
	    
End If
