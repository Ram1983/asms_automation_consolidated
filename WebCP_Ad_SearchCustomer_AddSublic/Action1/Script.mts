﻿

blnFind=True
err.number = 0

'Import data from Input data sheet  from folder
datatable.ImportSheet ProjectFolderPath&"\Input_Data_Sheet.xls","AdAdminSearchCustomerAddSublic","AdAdminSearchCustomerAddSublic"
n=datatable.GetSheet("AdAdminSearchCustomerAddSublic").GetRowCount

'For i = 1 To n Step 1
If n>0 Then
	datatable.SetCurrentRow(1)	


	'Navigate to Home page
	Browser("Admin Home Page").Page("Admin Home Page").Link("Home").Click @@ script infofile_;_ZIP::ssf1.xml_;_
	
	'Search Customer
	Browser("Admin Home Page").Page("Admin Home Page").Link("Search Customer or Application").Click @@ script infofile_;_ZIP::ssf2.xml_;_
	
	'Customer Number Selection
	CustomerNumber=GetDataFromDB("select top 1  RTRIM(ccNum) from Client C where C.proj='"&Environment("Client") &"' order by c.clientid desc")
	Browser("Admin Home Page").Page("Page").WebList("ccnum").Select CustomerNumber @@ script infofile_;_ZIP::ssf3.xml_;_
	
	'Submit Search @@ script infofile_;_ZIP::ssf4.xml_;_
	Browser("Admin Home Page").Page("Page").Link("Submit Search").Click @@ script infofile_;_ZIP::ssf5.xml_;_
	
	'New application	
	Browser("Admin Home Page").Page("Page").Link("+").Click @@ script infofile_;_ZIP::ssf6.xml_;_
	
	
End If

strTestCaseName = Environment.Value("TestName")
'Test results dsiplayed in Excel
    If err.number = 0 Then
		blnFind = True
	Else
		blnFind = False
	End If
		    
	UpdateTestCaseStatusInExcel strTestCaseName, blnFind
