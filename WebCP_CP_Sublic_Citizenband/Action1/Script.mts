﻿

blnFind=True
err.number = 0


'Import data from Input data sheet  from folder
datatable.ImportSheet ProjectFolderPath&"\Input_Data_Sheet.xls","cpCitizenband","cpCitizenband"
n=datatable.GetSheet("cpCitizenband").GetRowCount

'For i = 1 To n Step 1
If n>0 Then
    datatable.SetCurrentRow(1)
    If Strcomp(Datatable.Value("Edit","cpCitizenband"),"yes",1)<>0 Then
        '====Base Station	
        If Datatable.value("Stationtype","cpCitizenband")="Base Station" Then	
            Browser("Customer Home Page_2").Page("Application").Frame("Frame").Link("Base Station").Clicks
            '======Mobile station
        ElseIf Datatable.value("Stationtype","cpCitizenband")="Mobile Station" Then
            Browser("Customer Home Page_2").Page("Application").Frame("Frame").Link("Mobile Station").Clicks	
            Browser("Customer Home Page_2").Page("Application").Frame("Frame_2").WebEdit("tbl_site__VehregNum").Set Datatable.value("VehicleReg","cpCitizenband")&fnRandomNumber(4)
            '=======portable Station
        ElseIf Datatable.value("Stationtype","cpCitizenband")="Portable Station" Then
            Browser("Customer Home Page_2").Page("Application").Frame("Frame").Link("Portable Station").Clicks	
        End If	
        
    End If
    If Strcomp(Datatable.Value("Edit","cpCitizenband"),"yes",1)<>0 Then
 @@ script infofile_;_ZIP::ssf40.xml_;_
       If Browser("Customer Home Page_2").Page("Application_2").Frame("Frame").WebEdit("tbl_site__stationName").Exist Then @@ script infofile_;_ZIP::ssf41.xml_;_
          Browser("Customer Home Page_2").Page("Application_2").Frame("Frame").WebEdit("tbl_site__stationName").Sets ""
       End If
       If Browser("Customer Home Page_2").Page("Application").Frame("Frame_2").WebEdit("tbl_site__numOfSites").Exist Then
       	   Browser("Customer Home Page_2").Page("Application").Frame("Frame_2").WebEdit("tbl_site__numOfSites").Set Datatable.value("Numberofsites","cpCitizenband")
       End If 
        If Browser("Customer Home Page_2").Page("Application").Frame("Frame_2").WebEdit("tbl_site__siteInfo12").Exist Then
           Browser("Customer Home Page_2").Page("Application").Frame("Frame_2").WebEdit("tbl_site__siteInfo12").Set Datatable.value("purpose","cpCitizenband")
       End If
    Else
        fn_EditSubLicenses "station",Datatable.Value("Stationtype","cpCitizenband"),Datatable.Value("typeOfStation","cpCitizenband")
        Browser("Customer Home Page_2").Page("Application").Frame("Frame_2").WebEdit("tbl_site__numOfSites").Sets ""
    End If
    
    fn_LoadpageInWebCP
    
    If Browser("Customer Home Page").Page("Application_2").Frame("Frame").WebList("tbl_site__siteCity").Exist Then
    	Browser("Customer Home Page").Page("Application_2").Frame("Frame").WebList("tbl_site__siteCity").Select 2
    End If @@ script infofile_;_ZIP::ssf42.xml_;_
    '====Save Station
    Browser("Customer Home Page_2").Page("Application").Frame("Frame_2").WebElement("img_0").Clicks
   fn_LoadpageInWebCP
    If  Strcomp(Datatable.Value("Edit","cpCitizenband"),"yes",1)<>0  Then
        
        '=====Adding Equipment-Transceiver
        Browser("Customer Home Page_2").Page("Application").Frame("Frame_2").WebElement("img_1").Clicks
      fn_LoadpageInWebCP
        
        Browser("Customer Home Page_2").Page("Application").Frame("Frame_3").WebList("tbl_equipment__ApprovalNum").Select(1)'"BOT/TA/TRANSCEIVER/3"
       
        
          With Browser("name:=Application").Page("title:=Application")
                If .WebList("html id:=tbl_equipment__ApprovalNum").Exist(40) Then
                    If Datatable.Value("EquipmentApprovalNum","cpCitizenband")="" Then
                        .WebList("html id:=tbl_equipment__ApprovalNum").Selects 1'"random"
                    Else 
                        .WebList("html id:=tbl_equipment__ApprovalNum").Selects  1'Datatable.Value("EquipmentApprovalNum ","cpCitizenband")
                    End If
                    For intC = 1 To 20 Step 1
                        If Instr(1,.WebList("html id:=tbl_equipment__equipMake","name:=tbl_equipment__equipMake").GetROProperty("selection"),"Select",1)>0 Then
                            Wait 3
                        Else
                            Exit For
                        End If
                    Next
                End If
            End With
            
        
         fn_LoadpageInWebCP
        Browser("Customer Home Page_2").Page("Application").Frame("Frame_3").WebEdit("tbl_equipment__equipSerialNum").Set Datatable.value("Equipserial","cpCitizenband")&fnRandomNumber(4)
    Else
        fn_EditSubLicenses "",Datatable.Value("Stationtype","cpCitizenband"),Datatable.Value("typeOfStation","cpCitizenband")
        Browser("Customer Home Page_2").Page("Application").Frame("Frame_3").WebEdit("tbl_equipment__equipSerialNum").Sets ""
        
    End If	
    
    '=====Save Equipment
    Browser("Customer Home Page_2").Page("Application").Frame("Frame_3").WebElement("img_0").Clicks
     fn_LoadpageInWebCP
    
End If	

'Test results dsiplayed in Excel
If err.number = 0 Then
    blnFind = True
Else
    blnFind = False
End If

UpdateTestCaseStatusInExcel strTestCaseName, blnFind		

'fn_SendStatusToExcelReport blnFind @@ script infofile_;_ZIP::ssf41.xml_;_
