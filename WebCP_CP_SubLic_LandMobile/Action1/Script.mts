﻿

blnFind=False
err.number = 0

'Import data from Input data sheet  from folder
datatable.ImportSheet ProjectFolderPath&"\Input_Data_Sheet.xls","cpLandmobile","cpLandmobile"
n=datatable.GetSheet("cpLandmobile").GetRowCount

With Browser("Customer Home Page").Page("Application")
    'For i = 1 To n Step 1
    If n>0 Then
        datatable.SetCurrentRow(1)		
        If Strcomp(Datatable.Value("Edit","cpLandmobile"),"yes",1)<>0 Then		
            '======Base station
            If Datatable.value("Stationtype","cpLandmobile")= "Base Station" Then
                
                .Frame("Frame").Link("Base Station").Clicks
                .Frame("Frame_2").WebEdit("tbl_site__siteStreet").Sets Datatable.value("Streetname","cpLandmobile")' "street1"
                .Frame("Frame_2").WebList("tbl_site__siteCity").Selects(1) '"Aero Drome (Extension 127)"
                .Frame("Frame_2").WebEdit("tbl_site__siteInfo16").Sets Datatable.value("Contactperson","cpLandmobile")'"reception"
                .Frame("Frame_2").WebEdit("tbl_site__numOfSites").Sets Datatable.value("Numofstation","cpLandmobile")'"3"
            ElseIf Datatable.value("Stationtype","cpLandmobile")="Mobile Station" Then
                .Frame("Frame").Link("Mobile Station").Clicks
                .Frame("Frame_2").WebEdit("tbl_site__numOfSites").Sets Datatable.value("Numofstation","cpLandmobile")
                'Save station	
            ElseIf Datatable.value("Stationtype","cpLandmobile")="Portable Station"  Then				
                '=========portable station
                .Frame("Frame").Link("Portable Station").Clicks	
                .Frame("Frame_2").WebEdit("tbl_site__numOfSites").Sets Datatable.value("Numofstation","cpLandmobile")
            ElseIf Datatable.value("Stationtype","cpLandmobile")="Receiver Station"  Then		
                'Receiverstation
                .Frame("Frame").Link("Receiver Station").Clicks	
                .Frame("Frame_2").WebList("tbl_site__siteCity").Selects(1)' "Aero Drome (Extension 127)"
                .Frame("Frame_2").WebEdit("tbl_site__siteInfo16").Sets Datatable.value("Contactperson","cpLandmobile")
                .Frame("Frame_2").WebEdit("tbl_site__numOfSites").Sets Datatable.value("Numofstation","cpLandmobile")				
            ElseIf Datatable.value("Stationtype","cpLandmobile")="Repeater Station" Then  
                .Frame("Frame").WebElement("Repeater Station").Highlight
                Setting.WebPackage("ReplayType") = 2
                'Repeater station
                .Frame("Frame").WebElement("Repeater Station").Click
                Setting.WebPackage("ReplayType") = 1
                .Frame("Frame_2").WebList("tbl_site__siteCity").Selects(1)' "Aero Drome (Extension 127)"
                .Frame("Frame_2").WebEdit("tbl_site__numOfSites").Sets Datatable.value("Numofstation","cpLandmobile")
            End If
        End If
        If Browser("name:=Application").Page("title:=Application").Webedit("html id:=tbl_site__nomRadius","name:=tbl_site__nomRadius").Exist(8) And  Strcomp(Datatable.Value("Edit","cpLandmobile"),"yes",1)<>0 Then
            Browser("name:=Application").Page("title:=Application").Webedit("html id:=tbl_site__nomRadius","name:=tbl_site__nomRadius").sets RandomNumber(1,10)
        ElseIf Strcomp(Datatable.Value("Edit","cpLandmobile"),"yes",1)=0 Then
            fn_EditSubLicenses "station",Datatable.Value("Stationtype","cpLandmobile"),Datatable.Value("typeOfStation","cpLandmobile")
            Browser("name:=Application").Page("title:=Application").Webedit("html id:=tbl_site__nomRadius","name:=tbl_site__nomRadius").sets RandomNumber(10,20)
        End If
        With Browser("Application").Page("Application")
        		  If .Frame("Frame").WebEdit("tbl_site__transArea").Exist(5) Then
				  	.Frame("Frame").WebEdit("tbl_site__transArea").Set GenerateRandomString(5)
				  End If
				    If .Frame("Frame").WebList("tbl_site__siteInfo110").Exist(5) Then
				   	.Frame("Frame").WebList("tbl_site__siteInfo110").Select 2
	             End If
        End With


 @@ script infofile_;_ZIP::ssf193.xml_;_
        
        'Save station
        .Frame("Frame_2").WebElement("img_0").Clicks	
        'Equipment adding
        If Strcomp(Datatable.Value("Edit","cpLandmobile"),"yes",1)<>0 Then
            If trim(datatable.value("Equipmenttype","cpLandmobile"))="Add Receiver" Then			
                .Frame("Frame_2").WebElement("img_1").Clicks
                '=========Transeiver
            ElseIf  trim(datatable.value("Equipmenttype","cpLandmobile"))="Add Transceiver" Then			
                .Frame("Frame_2").WebElement("img_2").Clicks
                
                '========Transmiitter
            ElseIf  trim(datatable.value("Equipmenttype","cpLandmobile"))="Add Transmitter" Then				
                .Frame("Frame_2").WebElement("img_3").Clicks
            End If	
        End If		
        'Equipment for adding 
        If  Datatable.value("Stationtype","cpLandmobile")= "Base Station" Or Datatable.value("Stationtype","cpLandmobile")="Receiver Station" Or Datatable.value("Stationtype","cpLandmobile")="Repeater Station"  Then
            If Strcomp(Datatable.Value("Edit","cpLandmobile"),"yes",1)<>0 Then
                .Frame("Frame_3").WebList("tbl_equipment__ApprovalNum").Selects(1)' "BOT/TA/TRANSCEIVER/3"
                .Frame("Frame_3").WebEdit("tbl_equipment__equipSerialNum").Sets Datatable.value("Equipserial","cpLandmobile")&fnRandomNumber(4)'"serial345"
                
                With Browser("name:=Application").Page("title:=Application")
                    If .WebList("html id:=tbl_equipment__ApprovalNum").Exist(40) Then
                        If Datatable.Value("EquipmentApprovalNum","cpLandmobile")="" Then
	                      	.WebList("html id:=tbl_equipment__ApprovalNum").Selects 1
	                    Else 
                            .WebList("html id:=tbl_equipment__ApprovalNum").Selects 1'Datatable.Value("EquipmentApprovalNum ","cpLandmobile")
                        End If
                        For intC = 1 To 20 Step 1
                            If Instr(1,.WebList("html id:=tbl_equipment__equipMake","name:=tbl_equipment__equipMake").GetROProperty("selection"),"Select",1)>0 Then
                                wait 3
                            Else
                                Exit For
                            End If
                        Next
                    End If
                End With
                fn_LoadpageInWebCP
                
                With Browser("name:=Application").Page("title:=Application")
                    If .WebList("html id:=tbl_antenna__ApprovalNum").Exist(40) Then
                        If Datatable.Value("AntennaApprovalNum","cpLandmobile")="" Then
                           wait 5
                       	  .WebList("html id:=tbl_antenna__ApprovalNum").Selects 1
                        Else
                             wait 5
                            .WebList("html id:=tbl_antenna__ApprovalNum").Select  Datatable.Value("AntennaApprovalNum","cpLandmobile")
                        End If
                        
                        For intC = 1 To 20 Step 1
                            If Instr(1,.WebList("html id:=tbl_antenna__equipMake","name:=tbl_antenna__equipMake").GetROProperty("selection"),"Select",1)>0 Then
                                wait 3
                            Else
                                Exit For
                                
                            End If
                        Next
                    End If
                End With     
                fn_LoadpageInWebCP
                
                '.Frame("Frame_3").WebList("tbl_antenna__equipMake").Select(1)' "ADI"
                For count = 1 To 20 Step 1
                	If Browser("name:=Application").Page("title:=Application").WebEdit("name:=tbl_antenna__transgain","html id:=tbl_antenna__transgain").Exist Then
                	   If Browser("name:=Application").Page("title:=Application").WebEdit("name:=tbl_antenna__transgain","html id:=tbl_antenna__transgain").GetROProperty("value")="" Then
                          wait 3
                       Else
                          Exit For
                       End If
                     Else
                       Exit For
                	End If
                Next
                .Frame("Frame_3").WebEdit("tbl_antenna__physicHt").Sets Datatable.value("Height","cpLandmobile")'"1"
                .Frame("Frame_3").WebEdit("tbl_antenna__mainLobeAzi").Sets Datatable.value("Mobilz","cpLandmobile")
                .Frame("Frame_3").WebEdit("tbl_antenna__tiltAngle").Sets Datatable.value("Titangle","cpLandmobile")'"3"
                .Frame("Frame_3").WebList("tbl_antenna__feedtype").Selects(1)' "Coaxial line"
                If Lcase(Datatable.Value("FeederLoss","cpLandmobile"))="cable" Then
                    If Browser("name:=Application").Page("title:=Application").WebRadioGroup("html id:=tbl_antenna__By.*","name:=tbl_antenna__ByCableOrLineAttenuation").Exist(8) Then
                        Browser("name:=Application").Page("title:=Application").WebRadioGroup("html id:=tbl_antenna__By.*","name:=tbl_antenna__ByCableOrLineAttenuation").Select 1
                        .Frame("Frame_3").WebEdit("tbl_antenna__FeedLength").sets Datatable.Value("Feedlength","cpLandmobile")
                    ElseIf Browser("name:=Application").Page("title:=Application").WebRadioGroup("html id:=tbl_antenna__By.*","name:=tbl_antenna__ByCableOrLineAttenuation").Exist(8) Then
                        Browser("name:=Application").Page("title:=Application").WebRadioGroup("html id:=tbl_antenna__By.*","name:=tbl_antenna__ByCableOrLineAttenuation").Select 2            
                        .Frame("Frame_3").WebEdit("tbl_antenna__LineAtten").sets Datatable.Value("Lineatten","cpLandmobile")	
                    End If 
                End If
                If .Frame("Frame_3").WebList("tbl_antenna__CableID").exist(8) Then
                    .Frame("Frame_3").WebList("tbl_antenna__CableID").Selects 1
                End If
            Else
                fn_EditSubLicenses "",Datatable.Value("Stationtype","cpLandmobile"),Datatable.Value("typeOfStation","cpLandmobile")
                .Frame("Frame_3").WebEdit("tbl_equipment__equipSerialNum").Sets ""
            End If
        End If 
        
        'Mobile station and portable station equipment details
        
        If Datatable.value("Stationtype","cpLandmobile")="Mobile Station" Or Datatable.value("Stationtype","cpLandmobile")="Portable Station"  Then
            If Strcomp(Datatable.Value("Edit","cpLandmobile"),"yes",1)<>0 Then
               If .Frame("Frame_3").WebList("tbl_equipment__ApprovalNum").Exist(40) Then
               	  .Frame("Frame_3").WebList("tbl_equipment__ApprovalNum").Selects(1)' "BOT/TA/TRANSCEIVER/3"	
               End If
              
                .Frame("Frame_3").WebEdit("tbl_equipment__equipSerialNum").Sets Datatable.value("Equipserial","cpLandmobile")&fnRandomNumber(4)
                .Frame("Frame_3").WebEdit("tbl_equipment__regno").Sets Datatable.value("Vehiclereg","cpLandmobile")&fnRandomNumber(4)
                If .Frame("Frame_3").WebList("tbl_antenna__ApprovalNum").Exist(40) Then
                    fn_LoadpageInWebCP
                 	.Frame("Frame_3").WebList("tbl_antenna__ApprovalNum").Select 1
                     fn_LoadpageInWebCP
                End If
                For count = 1 To 20 Step 1
                    If Browser("name:=Application").Page("title:=Application").WebEdit("name:=tbl_antenna__transgain","html id:=tbl_antenna__transgain").GetROProperty("value")="" Then
                        wait 3
                    Else
                        Exit For
                    End If
                Next
            Else 
                fn_EditSubLicenses "",Datatable.Value("Stationtype","cpLandmobile"),Datatable.Value("typeOfStation","cpLandmobile")
                .Frame("Frame_3").WebEdit("tbl_equipment__equipSerialNum").Sets ""
            End If
            
        End If
        
        'Save Equipment	
        .Frame("Frame_3").WebElement("img_0").Clicks
        Wait 10
    End If 	
    
End With

strTestCaseName = Environment.Value("TestName")
'Test results dsiplayed in Excel
If err.number = 0 Then
    blnFind = True
Else
    blnFind = False
End If

UpdateTestCaseStatusInExcel strTestCaseName, blnFind	
'fn_SendStatusToExcelReport blnFind







