﻿

blnFind=False
err.number = 0

With Browser("Admin Home Page").Page("Type Approval_2")
    
    'Import data from Input data sheet  from folder
    datatable.ImportSheet ProjectFolderPath&"\Input_Data_Sheet.xls","Ad_TypeApproval_Add","Ad_TypeApproval_Add"
    n=datatable.GetSheet("Ad_TypeApproval_Add").GetRowCount
    
    'For i = 1 To n Step 1
    If n>0 Then
        datatable.SetCurrentRow(1)
        'Clicking Type approval
        Browser("Admin Home Page").Page("Admin Home Page").Link("Equipment Type Approval").Click
        wait 10
        Browser("Admin Home Page").Page("Admin Home Page").Sync	
        'A=====dd Type approval 
        If Datatable.Value("Functionalitytype","Ad_TypeApproval_Add")="ADD" Then
            Browser("Admin Home Page").Page("Type Approval").Link("Add Type Approval").Click
            wait 10
            '========Edit
        ElseIf Datatable.Value("Functionalitytype","Ad_TypeApproval_Add")="Edit"  Then
            
            'Edit the customer ID
            Browser("Type Approval").Page("Type Approval_2").WebList("select").Select Datatable.Value("Customerid","Ad_TypeApproval_Add")
            
            'Browser("Type Approval").Page("Type Approval").WebList("select").Select Datatable.Value("Customerid","Ad_TypeApproval_Add")
            Browser("Type Approval").Page("Type Approval").Image("edit").Click
            
        End If
        '				
        Set send=CreateObject("Wscript.Shell")
        .Sync
        wait 10
        If Browser("Type Approval").Page("Type Approval_4").WebEdit("WebEdit").Exist Then

        	Browser("Type Approval").Page("Type Approval_4").WebEdit("WebEdit").Set Datatable.Value("ClientName","Ad_TypeApproval_Add")
        End If
          
        Browser("Type Approval").Page("Type Approval_4").Image("combo_select_dhx_skyblue").Click
        Browser("Type Approval").Page("Type Approval_4").WebElement("xpath:=(//div[@class='dhx_selected_option combo_dhx_skyblue_sel'])[3][1]").click
        wait 5
     	Browser("Type Approval").Page("Type Approval_4").Image("combo_select_dhx_skyblue_3").Click @@ script infofile_;_ZIP::ssf131.xml_;_
		Wait 2
		Browser("Type Approval").Page("Type Approval_4").WebElement("xpath:=(//div[@class='dhx_combo_list dhx_skyblue_list'][4]//following::div)[1]").click
        .WebEdit("SupplierTelnum2").Set Datatable.Value("TelNum","Ad_TypeApproval_Add")
        .WebEdit("SupplierFaxnum2").Set Datatable.Value("FaxNum","Ad_TypeApproval_Add")
        .WebEdit("contact2").Set Datatable.Value("Contact","Ad_TypeApproval_Add")
        If .WebEdit("SupplierEmailid2").Exist(5) Then
            .WebEdit("SupplierEmailid2").Set Datatable.Value("Email","Ad_TypeApproval_Add")
        End If
        If .WebEdit("Contact3").Exist(10) Then
        	.WebEdit("Contact3").Set GenerateRandomString(5)
        End If @@ script infofile_;_ZIP::ssf142.xml_;_
        .WebEdit("SupplierWebAddress2").Set Datatable.Value("Web","Ad_TypeApproval_Add")
        
        'Repair Service provider 
        .Image("combo_select_dhx_skyblue_2").Click
        Browser("Type Approval").Page("Type Approval_4").WebElement("xpath:=(//div[@class='dhx_combo_list dhx_skyblue_list'][2]//following::div)[1]").click
        Set send=CreateObject("Wscript.Shell")
        .WebEdit("Contact3").Set Datatable.Value("Repaircontact","Ad_TypeApproval_Add")
        If .WebEdit("SupplierEmailid3").Exist(5) Then
            .WebEdit("SupplierEmailid3").Set Datatable.Value("Repairemail","Ad_TypeApproval_Add")
        End If
        .WebEdit("SupplierWebAddress3").Set Datatable.Value("RepairWeb","Ad_TypeApproval_Add")
        
        'Click Update
        .WebElement("img_1").Click
        
        'Request approval
        .WebElement("Requested Approval").Click
        wait 3
        .WebCheckBox("attachment1").Set "ON"
        .WebEdit("technical1").Set Datatable.Value("AllreportsTech1","Ad_TypeApproval_Add")&fnRandomNumber(4)
        .WebEdit("intention1").Set Datatable.Value("allrepcarriedout","Ad_TypeApproval_Add")
        .WebCheckBox("attachment2").Set "ON"
        .WebEdit("technical2").Set Datatable.Value("WithoutCertifyNum","Ad_TypeApproval_Add")&fnRandomNumber(4)
        .WebEdit("intention2").Set Datatable.Value("withoutcarriedby","Ad_TypeApproval_Add")
        .WebCheckBox("attachment3").Set "ON"
        .WebEdit("technical3").Set Datatable.Value("Declarationconfirm","Ad_TypeApproval_Add")
        .WebCheckBox("attachment4").Set "ON"
        .WebEdit("technical4").Set Datatable.Value("SupplementryNum","Ad_TypeApproval_Add")&fnRandomNumber(4)
        .WebEdit("intention4").Set Datatable.Value("SupplyNB","Ad_TypeApproval_Add")
        .WebCheckBox("attachment5").Set "ON"
        .WebEdit("technical5").Set Datatable.Value("AnotherinstitutionNum","Ad_TypeApproval_Add")&fnRandomNumber(4)
        .WebEdit("intention5").Set Datatable.Value("AnotherinstiIssuedby","Ad_TypeApproval_Add")
        .WebEdit("intention3").Set Datatable.Value("Countryissue","Ad_TypeApproval_Add")
        .WebEdit("intention9").Set Datatable.Value("Telissuer","Ad_TypeApproval_Add")
        .WebEdit("intention10").Set Datatable.Value("Faxissuer","Ad_TypeApproval_Add")
        .WebCheckBox("attachment6").Set "ON"
        .WebEdit("technical6").Set Datatable.Value("Radioapprvalnum","Ad_TypeApproval_Add")&fnRandomNumber(4)
        .WebEdit("intention6").Set Datatable.Value("Radiocarriedby","Ad_TypeApproval_Add")
        .WebCheckBox("attachment7").Set "ON"
        .WebEdit("technical7").Set Datatable.Value("Radioapprovalcertifynum","Ad_TypeApproval_Add")
        .WebEdit("intention7").Set Datatable.Value("Radiocerifyissuedby","Ad_TypeApproval_Add")
        .WebCheckBox("attachment8").Set "ON"
        .WebEdit("technical8").Set Datatable.Value("Earlierequiprefnum","Ad_TypeApproval_Add")
        .WebEdit("intention8").Set Datatable.Value("earlierequipNB","Ad_TypeApproval_Add")
        'update
        .WebElement("img_4").Click
        
        'Equipment Information
        .WebElement("Equipment Information").Click
        .Image("combo_select_dhx_skyblue_3").Click
        '				Browser("Type Approval").Page("Type Approval_3").WebElement("class:=dhx_combo_list dhx_skyblue_list","html id:=","title:=","index:=0").Highlight
        send.SendKeys "{DOWN}"
        send.SendKeys "{DOWN}"
        send.SendKeys "{TAB}"
        .WebEdit("equiptype").Set Datatable.Value("TypeEquipment","Ad_TypeApproval_Add")
        .WebEdit("version").Set Datatable.Value("Version","Ad_TypeApproval_Add")
        .WebEdit("modulation").Set Datatable.Value("Descibeequipment","Ad_TypeApproval_Add")
        .WebEdit("emissionclass").Set Datatable.Value("Emmission","Ad_TypeApproval_Add")
        If .WebEdit("frequencyList").Exist(5) Then
            .WebEdit("frequencyList").Set Datatable.Value("otherfrequncies","Ad_TypeApproval_Add")
        End If
        
        'upgrade
        .WebElement("img_2").Click
        
        'official use
        .WebElement("Official Use").Click
        
        .WebList("LNBType").Select(4)' "Fees"
        .WebList("feecode").Select(1)' "Short"
        'upgrade
        .WebElement("img_3").Click
        
        'Attachments
        
        .WebElement("Attached Documents").Click
        .Sync
        val=fileCreate
        wait 5
    	If Strcomp(Datatable.Value("Client","Ad_TypeApproval_Add"),"botswana",1)=0 Then
      	   .Frame("Frame").WebList("DocumentType").Select "Test report"
        Else
           .Frame("Frame").WebList("DocumentType").Select "* Test report"
        End If
        
        wait 10
        .Sync
'        .Frame("Frame").WebFile("BrowserHidden").Set val
				Setting.WebPackage("ReplayType") = 2
				 Browser("Type Approval").Page("Type Approval_4").WebElement("xpath:=(//input[@id='BrowserHidden'])","index:=1").Click
				If Window("Google Chrome").Dialog("Open").WinEdit("File name:").Exist(20) Then
					Window("Google Chrome").Dialog("Open").WinEdit("File name:").Set val
					Window("Google Chrome").Dialog("Open").WinButton("Open").Click
				End If
				Setting.WebPackage("ReplayType") = 1
        .Frame("Frame").Image("Pressing Upload, the document").Click
        .Sync
        wait 10
        .Frame("Frame").WebList("DocumentType").Select "* Declaration of conformity/certificate"
        .Sync
        wait 10
'        .Frame("Frame").WebFile("BrowserHidden").Set val
				Setting.WebPackage("ReplayType") = 2
				 Browser("Type Approval").Page("Type Approval_4").WebElement("xpath:=(//input[@id='BrowserHidden'])","index:=1").Click
				If Window("Google Chrome").Dialog("Open").WinEdit("File name:").Exist(20) Then
					Window("Google Chrome").Dialog("Open").WinEdit("File name:").Set val
					Window("Google Chrome").Dialog("Open").WinButton("Open").Click
				End If
				Setting.WebPackage("ReplayType") = 1
        wait 10
        .Frame("Frame").Image("Pressing Upload, the document").Click
        .Sync
        wait 10
        .Frame("Frame").WebList("DocumentType").Select "* Brief technical description (Product sheet)"
        wait 10
'        .Frame("Frame").WebFile("BrowserHidden").Set val
				Setting.WebPackage("ReplayType") = 2
				 Browser("Type Approval").Page("Type Approval_4").WebElement("xpath:=(//input[@id='BrowserHidden'])","index:=1").Click
				If Window("Google Chrome").Dialog("Open").WinEdit("File name:").Exist(20) Then
					Window("Google Chrome").Dialog("Open").WinEdit("File name:").Set val
					Window("Google Chrome").Dialog("Open").WinButton("Open").Click
				End If
				Setting.WebPackage("ReplayType") = 1
        Wait 10
        .Frame("Frame").Image("Pressing Upload, the document").Click
        .Sync
        If Strcomp(Datatable.Value("Client","Ad_TypeApproval_Add"),"botswana",1)=0 Then
            If .Frame("Frame").WebList("DocumentType").Exist Then
                .Frame("Frame").WebList("DocumentType").Select "* Standards_EMC"
'                .Frame("Frame").WebFile("BrowserHidden").Set val
				Setting.WebPackage("ReplayType") = 2
				 Browser("Type Approval").Page("Type Approval_4").WebElement("xpath:=(//input[@id='BrowserHidden'])","index:=1").Click
				If Window("Google Chrome").Dialog("Open").WinEdit("File name:").Exist(20) Then
					Window("Google Chrome").Dialog("Open").WinEdit("File name:").Set val
					Window("Google Chrome").Dialog("Open").WinButton("Open").Click
				End If
				Setting.WebPackage("ReplayType") = 1
                Wait 10
                .Frame("Frame").Image("Pressing Upload, the document").Click
                .Sync
            End If
            If .Frame("Frame").WebList("DocumentType").Exist Then
                .Frame("Frame").WebList("DocumentType").Select "* Health and Safety"
'                .Frame("Frame").WebFile("BrowserHidden").Set val
				Setting.WebPackage("ReplayType") = 2
				 Browser("Type Approval").Page("Type Approval_4").WebElement("xpath:=(//input[@id='BrowserHidden'])","index:=1").Click
				If Window("Google Chrome").Dialog("Open").WinEdit("File name:").Exist(20) Then
					Window("Google Chrome").Dialog("Open").WinEdit("File name:").Set val
					Window("Google Chrome").Dialog("Open").WinButton("Open").Click
				End If
				Setting.WebPackage("ReplayType") = 1
                Wait 10
                .Frame("Frame").Image("Pressing Upload, the document").Click
                .Sync
            End If
            If .Frame("Frame").WebList("DocumentType").Exist(3) Then
                .Frame("Frame").WebList("DocumentType").Select "* RF Test Report"
'                .Frame("Frame").WebFile("BrowserHidden").Set val
				Setting.WebPackage("ReplayType") = 2
				 Browser("Type Approval").Page("Type Approval_4").WebElement("xpath:=(//input[@id='BrowserHidden'])","index:=1").Click
				If Window("Google Chrome").Dialog("Open").WinEdit("File name:").Exist(20) Then
					Window("Google Chrome").Dialog("Open").WinEdit("File name:").Set val
					Window("Google Chrome").Dialog("Open").WinButton("Open").Click
				End If
				Setting.WebPackage("ReplayType") = 1
                Wait 10
                .Frame("Frame").Image("Pressing Upload, the document").Click
                .Sync
            End If
           End If

If Browser("Type Approval").Page("Type Approval_4").WebElement("Other Details").Exist(30) Then
	Browser("Type Approval").Page("Type Approval_4").WebElement("Other Details").Click
	If Browser("Type Approval").Page("Type Approval_4").WebEdit("manufacturer").Exist(6) Then
		Browser("Type Approval").Page("Type Approval_4").WebEdit("manufacturer").Set Datatable.Value("Make","Ad_TypeApproval_Add")
		Browser("Type Approval").Page("Type Approval_4").WebEdit("model").Set Datatable.Value("Model","Ad_TypeApproval_Add")
		Browser("Admin Home Page").Page("Type Approval_4").WebEdit("lowerfrequency").Set RandomNumber(1,10)
		Browser("Admin Home Page").Page("Type Approval_4").WebEdit("upperfrequency").Set  RandomNumber(1,10) @@ script infofile_;_ZIP::ssf133.xml_;_
	End If
	Browser("Type Approval").Page("Type Approval_4").WebElement("img_1").Click
	Browser("Type Approval").Page("Type Approval_4").Sync
	Browser("Type Approval").Page("Type Approval_4").WebElement("Applicant Information").Click
	Browser("Type Approval").Page("Type Approval_4").Sync
End If

        'Approval application
        .WebElement("WebElement_3").Click
        .Sync
        If Lcase(Datatable.Value("Approve","Ad_TypeApproval_Add"))="yes" Then
        	Browser("Admin Home Page").Page("Type Approval_4").WebElement("img_21_0").Click
        	Status="submitted"
        ElseIf Lcase(Datatable.Value("Reject","Ad_TypeApproval_Add"))="yes" Then
            Browser("Admin Home Page").Page("Type Approval_4").WebElement("img_23_0").Click
            Status="rejected"
        ElseIf Lcase(Datatable.Value("Remand","Ad_TypeApproval_Add"))="yes" Then
           Browser("Admin Home Page").Page("Type Approval_4").WebElement("img_3_0").Click
           Browser("Admin Home Page").Page("Type Approval_4").WebEdit("RemandReason").Set GenerateRandomString(5) @@ script infofile_;_ZIP::ssf134.xml_;_
           Browser("Admin Home Page").Page("Type Approval_4").WebElement("img_3_TA_0").Click @@ script infofile_;_ZIP::ssf135.xml_;_
           Status="remanded"
        End If
          .Sync

    End If
    If Browser("Admin Home Page").Page("Type Approval_4").WebElement("StatusofApp").Exist(10) Then
    	If Instr(1,Browser("Admin Home Page").Page("Type Approval_4").WebElement("StatusofApp").GetROProperty("innertext"), Datatable.Value("StatusOfApp","Ad_TypeApproval_Add"),1)>0 Then
    	     blnFind=True
    	     If Lcase(Datatable.Value("License","Ad_TypeApproval_Add"))="yes" Then
    	         blnFind=False
    	        Browser("Type Approval").Page("Type Approval_4").WebElement("img_10_0").Click
    	        Status="licensed"
             ElseIf Lcase(Datatable.Value("Reject","Ad_TypeApproval_Add"))="yes" Then
                blnFind=False
                Browser("Type Approval").Page("Type Approval_4").WebElement("img_23_0").Click
                Status="rejected"
    	     End If
    	     If Instr(1,Browser("Admin Home Page").Page("Type Approval_4").WebElement("StatusofApp").GetROProperty("innertext"), Datatable.Value("StatusAfterApprove","Ad_TypeApproval_Add"),1)>0 Then
    	        blnFind=True
    	     End If
	       	       
   	    End If
    End If

    
End With
Wait 4
ID=	Trim(Browser("Type Approval").Page("Type Approval").WebElement("keyTypeApprovalID").GetROProperty("innertext"))
Set objExcel = CreateObject("Excel.Application")
Set objWB = objExcel.Workbooks.open("C:\ASMS_Automation\WebCP_Input_Data_Sheet\Input_Data_Sheet.xls")
Set obj=objWB.Worksheets("Ad_TypeApproval_Add")
rowc=obj.usedrange.rows.count
obj.cells(2,3).value=ID
Wait 5
objExcel.ActiveWorkbook.Save
objExcel.Quit
Set objExcel= nothing
Set objWB =Nothing

strTestCaseName = Environment.Value("TestName")

fn_EmailLogin DataTable.Value("Email","Ad_TypeApproval_Add"),DataTable.Value("Password","Ad_TypeApproval_Add"),DataTable.Value("URL","Ad_TypeApproval_Add")

Set obj=Browser("version:=internet explorer.*","openurl:=https://.*mail.google.com/mail.*").Page("url:=https://mail.google.com/mail.*")
Set odesc=Description.Create
odesc("micclass").value="Link"
odesc("innertext").value=".*New Type Approval has been submitted.*"
Set Val=obj.ChildObjects(odesc)
For i = 0 To Val.count-1 Step 1
    Val(i).Click
    Wait 2
    If Val(i).exist(5) Then
    	Val(i).Click
    	Wait 10
    	If Val(i).exist Then
    		Val(i).Click
    	End If
    End If
    Exit For
Next

Set verify=Description.Create
verify("innertext").value=".*"&Status&".*"
Set verifyEmail=Browser("version:=internet explorer.*","openurl:=https://.*mail.google.com/mail.*").Page("url:=https://mail.google.com/mail.*").ChildObjects(verify)
For count = 0 To verifyEmail.count-1 Step 1
    verifyEmail(count).highlight
Next
Wait 3
    
    

'Test results dsiplayed in Excel
If err.number = 0 Then
    blnFind = True
Else
    blnFind = False
End If

UpdateTestCaseStatusInExcel strTestCaseName, blnFind 







