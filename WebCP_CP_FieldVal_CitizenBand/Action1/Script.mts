﻿
 
blnFind=False
err.number = 0

'Import data from Input data sheet  from folder
datatable.ImportSheet ProjectFolderPath&"\Input_Data_Sheet\Input_Data_Sheet.xls","cpCitizenband","Global"
n=datatable.GetSheet("Global").GetRowCount

'For i = 1 To n Step 1
If n>0 Then
    datatable.SetCurrentRow(1)
	
	If Browser("name:=Application","title:=Application").Page("title:=Application").WebElement("innertext:=TYPE:.*"&Datatable.Value("typeOfStation","Global")&".*","html tag:=SPAN").Exist Then
		Browser("name:=Application","title:=Application").Page("title:=Application").WebElement("innertext:=TYPE:.*"&Datatable.Value("typeOfStation","Global")&".*","html tag:=SPAN").Click
	End If
	wait 5
	If Browser("name:=Application","title:=Application").Page("title:=Application").Link("html id:=img.*","name:="&Datatable.Value("Stationtype","Global")).Exist(20) Then
	      Browser("name:=Application","title:=Application").Page("title:=Application").Link("html id:=img.*","name:="&Datatable.Value("Stationtype","Global")).Click
	 End If
End If
Set obj=Browser("name:=Application","title:=Application").Page("title:=Application").WebEdit("xpath:=(//*[@name='tbl_site__stationName'])[1]")
If obj.Exist Then
   fn_WebCPSubLic_FieldLevelVal obj,"alphanumeric",500,""
   fn_WebCPSubLic_FieldLevelVal obj,"","","Station Location"
End If
Set obj=Browser("name:=Application","title:=Application").Page("title:=Application").WebEdit("html id:=tbl_site__VehregNum","name:=tbl_site__VehregNum")
If obj.Exist Then
   fn_WebCPSubLic_FieldLevelVal obj,"alphanumeric",500,""
   fn_WebCPSubLic_FieldLevelVal obj,"","","Vehicle Registration"
End If


If Browser("name:=Application","title:=Application").Page("title:=Application").WebElement("innertext:=.*"&Datatable.Value("Equipmenttype","Global"),"html id:=img.*").exist Then
     Browser("name:=Application","title:=Application").Page("title:=Application").WebElement("innertext:=.*"&Datatable.Value("Equipmenttype","Global"),"html id:=img.*").click
     Browser("name:=Application","title:=Application").Page("title:=Application").Sync
     Wait 10     
    Set objEquipSerialNum=Browser("name:=Application","title:=Application").Page("title:=Application").WebEdit("name:=.*equipSerialNum")
	If objEquipSerialNum.Exist Then
	   fn_WebCPSubLic_FieldLevelVal objEquipSerialNum,"alphanumeric",500,""
	   fn_WebCPSubLic_FieldLevelVal objEquipSerialNum,"","","Equipment Serial Number"
	End If
End If

Set objRadius=Browser("name:=Application","title:=Application").Page("title:=Application").WebEdit("html id:=tbl_equipment__lowerFrequency_lower","name:=tbl_equipment__lowerFrequency")
If objRadius.Exist Then
   fn_WebCPSubLic_FieldLevelVal objRadius,"numeric",1,""
   fn_WebCPSubLic_FieldLevelVal objRadius,"","","Lower and Upper Frequency"
End If
Set objRadius=Browser("name:=Application","title:=Application").Page("title:=Application").WebEdit("html id:=tbl_equipment__upperFrequency_upper","name:=tbl_equipment__upperFrequency_upper")
If objRadius.Exist Then
   fn_WebCPSubLic_FieldLevelVal objRadius,"numeric",1,""
   fn_WebCPSubLic_FieldLevelVal objRadius,"","","Lower and Upper Frequency"
End If
Set objRadius=Browser("name:=Application","title:=Application").Page("title:=Application").WebEdit("html id:=tbl_equipment__upperFrequency_upper","name:=tbl_equipment__upperFrequency_upper")
If objRadius.Exist Then
   fn_WebCPSubLic_FieldLevelVal objRadius,"numeric",1,""
   fn_WebCPSubLic_FieldLevelVal objRadius,"","","Lower and Upper Frequency"
End If

    Set objAntennaPower=Browser("name:=Application","title:=Application").Page("title:=Application").WebEdit("html id:=tbl_equipment__antennaPower","name:=tbl_equipment__antennaPower")
	If objAntennaPower.Exist Then
	   fn_WebCPSubLic_FieldLevelVal objAntennaPower,"numeric",10000001,""
	   fn_WebCPSubLic_FieldLevelVal objAntennaPower,"","","Power to Antenna"
	End If



