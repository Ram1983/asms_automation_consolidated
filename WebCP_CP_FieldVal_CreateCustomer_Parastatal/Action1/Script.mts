﻿
' Filed Level validation with Empty 
blnValOne=False:blnValTwo=False:blnValThree=False:blnValFour=False:blnValFive=False

Set clickOnFields=Browser("Customer Portal - New").Page("Customer Portal - New").Link("Request Account")
Set firstName=Browser("Customer Portal - New").Page("Customer Portal - New").WebEdit("Eclient__clientFname")
Browser("Customer Portal - New").Page("Customer Portal - New").WebElement("Requestor Information").Click
If fn_FieldLevelValidation(firstName,"Empty","",clickOnFields,Browser("Customer Portal - New"),"Sign In To Your Account","opentitle")  Then
   blnValOne = True
   Reporter.ReportEvent micPass,"Validate First Name with empty Value","Successfully validated First Name with empty Value"
Else
   Reporter.ReportEvent micFail,"Validate First Name with empty Value","Failed to validated First Name with empty Value"
End If

Set lastName=Browser("Customer Portal - New").Page("Customer Portal - New").WebEdit("Rclient__clientLname")
Browser("Customer Portal - New").Page("Customer Portal - New").WebElement("Requestor Information").Click
If fn_FieldLevelValidation(lastName,"Empty","",clickOnFields,Browser("Customer Portal - New"),"Sign In To Your Account","opentitle")  Then
   blnValTwo = True
   Reporter.ReportEvent micPass,"Validate Last Name with empty Value","Successfully validated Last Name with empty Value"
Else
   Reporter.ReportEvent micFail,"Validate Last Name with empty Value","Failed to validated Last Name with empty Value"
End If

Set telephone=Browser("Customer Portal - New").Page("Customer Portal - New").WebEdit("Rclient__clientTelNum")
Browser("Customer Portal - New").Page("Customer Portal - New").WebElement("Requestor Information").Click
If fn_FieldLevelValidation(telephone,"Empty","",clickOnFields,Browser("Customer Portal - New"),"Sign In To Your Account","opentitle")  Then
   blnValThree = True
   Reporter.ReportEvent micPass,"Validate Telephone with empty Value in Requester Information screen","Successfully validated Telephone with empty Value in Requester Information screen"
Else
   Reporter.ReportEvent micFail,"Validate Telephone with empty Value in Requester Information screen","Failed to validated Telephone with empty Value in Requester Information screen"
End If
		
Set email=Browser("Customer Portal - New").Page("Customer Portal - New").WebEdit("Rclient__clientEmail")
Browser("Customer Portal - New").Page("Customer Portal - New").WebElement("Requestor Information").Click
If fn_FieldLevelValidation(email,"Empty","",clickOnFields,Browser("Customer Portal - New"),"Sign In To Your Account","opentitle")  Then
   blnValFour = True
   Reporter.ReportEvent micPass,"Validate Email with empty Value in Requester Information screen","Successfully Validated Email with empty Value in Requester Information screen"
Else
   Reporter.ReportEvent micFail,"Validate Email with empty Value in Requester Information screen","Failed to Validated Email with empty Value in Requester Information screen"
End If

Set password=Browser("Customer Portal - New").Page("Customer Portal - New").WebEdit("Rclient__Password")
Browser("Customer Portal - New").Page("Customer Portal - New").WebElement("Requestor Information").Click
If fn_FieldLevelValidation(password,"Empty","",clickOnFields,Browser("Customer Portal - New"),"Sign In To Your Account","opentitle")  Then
   blnValFive = True
   Reporter.ReportEvent micPass,"Validate password with empty Value in Requester Information screen","Successfully Validated password with empty Value in Requester Information screen"
Else
   Reporter.ReportEvent micFail,"Validate password with empty Value in Requester Information screen","Failed to Validated password with empty Value in Requester Information screen"
End If

Set company=Browser("Customer Portal - New").Page("Customer Portal - New").WebEdit("Eclient__clientCompany")
Browser("Customer Portal - New").Page("Customer Portal - New").WebElement("Customer Entity - Information").Click
If fn_FieldLevelValidation(company,"Empty","",clickOnFields,Browser("Customer Portal - New"),"Sign In To Your Account","opentitle")  Then
   blnValSix = True
   Reporter.ReportEvent micPass,"Validate Company with empty Value","Successfully validated Company with empty Value"
Else
   Reporter.ReportEvent micFail,"Validate Company with empty Value","Failed to validated Company with empty Value"
End If

Set companyName=Browser("Customer Portal - New").Page("Customer Portal - New").WebEdit("Eclient__clientCompany_2")
Browser("Customer Portal - New").Page("Customer Portal - New").WebElement("Customer Entity - Information").Click
If fn_FieldLevelValidation(companyName,"Empty","",clickOnFields,Browser("Customer Portal - New"),"Sign In To Your Account","opentitle")  Then
   blnValSeven = True
   Reporter.ReportEvent micPass,"Validate Company Name with empty Value","Successfully validated Company Name with empty Value"
Else
   Reporter.ReportEvent micFail,"Validate Company Name with empty Value","Failed to validated Company Name with empty Value"
End If

Set telephoneCustomerInfo=Browser("Customer Portal - New").Page("Customer Portal - New").WebEdit("Eclient__clientTelNum")
Browser("Customer Portal - New").Page("Customer Portal - New").WebElement("Customer Entity - Information").Click
If fn_FieldLevelValidation(telephoneCustomerInfo,"Empty","",clickOnFields,Browser("Customer Portal - New"),"Sign In To Your Account","opentitle")  Then
   blnValEight = True
   Reporter.ReportEvent micPass,"Validate Customer Telephone with empty Value in customer info screen","Successfully Validated Customer Telephone with empty Value in customer info screen"
Else
   Reporter.ReportEvent micFail,"Validate Customer Telephone with empty Value in customer info screen","Failed to Validated Customer Telephone with empty Value in customer info screen"
End If
Set emailCustomerInfo=Browser("Customer Portal - New").Page("Customer Portal - New").WebEdit("Eclient__clientEmail")
Browser("Customer Portal - New").Page("Customer Portal - New").WebElement("Customer Entity - Information").Click
If fn_FieldLevelValidation(emailCustomerInfo,"Empty","",clickOnFields,Browser("Customer Portal - New"),"Sign In To Your Account","opentitle")  Then
   blnValNine = True
   Reporter.ReportEvent micPass,"Validate Email with empty Value in customer info screen","Successfully validated Email with empty Value in customer info screen"
Else
   Reporter.ReportEvent micFail,"Validate Email with empty Value in customer info screen","Failed to validated Email with empty Value in customer info screen"
End If


If blnValOne And blnValTwo And blnValThree And blnValFour And blnValFive And blnValSix And blnValSeven and blnValEight And blnValNine Then
   	UpdateTestCaseStatusInExcel Environment.Value("TestName"), True
Else
   UpdateTestCaseStatusInExcel Environment.Value("TestName"), False
End If

