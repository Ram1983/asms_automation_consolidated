﻿blnVal=False
With VbWindow("frmMDI")
	.WinMenu("Menu").Select "Frequency;Frequency Allocations"
	With .VbWindow("frmFreqAlloc")
		While Not .ActiveX("MaskEdBox").Exist
		Wend
		Wait 5
		.ActiveX("MaskEdBox").Type RandomNumber(100,100) @@ hightlight id_;_2753790_;_script infofile_;_ZIP::ssf1.xml_;_
		.ActiveX("MaskEdBox_2").Type RandomNumber(10000,10000) @@ hightlight id_;_1313742_;_script infofile_;_ZIP::ssf3.xml_;_
		.ActiveX("TCIComboBox.tcb").VbComboBox("cboRegion").Select 1 @@ hightlight id_;_855838_;_script infofile_;_ZIP::ssf4.xml_;_
		.VbButton("Next").Click @@ hightlight id_;_1248064_;_script infofile_;_ZIP::ssf5.xml_;_
	End With
	With .VbWindow("frmITUFrequency")
		While Not .AcxTable("ITU Frequency Allocations").Exist
		Wend
		.VbButton("Close").Click @@ hightlight id_;_1313600_;_script infofile_;_ZIP::ssf7.xml_;_
	End With
End With

If Err.number=0 Then
	blnVal=True
End If

UpdateTestCaseStatusInExcel Environment("TestName"),blnVal
