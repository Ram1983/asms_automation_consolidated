﻿
blnFind=True
err.number = 0



'Import data from Input data sheet  from folder
datatable.ImportSheet ProjectFolderPath&"\Input_Data_Sheet.xls","Ad_AdminCreateCustomer","Ad_AdminCreateCustomer"
n=datatable.GetSheet("Ad_AdminCreateCustomer").GetRowCount

'For i = 1 To n Step 1
If n>0 Then
    datatable.SetCurrentRow(1)

    With  Browser("name:=.*Admin.*|.*asmswebcp.*|Edit").Page("url:=.*asmswebcp.*")
        
'        '===Screen navigation Customer creation in admin portal
        .Link("name:=Search Customer or Application","html id:=").Click
        
        '=====Add Customer
        .Link("name:=Add New Customer").Click

'        '====Person add new customer
'        
        If Datatable.value("Clienttype","Ad_AdminCreateCustomer")="Person" Then
            
           If  .WebElement("html id:=img_0","innertext:=Add Customer.*","index:=4").Exist(5) Then
           	 .WebElement("html id:=img_0","innertext:=Add Customer.*","index:=4").Click
           	 .Sync
           End If
            If .webelement("xpath:=//*[@id='frm_Person']//a").Exist Then
               	.webelement("xpath:=//*[@id='frm_Person']//a").DoubleClick
               	.Sync
            End If
           
            '===Edit clicking
            If .Image("html id:=img_0").Exist Then
            	.Image("html id:=img_0").Click
            	.Sync
            End If
            .Sync
            '====Customer information tab
            While Not .WebEdit("name:=.*clientFname").Exist
            Wend
            If .WebEdit("name:=.*clientFname").Exist(40) Then
            	.WebEdit("name:=.*clientFname").Set Datatable.Value("PFname","Ad_AdminCreateCustomer")&fnRandomNumber(4)
            End If
            If .WebEdit("name:=.*clientLname").Exist(8) Then
            	.WebEdit("name:=.*clientLname").Set Datatable.Value("PLname","Ad_AdminCreateCustomer")&fnRandomNumber(4)
            End If
            If .WebList("name:=.*clientIDType").Exist(5) Then
            	.WebList("name:=.*clientIDType").Select(2)' "National Id"
            End If
            .WebEdit("name:=.*ROT").Set Datatable.Value("Nid","Ad_AdminCreateCustomer")&fnRandomNumber(4)
            ''====Click CCustomer number genrated
            .WebElement("html id:=img_GCSclient__ccNum").Click
            .Sync
            If Browser("Admin Home Page").Page("Edit").WebElement("No").Exist(5) Then
                Browser("Admin Home Page").Page("Edit").WebElement("No").Click
                .Sync
            End If
            wait(2)
            If Datatable.value("Dealer","Ad_AdminCreateCustomer")="Dealer" Then 
                .Webcheckbox("html id:=client__Dealer").Set "ON"
            ElseIf Datatable.value("Director","Ad_AdminCreateCustomer")="Director"  Then
                .Webcheckbox("html id:=client__Director").Set "ON"
            End If
                
            If .WebEdit("html id:=client__clientTelNum").Exist(5) Then
            	.WebEdit("html id:=client__clientTelNum").Set Datatable.value("Tel","Ad_AdminCreateCustomer")
            End If
            If .WebEdit("html id:=client__ClientCellPhone").Exist(5) Then
            	.WebEdit("html id:=client__ClientCellPhone").Set Datatable.value("Cell","Ad_AdminCreateCustomer")
            End If
            If .WebEdit("html id:=client__clientFaxNum").Exist(5) Then
            	.WebEdit("html id:=client__clientFaxNum").Set Datatable.value("Fax","Ad_AdminCreateCustomer")
            End If
            If .WebEdit("html id:=client__clientEmail","visible:=true").Exist Then
            	.WebEdit("html id:=client__clientEmail","visible:=true").Set Datatable.value("email","Ad_AdminCreateCustomer")&RandomNumber(1,9999)&"@gmail.com"
            End If
     
            '====Clicking Update button
            .WebElement("innertext:=Update.*","html id:=img_0").Click
            'Financial information tab
  			.Sync
            If .WebElement("innertext:=Financial Information.*","html tag:=span").Exist Then
            	.WebElement("innertext:=Financial Information.*","html tag:=span").Click
            	.Sync
            End If
            'Exemption
            If Datatable.value("Exempt","Ad_AdminCreateCustomer")="Exempt" Then
                .WebCheckBox("client__legaltype").Set "ON"
            End If
            If .WebList("name:=client__accountSetCode").Exist(10) Then
            	.WebList("name:=client__accountSetCode").Select(1)' "Trade1-Mascom"
            End If
            'Financial tab update click
            .WebElement("innertext:=Update.*","html id:=img_0").Click
            .Sync
            '=====Address Tab

            If .WebElement("innertext:=Address","html tag:=span").Exist(10) Then
            	.WebElement("innertext:=Address","html tag:=span").Click
            	.Sync
            End If

            If .WebEdit("html id:=.*PlotNo").Exist(8) Then
            	.WebEdit("html id:=.*PlotNo").Set Datatable.value("Ploc","Ad_AdminCreateCustomer")
            End If
            If .WebEdit("html id:=.*Street1").Exist(8) Then
            	.WebEdit("html id:=.*Street1").Set Datatable.value("Pstreet","Ad_AdminCreateCustomer")
            End If
            If .WebList("html id:=.*clientCity1").Exist(5) Then
            	.WebList("html id:=.*clientCity1").Select(1)' "Aero Drome (Extension 127)"
            End If
            If .WebEdit("html id:=.*PostalLocation").Exist(5) Then
            	.WebEdit("html id:=.*PostalLocation").Set Datatable.value("postalloc","Ad_AdminCreateCustomer")
            End If
            If .WebEdit("html id:=.*clientStreet2").Exist(5) Then
            	.WebEdit("html id:=.*clientStreet2").Set Datatable.value("postvillage","Ad_AdminCreateCustomer")
            End If
            .WebElement("name:=client__PostalLocation").Click
            If .WebEdit("name:=client__clientZipCode1").Exist(5) Then
            	.WebEdit("name:=client__clientZipCode1").Set Datatable.value("postalpobox","Ad_AdminCreateCustomer")
            End If
            If .WebEdit("name:=client__clientDistrict2").Exist(5) Then
            	.WebEdit("name:=client__clientDistrict2").Set Datatable.value("postalta","Ad_AdminCreateCustomer")
            End If
            If .WebList("name:=client__clientCity2").Exist Then
            	.WebList("name:=client__clientCity2").Select(1)' "Aero Drome (Extension 127)"
            End If
            If  .WebElement("innertext:=Update.*","html id:=img_0").Exist Then
            	.WebElement("innertext:=Update.*","html id:=img_0").Click
            	.Sync
            End If
            '====Address tab update
            If .WebElement("innertext:=Address","html tag:=span").Exist(8) Then
            	.WebElement("innertext:=Address","html tag:=span").Click
            	.Sync
            End If
            
        End If
        ''=====Add Customer-Company or GoVERNAMENT OR NON GOVERNMENT OR parastatal
        
        If datatable.value("Clienttype","Ad_AdminCreateCustomer")="Company" Then
            
            If .WebElement("html id:=img_0","innertext:=Add Customer.*","index:=0").Exist(5) Then
            	.WebElement("html id:=img_0","innertext:=Add Customer.*","index:=0").Click
            	.Sync
            End If
            If .webelement("xpath:=//*[@id='frm_Company']//a").Exist(5) Then
            	.webelement("xpath:=//*[@id='frm_Company']//a").Click
            	.Sync
            End If
            
        ElseIf datatable.value("Clienttype","Ad_AdminCreateCustomer")="Government" Then
            
            If .WebElement("html id:=img_0","innertext:=Add Customer.*","index:=1").Exist(8) Then
            	.WebElement("html id:=img_0","innertext:=Add Customer.*","index:=1").Click
            	.Sync
            End If
            If .webelement("xpath:=//*[@id='frm_Government']//a").Exist(5) Then
            	.webelement("xpath:=//*[@id='frm_Government']//a").Click
            	.Sync
            End If
        ElseIf datatable.value("Clienttype","Ad_AdminCreateCustomer")="Non-Governmental Organization" Then
            If Browser("Admin Home Page").Page("Page_2").WebElement("img_3").Exist(8) Then
            	Browser("Admin Home Page").Page("Page_2").WebElement("img_3").Click
            	.Sync
            End If
            If .webelement("xpath:=//*[@id='frm_NonGovernmentalOrganization']//a").Exist(5) Then
            	.webelement("xpath:=//*[@id='frm_NonGovernmentalOrganization']//a").Click
            	.Sync
            End If
        ElseIf datatable.value("Clienttype","Ad_AdminCreateCustomer")="Parastatal" Then 
            .WebElement("html id:=img_0","innertext:=Add Customer.*","index:=3").Click
            .Sync
        ElseIf datatable.value("Clienttype","Ad_AdminCreateCustomer")="Corporation" Then
            .WebElement("html id:=img_0","innertext:=Add Customer.*","index:=2").Click
            .Sync
        End If 
        'Edit or adding information
        
        If datatable.value("Clienttype","Ad_AdminCreateCustomer")="Company" Or datatable.value("Clienttype","Ad_AdminCreateCustomer")="Government" Or datatable.value("Clienttype","Ad_AdminCreateCustomer")="Non-Governmental Organization"  Or datatable.value("Clienttype","Ad_AdminCreateCustomer")="Parastatal" Or datatable.value("Clienttype","Ad_AdminCreateCustomer")="Corporation" Then
            .Image("html id:=img_0").Click
            If datatable.value("Clienttype","Ad_AdminCreateCustomer")="Parastatal"  Then
            	If .WebEdit("html id:=.*Company","index:=0").Exist Then
            		.WebEdit("html id:=.*Company","index:=0").Set Datatable.Value("Orgname","Ad_AdminCreateCustomer")&fnRandomNumber(4) 
            	End If
            	If .WebEdit("html id:=.*Company","index:=1").Exist Then
            		.WebEdit("html id:=.*Company","index:=1").Set Datatable.Value("Orgname","Ad_AdminCreateCustomer")&fnRandomNumber(5)
            	End If
            else
               If .WebEdit("html id:=client__clientCompany").Exist Then
               		.WebEdit("html id:=client__clientCompany").Set Datatable.Value("Orgname","Ad_AdminCreateCustomer")&fnRandomNumber(4)
               End If
            End If
            
           If .WebEdit("name:=client__ClientWebAddress").Exist Then
              .WebEdit("name:=client__ClientWebAddress").Set Datatable.Value("webadd","Ad_AdminCreateCustomer")
           End If
            If .WebEdit("html id:=client__ROT").Exist Then
            	.WebEdit("html id:=client__ROT").Set Datatable.Value("Nid","Ad_AdminCreateCustomer")&fnRandomNumber(4)
            End If
            'Custmer number Genration
            If .WebElement("html id:=td_GCSclient__ccNum").Exist Then
           		 Setting.WebPackage("ReplayType") = 2
            	.WebElement("html id:=td_GCSclient__ccNum").Click
            	Setting.WebPackage("ReplayType") = 1
            	 	.Sync
            End If
            If  Lcase(Datatable.value("IsAmatureLicIncluded","Ad_AdminCreateCustomer"))="yes" Then
            	If Browser("AdminCustomer").Page("Edit_2").WebElement("Yes").Exist Then
            		Browser("AdminCustomer").Page("Edit_2").WebElement("Yes").Click
            	End If
            ElseIf Browser("Admin Home Page").Page("Edit").WebElement("No").Exist Then
                Browser("Admin Home Page").Page("Edit").WebElement("No").Click
                .Sync
            End If

            '=======Dealr
            If Datatable.value("Dealer","Ad_AdminCreateCustomer")="Dealer" Then
                .WebCheckBox("client__Dealer").Set "ON"
            End If
            If .WebEdit("html id:=client__clientTelNum").Exist(8) Then
            	.WebEdit("html id:=client__clientTelNum").Set Datatable.value("Tel","Ad_AdminCreateCustomer")
            End If
            If .WebEdit("html id:=client__ClientCellPhone").Exist(8) Then
            	.WebEdit("html id:=client__ClientCellPhone").Set Datatable.value("Cell","Ad_AdminCreateCustomer")
            End If
            If .WebEdit("html id:=client__clientFaxNum").Exist(8) Then
            	.WebEdit("html id:=client__clientFaxNum").Set Datatable.value("Fax","Ad_AdminCreateCustomer")
            End If
            If Not datatable.value("Clienttype","Ad_AdminCreateCustomer")="Company"  Then
               If  .WebEdit("html id:=client__clientEmail","visible:=true").Exist(8) Then
               	    .WebEdit("html id:=client__clientEmail","visible:=true").Set Datatable.value("email","Ad_AdminCreateCustomer")&RandomNumber(1,9999)&"@gmail.com"
               End If
            End If
            '===Information tab- update 
            
            .WebElement("innertext:=Update.*","html id:=img_0").Click
            .Sync
            '=====Financial information
            If .WebElement("innertext:=Financial Information.*","html tag:=span").Exist Then
            	.WebElement("innertext:=Financial Information.*","html tag:=span").Click
            	.Sync
            End If
            If Datatable.value("Exempt","Ad_AdminCreateCustomer")="Exempt" Then
                rowser("opentitle:=.*Administration.*").Page("url:=.*asmswebcp.*").WebCheckBox("name:=.*legaltype").Set "ON"
            End If
            
            If .WebList("name:=.*accountSetCode").Exist(8) Then
            	.WebList("name:=.*accountSetCode").Select(1)' "Trade1-Mascom"
            End If
            '-----Update
            .WebElement("innertext:=Update.*","html id:=img_0").Click
            .Sync
            '-----Address Tab
            If .WebElement("innertext:=Address","html tag:=span").Exist(8) Then
            	.WebElement("innertext:=Address","html tag:=span").Click
            	.Sync
            End If
            
            If .WebEdit("html id:=.*PlotNo").Exist(5) Then
            	.WebEdit("html id:=.*PlotNo").Set Datatable.value("Ploc","Ad_AdminCreateCustomer")
            End If
            If .WebEdit("html id:=.*Street1").Exist(5) Then
            	.WebEdit("html id:=.*Street1").Set Datatable.value("Pstreet","Ad_AdminCreateCustomer")
            End If
            If .WebList("html id:=.*clientCity1").Exist(5) Then
            	.WebList("html id:=.*clientCity1").Select(1)' "Aero Drome (Extension 127)"
            End If
            If .WebEdit("html id:=.*PostalLocation").Exist(5) Then
            	.WebEdit("html id:=.*PostalLocation").Set Datatable.value("postalloc","Ad_AdminCreateCustomer")
            End If
            If .WebEdit("html id:=.*clientStreet2").Exist(5) Then
            	.WebEdit("html id:=.*clientStreet2").Set Datatable.value("postvillage","Ad_AdminCreateCustomer")
            End If
            If .WebEdit("html id:=.*ZipCode.*").Exist(5) Then
            	.WebEdit("html id:=.*ZipCode.*").Set Datatable.value("postalpobox","Ad_AdminCreateCustomer")
            End If
            If .WebEdit("html id:=.*District.*").Exist(5) Then
            	.WebEdit("html id:=.*District.*").Set Datatable.value("postalta","Ad_AdminCreateCustomer")
            End If
            If .WebList("html id:=.*clientCity2").Exist(5) Then
            	.WebList("html id:=.*clientCity2").Select(1)' "Aero Drome (Extension 127)"
            End If
            'Financial update

	        End If
        
            If Browser("Edit").Page("Edit").WebElement("img_0").Exist Then
            	Browser("Edit").Page("Edit").WebElement("img_0").Click
            	.Sync
            End If
            .Sync
			If Browser("Edit").Page("Edit").WebElement("Email").Exist Then
				Browser("Edit").Page("Edit").WebElement("Email").Click
				.Sync
			End If
			.Sync
			If Browser("Edit").Page("Edit").WebEdit("client__clientEmail").Exist(8) Then
				Browser("Edit").Page("Edit").WebEdit("client__clientEmail").Set Datatable.value("email","Ad_AdminCreateCustomer")&RandomNumber(1,9999)&"@gmail.com"
			End If
			Browser("Edit").Page("Edit").WebElement("img_0").Click
			
			.Sync	
        '===Customer information landing tab
        
        If .Link("name:=Customer Information.*").Exist Then
        	.Link("name:=Customer Information.*").Click
        	.Sync
        End If
        
        '===Add Contact
        If .WebElement("html id:=img_0","title:=Add a New Contact to this Customer").Exist Then
        	.WebElement("html id:=img_0","title:=Add a New Contact to this Customer").Click
        	.Sync
        End If
        
        Wait(5)
        

        'Requestor selection
        If Browser("Admin Home Page").Page("Page").WebRadioGroup("xpath:=//input[@id='requestor']").Exist Then
        	Browser("Admin Home Page").Page("Page").WebRadioGroup("xpath:=//input[@id='requestor']").Click
        	.Sync
        End If
        If Datatable.value("PrimaryContact","Ad_AdminCreateCustomer")="Primarycontact" Then
            .WebCheckbox("name:=PrimaryContact").Set "ON"
        End If
        If Datatable.value("Legalrep","Ad_AdminCreateCustomer")="Legal"  Then
            Browser("Admin Home Page").Page("Page_3").WebCheckBox("IsLegalRep").Set "ON"
        End If 
        If Datatable.value("Director1","Ad_AdminCreateCustomer")="Director" Then
            Browser("Admin Home Page").Page("Page_3").WebCheckBox("IsDirector_22944").Set "ON"
            Browser("Admin Home Page").Page("Page_3").WebEdit("shares_22944").Set Datatable.value("Share","Ad_AdminCreateCustomer")
        End If
        
        'update click       
        .WebElement("innertext:=Update.*","html id:=img_0").Click
		.Sync

        'Edit customer info tab
        If .Image("html id:=img_0","alt:=Edit","index:=1").Exist Then
        	.Image("html id:=img_0","alt:=Edit","index:=1").Click
        	.Sync
        End If
        
        If .WebEdit("name:=client__clientFname").Exist Then
        	.WebEdit("name:=client__clientFname").Set Datatable.Value("PFname","Ad_AdminCreateCustomer")&fnRandomNumber(4)
        End If
        .WebEdit("name:=client__clientLname").Set Datatable.Value("PLname","Ad_AdminCreateCustomer")&fnRandomNumber(4)
        If .WebList("name:=client__clientIDType").Exist Then
        	.WebList("name:=client__clientIDType").Select(2)' "Driver License"
        End If
        If .WebEdit("name:=client__ROT").Exist Then
        	.WebEdit("name:=client__ROT").Set Datatable.Value("Nid","Ad_AdminCreateCustomer")&fnRandomNumber(4)
        End If
        If .WebList("name:=client__nationality").Exist Then
        	.WebList("name:=client__nationality").Select Datatable.value("Client","Ad_AdminCreateCustomer")
        End If
        If .WebEdit("name:=client__clientTelNum").Exist Then
        	.WebEdit("name:=client__clientTelNum").Set Datatable.value("Tel","Ad_AdminCreateCustomer")
        End If
        If .WebEdit("name:=client__ClientCellPhone").Exist Then
        	.WebEdit("name:=client__ClientCellPhone").Set Datatable.value("Cell","Ad_AdminCreateCustomer")
        End If
        If .WebEdit("name:=client__clientFaxNum").Exist Then
        	.WebEdit("name:=client__clientFaxNum").Set Datatable.value("Fax","Ad_AdminCreateCustomer")
        End If
        If .WebEdit("html id:=client__clientEmail","visible:=true").Exist Then
        	.WebEdit("html id:=client__clientEmail","visible:=true").Set Datatable.value("email","Ad_AdminCreateCustomer")&RandomNumber(1,9999)&"@gmail.com"
        End If
        
        '===Address tab
        If .WebElement("innertext:=Address","html tag:=span").Exist Then
        	.WebElement("innertext:=Address","html tag:=span").Click
        	.Sync
        End If
        If .WebEdit("html id:=.*PlotNo").Exist Then
        	.WebEdit("html id:=.*PlotNo").Set Datatable.value("Ploc","Ad_AdminCreateCustomer")
        End If
        If .WebEdit("name:=client__clientStreet1").Exist Then
        	.WebEdit("name:=client__clientStreet1").Set Datatable.value("Pstreet","Ad_AdminCreateCustomer")
        End If
        If .WebList("name:=client__clientCity1").Exist Then
        	.WebList("name:=client__clientCity1").Select(1)' "Aero Drome (Extension 127)"
        End If
        If .WebEdit("name:=client__PostalLocation").Exist Then
        	.WebEdit("name:=client__PostalLocation").Set datatable.value("postalloc","Ad_AdminCreateCustomer")
        End If
        If .WebEdit("name:=client__clientStreet2").Exist Then
        	.WebEdit("name:=client__clientStreet2").Set Datatable.value("postvillage","Ad_AdminCreateCustomer")
        End If
        If .WebEdit("name:=client__clientZipCode1").Exist Then
        	.WebEdit("name:=client__clientZipCode1").Set Datatable.value("postalpobox","Ad_AdminCreateCustomer")
        End If
        If .WebEdit("name:=client__clientDistrict2").Exist Then
        	.WebEdit("name:=client__clientDistrict2").Set Datatable.value("postalta","Ad_AdminCreateCustomer")
        End If
        If .WebList("name:=client__clientCity2").Exist Then
        	.WebList("name:=client__clientCity2").Select(1)' "Aero Drome (Extension 127)"
        End If
                
        '===email tab
        
        If .WebElement("innertext:=Email","html id:=","visible:=true","index:=0").Exist Then
           If .WebElement("innertext:=Email","html id:=","visible:=true","index:=0").Exist Then
           		.WebElement("innertext:=Email","html id:=","visible:=true","index:=0").Click
           		.Sync
           End If
            If .WebEdit("html id:=client__clientEmail","visible:=true").Exist Then
                .WebEdit("html id:=client__clientEmail","visible:=true").Set Datatable.value("email","Ad_AdminCreateCustomer")&RandomNumber(1,9999)&"@gmail.com"
            End If
           	If Browser("AdminCustomer").Page("Edit").WebEdit("client__Password").Exist(8) Then
			   Browser("AdminCustomer").Page("Edit").WebEdit("client__Password").Set Datatable.value("Password","Ad_AdminCreateCustomer")
			End If
            
           .WebElement("innertext:=Update.*","html id:=img_0").Click
            .Sync
            'send verification mail
            If Browser("Admin Home Page").Page("Edit").WebElement("img_1").Exist(8) Then
            	Browser("Admin Home Page").Page("Edit").WebElement("img_1").Click
            	.Sync
            End If
            'Force verify and approve for acess
            Browser("Admin Home Page").Page("Edit").WebElement("img_2").Click
            .Sync
            '.WebElement("img_0").Click
        End If
    End With	
End If

If err.number = 0 Then
    blnFind = True
Else
    blnFind = False
End If
    
UpdateTestCaseStatusInExcel Environment.Value("TestName"), blnFind













