﻿
blnFind=False
err.number = 0

'Import data from Input data sheet  from folder
datatable.ImportSheet ProjectFolderPath&"\Input_Data_Sheet.xls","WebCP_Ad_PostCPApprove","Global"
n=datatable.GetSheet("Global").GetRowCount
'For i = 1 To n Step 1
If n>0 Then
		datatable.SetCurrentRow(1)
		With Browser("Postal Operator").Page("Admin Home Page")
		     .Link("Home").Click
	    	.Sync
             .Link("POElement").Click @@ script infofile_;_ZIP::ssf12.xml_;_
			.Sync
			Wait 5
           Browser("Postal Operator").Page("Admin Home Page").WebElement("Firstrow").Click
           Wait 5
           Set obj=CreateObject("Wscript.Shell")
			For Iterator = 1 To 10 Step 1
				obj.SendKeys "{PGDN}"
				wait 2
			Next
             .sync
             Wait 3
             RC=.WebTable("PostalOperator").GetRowWithCellText(75)'Datatable.Value("PONumber","Global"))
			    .WebTable("PostalOperator").ChildItem(RC,1,"WebElement",1).click
			    .Sync
			    While Not Browser("Postal Operator").Page("Postal Operator").WebElement("Status").Exist
			    Wend
			    If Datatable.Value("CustomerPortalLicense","Global")<>"" Then
				    	If Browser("Postal Operator").Page("Postal Operator").WebElement("img_10_0").Exist Then
				    		Browser("Postal Operator").Page("Postal Operator").WebElement("img_10_0").Click
				    		Browser("Postal Operator").Page("Postal Operator").Sync
				    		Wait 8
				    		If Instr(1,Trim(Browser("Postal Operator").Page("Postal Operator").WebElement("Status").GetROProperty("innertext")),Datatable.Value("CustomerPortalLicStatus","Global"),1)>0 Then
				    		   If Browser("Postal Operator").Page("Postal Operator").WebElement("Application History").Exist Then
				    		   		Browser("Postal Operator").Page("Postal Operator").WebElement("Application History").Click
				    		   		Browser("Postal Operator").Page("Postal Operator").Sync
				    		   		If Not Browser("Postal Operator").Page("Postal Operator").WebElement("Application Licensed").Exist Then
				    		   			Wait 10
				    		   		End If
				    		   		If Instr(1,Trim(Browser("Postal Operator").Page("Postal Operator").WebElement("Application Licensed").GetROProperty("innertext")),1)>0 Then
				    		   		  blnFind=True	
				    		   		End If
				    		   End If
				    		End If
				    	End If @@ script infofile_;_ZIP::ssf18.xml_;_
				 Else
			    .WebList("LicenseTypeId").Select Datatable.Value("LicenseTypes","Global") @@ script infofile_;_ZIP::ssf15.xml_;_
			    If Datatable("CPLicense","Global")<>"" Then
			    	.WebElement("img_1").Click
			    	.Sync
			    	  	If Strcomp(Trim(Browser("Postal Operator").Page("Postal Operator").WebElement("Status").GetROProperty("innertext")),Datatable.Value("CPLicStatus","Global"),1)=0 Then
			    	      	blnFind=true
			    	    End If
			    	    If Datatable.Value("CPLicModify","Global")<>"" Then
			    	    	Browser("Postal Operator").Page("Postal Operator").WebElement("img_11_0").Click
			    	    	Browser("Postal Operator").Page("Postal Operator").Sync
			    	    	Browser("Postal Operator").Page("Postal Operator").WebEdit("tbModify").Set GenerateRandomString(5) @@ script infofile_;_ZIP::ssf18.xml_;_
			    	    	Browser("Postal Operator").Page("Postal Operator").Sync
							Browser("Postal Operator").Page("Postal Operator").WebElement("img_Modify").Click @@ script infofile_;_ZIP::ssf19.xml_;_
							Browser("Postal Operator").Page("Postal Operator").Sync
							Browser("Postal Operator").Page("Postal Operator").WebElement("img_21_0").Click @@ script infofile_;_ZIP::ssf20.xml_;_
							Browser("Postal Operator").Page("Postal Operator").Sync
							Browser("Postal Operator").Page("Postal Operator").WebElement("Yes").Click @@ script infofile_;_ZIP::ssf21.xml_;_
							Browser("Postal Operator").Page("Postal Operator").Sync
							If Strcomp(Trim(Browser("Postal Operator").Page("Postal Operator").WebElement("Status").GetROProperty("innertext")),Datatable.Value("Status","Global"),1)=0 Then
					    	   blnFind=true
					    	End If
			    	    End If
			    End If 
			     If Datatable.Value("Terminate","Global")<>"" Then
			    	Browser("Postal Operator").Page("Postal Operator").WebElement("img_30_0").Click
					Browser("Postal Operator").Page("Postal Operator").WebEdit("tbTerminate").Set GenerateRandomString(5) @@ script infofile_;_ZIP::ssf25.xml_;_
					Browser("Postal Operator").Page("Postal Operator").Sync
					Browser("Postal Operator").Page("Postal Operator").WebElement("img_Terminate").Click @@ script infofile_;_ZIP::ssf27.xml_;_
					Browser("Postal Operator").Page("Postal Operator").Sync
			    	If Strcomp(Trim(Browser("Postal Operator").Page("Postal Operator").WebElement("Status").GetROProperty("innertext")),Datatable.Value("TerminateStatus","Global"),1)=0 Then
			    	   blnFind=true
			    	End If
			     End If
			     If Datatable.Value("Cancel","Global")<>"" Then
			    	Browser("Postal Operator").Page("Postal Operator").WebElement("img_26_0").Click
			    	Browser("Postal Operator").Page("Postal Operator").Sync
			    	Browser("Postal Operator").Page("Postal Operator").WebEdit("tbCancel").Set GenerateRandomString(5) @@ script infofile_;_ZIP::ssf22.xml_;_
			    	Browser("Postal Operator").Page("Postal Operator").Sync
					Browser("Postal Operator").Page("Postal Operator").WebElement("img_Cancel").Click @@ script infofile_;_ZIP::ssf24.xml_;_
					Browser("Postal Operator").Page("Postal Operator").Sync
			    	If Strcomp(Trim(Browser("Postal Operator").Page("Postal Operator").WebElement("Status").GetROProperty("innertext")),Datatable.Value("CancelStatus","Global"),1)=0 Then
			    	   blnFind=true
			    	End If
			     End If
			 End If
        End With
End If
fn_VerifyEmailStatus Datatable.Value("EmailUser","Global"),Datatable.Value("EmailPassword","Global"),Datatable.Value("URL","Global"),Datatable.Value("MainSub","Global"),Datatable.Value("Sub","Global")
UpdateTestCaseStatusInExcel Environment("TestName"),blnFind




