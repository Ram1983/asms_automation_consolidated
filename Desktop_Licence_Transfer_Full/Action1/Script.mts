﻿wait(5)
blnFind=True
err.number = 0

datatable.ImportSheet ProjectFolderPath &"\Desktop_Input_Data_Sheet\Input_Data_Sheet.xls","LIc_Trans_Full","LIc_Trans_Full"
n=datatable.GetSheet("LIc_Trans_Full").GetRowCount

'For i = 1 To n Step 1
With VbWindow("frmMDI")
	If n>0 Then
		datatable.SetCurrentRow(1)
		On Error Resume Next:Err.clear
		Err.Number=0
		.WinMenu("ContextMenu").Select "Licence;Transfer;Full"
		If Err.Number<>0 Then
			fn_NavigateToScreen "Licence;Transfer;Full"
			Err.Clear
		End If
		With .VbWindow("frmModLicense")
			While not .exist
			Wend
			.ActiveX("TCIComboBox.tcb").VbComboBox("cboLicType").Select 0 'Datatable.value("LicType","LIc_Trans_Full")'
			.ActiveX("TCIComboBox.tcb").VbComboBox("cboLicType").Type  micTab @@ hightlight id_;_69298_;_script infofile_;_ZIP::ssf86.xml_;_
			.ActiveX("TCIComboBox.tcb_3").VbComboBox("cboApplicationNo").Select 1' Datatable.value("LicenceNumber","LIc_Trans_Full") '"00176-1 / AMR"
			.VbButton("Next").Click @@ hightlight id_;_133068_;_script infofile_;_ZIP::ssf64.xml_;_
		End With
		Wait 30

		With .VbWindow("frmFindClient")
			If Datatable.value("Client","LIc_Trans_Full")="JMC" Then
				.ActiveX("TCIComboBox.tcb").VbComboBox("cboClientNo").Select 2'Datatable.value("FindClientNum","LIc_Trans_Full")
				Wait 15
				Else
					Wait(40)
					.ActiveX("TCIComboBox.tcb").VbComboBox("cboClientNo").Select 2'Datatable.Value("CustomNo","LIc_Trans_Full")
			End If
			If .VbEditor("txtTransferReason").Exist(40) Then
				.VbEditor("txtTransferReason").Type GenerateRandomString(6)
			End If
			If .VbButton("Next").Exist Then
				.VbButton("Next").Click
			End If
		End With
		With .Dialog("#32770")
			If .WinButton("Yes").Exist Then
				.WinButton("Yes").Click
			End If
			If .WinEdit("Please enter the reason").Exist Then
				.WinEdit("Please enter the reason").Set Datatable.value("ReasonTrans","LIc_Trans_Full")'"test"
			End If
			wait(5)
			If 	.WinEdit("Please enter the reason").Exist Then
				.WinEdit("Please enter the reason").Type  micTab
			End If
			wait(5)
			If .WinButton("OK").Exist Then
				.WinButton("OK").Click
			End If
			wait(10)
			If .WinButton("OK").Exist Then
				.WinButton("OK").Click
			End If
		End With

		strTestCaseName = Environment.Value("TestName")
		If err.number = 0 Then
			blnFind = True
			Else
				blnFind = False
		End If

		UpdateTestCaseStatusInExcel strTestCaseName, blnFind
	End If
End With
