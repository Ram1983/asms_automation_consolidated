﻿
'Menu Navigation-Forlicense-new
blnFind=False
err.number = 0

datatable.ImportSheet ProjectFolderPath &"\Desktop_Input_Data_Sheet\Input_Data_Sheet.xls","asmsTypeApproval","Global"
n=datatable.GetSheet("Global").GetRowCount @@ hightlight id_;_3213178_;_script infofile_;_ZIP::ssf2.xml_;_
VbWindow("frmMDI").WinMenu("Menu").Select "Application Processing;Type Approval;Modify"
While Not VbWindow("frmMDI").VbWindow("frmBrowser").Page("Type Approval").WebList("select").Exist
Wend
Wait 3
Set objExcel = CreateObject("Excel.Application")
Set objWB = objExcel.Workbooks.open("C:\ASMS_Automation\WebCP_Input_Data_Sheet\Input_Data_Sheet.xls")
Set obj=objWB.Worksheets("asmsTypeApproval")
Num=obj.cells(2,2).value
objExcel.ActiveWorkbook.Save
objExcel.Quit
Set objExcel= nothing
Set objWB =Nothing
   
VbWindow("frmMDI").VbWindow("frmBrowser").Page("Type Approval").WebList("select").Select Cstr(Num) @@ hightlight id_;_VbWindow("frmMDI").VbWindow("frmBrowser").Page("Type Approval").WebList("select")_;_script infofile_;_ZIP::ssf3.xml_;_
If VbWindow("frmMDI").VbWindow("frmBrowser").Page("Type Approval").Image("edit").Exist Then
	VbWindow("frmMDI").VbWindow("frmBrowser").Page("Type Approval").Image("edit").Click
End If @@ hightlight id_;_VbWindow("frmMDI").VbWindow("frmBrowser").Page("Type Approval").Image("edit")_;_script infofile_;_ZIP::ssf4.xml_;_
While Not VbWindow("frmMDI").VbWindow("frmBrowser").Page("Type Approval_2").WebElement("Applicant Information").Exist
Wend
If Lcase(DataTable("License","Global"))="yes" Then
	VbWindow("frmMDI").VbWindow("frmBrowser").Page("Type Approval_2").WebElement("img_10_0").Click @@ hightlight id_;_VbWindow("frmMDI").VbWindow("frmBrowser").Page("Type Approval 2").WebElement("img 10 0")_;_script infofile_;_ZIP::ssf6.xml_;_
	Wait 5
	VbWindow("frmMDI").VbWindow("frmBrowser").Page("Type Approval_2").Sync
	If Instr( Trim(VbWindow("frmMDI").VbWindow("frmBrowser").Page("Type Approval_2").WebElement("Status").GetROProperty("innertext")),DataTable.Value("LicenseStatus","Global") ,1)>0 Then
		blnFind=True
	End If
	
ElseIf  Lcase(DataTable("Reject","Global"))="yes" Then
	VbWindow("frmMDI").VbWindow("frmBrowser").Page("Type Approval_2").WebElement("img_23_0").Click @@ hightlight id_;_VbWindow("frmMDI").VbWindow("frmBrowser").Page("Type Approval 2").WebElement("img 23 0")_;_script infofile_;_ZIP::ssf7.xml_;_
	VbWindow("frmMDI").VbWindow("frmBrowser").Page("Type Approval_2").Sync
	Wait 5
	If Instr( Trim(VbWindow("frmMDI").VbWindow("frmBrowser").Page("Type Approval_2").WebElement("Status").GetROProperty("innertext")),DataTable.Value("RejectStatus","Global") ,1)>0 Then
		blnFind=True
	End If
End If

If VbWindow("frmMDI").VbWindow("frmBrowser").Page("Type Approval_2").WebElement("img_1").Exist Then
	VbWindow("frmMDI").VbWindow("frmBrowser").Page("Type Approval_2").WebElement("img_1").Click
	Wait 20
	If VbWindow("frmReportViewer").VbButton("Print").Exist Then
		VbWindow("frmReportViewer").VbButton("Print").Click
		If VbWindow("frmReportViewer").Dialog("Print").WinButton("OK").Exist Then
			VbWindow("frmReportViewer").Dialog("Print").WinButton("OK").Click
			If Dialog("Printing Records").Dialog("Save Print Output As").WinEdit("File name:").Exist Then
				Dialog("Printing Records").Dialog("Save Print Output As").WinEdit("File name:").Set GenerateRandomString(6)
				If Dialog("Printing Records").Dialog("Save Print Output As").WinButton("Save").Exist Then
					Dialog("Printing Records").Dialog("Save Print Output As").WinButton("Save").Click
					If VbWindow("frmReportViewer").VbButton("Close").Exist Then
						VbWindow("frmReportViewer").VbButton("Close").Click
					End If
				End If
			End If
		End If
	End If
End If @@ hightlight id_;_VbWindow("frmMDI").VbWindow("frmBrowser").Page("Type Approval 2").WebElement("img 1")_;_script infofile_;_ZIP::ssf8.xml_;_
 @@ hightlight id_;_4655274_;_script infofile_;_ZIP::ssf9.xml_;_
UpdateTestCaseStatusInExcel Environment("TestName"),blnFind
