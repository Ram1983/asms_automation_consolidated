﻿blnVal=False

'Import data from Input data sheet  from folder
datatable.ImportSheet ProjectFolderPath&"\Input_Data_Sheet.xls","AD_CompliantEnquiry","AD_CompliantEnquiry"
n=datatable.GetSheet("AD_CompliantEnquiry").GetRowCount

'For i = 1 To n Step 1
If n>0 Then
			datatable.SetCurrentRow(1)
'
'		'Compliant or Enquiry page clicking
		Browser("Admin Home Page").Page("Admin Home Page").Link("Complaint or Enquiry").Click @@ script infofile_;_ZIP::ssf1.xml_;_
		'Add Compliant
		
	 If datatable.Value("Functionality","AD_CompliantEnquiry")="Add" Then
			Browser("Admin Home Page").Page("Complaint or Enquiry").Link("Add Complaint").Click @@ script infofile_;_ZIP::ssf2.xml_;_
			'Submitter information
	 ElseIf datatable.Value("Functionality","AD_CompliantEnquiry")="Edit"  Then	
			With	Browser("Complaint or Enquiry_2").Page("Complaint or Enquiry_2")
				.Sync
				If .WebEdit("WebEdit").Exist Then
					.WebEdit("WebEdit").Set DataTable.Value("ComplaintID","AD_CompliantEnquiry")
				End If
				If .Image("edit").Exist Then
					.Image("edit").Click
				End If
			End With
	End If

		If Browser("Admin Home Page").Page("Complaint or Enquiry_2").WebList("from_ClientID").Exist(3) Then @@ script infofile_;_ZIP::ssf3.xml_;_
'		Browser("Admin Home Page").Page("Complaint or Enquiry_2").WebList("from_ClientID").Select datatable.Value("Client","AD_CompliantEnquiry")
		End If
		wait(3)
	If not datatable.Value("Functionality","AD_CompliantEnquiry")="Edit" Then	
        If Browser("Complaint or Enquiry_2").Page("Complaint or Enquiry_4").WebList("from_ClientID").Exist(5) Then
        	If datatable.Value("ClientName","AD_CompliantEnquiry")<>"" Then
        		Browser("Complaint or Enquiry_2").Page("Complaint or Enquiry_4").WebList("from_ClientID").Select datatable.Value("ClientName","AD_CompliantEnquiry")
        	Else
					With Browser("Admin Home Page").Page("Complaint or Enquiry_2")
						.WebEdit("from_FirstName").Set datatable.Value("Fname","AD_CompliantEnquiry")&fnRandomNumber(4)
						.WebEdit("from_LastName").Set datatable.Value("Lname","AD_CompliantEnquiry")&fnRandomNumber(4)
					End With
			End If
        End If
		With	Browser("Admin Home Page").Page("Complaint or Enquiry_2")
			.WebEdit("from_CompanyID").Set datatable.Value("Comapnyid","AD_CompliantEnquiry")&fnRandomNumber(4) @@ script infofile_;_ZIP::ssf6.xml_;_
			.WebEdit("from_Phone").Set datatable.Value("PhoneNO","AD_CompliantEnquiry") @@ script infofile_;_ZIP::ssf7.xml_;_
			.WebEdit("from_fax").Set datatable.Value("Fax","AD_CompliantEnquiry") @@ script infofile_;_ZIP::ssf8.xml_;_
			.WebEdit("from_Address").Set datatable.Value("Street","AD_CompliantEnquiry") @@ script infofile_;_ZIP::ssf9.xml_;_
			If .WebEdit("from_district").Exist(5) Then
				.WebEdit("from_district").Set datatable.Value("District","AD_CompliantEnquiry")
			End If
			.WebEdit("from_city").Set datatable.Value("City","AD_CompliantEnquiry") @@ script infofile_;_ZIP::ssf11.xml_;_
			.WebEdit("from_state").Set datatable.Value("State","AD_CompliantEnquiry") @@ script infofile_;_ZIP::ssf12.xml_;_
			.WebEdit("from_country").Set datatable.Value("Country","AD_CompliantEnquiry") @@ script infofile_;_ZIP::ssf14.xml_;_
			.WebEdit("from_emailAddress").Set datatable.Value("Email","AD_CompliantEnquiry")&fnRandomNumber(4)&"@gmail.com" @@ script infofile_;_ZIP::ssf16.xml_;_
			.WebEdit("Password").SetSecure datatable.Value("Password","AD_CompliantEnquiry") @@ script infofile_;_ZIP::ssf17.xml_;_
		End With
	End If
		'Particular service provider
'		Browser("Admin Home Page").Page("Complaint or Enquiry_2").WebList("about_ClientID").Select datatable.Value("Client","AD_CompliantEnquiry")
		wait(3)
	With	Browser("Admin Home Page").Page("Complaint or Enquiry_2")
		If not datatable.Value("Functionality","AD_CompliantEnquiry")="Edit" Then
			.WebEdit("about_Company").Set datatable.Value("CompanyName","AD_CompliantEnquiry")&fnRandomNumber(4) @@ script infofile_;_ZIP::ssf19.xml_;_
			.WebEdit("about_Address").Set datatable.Value("Streetaddress","AD_CompliantEnquiry") @@ script infofile_;_ZIP::ssf20.xml_;_
			If .WebEdit("about_district").Exist(5) Then
				.WebEdit("about_district").Set datatable.Value("Dist","AD_CompliantEnquiry")
			End If
			If .WebEdit("about_city").Exist(5) Then @@ script infofile_;_ZIP::ssf22.xml_;_
				.WebEdit("about_city").Set datatable.Value("SPCity","AD_CompliantEnquiry")
			End If
			.WebEdit("about_state").Set datatable.Value("SPState","AD_CompliantEnquiry") @@ script infofile_;_ZIP::ssf23.xml_;_
			.WebEdit("about_country").Set datatable.Value("SpCountry","AD_CompliantEnquiry") @@ script infofile_;_ZIP::ssf24.xml_;_
		End if
	End With

	With Browser("Complaint or Enquiry_2").Page("Complaint or Enquiry_4")
		If .WebList("about_ClientID").Exist(5) Then
			.WebList("about_ClientID").Select datatable.Value("ClientName","AD_CompliantEnquiry")
		End If
	End With
	'Compliant details

	With Browser("Admin Home Page").Page("Complaint or Enquiry_2")
		If .WebList("Category").GetROProperty("items count")>1 Then @@ script infofile_;_ZIP::ssf26.xml_;_
			.WebList("Category").Select(1)
		End If
	End With
	wait(2)
		Browser("Complaint or Enquiry").Page("Complaint or Enquiry").WebList("ComplaintTypeID").Select(1)' "Non-Technical"
	With Browser("Admin Home Page")
		With .Page("Complaint or Enquiry_2")
			If .WebList("MethodOfComplaint").GetROProperty("items count")>1 Then
				.WebList("MethodOfComplaint").Select(1)
			End If
			.WebEdit("DetailsContent").Set datatable.Value("Description","AD_CompliantEnquiry") @@ script infofile_;_ZIP::ssf28.xml_;_
			.WebEdit("RemedyContent").Set datatable.Value("Remedy","AD_CompliantEnquiry") @@ script infofile_;_ZIP::ssf29.xml_;_
			.WebEdit("DisputedAmount").Set datatable.Value("Disputemoney","AD_CompliantEnquiry") @@ script infofile_;_ZIP::ssf30.xml_;_
			'Update
			.WebElement("img_1").Click @@ script infofile_;_ZIP::ssf32.xml_;_
			'Interfered Station
			.WebElement("Interfered Station").Click @@ script infofile_;_ZIP::ssf33.xml_;_
			.WebEdit("is_StationName").Set datatable.Value("interferedstation","AD_CompliantEnquiry") @@ script infofile_;_ZIP::ssf34.xml_;_
			.WebEdit("is_TypeOfEquipment").Set datatable.Value("interferedstation","AD_CompliantEnquiry") @@ script infofile_;_ZIP::ssf35.xml_;_
			.WebEdit("is_AssignFreq").Set datatable.Value("InterferedTypeequip","AD_CompliantEnquiry") @@ script infofile_;_ZIP::ssf37.xml_;_
			.WebEdit("is_BandWidth").Set datatable.Value("InterferedBandwidth","AD_CompliantEnquiry") @@ script infofile_;_ZIP::ssf39.xml_;_
			.WebEdit("is_Location").Set datatable.Value("interferedloc","AD_CompliantEnquiry") @@ script infofile_;_ZIP::ssf40.xml_;_
			If .WebList("is_ComplaintType").GetROProperty("items count")>1 Then
				.WebList("is_ComplaintType").Select 1
			End If
			If .WebList("is_District1").Exist(5) Then
				If .WebList("is_District1").GetROProperty("items count")>1 Then
					.WebList("is_District1").Select (8)
				End If
			End If

			'Browser("Admin Home Page").Page("Complaint or Enquiry_2").WebElement("Update     Submit").Click
			.WebElement("img_2").Click @@ script infofile_;_ZIP::ssf44.xml_;_
			.Sync
			'intefering station
			.WebElement("Interfering Station").Click @@ script infofile_;_ZIP::ssf45.xml_;_
			.WebEdit("sci_StationName").Set datatable.Value("interferedloc","AD_CompliantEnquiry") @@ script infofile_;_ZIP::ssf46.xml_;_
			.WebList("sci_ViolationType").Select(1) @@ script infofile_;_ZIP::ssf47.xml_;_
			.WebEdit("sci_ViolationDate").Set datatable.Value("Violationdate","AD_CompliantEnquiry") @@ script infofile_;_ZIP::ssf49.xml_;_
			.WebEdit("sci_UpperFreq").Set datatable.Value("Frequpper","AD_CompliantEnquiry") @@ script infofile_;_ZIP::ssf51.xml_;_
			.WebEdit("sci_LowerFreq").Set datatable.Value("FreqLower","AD_CompliantEnquiry") @@ script infofile_;_ZIP::ssf53.xml_;_

			.WebEdit("sci_Bandwidth").Set datatable.Value("Interferingbandwidth","AD_CompliantEnquiry") @@ script infofile_;_ZIP::ssf55.xml_;_
			.WebList("sci_EmissionClass").Select(1) @@ script infofile_;_ZIP::ssf56.xml_;_
			.WebEdit("sci_Location").Set datatable.Value("Interferingloc","AD_CompliantEnquiry") @@ script infofile_;_ZIP::ssf57.xml_;_
			.WebList("sci_ServiceNature").Select(1)' "Stations using adaptive system" @@ script infofile_;_ZIP::ssf58.xml_;_
			.WebList("sci_StationClass").Select(1)' "Amateur station" @@ script infofile_;_ZIP::ssf59.xml_;_
			.WebEdit("NotesContent").Set datatable.Value("Interferingnotes","AD_CompliantEnquiry") @@ script infofile_;_ZIP::ssf62.xml_;_

			'update
			.WebElement("img_3").Click @@ script infofile_;_ZIP::ssf60.xml_;_
			.Sync
			'Compliant and enquiry status

			.WebElement("Complaint or Enquiry Status").Click @@ script infofile_;_ZIP::ssf65.xml_;_

			If .WebList("SubStatusID").GetROProperty("items count")>1 Then @@ script infofile_;_ZIP::ssf66.xml_;_
				.WebList("SubStatusID").Select(1)
			End If
			If .WebList("PriorityID").GetROProperty("items count")>1 Then @@ script infofile_;_ZIP::ssf67.xml_;_
				.WebList("PriorityID").Select(1)
			End If
			.WebEdit("ActionTakenContent").Set datatable.Value("Action","AD_CompliantEnquiry") @@ script infofile_;_ZIP::ssf68.xml_;_
			.WebEdit("cs_MontoringSite").Set  datatable.Value("site","AD_CompliantEnquiry") @@ script infofile_;_ZIP::ssf69.xml_;_
			.WebEdit("CommentsContent").Set datatable.Value("Comments","AD_CompliantEnquiry") @@ script infofile_;_ZIP::ssf70.xml_;_
			.WebElement("img_4").Click
			.Sync
			'Attachment
			.WebElement("Attached Documents").Click @@ script infofile_;_ZIP::ssf71.xml_;_
			path=fileCreate
			wait 10
			With	.Frame("Frame")
				.WebFile("BrowserHidden").Set path @@ script infofile_;_ZIP::ssf72.xml_;_
				.Image("Pressing Upload, the document").Click @@ script infofile_;_ZIP::ssf73.xml_;_
			End With
			'Navigate to compliant enquiry tab
			.WebElement("Complaint or Enquiry").Click @@ script infofile_;_ZIP::ssf74.xml_;_
		End With
		'update
		'		Browser("Admin Home Page").Page("Complaint or Enquiry_2").WebElement("img_1").Click
		'Submit

		If datatable.Value("Functionality","AD_CompliantEnquiry")="Add"  Then
			.Page("Complaint or Enquiry_2").WebElement("img_2_0").Click @@ script infofile_;_ZIP::ssf77.xml_;_
			Set send=CreateObject("Wscript.Shell")
			send.SendKeys "{ENTER}"
			send.SendKeys "{ENTER}"
			With .Page("Complaint or Enquiry")
				If .WebElement("Complaint or Enquiry Status").Exist Then
					.WebElement("Complaint or Enquiry Status").Click
					If Instr(1,Trim(.WebElement("Status").GetROProperty("innertext")),"Submitted",1)>0 Then
						blnVal=True
					End If

				End If
			End With
		End If
	End With
End If


UpdateTestCaseStatusInExcel Environment("TestName"),blnVal
