﻿
 wait(2)
blnFind=True
err.number = 0

datatable.ImportSheet ProjectFolderPath &"\Input_Data_Sheet\Input_Data_Sheet.xls","ExportStations","ExportStations"
n=datatable.GetSheet("ExportStations").GetRowCount

'For i = 1 To n Step 1
With VbWindow("frmMDI")
	If n>0 Then
		datatable.SetCurrentRow(1)
		.WinMenu("Menu").Select "Analysis;Export to ATDI;Export Stations"
		With .VbWindow("frmFindAnalysis")
			While not .Exist
			Wend

			if Datatable.Value("SearchType","ExportStations")="ApplicationNo" Then
				.VbRadioButton("Application #").Set @@ hightlight id_;_1514672_;_script infofile_;_ZIP::ssf4.xml_;_
				Else
					.VbRadioButton("Licence Number").Set @@ hightlight id_;_2952916_;_script infofile_;_ZIP::ssf5.xml_;_
			End If
			.ActiveX("TCIComboBox.tcb").WinEdit("Edit").Set Datatable.Value("AppNo_LicNo","ExportStations") @@ hightlight id_;_5836714_;_script infofile_;_ZIP::ssf31.xml_;_
			.ActiveX("TCIComboBox.tcb").WinEdit("Edit").Type  micTab @@ hightlight id_;_5836714_;_script infofile_;_ZIP::ssf32.xml_;_
			Wait(2)
			.VbButton("Next").Click @@ hightlight id_;_2033880_;_script infofile_;_ZIP::ssf39.xml_;_
		End With

		wait(2)
		While not .VbWindow("frmTXinAppVHF").Exist
		Wend

		If .VbWindow("frmTXinAppVHF").AcxTable("Please select a Transmitter").RowCount>0 Then
			.VbWindow("frmTXinAppVHF").VbButton("OK").Click
			With .VbWindow("frmExportSites")
				While not .Exist
				Wend

				.VbButton("Show Transmitters").Click @@ hightlight id_;_1645744_;_script infofile_;_ZIP::ssf12.xml_;_
			End With

			If .VbWindow("frmExportSites").AcxTable("Select an Equipment from").RowCount>0 Then
				.InsightObject("InsightObject_3").Click @@ hightlight id_;_20_;_script infofile_;_ZIP::ssf37.xml_;_
					wait(1)
				.VbWindow("frmExportSites").VbButton("Ok").Click @@ hightlight id_;_7801912_;_script infofile_;_ZIP::ssf14.xml_;_
				wait(1)
				With	.Dialog("#32770")
					.WinButton("No").Click @@ hightlight id_;_1577276_;_script infofile_;_ZIP::ssf15.xml_;_
					wait(1)
					.WinButton("OK").Click @@ hightlight id_;_1972422_;_script infofile_;_ZIP::ssf16.xml_;_
				End With
				wait(1)
				.VbWindow("frmExportSites").VbButton("Close").Click		
				Else
					Print "Site(s) are not available for selected AppNo_LicNo "& Datatable.Value("AppNo_LicNo","ExportStations")
					.VbWindow("frmExportSites").VbButton("Close").Click    

			End If
			Else
				Print "Transmitter(s) are not available for selected AppNo_LicNo "& Datatable.Value("AppNo_LicNo","ExportStations")
				.VbWindow("frmTXinAppVHF").VbButton("Close").Click

		End If

		strTestCaseName = Environment.Value("TestName")
		If err.number = 0 Then
			blnFind = True
			Else
				blnFind = False
		End If

		UpdateTestCaseStatusInExcel strTestCaseName, blnFind
	End If
End With
