﻿Wait(1)
blnFind=False
err.number = 0
If Lcase(Environment("Client"))="moz" Then
   fn_NavigateToScreen "Contabilidade;Informação do Cliente"
Else
	VbWindow("VbWindow").WinMenu("Menu").Select "Accounting;Customer Information"
End If

Wait(2)
'================= Menu Navigation completed

datatable.ImportSheet ProjectFolderPath &"\Desktop_Input_Data_Sheet\Input_Data_Sheet.xls","AddCustomer_Person","AddCustomer_Person"
n=datatable.GetSheet("AddCustomer_Person").GetRowCount

'For i = 1 To n Step 1
If n>0 Then
datatable.SetCurrentRow(1)	


	With VbWindow("frmMDI")
		With	.VbWindow("vbname:=frmFindClient")
			If ((.Exist(60))) Then
				Print "Wait completed, continue with test execution"
			End If

			If (.VbButton("vbname:=cmdAdd").Exist(10)) Then
				Print "Wait is completed..."
			End If
			Wait 5
			.VbButton("vbname:=cmdAdd").Click
		End With
		'================= Find screen completed
		WriteToLogFile("Creating new customer")
		WaitForForm(.VbWindow("vbname:=frmClientEntity"))

		subType = "text:=" & Datatable.Value("CustType","AddCustomer_Person")	'Person / Company / Government / Non-Governmental Organization
		While VbWindow("frmMDI").VbWindow("frmFindClient").Exist
		Wend
		If VbWindow("frmMDI").VbWindow("frmFindClient").VbButton("Add").Exist(10) Then
			VbWindow("frmMDI").VbWindow("frmFindClient").VbButton("Add").Click
		End If
		
		With .VbWindow("frmClientEntity")
			.VbRadioButton(subType).Set
			isDealer = Datatable.Value("Dealer","AddCustomer_Person")				'Yes / No
			If (isDealer = "Yes") Then
				.VbCheckBox("text:=Dealer").Set "ON"
			End If
			Wait 5
			.VbButton("vbname:=cmdOK").Click
		End With
		'================== Customer Type screen completed
		With .VbWindow("vbname:=frmClientInfo")
			WaitForForm(.ActiveX("progid:=TabDlg.SSTab.1"))
			Wait 30
			While Not .ActiveX("progid:=TabDlg.SSTab.1").VbEdit("vbname:=txtFirstName").Exist
			Wend
			'Setting Customer Details
			If .ActiveX("progid:=TabDlg.SSTab.1").VbEdit("vbname:=txtFirstName").Exist Then
			     wait 8
				.ActiveX("progid:=TabDlg.SSTab.1").VbEdit("vbname:=txtFirstName").Set Datatable.Value("Fname","AddCustomer_Person") & "" & fnRandomNumber(8)	
			End If
			.ActiveX("progid:=TabDlg.SSTab.1").VbEdit("vbname:=txtLastName").Set Datatable.Value("Lname","AddCustomer_Person")& "" & fnRandomNumber(7)
			.ActiveX("progid:=TabDlg.SSTab.1").VbEdit("vbname:=txtNationalID").Set Datatable.Value("Nid","AddCustomer_Person") & "" & fnRandomNumber(5)

			'Setting Physical Address
			.ActiveX("progid:=TabDlg.SSTab.1").VbEdit("vbname:=txtPlotNo").Set Datatable.Value("Phy_PtNo","AddCustomer_Person")
			.ActiveX("progid:=TabDlg.SSTab.1").VbEdit("vbname:=txtStreet").Set Datatable.Value("Phy_St","AddCustomer_Person")
			.ActiveX("progid:=TabDlg.SSTab.1").VbComboBox("vbname:=cboCity").Select(0)' Datatable.Value("Phy_City")

			'Setting Postal Address
			.ActiveX("progid:=TabDlg.SSTab.1").VbEdit("vbname:=txtPostalLocation").Set Datatable.Value("Po_Loc","AddCustomer_Person")
			.ActiveX("progid:=TabDlg.SSTab.1").VbEdit("vbname:=txtPostStreet").Set Datatable.Value("Po_St","AddCustomer_Person")
			.ActiveX("progid:=TabDlg.SSTab.1").VbEdit("vbname:=txtPostal").Set Datatable.Value("Po_Pbox","AddCustomer_Person")

			If Datatable.Value("Client","AddCustomer_Person")="JMC" Then
				.ActiveX("progid:=TabDlg.SSTab.1").VbEdit("vbname:=txtTA").Set Datatable.Value("Po_Zip","AddCustomer_Person")
				.ActiveX("progid:=TabDlg.SSTab.1").VbComboBox("vbname:=cboPostCity").Select(0) 'Datatable.Value("Po_City")
			End If

			'Setting Communication Details
			.ActiveX("progid:=TabDlg.SSTab.1").VbEdit("vbname:=txtTel1").Set Datatable.Value("Tel","AddCustomer_Person")
			.ActiveX("progid:=TabDlg.SSTab.1").VbEdit("vbname:=txtMobile").Set Datatable.Value("Mob","AddCustomer_Person")
			.ActiveX("progid:=TabDlg.SSTab.1").VbEdit("vbname:=txtEmail").Set Datatable.Value("Email","AddCustomer_Person")&RandomNumber(1111,9999999)&"@gmail.com"
			.ActiveX("progid:=TabDlg.SSTab.1").VbEdit("vbname:=txtWebSite").Set Datatable.Value("Web","AddCustomer_Person")
			.ActiveX("progid:=TabDlg.SSTab.1").VbEdit("vbname:=txtCertifyingOrganization").Type Datatable.Value("Corg","AddCustomer_Person")
			Wait(1)
			.ActiveX("progid:=TabDlg.SSTab.1").VbEdit("vbname:=txtFax").Set Datatable.Value("Fax","AddCustomer_Person")
			.ActiveX("progid:=TabDlg.SSTab.1").Click 300,13		' Switch to Second Tab
			Wait 8
			If Datatable.Value("Client","AddCustomer_Person")="JMC" Then
				.ActiveX("progid:=TabDlg.SSTab.1").VbCheckBox("vbname:=chkNeedToPay").Set "ON"
			End If
			.ActiveX("progid:=TabDlg.SSTab.1").VbEdit("vbname:=txtEmployerName").Set GenerateRandomString(5)
			.ActiveX("progid:=TabDlg.SSTab.1").VbEditor("vbname:=txtEmployerAddress").Type GenerateRandomString(5)

			.ActiveX("progid:=TabDlg.SSTab.1").Click 135,11		' Switch to Primary Tab
			Wait 8
			If .ActiveX("progid:=TabDlg.SSTab.1").VbButton("vbname:=cmdOK").Exist(40) Then
				.ActiveX("progid:=TabDlg.SSTab.1").VbButton("vbname:=cmdOK").Click
				
			End If

			Set oParent = .ActiveX("progid:=TabDlg.SSTab.1").ActiveX("vbName:=txtClientNum")
		End With
		Do
			If(oParent.exist) Then
				Exit Do
			End If
		Loop While (1)
		customerID = oParent.GetROProperty("text")

		If (.Dialog("nativeclass:=#32770").Exist(10)) Then
			.Dialog("nativeclass:=#32770").WinButton("text:=&Yes").Click		
			Set oParent = .VbWindow("vbname:=frmClientInfo").ActiveX("progid:=TabDlg.SSTab.1").ActiveX("vbName:=txtClientNum")
			Do
				If(oParent.exist) Then
					Exit Do
				End If
			Loop While (1)
			customerID = oParent.GetROProperty("text")
		End If
		Wait 5
		Print "Customer ID : " & customerID & " added successfully - Customer Type: Person"
'		With .VbWindow("vbname:=frmClientInfo")
		With .VbWindow("vbname:=frmClientInfo")
			If .ActiveX("progid:=TabDlg.SSTab.1").VbButton("vbname:=cmdCancel").Exist Then
				.ActiveX("progid:=TabDlg.SSTab.1").VbButton("vbname:=cmdCancel").Click
			End If
		End With
		WriteToLogFile("New customer created with customer ID :" & customerID)
		With .Dialog("nativeclass:=#32770")
			If (.WinButton("text:=&Yes").Exist(10)) Then
				.WinButton("text:=&Yes").Click
				
			End  If
		End With

		With .VbWindow("vbname:=frmClientInfo")
			If .ActiveX("progid:=TabDlg.SSTab.1").VbButton("vbname:=cmdCancel").Exist(6) Then
				.ActiveX("progid:=TabDlg.SSTab.1").VbButton("vbname:=cmdCancel").Click
			End If
		End With
	End With
	With VbWindow("frmMDI").VbWindow("frmClientInfo")
		If .ActiveX("SSTab").VbButton("Close").Exist(10) Then
			.ActiveX("SSTab").VbButton("Close").Click
		End If
	End With
	With VbWindow("vbname:=frmMDI").Dialog("nativeclass:=#32770")
		If (.WinButton("text:=&Yes").Exist(10)) Then
			.WinButton("text:=&Yes").Click
		End  If
	End With
	strTestCaseName = Environment.Value("TestName")
If err.number = 0 Then
	blnFind = True
Else
	blnFind = False
End If

UpdateTestCaseStatusInExcel strTestCaseName, blnFind

End If




